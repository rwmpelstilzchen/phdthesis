\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\RequirePackage{fix-cm}% https://tex.stackexchange.com/a/4825/45607
\ProvidesClass{dissertation}[2018/01/01 v0.1 Dissertation Class]

% \DeclareOption{draft}{\PassOptionsToClass{\CurrentOption}{memoir}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions\relax

% \def\debug{True}
\def\debug{False}

\LoadClass[%
    a4paper,
    12pt,
    %twoside,
	oneside,
]{memoir}

\RequirePackage{ifthen}
\def\docfontsize{12}

%\renewcommand{\footnotesize}{\fontsize{8pt}{9pt}\selectfont}
%\renewcommand{\normalsize}{\fontsize{10pt}{14pt}\selectfont}
%\fontsize{10pt}{14pt}\selectfont

\ifthenelse{\equal{\docfontsize}{12}}{%
	%\renewcommand{\normalsize}{\fontsize{10pt}{14pt}\selectfont}
	\renewcommand{\footnotesize}{\fontsize{9.5pt}{14.0pt}\selectfont}
	%\renewcommand{\footnotesize}{\fontsize{8pt}{9pt}\selectfont}
}
{}

\OnehalfSpacing
%\DoubleSpacing

\RequirePackage{geometry}



%%% Layout the Page %%%
\setlrmarginsandblock{26mm}{211pt}{*}
\setmarginnotes{9mm}{12pc}{1em}

\setulmarginsandblock{6pc}{8pc}{*}

\setheadfoot{1.25\baselineskip}{10mm}
\setheaderspaces{*}{8mm}{*}

\checkandfixthelayout

%%% Header und Footer Styles
\nouppercaseheads
\makepagestyle{diss}
\makepsmarks{diss}{%
\nouppercaseheads
\createmark{chapter}{both}{nonumber}{}{}
\createmark{section}{right}{nonumber}{}{}
\createplainmark{toc}{both}{\contentsname}
\createplainmark{lof}{both}{\listfigurename}
\createplainmark{lot}{both}{\listtablename}
\createplainmark{bib}{both}{\bibname}
\createplainmark{index}{both}{\indexname}
\createplainmark{glossary}{both}{\glossaryname}
}
\makerunningwidth{diss}[480pt]{480pt}
\makeheadposition{diss}{flushright}{flushleft}{flushright}{flushleft}
\makeevenhead{diss}{\small\thepage\hspace{1em}\textsc{\MakeLowercase\leftmark}}{}{}
%TWOSIDE
% \makeoddhead{diss}{}{}{\small\textsc{\MakeLowercase\rightmark}\hspace{1em}\thepage}
% ONESIDE
\makeoddhead{diss}{\small\color{darkgray}\textsc{\MakeLowercase\leftmark}}{}{\small\textsc{\color{darkgray}\MakeLowercase\rightmark}\hspace{1em}\thepage}

\makepagestyle{fullwidth}
\makepsmarks{fullwidth}{%
\nouppercaseheads
\createmark{chapter}{both}{nonumber}{}{}
\createmark{section}{right}{nonumber}{}{}
\createplainmark{toc}{both}{\contentsname}
\createplainmark{lof}{both}{\listfigurename}
\createplainmark{lot}{both}{\listtablename}
\createplainmark{bib}{both}{\bibname}
\createplainmark{index}{both}{\indexname}
\createplainmark{glossary}{both}{\glossaryname}
}
\makerunningwidth{fullwidth}[450pt]{450pt}
\makeheadposition{fullwidth}{flushright}{flushleft}{flushright}{flushleft}
\makeevenhead{fullwidth}{\small\thepage\hspace{1em}\textsc{\MakeLowercase\leftmark}}{}{}
% TWOSIDE
\makeoddhead{fullwidth}{}{}{\small\textsc{\MakeLowercase\rightmark}\hspace{1em}\thepage}
% ONESIDE
% \makeoddhead{fullwidth}{\small\textsc{\MakeLowercase\leftmark}}{}{\small\textsc{\MakeLowercase\rightmark}\hspace{1em}\thepage}

\makepagestyle{plain}
\makerunningwidth{plain}[480pt]{480pt}
\makeheadposition{plain}{flushright}{flushleft}{flushright}{flushleft}
\makeevenfoot{plain}{\small\thepage}{}{}
\makeoddfoot{plain}{}{}{\small\thepage}

\makepagestyle{fullwidthplain}
\makerunningwidth{fullwidthplain}[450pt]{450pt}
\makeheadposition{fullwidthplain}{flushright}{flushleft}{flushright}{flushleft}
\makeevenfoot{fullwidthplain}{\small\thepage}{}{}
\makeoddfoot{fullwidthplain}{}{}{\small\thepage}

\aliaspagestyle{part}{empty}
\aliaspagestyle{chapter}{plain}

\newenvironment{fullwidth}{%
    \newgeometry{inner=26mm, outer=26mm, top=6pc, bottom=102pt}
    \aliaspagestyle{chapter}{fullwidthplain}
    \pagestyle{fullwidth}
}{%
    \restoregeometry
}

\newenvironment{halffullwidth}{%
    \newgeometry{left=26mm, right=52mm, top=6pc, bottom=102pt}
    \aliaspagestyle{chapter}{fullwidthplain}
    \pagestyle{fullwidth}
}{%
    \restoregeometry
}

% A modified environment made so the abstract will fit within two pages…
\newenvironment{halffullwidthabstract}{%
    \newgeometry{left=26mm, right=40mm, top=6pc, bottom=102pt}
    \aliaspagestyle{chapter}{fullwidthplain}
    \pagestyle{fullwidth}
}{%
    \restoregeometry
}

\newenvironment{rtlhalffullwidth}{%
    \newgeometry{right=26mm, left=52mm, top=6pc, bottom=102pt}
    \aliaspagestyle{chapter}{fullwidthplain}
    \pagestyle{fullwidth}
}{%
    \restoregeometry
}

% implements a new environment for figures, tables, etc
% that are able to spread the full width of the page,
% while keeping the caption on the side margin
\newlength{\fullwidthlen}
\setlength{\fullwidthlen}{\marginparwidth}
\addtolength{\fullwidthlen}{\marginparsep}
\newenvironment{fullwidthfig}{%
	\checkoddpage%
	\ifoddpage%
		\begin{adjustwidth*}{}{-\fullwidthlen}%
	\else%
		\begin{adjustwidth*}{-\fullwidthlen}{}%
	\fi%
}{%
  \end{adjustwidth*}\vspace{\baselineskip}%
}

\newlength{\widetextlen}
\setlength{\widetextlen}{0.5\marginparwidth}
\addtolength{\widetextlen}{\marginparsep}
\newenvironment{widetext}{%
	\checkoddpage%
	\ifoddpage%
		\begin{adjustwidth*}{}{-\widetextlen}%
	\else%
		\begin{adjustwidth*}{-\widetextlen}{}%
	\fi%
	% this is a test for a very long text which should surely be more than one line in total so I can see how long it is.
}{%
  \end{adjustwidth*}\vspace{\baselineskip}%
}

% Example on how to use the fullwidthfig env
% \begin{figure}[t]
%   \begin{fullwidth}
%     \includegraphics[width=\linewidth+\marginparsep]{demo}
%   \end{fullwidth}
%   \vspace{-\baselineskip}\vspace{-\baselineskip}
%   \sideparmargin{outer}
%   \sidepar{\vspace{\baselineskip}
%     \caption{Caption for a full-width figure appearing in the margin
%       below it.}}
% \end{figure}

\pagestyle{diss}



%%% Part Style %%%
\renewcommand{\partnamefont}{\normalfont\huge}
\renewcommand{\partnumfont}{\normalfont\huge}
\renewcommand{\parttitlefont}{\normalfont\huge\color{Burgundy}\MakeTextUppercase}

%%% Chapter Style %%%
\setlength{\beforechapskip}{0pt}
\renewcommand{\chapnumfont}{\fontsize{72pt}{96pt}\color{darkgray}\selectfont}
\renewcommand{\chaptitlefont}{\huge\rmfamily\itshape\color{darkgray}}
\renewcommand*{\printchaptername}{\chapnumfont}
\renewcommand*{\afterchaptertitle}{\par\nobreak\vspace*{-10pt}\hrulefill\vskip\afterchapskip}
\setlength{\afterchapskip}{\baselineskip}

%%% ToC Style %%%
\newlength{\aftertocskip}
\renewcommand*{\aftertoctitle}{\par\nobreak\vspace*{-10pt}\hrulefill\vskip\aftertocskip}


%%% Paragraph Style %%%
%\setparaheadstyle{\scshape}
%\setbeforeparaskip{1em}
%\setafterparaskip{-0.5em}





%%% Doesn’t seem to work. Rwmpelstilzchen
%%% \settocpreprocessor{chapter}{%
%%% \let\tempf@rtoc\f@rtoc%
%%% \def\f@rtoc{%
%%% \texorpdfstring{\MakeTextLowercase{\tempf@rtoc}}{\tempf@rtoc}}%
%%% }


% https://tex.stackexchange.com/questions/275565/tufte-layout-in-painless-memoir
% Attention: does not work when \sidebar is used

\RequirePackage{marginfix}
\ifthenelse{\equal{\docfontsize}{12}}{%
	\marginposadjustment=1.75pt
}
{%10pt
	\marginposadjustment=3.5pt
}

% Margin Notes like Footnotes:
% http://latex.org/forum/viewtopic.php?t=11761
% Extra Space with marginnotes
% https://tex.stackexchange.com/questions/315258/prevent-marginnote-from-creating-extra-paragraph
% Aligning marginnotes
% https://tex.stackexchange.com/questions/36554/vertically-aligning-a-marginnote-and-a-section-title
\RequirePackage{marginnote}
\newcounter{sidenote}
\providecommand\sidenote[1]{%                             a simple margin note
    \refstepcounter{sidenote}%                            step counter
    \mbox{\textsuperscript{\thesidenote}}%                the number (superscript) in text
    \marginpar{\footnotesize\mbox{\textsuperscript{\thesidenote}}\ #1}% the note
}
\footnotesinmargin
%\setlength{\footmarkwidth}{0.8em}
\setlength{\footmarkwidth}{0.0em}
\setlength{\footmarksep}{-\footmarkwidth}
\setlength{\footparindent}{1em}
%\renewcommand{\foottextfont}{\footnotesize\baselineskip=1.33\baselineskip}
\footmarkstyle{{\tiny\textsuperscript{#1}}\ }

\setsidecaps{\marginparsep}{\marginparwidth}
\sidecapmargin{outer}
\setsidecappos{c}

%https://tex.stackexchange.com/questions/428891/sidecaption-in-memoir-in-wrong-margin-when-floated-to-next-page
\strictpagechecktrue

\renewcommand{\fnum@figure}{\footnotesize\figurename~\thefigure}
\renewcommand{\fnum@table}{\footnotesize\tablename~\thetable}
\renewcommand*{\sidecapstyle}{%
\ifscapmargleft
\captionstyle{\raggedleft\footnotesize}%
\else
\captionstyle{\raggedright\footnotesize}%
\fi}

% \newsubfloat{figure}
% \newfixedcaption[\subcaption]{\subfigcaption}{figure}

\RequirePackage[font=footnotesize,hypcap=false]{caption}

\providecommand\blankpage{\newpage\hbox{}\thispagestyle{empty}\newpage}
\providecommand\setinputpath[1]{%
    \makeatletter
    \def\input@path{{#1}}
    \graphicspath{{#1}}
    \makeatother
}

%\RequirePackage{amssymb}
\newskip\newthoughtskipamount
\newthoughtskipamount=1.0\baselineskip plus 0.5ex minus 0.2ex
\providecommand\newthoughtbreak{\par\ifdim\lastskip<\newthoughtskipamount
  \removelastskip\penalty-100\newthoughtskip\fi}

\newcommand{\newthoughtskip}{\vspace\newthoughtskipamount}
\newcommand{\orientationsymbol}{{\color{black!30}\scriptsize\raisebox{1pt}{\fontspec{Symbola}▶}}}
\providecommand\newthought[1]{%
    \nopagebreak
    \newthoughtbreak
    \noindent%
    %{\llap{\color{black!30}\scriptsize\raisebox{1pt}{$\blacktriangleright$}\quad}}%
    {\llap{\orientationsymbol{}\quad}}%
    \textsc{#1}%
}
\providecommand\newthoughtpar[1]{\nopagebreak\newthought{#1}\par\nopagebreak\noindent\ignorespaces}
\providecommand\orientation{\newthought{}}
\providecommand\orientationinitial{%
    \noindent%
    {\llap{\orientationsymbol{}\quad}}%
}





\RequirePackage{paralist}
\RequirePackage{threeparttable}




%%% Math Stuff %%%
\RequirePackage{amsmath, amsthm, mathtools, amssymb, euler}

%\RequirePackage{csquotes}

