\presectionopenepigraphhack{}%
\section{Object of study}%
\label{sec:intro:object}

\orientationinitial{}%
Having discussed the general theoretical (\cref{sec:intro:text}) and methodological (\cref{sec:intro:method}) basis, here follows a definition of the object of study:
first an overview of the language in question is provided (\cref{sec:intro:object:modern_literary_welsh}),
followed by a definition of the corpus (\cref{sec:intro:object:corpus}) and
the particular topics of investigation (\cref{sec:intro:object:topics}).



\presectionopenepigraphhack{}%
\subsection{Modern Literary Welsh}%
\label{sec:intro:object:modern_literary_welsh}

\subsubsection{Background}%
\label{sec:intro:object:modern_literary_welsh:background}

% \orientationinitial{}%
% This subsubsection outlines linguistic, demographic and historical background on Modern Literary Welsh.

\begin{figure}
	\caption{Maps portraying the geographical spread of Celtic languages}%
	\label{fig:intro:object:modern_literary_welsh:background:maps}%
	%
	\captionsetup{format=plain}%
	\subcaptionbox{Ethno-political map of the extant Celtic languages\label{fig:intro:object:modern_literary_welsh:background:celtic}}{%	
		\begin{minipage}[t]{0.4\textwidth}
			\centering
			\includegraphics[height=6cm]{chapters/introduction/celtic-map.pdf}
		\end{minipage}
	}%
	\hfill
	\captionsetup{format=plain}%
	\subcaptionbox{Proportion of people (aged 3 and over) able to speak Welsh, by local authority, 2011\label{fig:intro:object:modern_literary_welsh:background:census-2011}}{%	
		\begin{minipage}[t]{0.4\textwidth}
			\centering
			\includegraphics[height=6cm]{chapters/introduction/census-2011.jpg}
		\end{minipage}
	}%
	\marginnote{%
		\footnotesize
		Source and copyright:
		% (a)~Wikimedia Commons (\url{https://w.wiki/3dpK});
		% (b)~\textcite{2011-census:2012:welsh-language}.
		(\subref{fig:intro:object:modern_literary_welsh:background:celtic})~Wikimedia Commons (\url{https://w.wiki/3dpK}, altered);
		(\subref{fig:intro:object:modern_literary_welsh:background:census-2011})~\textcite{2011-census:2012:welsh-language}.

		\vspace{\baselineskip}

		(\subref{fig:intro:object:modern_literary_welsh:background:celtic})~The extant Celtic languages fall into two branches:
		Brythonic (\circlegend{sronred}~Welsh, \circlegend{srongrey}~Breton\index[langs]{Breton} and \circlegend{sronyellow}~Cornish\index[langs]{Cornish}) and
		Goidelic (\circlegend{srongreen}~Irish\index[langs]{Irish}, \circlegend{sronblue}~Scottish Gaelic\index[langs]{Scottish Gaelic} and \circlegend{sroncyan}~Manx\index[langs]{Manx}).
		This map represents broad ethno-political borders; the distribution of speakers and their percentage in the population vary greatly.

		(\subref{fig:intro:object:modern_literary_welsh:background:census-2011})~Darker shades represent a higher proportion of Welsh speakers.
	}[-0cm]% Ugly hack. I’m sure there is a proper solution
\end{figure}
Welsh is a Brythonic Celtic language (Indo-European) spoken mainly in Wales (Britain, Europe); see \cref{fig:intro:object:modern_literary_welsh:background:celtic} for basic geographic and genealogical information.
Current estimates suggest 562,000 (\textcite{2011-census:2012:welsh-language}) to 883,600 (\textcite{2020-aps:2021:welsh-language}) speakers aged 3 and above (including L2 speakers), constituting 18.3\% to 29.1\% of the population of Wales\index{minority languages}; see \cref{fig:intro:object:modern_literary_welsh:background:census-2011} for a map of areal distribution according to the 2011 United Kingdom census%
\footnote{%
	At the moment results from the recent 2021 decennial census are yet to be fully published, and to the best of my knowledge no map representing the 2020 Annual Population Survey results is publicly available.
}.
Since the end of the \nth{20} century virtually all Welsh speakers in Wales are bilingual with English\index[langs]{English!Modern},
with no (adult) monolingual speakers left \parencite[chart~1]{2011-census:2012:welsh-language}.
In addition, there are speakers of Welsh in Y Wladfa, Welsh settlements in Patagonia, Argentina.%
\footnote{%
	Estimations regarding the number of speakers are roughly around 5,000, making a very small minority of the population \parencite{rees.i.w:2021:hispanicization-welsh}.
}

The shape of the modern Welsh language is the result of both internal and external factors throughout its history.
Very little is known about the pre-Celtic population of Britain and the languages they spoke \parencites{koch.j.t:2006:pre-celtic}[ch.~1]{price.g:1985:languages-britain}, but after the migration of Celtic people to Britain in the iron age two languages had a major influence:
one is Latin (mainly during the Roman rule, 43~\textsc{bce} to 410~\textsc{ce}; see \cite[ch.~12]{price.g:1985:languages-britain} and \cite{lewys.h:1980:elfen-ladin}) and
the other is English \parencites[ch.~12]{price.g:1985:languages-britain}, whose influence began with the Anglo-Saxon invasion and settlement in Britain and is still ongoing.

The sociolinguistic situation of Welsh is characterised by a diglossic\index{diglossia} relationship between a literary language (\C{\gl{yr}{\Def{}} \gl{iaith}{language} \gl{lenyddol}{literary}}) and a colloquial, spoken one (\C{\gl{yr}{\Def{}} \gl{iaith}{language} \gl{lafar}{speech}}); see \textcites[p.~192f.]{ball.m.j+:1988:broadcast-welsh}[382]{kaye.a.s:2002:diglossia-millennium}.%
\footnote{%
	The difference between the two is stark and encompasses all aspects of language.
	It is not unlike the difference between Modern Standard Arabic\index[langs]{Arabic} and the colloquial Arabic varieties \parencite{fife.j:1986:literary-colloquial}.
}
Literary Welsh has a long and rich written history, going back in one form or another about a thousand years into the past (for a historical overview of the Welsh language, see \cite{koch.j.t:2006:welsh-language}).
Naturally, for the most part Colloquial Welsh is only sporadically and unsatisfactorily documented before modern times.
\footnote{%
	For an overview of Literary Welsh and its relation to the colloquial language, see \textcite{jones.d.g:1988:literary-welsh}.%
	For a recent study of another layer of diglossia, namely between Welsh and English\index[langs]{English!Modern}, see \textcite{price.a.r:2020:inverted-diglossia}.
}

From a typological point of view, many of the linguistics features of Welsh differ markedly from these of other, non-Celtic neighbouring European languages \parencite{haspelmath.m:2001:sae}%
\footnote{%
	There is, however, evidence for features in which English\index[langs]{English} diverged from Continental West Germanic (or Gallo-Romance) and converged with Celtic, resulting in what can be described as a linguistic area encompassing Britain and Ireland \parencite{dedio.s+:2019:britain-ireland}.
}.
These include features that are not only areally exceptional but also cross-linguistically rare \parencite{phillips.j:2007:subsystems}, of which the most prominent is the \glossaryterm{initial consonant mutation} system\index{initial consonant mutation}%
\footnote{%
	For descriptions and typological discussions, see \textcites%
	{grijzenhout.j:2011:consonant-mutation}%
	{hannahs.s.j:2011:celtic-mutations}%
	{iosad.p:2010:right-left}%
	{zimmer.s:2005:mutations}%
	{hickey.r:1996:typological-mutation}%
	{ball.m.j+:1992:mutation}.
},
which is \mbox{(morpho-)}syntactical in what it marks yet phonological in how it is marked it.
\begin{table}
	\caption{An overview of the consonant mutations in Literary Welsh}%
	\label{tab:intro:object:modern_literary_welsh:background:mutations}%
	\centering%
	\marginnote{
		\footnotesize%
		Notes:
		Empty cells mean no alternation.
		The terms \emph{soft mutation} and \emph{lenition} (\Len{}) are interchangeable.
		A consonant affected by \Len{} is described as \emph{lenited}.
		Lenited \C{g-} is zeroed (cf.\ radical \C{gardd}[a garden] and lenited \C{yr ardd}[the garden]).
		For conciseness two mutations are not represented in the table:
		one is the \emph{limited soft mutation} (\Limlen{}), which operates like \Len{} but does not affect \C{ll-} and \C{rh-};
		the other is the \emph{mixed mutation} (\Mix{}), which operates like spirant mutation (\Spi{}) on \C{p-}, \C{t-} and \C{c-} and like \Len{} on the other consonants.
		% The mutations of \grapheme{ts-} \ipaem{tʃ-} are marginal, especially in Literary Welsh of the period under discussion, where they are virtually absent.
	}[\baselineskip]%
	\footnotesize%
	\begin{tabular}[t]{cclcclcclcc}
		\toprule
		\multicolumn{2}{c}{\Rad{}}  & & \multicolumn{2}{c}{\Len{}}  & & \multicolumn{2}{c}{\Nas{}}  & & \multicolumn{2}{c}{\Spi{}} \\
		%& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}\\
		\midrule
		p   & \ipaem{p}   &  & b  & \ipaem{b} &  & mh  & \ipaem{m̥} &  & ph & \ipaem{f}\\
		t   & \ipaem{t}   &  & d  & \ipaem{d} &  & nh  & \ipaem{n̥} &  & th & \ipaem{θ}\\
		c   & \ipaem{k}   &  & g  & \ipaem{g} &  & ngh & \ipaem{ŋ̊} &  & ch & \ipaem{χ}\\[1.5ex]
		b   & \ipaem{b}   &  & f  & \ipaem{v} &  & m   & \ipaem{m}    \\
		d   & \ipaem{d}   &  & dd & \ipaem{ð} &  & n   & \ipaem{m}    \\
		g   & \ipaem{g}   &  & ∅  & \ipaem{\enspace{}} &  & ng  & \ipaem{ŋ}    \\[1.5ex]
		m   & \ipaem{m}   &  & f  & \ipaem{v}\\
		ll  & \ipaem{ɬ}   &  & l  & \ipaem{l}\\
		rh  & \ipaem{r̥}   &  & r  & \ipaem{r}\\%[1.5ex]
		% (ts & \ipaem{tʃ}  &  & j  & \ipaem{dʒ}&  &     & \ipaem{n̥ʃ}&  &    & \ipaem{θj})\\
		\bottomrule
	\end{tabular}
\end{table}
This system marks various grammatical categories and relations by apophonic distinctions on the initial consonants of morphemes; see \cref{tab:intro:object:modern_literary_welsh:background:mutations} for a schematic overview of the different mutations in Welsh.

Among the grammatical systems that contribute to text construction in Welsh two stand out.
One is information structure\index{information structure} and information status\index{information status}.
Welsh is sensitive to distinctions related to flow of information in the text, which is reflected in an array of constructions and distinctions that come together to structure that flow and make the text cohesive.
These include but are not limited to the use of different pronoun series and indexing, pre-verbal particles, cleft sentences\index{cleft sentence}, articles, clause models and constituent order.
The other grammatical system is that of tense.
The rather complex tense system of Literary Welsh consists of several synthetic tenses as well as compound, periphrastic tenses, most of which are founded on \glossaryterm{converbal} (\cref{sec:intro:meta:terminology:converb}) phrases.%
\footnote{%
	The rich tense system is employed differently in different text-types (\cref{sec:intro:text:text-types}), including minute distinctions of narrative tenses, which are often impossible to render in translation.
}



\subsubsection{Research on text linguistics}%
\label{sec:intro:object:modern_literary_welsh:research}

The Welsh language has received scholarly attention in numerous branches of linguistics, including phonology, morphology, micro-syntax (sentence syntax), sociolinguistics, lexicography, etymology, typology, language contact and historical linguistics, as well as in related fields like philology.
However, with few exception not much research has been conducted on Welsh text linguistics.

Two scholars who dedicated much effort in the last decades to the description of Welsh syntax with a focus on textual factors are Erich Poppe and Ariel Shisha-Halevy.
Poppe’s work is concerned with medieval Celtic languages; some of his publications concerning Welsh syntax include \textcite{
	poppe.e:1989:maxen,
	poppe.e:1990:ronabwy,
	poppe.e:1991:cyfranc,
	poppe.e:1995:narrative-present,
	poppe.e:1999:adaption-akkulturation,
	poppe.e:2000:order-middle-welsh,
	poppe.e:2003:progressive,
	poppe.e:2012:locative,
	poppe.e:2014:achieve,
	poppe.e:2017:theoretical-applied,
	poppe.e:2022:coordination-vn,
	} and \textcite{harlos.a+:2014:ambiguity}%
	%\textcite{poppe.e:2012:gwenole} (Breton) and \textcite{poppe.e:1994:pragmatics} (Irish)%
.
Shisha-Halevy’s publications concerning Modern Welsh are \textcite{
	shisha-halevy.a:1997:infinitive-aorist,
	shisha-halevy.a:1998:roberts,
	shisha-halevy.a:2003:juncture-welsh,
	shisha-halevy.a:2005:epistolary,
	shisha-halevy.a:2010:celtic-converbs,
	shisha-Halevy.a:2015:fe-mi,
	shisha-Halevy.a:2016:presentatives,
	shisha-halevy.a:2022:converbs-narrative,
}%
\footnote{%
	He also published a series of two articles concerning Middle Welsh\index[langs]{Welsh!Middle} syntax \parencite{shisha-halevy.a:1995:middle-welsh-1, shisha-halevy.a:1999:middle-welsh-2}, which include specific references to and several excursus on Modern Welsh syntax.
};
Kate Roberts’s works make the main corpus for his research on Modern Welsh.

Thus, the field of Welsh text linguistics is not a \foreign{terra incognita}, but it is far from being fully charted.
The present study aims at contributing to our understanding of Welsh text syntax, making this \foreign{terra} somewhat \foreign{cognitior}.



\presectionopenepigraphhackwithlongopenbiling{}%
\subsection{The corpus}%
\label{sec:intro:object:corpus}

{}\index{corpus|(}%
\openbilingepigraph
{%
	% Rydw i’n cael y teimlad weithiau, credwch neu beidio, rydw i’n cael y teimlad nad ydw i fy hun yn ddim byd ond iaith~— dim byd ond geiriau a brawddegau.
	\selectlanguage{welsh}%
	Rydw i’n cael y teimlad weithiau \ellipsis{} nad ydw i fy hun yn ddim byd ond iaith~— dim byd ond geiriau a brawddegau.
	Fyddwn i ddim yn bod oni bai am iaith.
}
{%
	% Sometimes I get the feeling, believe it or not, I get the feeling that I myself am nothing at all but language~— nothing but words and sentences.
	Sometimes I get the feeling \ellipsis{} that I myself am nothing at all but language~— nothing but words and sentences.
	I would not exist were it not for language.
}
{\worktitle{Te Gyda’r Frenhines}, Mihangel Morgan \parencite{morgan.m:1994:frenhines}}%
Given the synchronic nature of description, the scope of the present study is limited to a specific period in the history of Literary Welsh: the twentieth century.
Fortunately, that century has produced some remarkable works of literature by Welsh writers, including
the 1936 novel \C{\gl{Traed}{feet} \gl{mewn}{in} \gl{Cyffion}{stock.\Pl{}}}[Feet in Chains] by Kate Roberts \parencite{roberts.k:1988:traed},
the linguistically motley \parencite[p.~91 and passim]{morris.c.e:2018:iaith-drama} 1948 drama \C{\gl{Blodeuwedd}{‘flower-faced’}}[The Woman of Flowers] by Saunders Lewis \parencite{lewis.s:2017:blodeuwedd},
the 1953 novel \C{\gl{Cysgod}{shadow} \gl{y}{\Def{}} \gl{Cryman}{sickle}}[Shadow of the Sickle] by Islwyn Ffowc Elis \parencite{elis.i.f:2011:cysgod-cryman} and
the 1961 novel \C{\gl{Un}{one} \gl{Nos}{night} \gl{Ola}{light} \gl{Leuad}{moon}}[One Moonlit Night] by Caradog Prichard \parencite{prichard.c:1999:nos-night}.
The problem one faces is \emph{choosing} among the available options, which are intriguing and rich both language-wise and content-wise…
The answer lays not only in one’s personal aesthetic preferences but in the research question: aiming at deepening our understanding of the linguistic expression of text-types and their interrelation, the choice of works by Kate Roberts (1891–1985)\index{Roberts, Kate (biographic)|(} appears only natural.
Roberts~— commonly acknowledged as \C{\gl{Brenhines}{queen} \gl{ein}{\ein{}} \gl{Llên}{literature}}[the Queen of our Literature] \parencite{humphreys.e:1983:kate-roberts}~— was a prolific writer in a noteworthy variety of genres and media%
\footnote{%
	Her \foreign{œuvre} includes numerous short stories (for which she is most famous), novels, novellas (including a novella in the form of a diary), recollections, letters \parencite{lewis.s+:1992:annwyl}, plays, essays and journalistic writing.
}.
\begin{table}
	\caption{Works by Kate Roberts which serve as the corpus}%
	\label{tab:intro:object:corpus:kr}%
	\begin{tabular}{lllll}
		\toprule
		Year & Title                     & Genre                  & Chapter              & Reference\\
		\midrule
		1920 & \worktitle{Y Fam}         & Play                   & \cref{sec:drama}     & \parencite{davies.b.e+:1920:y-fam}\\
		1954 & \worktitle{Y Cynddrws}    & Play (radio drama)     & \cref{sec:drama}     & \parencite{roberts.k:2014:y-cynddrws}\\
		1959 & \worktitle{Te yn y Grug}  & Short stories          & \cref{sec:rd}        & \parencite{roberts.k:2004:te}\\
		1960 & \worktitle{Y Lôn Wen}     & Memoir                 & \cref{sec:anecdotes} & \parencite{roberts.k:1960:lon-wen}\\
		1972 & \worktitle{Atgofion}      & Memoir (radio episode) & \cref{sec:anecdotes} & \parencite{roberts.k:1972:atgofion}\\
		1981 & \worktitle{Haul a Drycin} & Short stories          & \cref{sec:rd}        & \parencite{roberts.k:1981:haul-drycin}\\
		\bottomrule
	\end{tabular}
\end{table}
One \thesis{} cannot encompass all the text-types in which Roberts has written over the span of several decades in an adequate depth; see \cref{sec:intro:object:topics} for an overview of the three topics chosen for the present study and \cref{tab:intro:object:corpus:kr} for an overview of the corpus.
As evident from the table, each chapter covers \emph{two} works; this not only strengthen the validity of the findings in comparison to one work each, but also allows a more refined analysis as well.
The works which constitute the studied corpus range from her earliest published work (\textcite{davies.b.e+:1920:y-fam}, when she was 29) to the last one (\textcite{roberts.k:1981:haul-drycin}, 61 fruitful years later).

Although Roberts is widely celebrated in Wales as an author and a public figure, arguably ‘[occupying] a position in Welsh literature analogous to that enjoyed by Virginia Woolf in English literature’ (\cite[p.~v]{gramich.k:2011:kate-roberts}; see also \cite{rhydderch.f:2000:roberts-woolf}), her works~— although considered classics and are taught in schools and universities~— remain ‘critically neglected’ according to \textcite{gramich.k:2011:intimate-circle} and she is little known outside of Wales despite the fact that much of her fiction has been translated into English (with a few works translated into other languages as well).
Nevertheless, some scholarly publications about her and her writing have been published, including monographs \parencite{emyr.j:1976:enaid-clwyfus,jones.g.w:2010:dror-fwrdd}, doctoral theses \parencite{rhydderch.f:2000:roberts-woolf,jones.d:2014:kr-drama}, collections of articles \parencite{jones.b:1969:kate-roberts,williams.r:1983:roberts}, as well as biographies and biographic and literary overviews \parencite{morgan.d:1991:roberts,roberts.e.l:1994:kate-roberts,gramich.k:2011:kate-roberts,llwyd.a:2011:kate}.
% To the best of my knowledge, the only published \emph{linguistic} investigation of her writing is that which has been conducted by Shisha-Halevy (\cref{sec:intro:object:modern_literary_welsh:research}).

The focus on a single writer determines another variable in addition to the three discussed above%
\footnote{%
	The language as Welsh, the language variety or register as literary, and the synchronic period as \nth{20} century.
},
namely \emph{dialect}\index{Welsh dialects}.
Roberts grew up and spent her formative years (linguistically and otherwise) in Rhosgadfan, a rural village in the county of Gwynedd%
\footnote{%
	Historically in Caernarfonshire, until the Local Government Act 1972, which reorganised the local authorities in England and Wales.
},
North Wales (see \cref{fig:intro:object:corpus:rhosgadfan}).
\begin{marginparfigure}%
	{Location of Rhosgadfan within Gwynedd, and of Gwynedd within Wales}%
	{Location of Rhosgadfan within Gwynedd, and of Gwynedd within Wales}%
	{fig:intro:object:corpus:rhosgadfan}%
	\includegraphics[width={\marginparwidth}]{chapters/introduction/rhosgadfan.png}

	Source and copyright: ‘Rhosgadfan’ on Wikipedia (\url{https://en.wikipedia.org/wiki/Rhosgadfan}).
\end{marginparfigure}
In general the grammatical system of Literary Welsh cuts across dialect boundaries, but a writer’s personal background, including dialectal factors, does have an influence on their \rev{2}{writing}.
This is particularly true in the case of Roberts, whose writing reflects many local features, not only in dialogues representing the speech of local characters but in other portions of the texts as well.
{}\index{Roberts, Kate (biographic)|)}
{}\index{corpus|)}



\subsection{Topics and structure of the \thesis{}}%
\label{sec:intro:object:topics}

\orientationinitial{}%
Now that all of the necessary foundations have been laid, we can proceed to defining the topics that are examined.%
\footnote{%
	See also the abstract (p.~\pageref{sec:abstract}~ff.) for an overview of the topics dealt with in this \thesis{}.
}

The subject of the relationship between text and language is vast, and many monographs, theses and articles have been dedicated to it.
Thus, for the sake of effective, focussed description that can draw conclusions based on empirical data, three topics have been chosen.
The choice of topics is not incidental, as all have broader theoretical implications beyond the specific linguistic facts that are arranged and described%
\footnote{%
	Hopefully from the point of view of the language itself, as suggested in \cref{sec:intro:method:neutral}.
},
and all share common themes (see below).

\begin{plainlist}
	\item \Cref{sec:anecdotes} deals with a distinct embedded text-type~— \emph{the anecdote}~— which is defined, characterised and examined according to a recurrent systematic macro-syntactic structure (\cref{sec:anecdotes:structure,sec:anecdotes:components}) that emerges from analysing the studied corpus%
		\footnote{%
			Two autobiographical texts \parencite{roberts.k:1960:lon-wen,roberts.k:1972:atgofion}, which contain in total more than \anecnum{150} such narrative miniatures.
		}.
		Many of the anecdotes are rather concise; that makes them a perfect candidate for the study of features of one of the most complex text-types~— narrative~— under ‘controlled conditions’ that highlight the essence of narrativity and its relation with grammar.
		The anecdotes are dependent upon the surrounding text and serve as means for elaboration and corroboration of statements, claims, comments and descriptions by means of concrete narrative instances (\cref{sec:anecdotes:embedding}).
		The description of complex structures can benefit from considering edge cases, which can help improve our understanding of the common, more average cases, by delineating the boundaries (\cref{sec:anecdotes:edge}).
		The corpus for this chapter is made of two source texts which have much in common, but have some dissimilarities as well; \cref{sec:anecdotes:comparison} concludes this chapter by exploring aspects of similarity and difference, including a comparison of anecdotes from one text which are retold in the other.

	\item \Cref{sec:rd} is dedicated to \emph{reporting of speech in narrative}, focussing on the seam between two primary components (or \emph{modes}) of most narratives: dialogue and the narration in which it is embedded.
		The source material here is two collections of short stories \parencite{roberts.k:2004:te,roberts.k:1981:haul-drycin}.
		Several aspects of this seam are examined:
		the micro- and macro-syntax of the three types of quotative indexes%
		\footnote{%
			Also known as \emph{quotation formulae} or \emph{inquit formulae} in the scholarly literature, among a plethora of other terms.
		} (\cref{sec:rd:patterns,sec:rd:internal});
		the system that governs overt quotative indexes and zero ones (\cref{sec:rd:zero});
		interruption and resumption of conversations (\cref{sec:rd:interruptions});
		and a few related topics (\cref{sec:rd:related}).

	\item \Cref{sec:drama} is an inquiry of the text-grammatical characteristics of a unique type of textual component: \emph{stage directions} in plays.
		Two plays \parencite{davies.b.e+:1920:y-fam,roberts.k:2014:y-cynddrws} make the data for this chapter: one is a stage play and the other is a radio play.
		Stage directions display unique linguistic characteristics and are especially interesting thanks to their interrelation to the dramatic text%
		\footnote{%
			The text which is spoken aloud by the actors.
		}
		with which they are intertwined, as well as their inherent performativity and connection to extralinguistic, dramatic aspects%
		\footnote{%
			The different modality of the two plays plays a pertinent role in this: stage directions for a theatre are different from stage directions for an acoustic performance.
		}
		and the question of the target audience (‘who reads stage directions?’).
		This shorter chapter consist of two parts:
		one (\cref{sec:drama:pre}) discusses the introductory specifications (\foreign{dramatis personae}, place and time, and setting);
		the other (\cref{sec:drama:sd}) examines stage directions which accompany the dramatic text, on the basis of two axes of analysis (textual environment and syntactic form).
\end{plainlist}

As evident from this bird’s-eye view, three themes thread through all chapters, namely the linguistic expression of
\begin{inparaenum}[(a)]
	\item structural regularities of textual functions,
	\item the interrelation and interconnectivity of textual units or components and
	\item the multifaceted nature of narrative.
\end{inparaenum}
By shedding light on core text-linguistic questions from $3 \times 3$ angles, my hope is that this \thesis{} offers a whole that is greater than the sum of its parts%
\footnote{%
	While each of the topics can in theory make a stand-alone description of a particular aspect of language, the combination of all three makes a holistic outcome that may not only elucidate these aspects better (in comparison to three independent studies) but also assist in advancing other, related text-linguistic topics.
}.
Welsh presents an exceptionally fascinating text-linguistic grammatical system, of which much is yet to be studied and researched, and the diverse writings of Kate Roberts present an opportunity to study it.
Thus, the goal of the present \thesis{} is to offer new findings that both contribute to the particular topics and language in question, and are beneficial for text linguistics in a broader sense.

\orientation{}%
‘Meta’ matters are discussed in a dedicated appendix (\cref{sec:intro:meta}).
These include issues of accessibility (\cref{sec:intro:meta:accessibility}) and terminology (\cref{sec:intro:meta:terminology}), as well as some technical notes (\cref{sec:intro:meta:tech}).
