\chapter{Appendix: Meta}%
\label{sec:intro:meta}

\section{Accessibility}%
\label{sec:intro:meta:accessibility}

In order to make the results of research as accessible as possible to all scholars, regardless of previous familiarity with Welsh, I followed these guidelines, aiming at removing any language-specific possible obstacles:

\begin{compactitem}
	\item Clearly and concisely explaining non-trivial language-speci\-fic features and key concepts whenever needed.
		This is done along the text, in a way that binds the explanation to the specific discussion in question.%
		\footnote{%
			In order to avoid the clutter of unrelated explanations, only directly pertinent features which cannot be adequately understood without such explanations are explained.
		}

	\item Defining in a glossary (\cref{app:glossary}) terms that may not be necessarily familiar to all in the particular sense used in this \thesis{}.

	\item Including translations for cited examples (see \cref{sec:intro:meta:translations}).

	\item Glossing Welsh words according to the accepted standard (see \cref{sec:intro:meta:glossing}).

	\item Annotating\index{annotation} the examples in the appendices, highlighting relevant structural properties, both in the original text and the translation%
		\footnote{%
			This annotation has a dual function.
			While its primary goal is to demonstrate the \mbox{(text-)}linguistic structure, it has a secondary purpose as well: one can navigate the source text with the aid of the mirrored annotation in the translation.
		}
		(see \cref{app:anecdotes:annotation} regarding the anecdote text-type, for example).
\end{compactitem}

In addition, whenever colour is used (in tables and fragments or for highlighting the stage directions in \cref{app:drama}), it follows Paul Tol’s colour schemes%
\footnote{%
	\url{https://personal.sron.nl/~pault/}
}, which should be are distinct for colour-blind people, to the best of my knowledge.%
\footnote{%
	Colour almost always accompanied by textual indications (e.g.\ green cells with $+$, red cells with $-$), for better legibility.
}




\subsection{Translations}%
\label{sec:intro:meta:translations}

As a general rule, whenever an existing literary translation into English is available, it is used as is for accompanying the cited examples; see \cref{tab:intro:meta:translations}.
\begin{table}
	\caption{Translations used for cited examples}%
	\label{tab:intro:meta:translations}%
	\footnotesize%
	\begin{tabularx}{\myfullwidth}{XXXXX}
		\toprule
		Original title & Reference & Translator & Translated title & Reference\\
		\midrule
		\worktitle{Traen mewn Cyffion} & \textcite{roberts.k:1988:traed} & John Idris Jones & \worktitle{Feet in Chains} & \textcite{roberts.k:1977:feet-jones}\\
		\worktitle{Te yn y Grug} & \textcite{roberts.k:2004:te} & Joseph P.\ Clancy & \worktitle{Tea in the Heather} & \textcite{roberts.k:1991:world}\\
		\worktitle{Y Lôn Wen} & \textcite{roberts.k:1960:lon-wen} & Gillian Clarke & \worktitle{The White Lane} & \textcite{roberts.k:2009:white-lane}\\
		\worktitle{Haul a Drycin a storïau eraill} & \textcite{roberts.k:1981:haul-drycin} & Carolyn Watcyn & \worktitle{Sun and Storm and other stories} & \textcite{roberts.k:2001:sun-storm}\\
		\bottomrule
	\end{tabularx}
\end{table}
Where the translation departs from the text in ways that may be misleading with regard to constructions under discussion it is altered so that it fits our needs better (even at the cost of idiomaticity and flow); these are marked with the degree symbol~(\notsic{}) at the beginning of the sentence.

To the best of my knowledge \YF{} \parencite{davies.b.e+:1920:y-fam} and \Atgofion{} \parencite{roberts.k:1972:atgofion} have never been translated into English.
The translations of the examples from them are mine.%
\footnote{%
	These are by no means a literary translation, and should not be read as such, as if they were independent texts to be read on their own.
	Rather, they is direct and simple, not aiming at having artistic value but serving as means for making the original texts more accessible.
	The English and Welsh systems of syntax are quite dissimilar; in order to make the translation readable I chose not to reflect Welsh structures in a servile manner but to render them in a (hopefully) intelligible way, even at the cost of failing to represent grammatical distinctions in the original that have no direct equivalence in English.
}



\subsection{Interlinear glossing}%
\label{sec:intro:meta:glossing}

{}\index{glossing|(}%
All Welsh syntagms in the main text are glossed according to the Leipzig Glossing Rules \parencite{bickel.b+:2015:leipzig-glossing}.
This includes cited examples and in-line Welsh text, but not examples in the appendices or marginalia.

A list of abbreviations is available on p.~\pageref{glabbreviations}.
All are in common use or explicitly explained whenever pertinent to the topic under discussion.

Given the macro level of investigation, the glosses indicate only basic morphological and morpho-syntactic information, avoiding detailed morphological analysis for the sake of better readability and accessibility.
A practical, minimalist approach is adopted, following these guidelines:

\begin{itemize}
	\item The glossing of inflectional categories is consistent regardless of morphological particularities%
		\footnote{%
			For example,
			\C{cathod} ‘cats’ (plural of \C{cath} ‘cat’, marked by a suffix),
			\C{cerrig} ‘stones’ (plural of \C{carreg} ‘stone’, marked by vowel apophony) and
			\C{cewri} ‘giants’ (plural of \C{cawr} ‘giant’, marked by both a suffix and apophony)
			are all glossed using ‘.\Pl{}’.
			For simplicity, singular number of nouns is left unmarked, singulative forms are marked (\Sgv{}).
			%The non-overt markings are glossed the same way; for example, \C{moch} ‘pigs’ is glossed ‘pig.\Col{}’, while its singulative form \C{mochyn} ‘a pig’ is glossed ‘pig.\Sgv{}’.
		}.

	\item Idiosyncracies of Welsh grammar that are not directly related to the topic under discussion are not indicated in the glosses.
		For example, despite the syntactic difference between the negators \C{na(c)~\textsuperscript{+\Mix{}}} (in \Rspv{} or followed by \Imp{}.\Imprs{}), \C{ni(d)~\textsuperscript{+\Mix{}}} and \C{nid} not triggering a mutation \parencite[see][§~6.165]{thomas.p:2006:gramadeg} all three are simply glossed ‘\Neg{}’.%, and similarly both \C{y(r)} and \C{a \textsuperscript{+\Len{}}} are glossed ‘\Rel{}’.

	\item
		As a rule, initial consonant mutations are not marked in the glosses on purpose, as indicating them would be benefactory to a limited degree but would make the glosses unmanageably cluttered.
		Wherever distinctive, the \emph{effect} of the different mutations is indicated; e.g. \C{\gl{ei}{\eif{}} \gl{chath}{cat}}[her cat] versus \C{\gl{ei}{\eim{}} \gl{gath}{cat}}[his cat] (radical \C{\gl{cath}{cat}}) or \C{\gl{a}{and} \gl{chanodd}{sing.\Pret{}.\Tsg{}}}[and sang] versus \C{\gl{a}{\aRel{}} \gl{ganodd}{sing.\Pret{}.\Tsg{}}}[that sang] (radical \C{\gl{canodd}{sing.\Pret{}.\Tsg{}}}).
		%\footnote[%
		%	\C{ei} followed by spirant mutation (radical \C{cath}{a cat}~→ mutated \C{chath}) indicates possession by \Tsg{}.\F{}, while \C{ei} followed by soft mutation (radical \C{cath}~→ mutated \C{gath}) indicates possession by \Tsg{}.\M{}.
		%].
		For convenience, a summary of the Welsh mutations is provided as \cref{tab:intro:object:modern_literary_welsh:background:mutations}.

	\item The choice of glosses is adaptive, and may differ depending on the specific usage in a particular example%
	\footnote{%
		For example, \C{dim} is glossed \E{nothing}, \E{anything} or \E{\Neg{}}, depending on the particular case.
		Similarly the preposition \C{am} is glossed either \E{for} or \E{about} and \C{o} is glossed either \E{from} or \E{of}.
		In general prepositions are often impossible to render directly in a one-to-one correspondence between languages.
	}
	or the focus in the given context.
	Whenever gender agreement is helpful for understanding the example, it is indicated within round parentheses (rule~7 in \cite{bickel.b+:2015:leipzig-glossing}).
\end{itemize}

The distinction between the four superficially homonymic \C{yn} grammatical elements follows \textcite{sims-williams.p:2015:yn}:
\begin{itemize}
	\item \ynA{} precedes definite or proper nouns and functions as a locative preposition; triggers nasal mutation: e.g.\ \C{\gl{yn}{\ynA{}} \gl{Nhwrci}{Turkey}}[in Turkey] (radical \C{\gl{Twrci}{Turkey}}).

	\item \ynB{} precedes adjectives to form adjunctive adverbs%
		\footnote{%
			All four types are \emph{adverbial} in the broad structural sense of adverbial commutability.
			The use of the \Adv{} here is for practical purpose.
		};
		triggers limited soft mutation: \C{\gl{yn}{\ynB{}} \gl{fawr}{great}}[greatly] (radical \C{\gl{mawr}{great}}).

	\item \ynC{} precedes adjectives and nouns and signals their predicative status;
		triggers limited soft mutation as well:
		\C{\gl{Mae}{\mae{}} \gl{ef}{\ef{}} \gl{yn}{\ynC{}} \gl{gawr}{giant}}[He is a giant] (radical \C{\gl{cawr}{giant}}).

	\item \ynD{} precedes infinitives to form converbs (\cref{sec:intro:meta:terminology:converb});
		does not trigger a mutation:
		\C{\gl{yn}{\ynD{}} \gl{rhedeg}{run.\Inf{}}}[running (\Cvb{})] (radical \C{\gl{rhedeg}{run.\Inf{}}}).
\end{itemize}

{}\index{glossing|)}



\subsection{Transcription}%
\label{sec:intro:meta:transcription}

{}\index{transcription|(}%
Throughout the text there are sporadic typological comparisons with other languages (see the language index on p.~\pageref{idx:langs}), including languages written in non-Latin scripts.
These are transcribed according to common scholarly transcription methods.
Japanese\index[langs]{Japanese} in transcribed using the \foreign{Nihon-siki} romanisation system \parencite{gottlieb.n:2010:romaji-japan} and Coptic\index[langs]{Egyptian!Coptic} using the Leipzig-Jerusalem transliteration \parencite{grossman.e+:2015:leipzig-jerusalem}.
{}\index{transcription|)}



\section{Terminology}%
\label{sec:intro:meta:terminology}

{}\index{terminology|(}%
The terminology in this \thesis{} does not deviate far from the common practice and conventions among linguists.
Welsh has its own traditions with regard to terminology, both native and in other languages (primarily English and German).

\orientation{}%
Given these traditions, two terms used here call for an explanation (\cref{sec:intro:meta:terminology:infinitive,sec:intro:meta:terminology:converb}).
Another term stems from traditional grammar due to the lack of a better alternative although it is non-descriptive when interpreted nominally (\cref{sec:intro:meta:terminology:gor}), and a fourth necessitates an explanation (\cref{sec:intro:meta:terminology:conj}).



\subsection{Infinitive}%
\label{sec:intro:meta:terminology:infinitive}

One is \emph{infinitive}\index{infinitive|(} (\Inf{}), applied to forms like \C{\gl{gweld}{see.\Inf{}}}[to see].
Although \emph{infinitive} is a very common general linguistic term, the Celtic grammatical tradition uses \emph{verbal noun}%
\footnote{%
	See \cite[§~171.iv.2]{morris-jones.j:1913:welsh-grammar}, being one of the most influential works of the Welsh grammatical tradition.
}
or \mbox{\emph{verb(-)noun}} (\C{berfenw} in Welsh, a compound of \C{berf}[verb] and \C{enw}[name]).
Both \emph{verb(al) noun} and \emph{infinitive} are used by contemporary scholars, referring to the same linguistic entity by different names.
For example, the grammars of
\textcite[§§~198–209]{king.g:2015:welsh-grammar-3},
\textcite[§§~2.1 and 3.1]{thomas.p:2006:gramadeg} and
\textcite[§§~315–317]{thorne.d:1993:grammar}
use the first, while
\textcite[§~1]{shisha-halevy.a:1997:infinitive-aorist},
\textcite[§§~3.1.1 and 3.1.2]{borsley.r+:2007:syntax} and
\textcite{miller.d.g:2004:conjugated-infinitive}
use the latter.

Arguments that can be made for the use of \emph{infinitive} include%
\footnote{%
	See \textcite[n.~1 on p.~85]{shisha-halevy.a:1997:infinitive-aorist} and \cite[§~3.1.2]{borsley.r+:2007:syntax} for further discussion.
}:
\begin{itemize}
	\item The common use of the term in linguistics makes it readily accessible for linguists outside Celtic Studies (\C{\gl{Astudiaethau}{study.\Pl{}} \gl{Celtaidd}{Celtic}}; \foreign{Keltologie}).
		Choosing a prevalent term also makes typological comparison more straightforward: although comparative concepts and language-specific descriptive categories should not be confused \parencite{haspelmath.m:2010:comparative}, the term \emph{infinitive} not only suits the Welsh case well, but also lends itself better for comparison.

	\item This term avoids the dependence \emph{verb(al) noun} has on traditional parts of speech.
		This theoretical advantage also allows us to bypass issues like the question regarding its identity as a noun or a verb \parencite[cf.][]{willis.p:1988:verbal-noun}.
		\emph{Infinitive} on the other hand only suggests the non-finite nature of the form (and implies structural affinity with infinitives of other languages, as claimed above).

	\item There are actual verbal nouns in Welsh, and they should be distinguished in terminology.
		For example, the infinitive \C{\gl{dehongli}{interpret.\Inf{}}}[to interpret] is to be kept apart from the (de)verbal noun \C{\gl{dehongliad}{interpretation}}[(an) interpretation] in the terms used for describing them.
		For a general discussion on this point, see \textcite{ylikoski.j:2003:defining-non-finites}.
\end{itemize}
\index{infinitive|)}


\subsection{Converb}%
\label{sec:intro:meta:terminology:converb}

The other term is \emph{converb}\index{converb|(} (\Cvb{}), applied to constructions like \C{\gl{yn}{\ynD{}} \gl{canu}{sing.\Inf{}}}[singing%
\footnote{%
	English \E{singing} can be misleading.
	\C{yn canu} does not correspond to the nominal \E{singing} in \E{Singing is my hobby} but to that of \E{She is singing} (predicative, primary), \E{I heard him singing} (predicative, secondary) and \E{He went home singing to herself} (adjunctive).\\
	A side note: it has been claimed the development of the progressive (or converbal) constructions of English has to do with Celtic influence \parencite[§§~2.2.5 and~4.2.2.1]{filppula.m+:2008:english-celtic}.
}]
and \C{\gl{dan}{under} \gl{wenu}{smile}}[smiling%
\footnote{%
	Here English proves problematic as a metalanguage, for another reason: prepositional slot (filled by \C{yn} and \C{dan} in these examples) in Welsh shows a richer paradigm that cannot be reflected by the English \E{\V{}-ing} form.
}].
The term originated from Ramstedt’s writing on Khalkha Mongolian\index[langs]{Mongolian!Khalkha} \parencite{ramstedt.g:1902:konjugation}, seeing a constant growing recognition and use over more than a hundred years, first to ‘Altaic’ or ‘Ural-Altaic’%
\footnote{%
	‘\mbox{(Ural-)}Altaic’ being disciplinary terms here, referring to the history of the field (as opposed to a linguistic descriptive realities as language families).
}
languages and then to other, unrelated languages such as Gurage\index[langs]{Gurage} (Semitic; \cite{polotsky.h.j:1951:notes-gurage}) or Kurtöp\index[langs]{Kurtöp} (Tibeto-Burman; \cite{hyslop.g:2017:grammar-kurtop}).
The publication of \textcite{haspelmath.m+:1995:converbs} proved important to the expansion of the term, not only thanks to the language-specific articles in it but also \textcite{haspelmath.m:1995:converb-valid}, which provided a solid foundation to its typological and cross-linguistic validity and applicability.

Definitions of the term vary in terms of specificity and foci; see \textcite[§~3.2]{ylikoski.j:2003:defining-non-finites}, \textcite[p.~269ff.]{shisha-halevy.a:2010:celtic-converbs} and \textcite[§~1]{shisha-halevy.a:2022:converbs-narrative} for discussion of different definition.
In its most general definition, it refers to adverbial verb forms.
For our consideration is should be stressed that \E{form} does not imply a morphological, synthetic nature%
\footnote{%
	As they are in the languages that forms the historical core of the use of the term, that is ‘Altaic’ or ‘Ural-Altaic’: usually an oblique case form of a deverbal form.
};
converbs can be of analytic (periphrastic, syntactic) nature as well.
Indeed, so are the converbs in Welsh, constructed as a \C{[\Prep{} \Inf{}]}%
\footnote{%
	\C{[newydd \Inf{}]} is an exception, albeit a relatively marginal one.
}
\index[welsh]{[prep inf]@[\Prep{} \Inf{}]} complex.

Adoption of the term to Celtic languages%
\footnote{%
	Analogous construction are found in both Brythonic and Goidelic branches.
}
varies among scholars.
For example, \textcite[][includes a general and methodological discussion]{shisha-halevy.a:2010:celtic-converbs}, \textcite[§~1.5.3]{eshel.o:2015:narrative} and \textcite{stifter.d:2009:early-irish} adopt it.
\textcite{poppe.e:2012:gwenole} considers the term but chooses to postpone its adoption until more research has been conducted on the said construction within a larger typological framework (p.~59).
Concerning \C{[\gl{yn}{\ynD{}}~+ \Inf{}]} (that is, in the terms used here, a converb with \C{yn} as its preposition), \textcite[§~5]{sims-williams.p:2015:yn} mentions \emph{converb} among other terms (\emph{participle} and \emph{progressive}), but does not take a stance on terminology and simply calls it \C{[\textsc{yn}\textsuperscript{R}~+ \textsc{verbal noun}]} (\emph{R} stands for \E{radical}, meaning it does not trigger a mutation).
On the other end of the spectrum, \textcite[§~2.5]{nedjalkov.i.v:1998:converbs-europe} uses a much stricter definition of \emph{converb} and rejects its use for such constructions in Welsh (and other languages), which he describes as ‘free phrases consisting of two more or less independent elements’%
\footnote{%
	It is noteworthy that he does consider the Irish\index[langs]{Irish} analogous construction a \emph{converb}, although they are equivalent in almost every aspect (see \textcite{shisha-halevy.a:2010:celtic-converbs} for a description of converbs in the two languages).
	I must admit I do not understand this discrepancy, which may stem from misinterpretation of the questionnaire data (p.~452) or difference in \emph{conceptualisation} of the linguistic reality, and from not actual linguistic difference.
},
naming them \emph{converb-like}.
In my opinion it is best to define \emph{converb} according to its \emph{syntactic structural features}, i.e.\ its paradigmatic commutation and syntagmatic slotting as a linguistic sign.%, regardless of the surface homonymy of its nucleus with actual prepositions or the infinitival nature of its satellite.

Similarly to \emph{infinitive}, the term \emph{converb} has a clear \foreign{designatum} when compared to the alternatives%
\footnote{%
	These include: \emph{(adverbial-, conjunctive- \emph{or} \mbox{∅-)} participles}, \emph{progressives} (with respect to the said \C{yn~+ \Inf{}} converb), \emph{gerund} and \emph{gerundive}.
	The historical baggage of these term, which have been used in wholly different meanings by different scholars over the years, makes them confusing and potentially misleading.
} and invites general and typological consideration within a larger framework.
\index{converb|)}



\subsection{‘Genitive of respect’}%
\label{sec:intro:meta:terminology:gor}

While \C{infinitive} and \C{converb} make good terms in that they are both descriptively suitable and cross-linguistically meaningful, ‘genitive of respect’ (\Gor{}) is neither.
Nevertheless, due to the lack of better alternatives I chose to stick with the traditional name, which stems from classical terminology and was applied to Welsh by \textcite{morris-jones.j:1931:welsh-syntax}.%
Although the Welsh ‘genitive of respect’ construction was described by various scholars coming from different schools and using diverse frameworks, all seem either to avoid naming it or comment on terminology; see, among others, \textcites{mac-cana.p:1966:nominal-relative}[§~3.4.3]{shisha-halevy.a:1998:roberts}[§~5.6.7]{borsley.r+:2007:syntax}{mittendorf.i+:2008:np-gf}.

The construction denoted by this term consists of an adjective followed by an inalienably possessed noun, as demonstrated in \exfullref{ex:intro:meta:terminology:gor:cryf ei calon}, where \C{\gl{cryf}{strong} \gl{ei}{\eim{}} \gl{galon}{heart}}[strong of heart (=whose heart is strong)] adnominally refers back to \C{\gl{dyn}{man} (\gl{gwydn}{tough})}[a tough man]%
\footnote{%
	Affinities with strikingly similar constructions in Afroasiatic languages have been noted and discussed; see \textcites[§~3.1.III.e]{shisha-halevy.a:2003:celtic-egyptian}[§~3.4.3.2.Obs.4]{shisha-halevy.a:1998:roberts} for comparison with Egyptian\index[langs]{Egyptian} and Arabic\index[langs]{Arabic} and \textcite{al-sharifi.b+:2009:adjectival-construct} for comparison with Arabic\index[langs]{Arabic}.
	The Arabic\index[langs]{Arabic} term \transcribe{\Arabic{نعت سببي}}{naʿt sababī} is occasionally used as a general linguistic term for such constructions \parencite[§~14.15.1]{goldenberg.g:2012:semitic}.
}.

\preex{}\ex<ex:intro:meta:terminology:gor:cryf ei calon>
	\gloss
	{Yr oedd yn ddyn gwydn, \hlbegin{}cryf ei galon\hlend{}, \nogloss{\glafmt{}\ellipsis{}}}
	{\yr{} \oedd{} \ynC{} man(\M{}) tough strong \eim{} heart}
	{He was a tough man, strong of heart, \ellipsis{}}
	{\exsrcylw{8}{90}}
\xe\postex{}



\subsection{Conjunctive pronouns}%
\label{sec:intro:meta:terminology:conj}

{}\index{pronouns!conjunctive|(}%
Welsh has a rather complex personal pronoun system, involving several series of pronouns.
One of them is the conjunctive personal pronouns (\Conj{}; \C{\gl{rhagenw}{pronoun} \gl{personol}{personal} \gl{cysylltiol}{connective}} in \cite[§~4.129]{thomas.p:2006:gramadeg}).
As \textcite[§~159.iii]{morris-jones.j:1913:welsh-grammar} observes, pronouns of this series are ‘always set against a noun or pronoun that goes before (or is implied) \ellipsis{}. The series is in common use in M[oder]n W[elsh]; sometimes the added meaning is so subtle to be untranslatable’.
When glossed in English, which has a dissimilar pronoun system, they are usually given as
‘\Pro{} too’,
‘even \Pro{}’,
‘\Pro{} for \Pro{}.\Poss{} part’,
‘but \Pro{}’,
‘while \Pro{}’,
‘\Pro{} on the other hand’ or
‘\Pro{} on the contrary’ \parencite[see also][§~minnau, finnau]{gpc:arlein}.
Functionally, setting against an actual or implied (pro)noun means these pronouns signal a comparison (by either \emph{contrasting} or \emph{likening}, which at first glance might seem contradictory) and often indicate a change of topic \parencite[§~1.4.5]{borsley.r+:2007:syntax}.

The term \emph{conjunctive} stands here for their connective function.
Grammatical terminology has at least three unrelated uses for \emph{conjunctive}:
the \emph{conjunctive mood} (related or identical to \emph{subjunctive mood}, depending on terminology),
relating to a \emph{conjunction} (part of speech),
and \emph{conjunctive pronouns} in other senses%
\footnote{%
	Such as the conjunctive pronouns in the English terminology for Irish\index[langs]{Irish} grammar, where the term is to be understood in terms of micro-syntactic juncture and is differentiated from \emph{disjunctive pronouns}, appearing immediately after a verb or not, respectively.
}.
{}\index{pronouns!conjunctive|)}
{}\index{terminology|)}


\section{Technical notes}%
\label{sec:intro:meta:tech}

\subsection{Internal connectivity}%
\label{sec:intro:meta:tech:hyperref}

This \thesis{} is produced in a digital-first manner, meaning that while it can be fully read and understood in its hard copy form, the intended way to read it is in a digital PDF form.
Apart from slight typographic details, the main difference is the way internal connectivity is implemented.

% \begin{itemize}
	% \item
		In the digital format, references are hyperlinked.%
		\footnote{%
			The exact usage of hyperlinks differs among PDF viewer, but usually a single or double click on a hyperlink follows it.
			Returning to the previous position, before following the hyperlink, is done using a back button (←) or a keyboard shortcut, depending on the software used; please refer to the PDF viewer’s documentation.
			Keyboard shortcuts include:\\
			\keys{\ctrlwin + O} \hfill{}(Zathura)\\
			\keys{\Altwin + P} \hfill{}(Evince)\\
			\keys{\Altwin + \shift + \arrowkeyleft} \hfill{}(Okular)\\
			\keys{\Altmac + \arrowkeyleft}\quad{}/\quad{}\keys{\Altwin + \arrowkeyleft}\\\hfill{}(PDF.js, Sumatra, Adobe Reader).\\
		}
		This includes references to sections, examples, tables, figures, bibliography items (where the hyperlink is on the year of publication), etc.; text coloured \textcolor{darkscarlet}{dark scarlet} is hyperlinked.
		%
		In addition, an interactive machine-readable table of contents is included in the file.%
		\footnote{%
			Accessing it is inconsistent among PDF viewers:
			\keys{\tabwin} (Zathura);
			\keys{F9} and pressing the \emph{outline} button (Evince);
			\keys{F7} and pressing the \emph{contents} button (Okular);
			\keys{F4} and pressing the \emph{show document outline} button (PDF.js);
			\keys{F12} (Sumatra);
			\menu[,]{View,{Navigation Panels},Bookmarks} (Adobe Reader).
		}
		The page numbers in the human-readable table of contents (p.~\pageref{toc}~ff.) are hyperlinked to their respective destinations.

	% \item In print one has to manually turn pages back and forth.%
		% This is facilitated by two means:
		% \begin{itemize}
			% \item The appendices are bound as a separate volume, so the reader can open the two volumes independently (this can be imitated in the digital format by opening two reader windows side by side).

			% \item The printed copy is equipped with a bound bookmark as an aid in keeping track.
		% \end{itemize}
% \end{itemize}

Doctoral theses by their very nature are complex documents.
Communicating the findings in a comprehensible, readable and hopefully enjoyable way calls for the suitable presentation.
In addition to the interactive table of contents discussed above, four indices are included (from p.~\pageref{indices}): by subject (p.~\pageref{idx:subjects}), by languages (p.~\pageref{idx:langs}, mostly referred to for comparative purposes), by Welsh elements and constructions (p.~\pageref{idx:welsh}), and by location in the text (an \foreign{index locorum}, p.~\pageref{idx:locorum}).
A name or author index is rendered unnecessary by the use of backreferences in the references section (p.~\pageref{references}~ff.)



\weakFloatBarrier%
\subsection{Digital resources}%
\label{sec:intro:meta:tech:digital}

Some of the source materials are available online in digital text format for research purpose, as listed in \cref{tab:intro:meta:tech:digital:git}.

\begin{table}
	\caption{Source materials in digital text format}%
	\label{tab:intro:meta:tech:digital:git}%
	\begin{tabularx}{\myfullwidth}{lcX}
		\toprule
		Title & Translated? & URL\\
		\midrule
		\Atgofion{} & \circlegend{sronpalecyan} &
		\url{https://gitlab.com/rwmpelstilzchen/atgofion-i-raw/-/blob/master/01-Kate\%20Roberts.xml}\\
		\\
		\HD{} & \circlegend{sronpalegreen} &
		\url{https://gitlab.com/corpws-cymraeg/kate-roberts/haul-a-drycin}\\
		\\
		\GB{} & \circlegend{sronpaleyellow} &
		\url{https://gitlab.com/corpws-cymraeg/kate-roberts/o-gors-y-bryniau}\\
		\\
		\TG{} & \circlegend{sronpalegreen} &
		\url{https://gitlab.com/corpws-cymraeg/kate-roberts/te-yn-y-grug}\\
		\\
		\YC{} & \circlegend{sronpalecyan} &
		Original text with commentary \parencite{jones.d:2014:kr-drama}: \url{https://core.ac.uk/display/228918700}\newline%
		Only stage directions: \url{https://gitlab.com/rwmpelstilzchen/phdthesis/-/blob/master/tex/appendices/drama/cynddrws.tex}\\
		\\
		\YF{} & \circlegend{sronpaleblue} &
		Raw: \url{https://gitlab.com/corpws-cymraeg/kate-roberts/y-fam/-/blob/main/testun.xml}\newline%
		Annotated and translated:\newline\url{https://gitlab.com/rwmpelstilzchen/phdthesis/-/blob/master/tex/appendices/drama/fam.tex}\\
		\\
		\YLW{} & \circlegend{sronpalegreen} &
		\url{https://gitlab.com/rwmpelstilzchen/y-lon-wen-raw/}\\
		\bottomrule
	\end{tabularx}

	\explanationundertable{}%
	Translation status in the above links:
	\vspace{\onelineskip}

	\begin{tabular}{ll}
		\circlegend{sronpalegreen}  & Fully translated by a professional translator\\
		\circlegend{sronpaleyellow} & Partially translated by a professional translator\\
		\circlegend{sronpaleblue}   & Fully translated by me\\
		\circlegend{sronpalecyan}   & Partially translated by me\\
	\end{tabular}
\end{table}

A Git repository of this \thesis{}’ \LaTeX{} source files is available on:\\
\url{https://gitlab.com/rwmpelstilzchen/phdthesis}

The bibliography database is available on:\\
\url{https://gitlab.com/rwmpelstilzchen/bibliography.bib}

A precompiled PDF file is available on:\\
\url{https://ac.digitalwords.net/digital/phdthesis.pdf}
