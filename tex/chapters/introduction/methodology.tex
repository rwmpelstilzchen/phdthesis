\presectionopenepigraphhackwithlongopenbiling{}%
\section{Methodology}%
\label{sec:intro:method}%
{}\index{methodology|(}%

\subsection{Neutral, empirically-based description}%
\label{sec:intro:method:neutral}

The present study is descriptive.
The fundamental principle for a sound linguistic description, I believe, is that which \textcite[p.~228]{meillet.a:1948-1952:linguistique:1} concisely formulates as follows:
\begin{quote}\quotesize%
	\margintranslation{to organise the linguistic facts from the point of view of the language itself}{}%
	\foreign{ordonner les faits linguistiques au point de vue de la langue même}
\end{quote}
As \textcite[§1.2]{goldenberg.g:2012:semitic} writes, this ideal can be characterised as prejudice-free, non-aprioristic and empirically-based%
\footnote{%
	The raw data on which the present study is based~— i.e.\ its corpus~— is discussed below in \cref{sec:intro:object:corpus}.
},
meaning that the descriptive linguist has to approach the object of study without preconceptions about how it is structured, but derive the description from the empirical data itself.%
\footnote{%
	This stands in contrast to some framework-bound approaches~— either modern or traditional~— that place an array of aprioristic theoretical presuppositions and restrictions before approaching the data.
}
\Textcite{haspelmath.m:2010:framework-free} advocates a similar approach, which he calls \emph{framework-free grammatical theory}; see also \posscite{frajzyngier.z:2010:non-aprioristic} \emph{non-aprioristic syntactic theory}.

It should be stressed that ‘organising the linguistic facts from the point of view of the language itself’ does not mean abandoning all theoretical developments and scholarly knowledge that has been accumulated over the years in favour of a naïve, pre-scientific impressionistic approach.
On the contrary, it means acknowledging that the differences between languages (or better language varieties)~— each exhibiting its own categories and internal organisation%
\footnote{%
	\cref{sec:intro:text:diversity} above has discussed the issue of linguistic diversity and text linguistics.
}~— 
calls for an open-minded description of each on a neutral basis.
This acknowledgement does not imply one cannot draw insights from other languages or develop general and theoretical tools for linguistic inquiry, but it does mean one has to be cautious not to impose preconceived notions, either by making unjustified deductions from other languages or by describing a language through the lens of restrictive frameworks.

Laconically put, a language provides its own ‘instruction manual’ in itself, and it is the linguist’s descriptive task to decipher it.



\subsection{Structural linguistics}
\label{sec:intro:method:struct}

{}\index{structural linguistics|(}%
But what set of tools should the linguist employ for the said deciphering?
The approach the present study takes is \emph{structural linguistics}%
\footnote{%
	The intersection between structural linguistics and text linguistics has been discussed in \cref{sec:intro:text:langue-parole,sec:intro:text:types:struct}.
}.
In essence, structural linguistics can be described as the scientific endeavour to describe and understand languages (\foreign{langue}\index{langue@\foreign{langue}}, linguistic system) and texts%
\footnote{
	As \textcite{hjelmslev.l:1966:sprogteoriens-grundlaeggelse} puts it:
	\foreign{%
		De emner sprogteorien interesserer sig for er texter.
		Sprogteoriens formaal er at tilvejebringe en fremgangsmaade ved hjælp af hvilken en forelagt text kan erkendes gennem en modsigelsesfri og udtømmende beskrivelse.
	}
	‘The objects of interest to linguistic theory are texts.
	The aim of linguistic theory is to provide a procedural method by means of which a given text can be comprehended through a self-consistent and exhaustive description.’ \parencite[p.~16]{hjelmslev.l:1969:prolegomena}.
}
(\foreign{parole}\index{parole@\foreign{parole}}, linguistic output) by the means of uncovering the systematic relations of interdependent (i.e.\ differentially, relationally and negatively defined) signs (\foreign{signe}\index{linguistic sign}) and describing their value (\foreign{valeur}\index{valeur@\foreign{valeur}}) or function, thus linking signifiers (\foreign{signifiant}\index{signifiant@\foreign{signifiant}}) with signifieds (\foreign{signifié}\index{signifié@\foreign{signifié}}) and revealing the systemic link binding form and function.
A structural linguistic description is fundamentally system-oriented (that is, aims at describing a system and its internal structure) rather than feature-oriented (that is, aims at describing features \foreign{in vacuo}).

Originated from the work of Ferdinand de Saussure \parencite{saussure.f:1995:course,saussure.f:2011:course-baskin} structural linguistics was pivotal in \nth{20}-century thinking as a part of Structuralism\index{Structuralism}.
It has been developed since by linguists from different schools~— such as the Geneva, Prague and Copenhagen schools%
\footnote{%
	To which one might add the Jerusalem school \parencite{rosen.h:2005:jerusalem,shisha-halevy.a:2006:polotsky}, which has been occupied for some decades now with developing structural~— often text-linguistic~— descriptions of particular languages (as opposed to purely theoretical developments), mostly Indo-European
	and Afro-Asiatic.
	From these descriptive studies a set of theoretical and generally-applicable developments has been made.
	%(English and Irish English, German, Greek, Irish, Lithuanian, Polish, Welsh and Yiddish)
	%(Classical and Spoken Arabic, Neo-Aramaic, Old Babylonian and Egyptian)
	%Burmese
}
~— and has proved apt in inquiries of diverse languages worldwide.
According to some views (such as \textcite[§I.2]{weinrich.h:1977:tempus}; see \textcite{harweg.r:2000:struktext} for historical context), text linguistics can be seen in a way as a further development of structural linguistics, expanding its scope from the sentence to the text as a whole.%
% \footnote{%
	% As \textcite[§I.2]{weinrich.h:1977:tempus} puts it:
	% \foreign{‘Die Textlinguistik ist eine Weiterentwicklung der strukturalen Sprachwissenschaft’}
	% (‘Text linguistics is a development of structural linguistics’).
	% For historical context, see \textcite{harweg.r:2000:struktext}.
% }.

{}\index{structural linguistics|)}



\presectionopenepigraphhack{}%
\subsection{Corpus}%
\label{sec:intro:method:ubl-corpus}

{}\index{corpus|(}%
\openbilingepigraph
{%
	In specialibus generalia quaerimus.
}
{%
	We seek the generalities in the specifics.
}
{}
In \cref{sec:intro:method:neutral} above an empirical approach has been advocated.
An immediate corollary from this is the need to define empirical data and base the inquiry on it.
The approach taken here is to study data that consists of actual language usage in a naturally occurring, neutral, non-elicited, research-independent environment, ‘in the wild’.
From that pre-existing observable concrete \foreign{parole} generalisations about the nonobservable \foreign{langue} can be inferred (\cref{sec:intro:text:langue-parole}).
Among the advantages of this approach are:

\begin{plainlist}
	\item \emph{Bypassing the Observer’s Paradox}\index{Observer’s Paradox}%
		\footnote{%
			See \textcite[§~8.1, p.~207~ff.]{labov.w:1985:sociolinguistic-patterns}; see also \textcite{angrosino.m.v:2004:observer-bias}.
		},
		meaning that this way one can systematically observe how people use language without any external interference caused by the act of observation.

	\item \emph{Refutability}%
		\footnote{%
			In the Popperian sense; see \textcite{popper.k:2002:conjectures-refutations}.
		}.
		As the raw data is already given and is research-independent, analyses of that data can be tested and falsified by others.
		Extensive annotation\index{annotation} (see below) makes assessing the proposed analyses more readily accessible.

	\item \emph{Unrestricted use of language}.
		Language is complex, and the scope of the parameters pertinent to a studied phenomenon varies in size.
		Thus, studying complete texts without external research-induced restrictions enables one to observe language use in its fullest without external limitations.
\end{plainlist}

The present study is synchronic%
\footnote{%
	While most text-linguistic studies are synchronic, not all are by necessity.
	\textcite{gorlach.m:2004:text-history}, for instance, is concerned with diachronic and comparative aspects of text-types.
}
and focusses on \rev{2}{the language of a single author}: the studied corpus consists of literary works by Kate Roberts (\cref{sec:intro:object:corpus}), a Welsh-language author who wrote for other native speakers and made a masterful use of the language.
By virtue of confining ourselves to the language of \rev{2}{a single speaker} we gain consistency in our data.
One might argue, though, that we lose some breadth and applicability.
This is a question of resolution and an unavoidable trade-off%
\footnote{%
	See \textcite[p.~192~ff.]{labov.w:1985:sociolinguistic-patterns} for discussion.
}.
By relying on a large sample of \rev{2}{speakers} and aiming at describing ‘Welsh’ as an all-encompassing term one can draw conclusions with broader validity but on the down side these conclusions must sacrifice ‘sharpness’ (if we draw a metaphor from photography) due to variation between speakers, resulting in non-uniform data.
However~— pursuing this metaphor~— if we choose to ‘zoom in’ on \rev{2}{the language of a single speaker} we gain a sharper image at the expanse of field of view.
Each approach has its inherent limitations and advantages.
In any case, in-depth descriptions of the language of individual speakers and authors \emph{are} valuable to understanding the language of the larger speech community, as these individuals do not live \foreign{in vacuo} but share the greater part of their linguistic features with their community%
\footnote{%
	\label{fn:sec:intro:method:ubl-corpus:speech community}%
	Cf.\ \posscite{hjelmslev.l:1942:langue-parole} notions of \foreign{schéma}, \foreign{norme} and \foreign{usage}.
	See also Coșeriu’s triad \foreign{langue}/\foreign{sistema} (language system), \foreign{norme}/\foreign{norma} (traditions of discourse) and \foreign{parole}/\foreign{habla} (speech itself) and \posscite[ch.~I]{barthes.r:1968:elements} discussion.
}.
Wider applicability of findings, then, can be tested by comparing several ‘zoomed-in’ or ‘high-resolution’ descriptions.%
\footnote{%
	From my own experience of reading Literary Welsh text, a large portion of the findings described here are widely applicable (with due adaptations), but certain specific features that pertain to the particular linguistic signature of Kate Roberts do exist.
}

In the case of the present \thesis{}, three levels of abstraction or processing%
\footnote{%
	These are resemblant but not identical to \posscite[§~4.1]{wallis.s+:2001:knowledge-discovery} notion of \emph{3A perspective}: \emph{Annotation—Abstraction—Analysis}.
}
are available, from \emph{raw material}, through \emph{annotation}\index{annotation} to \emph{analysis}:

\begin{plainlist}
	\item \emph{Raw material.}
		Digital editions of the whole corpus are available online; see \cref{sec:intro:meta:tech:digital}.

	\item \emph{Annotation.}\index{annotation}
		An catalog of annotated examples found in the corpus is available for each of the chapters.%
		\footnote{%
			\Cref{sec:anecdotes}: \Cref{app:anecdotes} is dedicated to structural annotation of anecdotes.

			\Cref{sec:rd}: Two conversations are annotated and commented upon in \cref{app:rd:zero}.
				The description proposed in this chapter distinguishes three types of \glossaryterm{quotative indexes}, as explained there: QI1, QI2 and QI3.
				All of the examples of QI2 and QI3 in the corpus are annotated in the chapter itself (\cref{sec:rd:patterns:2,sec:rd:patterns:3}).
				In addition, the all of the modification components (of QI1, QI2 and QI3) are categorised in \cref{sec:rd:internal:mod}

			\Cref{sec:drama}: All stage directions in the corpus are colour-coded according to their syntactic form in \cref{app:drama}.
		}
		
	\item \emph{Analysis.}
		The analysis makes the main part of the chapters of the \thesis{} \emph{per se}.
		The primary goal of the analysis is to provide systematic generalisations, but as these are grounded on particular examples the latter are often discussed (especially if they pose a complication or are interesting and relevant in another way).
		Even when not discussed explicitly, references to examples are often provided in tabular or other form.
\end{plainlist}

{}\index{corpus|)}


\subsection{Methodological issues of procedure}%
\label{sec:intro:method:procedure}

\subsubsection{External and internal definitions of linguistic units}%
\label{sec:intro:method:procedure:ext-int}

Upon attempting to define any complex linguistic unit, one faces a problem: in order to define the unit in question, one has to define its components, but the definition of components is dependent on the definition of the unit itself, resulting in cyclic definitions%
\footnote{%
	The mathematical or computer science equivalent of this would be \emph{mutual recursion}: $A$ calls $B$, while $B$ calls $A$.
}.
This problem is discussed in \textcites[§~10]{hjelmslev.l:1966:sprogteoriens-grundlaeggelse}[§~10]{hjelmslev.l:1969:prolegomena}, with the proposed solution of creating a set of interdependent~— yet not cyclic~— definitions, whereby each unit is defined twice:
\begin{plainlist}
	\item \emph{External definition:} first, as a component of a class within a larger environment.
	\item \emph{Internal definition:} secondly, as a class which itself consists of components or smaller units.
\end{plainlist}
In practice the \emph{entire text} makes the ultimate unit of analysis.

This method can be applied to micro-syntactic linguistic units%
\footnote{%
	See \textcite{barri.n:1978:substantivized-adjectivized} for a concise implementation of this theoretical idea with regard to substantivised adjectives and adjectivised substantives in Hebrew\index[langs]{Hebrew!Modern}.
}
as well as to macro-syntactic ones.
Looking back at the discussion regarding text-types and structural linguistics (\cref{sec:intro:text:types:struct}), the definition of sub-textual units (or \emph{textemes} in the etic sense) within the surrounding text (\cref{sec:intro:text:types:struct:ext}) is \emph{external} and the definition of the components making these sub-textual units (\cref{sec:intro:text:types:struct:int}) is \emph{internal}.



\subsubsection{Iterative process of defining text-types}%
\label{sec:intro:method:procedure:iterative}

As discussed in \cref{sec:intro:text:text-types}, in defining a text-type (as opposed to the extralinguistic notion of \emph{genre}) one should rely on text-internal characteristics alone.
In order to make a text-linguistic definition of a certain text-type one has to delineate the features that make that text-type a distinctive and consistent subsystem of grammar.

The \emph{results} of the systematic observation of linguistic features are presented here, but the \emph{process} of defining and discovering a text-type and its internal structure is not done in a single step but in numerous \emph{iterative} steps, each refining the definition on the basis of the data given in the corpus, as each iteration builds on the previous ones and enables the linguist to better understand and describe the systems in question.


\subsubsection{Neutralisation and selectability}%
\label{sec:intro:method:procedure:selectability}

Another procedural principle is that which \textcite{rosen.h:2005:jerusalem} formulates as follows:

\begin{quote}\quotesize%
	[Isolate] the cases of neutralization\index{neutralisation} and archi-elements first, in order to be able to effectively examine the functions of the forms under scrutiny in conditions of genuine opposition and selectability.
\end{quote}

The application of this principle is quite straightforward: in order to sift out environments in which structural opposition does occur and linguistic elements do have a structural value, one has first to identify neutralising\index{neutralisation}%
\footnote{%
	See \textcite{barri.n:1979:neutralisation} for a structural linguistic critique of the notion of \emph{neutralisation}\index{neutralisation}.
}
environments in which such opposition does not occur and linguistic elements have no structural value.
Once the neutralising environments are taken out, one can focus on describing the opposition between the signs in the environments which remain, delineating the systematic relationship between signifiers (forms) and signifieds (functions).



\presectionopenepigraphhackwithlongopenbiling{}%
\subsection{Summary}
\label{sec:intro:method:summary}
\openbilingepigraph%
{%
	\selectlanguage{welsh}%
	Byddaf yn hoffi medru dehongli cystrawen yn fathemategol hollol.
}
{%
	I would like to be able to interpret syntax in a completely mathematical manner.\\
	{\color{darkgray!50!black}\textsmaller{\upshape{}(If I understand correctly, by \C{yn fathemategol}[\emph{(lit.)} mathematically] she means ‘in a systematic, methodical manner’. J.~R.)}}
}%
{\cite[\\\hfill{}KR at SL, 2 Chwefror 1933; no.~75]{lewis.s+:1992:annwyl}}%

\Textcite[§~1.5]{de-fina.a+:2012:analyzing-narrative} list five research parameters for narrative analysis.
Adapting their focus on narrative to a more general textual purpose, the present study can be described as follows:

\begin{plainlist}
	\item \emph{Object of analysis}.
		The objects of analysis are the texts themselves and their language; not events related in them, identities or social phenomena nor the act of writing as a communicative, interactional process.

	\item \emph{General methodological approach}.
		Qualitative, with quantitative elements. Not experimental.

	\item \emph{Methods of data collection}.
		Based on natural non-elicited, research-independent (not experimental) corpus.

	\item \emph{Types of data}.
		Written; not oral\slash{}interactional nor multimodal.

	\item \emph{Data analysis}.
		Focus on language, as opposed to content, themes, interactional processes or social practices.
\end{plainlist}

% \orientation{}%
Concluding the key points so far, the present study aims at providing a
structural,
qualitative,
empirical and corpus-based
text-linguistic (macro-syntactic) analysis of
Literary Modern Welsh,
with focus on certain text-types and sub-textual composition,
striving to organise and describe the linguistic facts from the point of view of the language itself.

{}\index{methodology|)}%
