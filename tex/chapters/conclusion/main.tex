\chapter{Conclusion}%
\label{sec:conclusion}

\section{Three topics and three common threads}%
\label{sec:conclusion:three}

\begin{marginparfigure}%
	{\worktitle{The Blind Men and the Elephant} (\Japanese{群盲撫象}, 1888), an \foreign{ukiyo-e} (\Japanese{浮世絵}) print by Hanabusa Ittyou (\Japanese{英一蝶})}%
	{\worktitle{The Blind Men and the Elephant} (\Japanese{群盲撫象}, 1888), an \foreign{ukiyo-e} (\Japanese{浮世絵}) print by Hanabusa Ittyou (\Japanese{英一蝶})}%
	{fig:conclusion:hanabusa}%
	\includegraphics[width={\marginparwidth}]{chapters/conclusion/hanabusa.jpg}\\
	Source: Wikimedia Commons (\url{https://w.wiki/52Vm}).
\end{marginparfigure}%
An old parable%
\footnote{%
	One of the earliest version which came down to us in a fully-fledged form is from the Pāli Canon of Theravada Buddhist scriptures (\foreign{Tittha sutta}, \foreign{Udāna} 6.4, \foreign{Khuddakanikāya}, \foreign{Suttapiṭaka}).
	The parable probably pre-dates that Buddhist version; see \textcites{zlotnick.d.m:2001:buddha-parable}[9]{ireland.j.d:2007:udana-itivuttaka} for discussions of its development, suggesting a Jainist origin.
}
tells of blind men%
\footnote{%
	Or sighted men who being blindfolded, in a dark room or on a dark night, depending on the version.
}
who have never encountered an elephant.
They are presented with an elephant, and each touches a different part of the animal.
Then, they are asked by the king of that region to describe what an elephant is (from the \foreign{Tittha sutta}, \cite{sujato.bh:2021:heartfelt-sayings}):

\bgroup
\footnotesize
\egaliterianbilingnonwelsh{%
	‘Vadetha, jaccandhā, kīdiso hatthī’ti?
}
{%
	‘Then tell us, what kind of thing is an elephant?’
}
\egaliterianbilingnonwelsh{%
	Yehi, bhikkhave, jaccandhehi hatthissa sīsaṁ diṭṭhaṁ ahosi, te evamāhaṁsu:
	‘ediso, deva, hatthī seyyathāpi kumbho’ti.
}
{%
	The blind people who had been shown the elephant’s head said,
	‘Your Majesty, an elephant is like a pot.’
}
\egaliterianbilingnonwelsh{%
	Yehi, bhikkhave, jaccandhehi hatthissa kaṇṇo diṭṭho ahosi, te evamāhaṁsu:
	‘ediso, deva, hatthī seyyathāpi suppo’ti.
}
{%
	Those who had been shown the ear said, ‘An elephant is like a winnowing fan.’
}
\egaliterianbilingnonwelsh{%
	Yehi, bhikkhave, jaccandhehi hatthissa danto diṭṭho ahosi, te evamāhaṁsu:
	‘ediso, deva, hatthī seyyathāpi khīlo’ti.
}
{%
	Those who had been shown the tusk said,
	‘An elephant is like a ploughshare.’
}
\egroup

And so on: each of the blind \revsimple{2}{men} perceives an incomplete view of the elephant, and they cannot reach to an agreement on the nature of the elephant as an entity.
This parable, which since its inception circulated far and wide beyond the Indian subcontinent and its traditions, has been adapted and adopted in various contexts, including philosophy, science and education.

The topic this \thesis{} set out to contribute to its study~— namely, the linguistic means of text construction and organisation (in Literary Welsh)~— is not unlike an elephant, which we cannot comprehend as one whole but in parts: as linguists we have only an \emph{indirect} access to the abstract linguistic system (\foreign{langue}; \cref{sec:intro:text:langue-parole}) and language is vast and complex.
Each of the partial linguistic examinations, despite being incomplete on its own, sheds light on the whole from a different direction.

Three case studies have been chosen, each making one part of the proverbial elephant:
\emph{anecdotes} (\cref{sec:anecdotes}),
\emph{reporting of speech in narrative} (\cref{sec:rd}) and
\emph{stage directions} (\cref{sec:drama}).
Each of these is interesting as an object of study and deserves description on its own right, but the combined study of them results in a better understanding of the whole, which is greater than the sum of its parts.
\begin{table}
	\caption{An overview of the common themes shared by the three topics of the \thesis{}}%
	\label{tab:conclusion:themes}%
	\footnotesize%
	\begin{tabularx}{\myfullwidth}{>{\hsize=.55\hsize\linewidth=\hsize}XXXXX}
		\toprule
		Topic & Structural regularities & Interrelation and interconnectivity & Narrativity\\
		\midrule
		Anecdotes &
		five \revsimple{2}{sections} and two anchors &
		inherently embedded within a broader text &
		anecdotes are narratives
		\\\\
		Reporting of speech in narrative &
		internal structure and QI-types &
		three QI-types as three types of textual interconnectivity &
		bridge between narrative and dialogue
		\\\\
		Stage directions &
		syntactic forms &
		intermixed with the dramatic text &
		the narrative spine of the play
		\\
		\bottomrule
	\end{tabularx}
\end{table}
Three general themes thread through the three chapters, as outlined schematically and laconically in \cref{tab:conclusion:themes} and depicted in more detail below.

One theme is \emph{the linguistic expression of structural regularities of textual functions}.
Each of the textual units described in the three topics exhibits regular text-linguistic structure.
Anecdotes have a recurring structure (\cref{sec:anecdotes:structure}), consisting of five ordered segments (\Cabstract{}, \Cexposition{}, \Cdevelopment{}, \Cepilogue{} and \Cconclusion{}) and two smaller-scale phrases which are contained within any of the first three sections (\Cintanch{} and \Ctempanch{}), all of which are linguistically characterisable.
Quotative indexes also show in internal structure (\cref{sec:rd:internal}), consisting of several components whose identity and arrangement are crucial for their function in the text.
Stage directions (\cref{sec:drama:pre,sec:drama:sd:syntax}) have a number of micro-syntactic forms which correspond with textual functions within the plays.

The second theme is \emph{the linguistic expression of the interrelation and interconnectivity of textual units or components}.
None of the units treated in this \thesis{} operates \foreign{in vacuo}; all are intertwined within other components of a broader text.
As mentioned in \cref{sec:rd:intro:background}, texts are~— in etymology and in actuality~— made of constituents which are woven together into a complex fabric.
Anecdotes are by their very nature embedded within the broader text, in which they commonly function as an illustrative device (\cref{sec:anecdotes:embedding}).
Quotative indexes in narrative are situated at the meeting point of the narrative portions \foreign{per se} and the dialogue portions of the text, and make the \emph{‘connective tissue’} between them.
The three QI-types (\cref{sec:rd:patterns}) mark three types of interconnectivity between the textual components they connect.
Stage directions occur intermixed with the dramatic text, in one of three distinct textual environments (\cref{sec:drama:sd}), or set apart in the setting section of the introductory specifications (\cref{sec:drama:pre}).

The third theme is \emph{the linguistic expression of the multifaceted nature of narrative}.
Narrative is a complex phenomenon that is central to many expressions of language, including those around which this \thesis{} revolves.
Anecdotes are an intriguing kind of narrative, owing to the fact that many of them are rather basic (embryonic even, at times), which invites examination of narrative under ‘controlled conditions’.
The quotative indexes of \cref{sec:rd} bridge between narrative and dialogue and seam them together into a cohesive textual whole.
Stage directions fill multiple functions (or \emph{modes}), one of which is to depict the narrative spine of the events which unfold on stage.



\section{Data and theory}%
\label{sec:conclusion:data-theory}

The approach of this \thesis{} is empirical and corpus-based (\cref{sec:intro:method:ubl-corpus}).
\rev{2}{%
	While linguistic variability between speakers is a fascinating topic in its own right, limiting a study to a single speaker offers a clearer, simpler and more consistent system to describe.
	Thus, in order to cancel the effects of linguistic variability between speakers, the data consists of writings of one author%
	\footnote{%
		With the exception of the co-author of \YF{} (see \cref{sec:drama:intro:corpus}).
	},%
}
Kate Roberts, whose \foreign{œuvre} includes a variety of forms, genres and media.
The range of works taken as data spans over her whole career, from her earliest published work (1920) to the last one (1981).
For each topic two works have been chosen: two memoirs, two collections of short stories and two plays, respectively.
This has the advantage of being able to obtain more precise and more strongly valid findings through more refined analysis, in comparison to one textual specimen of each topic.

On the one hand one cannot formulate and sustain any general description without sufficient concrete data in which it is grounded, but on the other hand any particular piece of data cannot be fully comprehended without a general description in light of which it is understood as a part of a greater picture.
In order to cater for both ends of this seeming ‘paradox’, the \thesis{} zooms in and out, discussing particular examples where it is beneficial but always with the intention of seeing the forest for the trees.



\section{The aim fulfilled in the \thesis{}}%
\label{sec:conclusion:aim}

If I were to try to encapsulate the core of the whole \thesis{} in one long sentence, it could be something like this:
a study which
	describes the linguistic means of text construction and organisation in Literary Modern Welsh,
	as manifested in \rev{2}{the language of one author},
	through the lenses of structural and text linguistics,
	by exploring the linguistic expression of
		structural regularities of textual functions,
		the interrelations and interconnectivity of textual units or components and
		the multifaceted nature of narrative,
	focussing on the case studies of
		anecdotes in autobiographical texts,
		reporting of speech in short stories and
		stage directions in plays.
These have not been studied in Welsh before, and elegant text-linguistic systems emerge, systems which reflect the use of the language by an author who utilised the artistic potential of Welsh to the fullest.


\section{Further research}%
\label{sec:conclusion:further}

It is the nature of science that every new finding or answer opens a dozen of new questions.
Given that the field of text linguistics in Welsh is underresearched, much is yet to be studied.
Of the plethora of potential avenues for further research, I would like to focus on two, which share the common idea of \emph{application}.



\subsection{Other authors}%
\label{sec:conclusion:further:idiolects}

One avenue is the question of the applicability of the findings described here for similar writings by other authors.
The present study limits itself to one author on purpose.
\rev{2}{This} allows it to be \emph{‘fine-grained’} enough to describe the system in a manner that is both adequately minute and founded.
When a study \rev{2}{indiscriminately} mixes the linguistic output of multiple individuals it might yield more general \rev{2}{conclusions, but at the expanse of possible loss of details}.
This is a question of resolution~— a lower resolution allows covering the linguistic norm of a larger part of the speech community, while concentrating on one or a handful of \rev{2}{speakers} allows a higher resolution.
Both ends of the spectrum and any point in between are valid and complement the others.%
\footnote{%
	See \cref{fn:sec:intro:method:ubl-corpus:speech community} on p.~\pageref{fn:sec:intro:method:ubl-corpus:speech community} regarding theory by Hjelmslev, Coșeriu and Barthes.
}
It seems interesting to determine how much of what is described here is unique to Kate Roberts, and how much is shared with other contemporary authors.
The only way to ascertain this is to conduct similar ‘high-resolution’ studies on the basis of corpora by other authors and compare the results.%
\footnote{%
	\rev{2}{This way, of conducting studies on several authors and comparing the results, may result in a whole that is greater than the sum of its parts, as the differences and similarities that are bound to emerge from the comparison are a worthwhile object of research for its own sake.}
}
A cursory examination and my own acquaintance with Welsh language and literature suggest that much, but definitely not everything, is shared.

\begin{plainlist}
	\item \emph{\Cref{sec:anecdotes}}.
		As discussed in \cref{sec:anecdotes:structure:labov:comparison}, our anecdotes share some key features with the Labovian model, which has been applied with varying degrees of compatibility to various narratives from different languages and cultures.
		It is not implausible that Welsh anecdotes in similar kinds of memoirs will display similar text-linguistic behaviour.%
		\footnote{%
			Fortunately, numerous such writings have been published.
			The category \C{Atgofion a Hunangofiannau}[Reminiscences and autobiographies] on \texttt{gwales.com}, a website operated by \C{Cyngor Llyfrau Cymru}[Books Council of Wales], contains no less than 467 Welsh language titles (as of August 2022).
			For a speech community the size of Welsh (\cref{sec:intro:object:modern_literary_welsh:background}) this seems to me an impressive number.
		}
		Not all autobiographical writings are of the same nature; for example \C{Pigau’r Sêr} \parencite{williams.j.g:1969:pigau} is markedly different from \YLW{} and \Atgofion{}, as it reads more like a first-person novel.

	\item \emph{\Cref{sec:rd}}.
		As mentioned in \Cref{sec:rd:internal:nucl:generic:meddai+ebe:two}, other works%
		\footnote{%
			\Textcite{roberts.k:1932:gors-bryniau,llywelyn.r:1997:seren,rowling.j:2003:harri_maen,dafydd.ff:2009:y-llyfrgell} are referred to there.
		}
		behave differently than our corpus, at least with respect to the use of \C{\gl{meddai}{\meddai{}}} nad \C{\gl{ebe}{\ebe{}}}.
		How much is shared between the system described in \cref{sec:rd} and other literary works is yet to be determined by a comparative description.
		Some of the features are without doubt common to Literary Welsh in general, but some may characterise certain preferences, norms and traditions.
		Trying to generalise over Literary Welsh as an abstract entity cannot avoid loss of details which result in a more blurry image.

	\item \emph{\Cref{sec:drama}}.
		Even though I have not conducted a thorough examination, on the whole the use of stage directions in the studied corpus seem to conform with other contemporary Welsh-language plays I have looked at.
		This might stem from contemporary conventions of how plays are written.
\end{plainlist}



\subsection{Other text-types}%
\label{sec:conclusion:further:text-types}

This \thesis{} focusses on three text-types: anecdotes, short story narratives (reporting of speech therein), and plays (stage directions therein).
These were chosen for their broader theoretical implications and structural interest.
Nevertheless, they are only three of many other text-types which merit description.
Four others caught my attention as well:

\begin{plainlist}
	\item \emph{Diary-like narrative writing}.
		Roberts explored this literary form of fiction in several works%
		\footnote{%
			\C{Stryd y Glep}[Gossip Street] \parencite{roberts.k:1949:stryd}, subtitled \C{stori hir fer ar ffurf dyddiadur}[a short long story in the form of a diary], and two different shorter pieces named \C{Gwacter}[Emptiness]:
			one in the collection \C{Gobaith a Storïau Eraill}[\worktitle{Hope} and Other Stories] \parencite{roberts.k:2001:gobaith} and
			one in the collection \C{Haul a Drycin}[Sun and Storm] \parencite{roberts.k:1981:haul-drycin}.
		},
		which exhibit unusual linguistic features (including but not limited to their use of infinitives and \C{[\ProNP{} \ynD{} \Inf{}]}\index[welsh]{[ProNP yn INF]@[\ProNP{} \ynD{} \Inf{}]}).

	\item \emph{Memory pictures}.
		The first chapter of \YLW{} (\C{\gl{Darluniau}{picture.\Pl{}}}[Pictures]) consists of twenty-two ‘memory pictures’ from the author’s reminiscences of her childhood and adolescence.
		Here the use of language is notable as well, and some aspects are reminiscent of the \foreign{tableaux vivants} which occur within Roberts’s short stories and novels.

	\item \emph{Description of children’s games}.
		The sixth chapter of \YLW{} (\C{\gl{Chwaeron}{game.\Pl{}} \gl{Plant}{child.\Col{}}}[Children’s Games]) is dedicated to a semi-anthropological or -folkloristic description of games from the author’s childhood.
		Although they have some superficial affinities with narratives~— chiefly chronological concatenation~— these descriptions differ from narratives in a number of aspects (verbal forms%
		\footnote{%
			For example, the extensive use of the impersonal.
		},
		syntactic structures, etc.).
		Some of these games described by Roberts are also described in the booklet \C{\gl{Teganau}{toy.\Pl{}} \gl{gwerin}{folk} \gl{plant}{child.\Col{}} \gl{Cymru}{Wales}}[Folk toys of Welsh children] \parencite{jones.t.v:1987:teganau-gwerin}, a fact which invites a comparative examination.

	\item \emph{Cookbooks}.
		In \cref{sec:intro:text:sub-textual} cookbooks and recipes have been mentioned in the context of sub-textual composition.
		These might be interesting to describe linguistically, because their form is usually rather strict and conventionalised.
		How much is set, how much is free, and what are the structural linguistic implications of that?%
		\footnote{%
			See \textcite[ch.~1]{jespersen.o:1924:philosophy} regarding the tension between formulas and free expression.
		}
		Another question tackles information status: if the ingredients appear before the instructions in a special section, what implications does that have on the given-new organisation of information?
\end{plainlist}

An analytic approach, tools and framework similar to those which have been used here can be applied to these and other text-types.
Apart from some charted islands, Welsh text linguistics is for the most part a \foreign{terra incognita} waiting to be explored; I hope I did manage to make it somewhat \foreign{cognitior} (\cref{sec:intro:object:modern_literary_welsh:research}).

On this note of looking forward into the unknown, I would like to conclude by quoting the very last words of \YLW{}:

% \begin{quote}\quotesize%
	% \margintranslation{Tomorrow will come, and I can go on asking questions.}{}%
	% \C{Fe ddaw yfory eto, a chaf ddal i ofyn cwestiynau.}
% \end{quote}

% \egaliterianbiling{%
	% \C{Fe ddaw yfory eto, a chaf ddal i ofyn cwestiynau.}
% }
% {%
	% Tomorrow will come, and I can go on asking questions.
% }

\vspace{0.5\onelineskip}

\bgroup
\quotesize{}
\noindent%
\begin{minipage}[b]{\textwidth}%
	\begin{minipage}[t]{0.5\textwidth-1em}%
		\C{Fe ddaw yfory eto, a chaf ddal i ofyn cwestiynau.}
	\end{minipage}%
	\hfill%
	\begin{minipage}[t]{0.5\textwidth-1em}%
		Tomorrow will come, and I can go on asking questions.
	\end{minipage}
\end{minipage}

\vfill

\begin{center}
	{\HUGE\fontspec{JuniusX}\inkB }
\end{center}

\vfill

~
