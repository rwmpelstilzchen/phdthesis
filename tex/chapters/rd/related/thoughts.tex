\subsection{Reported thoughts}%
\label{sec:rd:thoughts}

\openbilingepigraph%
{%
	\selectlanguage{welsh}%
	Unwaith neu ddwy roedd hi wedi edrych dros ysgwydd ei chwaer ar y llyfr a ddarllenai, ond doedd ynddo ddim darluniau na sgwrsio, ‘a pha werth,’ meddyliodd Alys, ‘ydi llyfr heb ddarluniau na sgwrsio?’
}
{%
	\ellipsis{} once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it, ‘and what is the use of a book,’ thought Alice, ‘without pictures or conversation?’
}
{\worktitle{Anturiaethau Alys yng Ngwlad Hud}, ch.~4 \parencite{carroll.l:2010:anturiaethau-alys}}%
\index{free indirect speech|(}%
The linguistic representation of speech and thought exhibits some similarities, especially in syntax.
Due to these similarities they are often grouped together in linguistic treatments, under the umbrella term \emph{reported discourse}\index{reported discourse}; see \cref{sec:rd:intro:background:terminology}, as well as \textcite[§~1.2.1]{guldemann.t:2008:quotative-indexes} and many of the publication referred to in \textcite{guldemann.t+:2002:bibliography-reported}.
As heavily discussed in this chapter, reporting of speech has many unique features; most of its particularities are not shared with the representation of thoughts in the text.



\subsubsection{Free indirect speech}%
\label{sec:rd:thoughts:fis}

One special kind of thought representation which shows very little semblance to reported speech is the \emph{free indirect speech} \frde{discours indirect libre}{erlebte Rede}.
This important literary-linguistic technique has been exhaustively treated in the scholarly literature; some key publications, which are especially relevant to our linguistic discussion are \textcites{banfield.a:1982:unspeakable}{banfield.a:1973:narrative-speech}[§~7.3]{fleischman.s:1990:tense}{fludernik.m:1993:fiction}%
\footnote{%
	See also \textcite{pascal.r:1977:dual-voice}, whose focus is more on stylistics and literary criticism.
}.
Free indirect speech is heavily employed in Roberts’s works, which explores the inner world of the characters, who often face social and personal hardship.
Some works, like \SG{} \parencite{roberts.k:1949:stryd} and two different pieces titled \C{\gl{Gwacter}{emptiness}} \parencites[ch.~4]{roberts.k:1981:haul-drycin}{roberts.k:2001:gobaith}, are centred around the thoughts of the protagonist, being fictional diaries, but most of her common narrative writing still offers ample glimpses to the thoughts of the focalising\index{focalisation (narratology)} protagonists, mainly though free indirect speech.
In our corpus, Begw is the focalising protagonist of \TG{} and Winni is the one of \HD{}: the stories are told through their eyes.

For clarity, let us examine \exfullref{ex:rd:thoughts:fis.fis} as an example of free indirect speech: after Begw finds out in the story it was her father who killed Sgiatan the cat, the report of her thoughts understanding this is not presented in the explicit manner of \exfullref{ex:rd:thoughts:fis.de-fis} (my own rephrasing of \exfullref{ex:rd:thoughts:fis.fis}) but without any framing (\exfullref{ex:rd:thoughts:fis.fis}): the bare thought \C{\gl{Felly}{\felly{}} \gl{ei}{\eif{}} \gl{thad}{father} \gl{a}{\aRel{}} \gl{wnaeth}{do.\odd{}}}[So it was her father had done it] is plainly presented in the text without any subordination of quoting frame.
Take note of the use of use of the discourse marker \C{\gl{felly}{\felly{}}}[so] and the person marking: free indirect speech combines elements which belong to the discursive sphere (\C{felly} in our case) with elements which belong to the narrative sphere (third-person reference in our case, as opposed to first person in the rephrasing, \exfullref{ex:rd:thoughts:fis.de-fis}).
Forms similar to \exfullref{ex:rd:thoughts:fis.de-fis} are also found in the corpus, as exemplified in \exfullref{ex:rd:thoughts:non-fis} and discussed below.

\preex{}\pex<ex:rd:thoughts:fis>
	\a<fis>
		\gloss
		{%
			{} @ \nogloss{\hlbegin{}} @ Felly ei thad a wnaeth.\hlend{}
			Cododd Begw ac aeth i ben y soffa ac edrych allan.
		}
		{%
			{} \felly{} \eif{} father \aRel{} do.\odd{}
			raise.\odd{} \Pn{} and go.\odd{} to head \Def{} sofa and look.\Inf{} out
		}
		{%
			\highlight{So it was her father had done it.}
			Begw got up and went to the end of the sofa and looked outside.
		}
		{\exsrctyyg{1}{10}}

	\a<de-fis>
		\gloss
		{%
			{} @ \nogloss{\hlbegin{}} @ (‘)Felly fy nhad a wnaeth,(’) meddyliai Begw.\hlend{}
			Cododd ac aeth i ben y soffa ac edrych allan.
		}
		{%
			{} so \fy{} father \aRel{} do.\odd{} think.\odd{} \Pn{}
			raise.\odd{} and go.\odd{} to head \Def{} sofa and look.\Inf{} out
		}
		{%
			\highlight{(‘)So it was my father had done it,(’) thought Begw.}
			She got up and went to the end of the sofa and looked outside.
		}
		{(rephrasing for explanatory purposes)}
\xe\postex{}

\preex{}\ex<ex:rd:thoughts:non-fis>
	\gloss
	{%
		Sgiatan wedi dŵad {yn ôl}, meddyliai Begw wrthi hi ei hun.
	}
	{%
		\Pn{} after come.\Inf{} back think.\ai{} \Pn{} with.\Tsg{}.\F{} \Tsg{}.\F{} \eif{} \Refl{}
	}
	{%
		Sgiatan’s come back, Begw thought to herself.
	}
	{\exsrctyyg{1}{11}}
\xe\postex{}

In \exfullref{ex:rd:thoughts:fis-gollyngdod} the spheres of the narrator and the protagonist’s mental processes are combined~— similarly to \exfullref{ex:rd:thoughts:fis.fis}~— and in fact collapse into one sentence.
About a page before this young Begw learns a new word~— \C{gollyngdod}[release, relief].
\Exfullref{ex:rd:thoughts:fis-gollyngdod} begins and ends as a plain narrative sentence (\C{\gl{A}{and} \gl{chafodd}{get.\odd{}} \gl{hithau}{\hithau{}}, \gl{Begw}{\Pn{}}, \gl{\ellipsis{}}{} \gl{ollyngdod}{\Obj{}\textbackslash{}release} \gl{wrth}{with} \gl{daflu}{throw.\Inf{}} \gl{ei}{\eif{}} \gl{dymuniad}{wish} \gl{i}{to} \gl{fyny}{up} \gl{yn}{\ynC{}} \gl{gelwydd}{lie} \gl{twt}{neat} \gl{cyfa}{whole}}[And she, Begw, got \ellipsis{} relief, in throwing up her wish as a total lie]), but in the middle another segment is inserted: \C{\ellipsis{} \gl{beth}{what} \gl{ddwedodd}{\aRel{}\textbackslash{}\dweud{}.\odd{}} \gl{ei}{\eif{}} \gl{mam}{mother} \gl{hefyd}{also}? \gl{O}{\Interj{}} \gl{ie}{\Interj{}}, \ellipsis{}}[\ellipsis{}—what had her mother said again? Oh yes—\ellipsis{}]%
\footnote{%
	The English translation presents the parenthetic status more clearly, using dashes.
}.
Interestingly, a feature of Welsh syntax marks the juncture between the two parts separated by the inserted segment: the direct object of the finite transitive verb \C{\gl{cafodd}{get.\odd{}}} is \C{\gl{gollyngdod}{release}}, and it is marked by lenition (\C{ollyngdod}, with \C{g-}~→~\C{∅-} mutation; see \cref{tab:intro:object:modern_literary_welsh:background:mutations} in \cref{sec:intro:object:modern_literary_welsh:background}), which is the direct object marker of finite verbs%
\footnote{%
	As opposed to infinitival forms, whose direct object marking does not involve lenition.
}.

\preex{}\ex<ex:rd:thoughts:fis-gollyngdod>
	\gloss
	{%
		A chafodd hithau, Begw, beth ddwedodd ei mam hefyd? O ie, ollyngdod wrth daflu ei dymuniad i fyny yn gelwydd twt cyfa.
	}
	{%
		and get.\odd{} \hithau{} \Pn{} what \aRel{}\textbackslash{}\dweud{}.\odd{} \eif{} mother also \Interj{} \Interj{} \Obj{}\textbackslash{}release with throw.\Inf{} \eif{} wish to up \ynC{} lie neat whole
	}
	{%
		And she, Begw, got—what had her mother said again? Oh yes—relief, in throwing up her wish as a total lie.
	}
	{\exsrctyyg{2}{25}}
\xe\postex{}

Discourse markers and interjections are common in free indirect speech segments in the corpus, as demonstrated in \exfullref{ex:rd:thoughts:fis:discourse markers}, as well as the previous example.

\preex{}\pex<ex:rd:thoughts:fis:discourse markers>
	\a
		\gloss
		{%
			{} @ \nogloss{\hlbegin{}} @ O, diar\hlend{}, yr oedd bywyd yn galed. \ellipsis{}
		}
		{%
			{} \Interj{} \Interj{} \yr{} \oedd{} life \ynC{} hard
		}
		{%
			\highlight{Oh, dear}, life was hard. \ellipsis{}
		}
		{\exsrctyyg{1}{9}}

	\a
		\gloss
		{%
			Yr oedd Bilw'n rhy ffeind, ond \nogloss{\hlbegin{}} @ O\hlend{}! i beth oedd eisiau iddo sôn am yr hen gwarfod yma?
		}
		{%
			\yr{} \oedd{} \Pn{}-\ynC{} too nice but \Interj{} to what \oedd{} need \iddo{} mention about \Def{} old meeting \ymadem{}
		}
		{%
			Bilw was too kind, but \highlight{Oh}! why did he need to mention the old meeting here and now?
		}
		{\exsrctyyg{3}{35}}
\xe\postex{}

{}\index{free indirect speech|)}



\subsubsection{Verbs of cognition}%
\label{sec:rd:thoughts:cognition}

{}\index{verbs of cognition|(}%
While cases of indirect \emph{speech} are rare in the studied corpus, it is more common for verbs of cognition to take indirect complements, as demonstrated in \exfullref{ex:rd:thoughts:cognition:indirect}, with \C{mai}-nominalisation of nominal predication in \exfullref{ex:rd:thoughts:cognition:indirect.mai} and \C{y}-nominalisation in exx.~\getfullref{ex:rd:thoughts:cognition:indirect.y-byddai}–\getref{ex:rd:thoughts:cognition:indirect.y-medrai}.

\preex{}\pex<ex:rd:thoughts:cognition:indirect>
	\a<mai>
		\gloss
		{%
			Wrth iddynt droi {oddi wrth} y drws, meddyliai {Elin Gruffydd} mai ‘slebog’ oedd y gair iawn am {Lisi Jên}.
		}
		{%
			with \iddynt{} \Len{}\textbackslash{}turn.\Inf{} from \Def{} door think.\ai{} \Pn{} \Nmlz{} slob \copoedd{} \Def{} word right about \Pn{}
		}
		{%
			\notsic{}As they turned from the door, Elin Gruffydd thought that ‘slob’ was the proper word for Lisi Jên.
		}
		{\exsrctyyg{5}{63}}

	\a<y-byddai>
		\gloss
		{%
			\ellipsis{}
			Credai Begw y byddai rhywbeth siŵr o dorri ym mrest Bilw.
			\ellipsis{}
		}
		{%
			{}
			believe.\ai{} \Pn{} \Nmlz{} \byddai{} something sure of break.\Inf{} \ynA{} breast \Pn{}
		}
		{%
			\ellipsis{}
			\notsic{}Begw believed that something was sure to break in Bilw’s chest.
			\ellipsis{}
		}
		{\exsrctyyg{3}{34}}

	\a<y-medrai>
		\gloss
		{%
			Gobeithiai y medrai hwnnw wneud rhywbeth i atal ei thad rhag ei gorfodi i fynd adref.
		}
		{%
			hope.\ai{} \Nmlz{} \medru{}.\ai{} \hwnnw{} make.\Inf{} something to stop.\Inf{} \eif{} father \Prep{} \eif{} oblige.\Inf{} to go.\Inf{} \Adv{}\textbackslash{}home
		}
		{%
			She hoped that he would be able to do something to stop her father from forcing her to go home.
		}
		{\exsrctyyg{2}{22}}
\xe\postex{}

Direct verbalisation of the thoughts of characters does also occur, as demonstrated in \exfullref{ex:rd:thoughts:non-fis} above and \exfullref{ex:rd:thoughts:cognition:direct}.
No quotation marks are used with verbs of cognition in the corpus, but this combination is attested in other works by Roberts.

\preex{}\pex<ex:rd:thoughts:cognition:direct>
	% \a
		% \gloss
		% {%
			% Biti ei fod yn cnoi baco, meddyliai Begw.
		% }
		% {%
			% pity \eim{} \bod{} \ynD{} chew.\Inf{} tobacco think.\ai{} \Pn{}
		% }
		% {%
			% A pity he chews tobacco, Begw thought.
		% }
		% {\exsrctyyg{3}{30}}
    %
	\a<meddyliai>
		\gloss
		{%
			Mae lot o’i dad yn y bwli bach yma, meddyliai Winni.
		}
		{%
			\mae{} lot of-\eim{} father \ynA{} \Def{} bully small \ymadem{} think.\ai{} \Pn{}
		}
		{%
			There’s a lot of his father in this little bully, thought Winni.
		}
		{\exsrchd{6}{56}}

	\a<debygai>
		\gloss
		{%
			\ellipsis{}
			Yr oedd yn gas ar Mair debygai Begw, ei thad yn dweud gras bwyd ac yn tyfu barf.
			\ellipsis{}
		}
		{%
			{} \yr{} \oedd{} \ynC{} hateful on \Pn{} \Len{}\textbackslash{}suppose.\ai{} \Pn{} \eif{} father \ynD{} \dweud{}.\Inf{} grace food and \ynD{} grow.\Inf{} beard
		}
		{%
			\ellipsis{}
			It was horrid for Mair, Begw supposed, her \inlinecomment{Mair’s} father saying grace at meals and growing a beard.
			\ellipsis{}
		}
		{\exsrctyyg{2}{14}}
\xe\postex{}

With regard to the relative position of the direct reported thought and the verb of cognition, at least in the studied corpus both \C{\gl{meddyliai}{think.\ai{}}} (as in \exfullref{ex:rd:thoughts:cognition:direct.meddyliai}) and \C{\gl{debygai}{\Len{}\textbackslash{}suppose.\ai{}}} (as in \exfullref{ex:rd:thoughts:cognition:direct.debygai}) are never preposed.
Preposed forms seem to collocate with \emph{indirect} complements (as in \exfullref{ex:rd:thoughts:cognition:indirect}).
The intraposed or postposed status of \C{\gl{debygai}{\Len{}\textbackslash{}suppose.\ai{}}} has a syntactic implication which does not affect \C{\gl{meddyliai}{think.\ai{}}}: the \C{t-}~→~\C{d-} lenition of \C{debygai} (dictionary form \C{tebyg}) has to do with this parenthetic status.
The lenition-triggering pre-verbal particle \C{\gl{mi}{\mi{}}} co-occur with verbs in parenthetic epistemic verbs, such as
\C{\gl{mi}{\mi{}} \gl{gredaf}{believe.\af{}}}[…, I believe (…)],
\C{\gl{mi}{\mi{}} \gl{dybiaf}{suppose.\af{}}}[…, I suppose (…)],
\C{\gl{mi}{\mi{}} \gl{wn}{know.\af{}}}[…, I know (…)] and
\C{\gl{mi}{\mi{}} \gl{wranta}{guarantee.\af{}}}[…, I’m sure (…)];
this syntactic feature is discussed in \textcite[§~7]{shisha-halevy.a:2005:epistolary}[§~1.3.4.b].
The parenthetic \C{debygai} may be analysed as a form with \emph{zero} pre-verbal particle which triggers lenition%
\footnote{%
	Such particles are discussed by \textcite{awbery.g.m:2004:clause-initial}, although with other type of data: spoken Welsh of north Pembrokeshire.
}.

{}\index{verbs of cognition|)}%



\subsubsection{\C{meddai wrth- ei hun} ‘said to oneself’}%
\label{sec:rd:thoughts:meddai-refl}

Four examples in the corpus (exx.~\getfullref{ex:rd:thoughts:meddai-refl:dialogue-1}–\getfullref{ex:rd:thoughts:meddai-refl:meddyliau}) have the idiomatic reflexive construction \C{\gl{meddai}{\meddai{}} \gl{wrth-}{with} \gl{ei}{\ei{}} \gl{hun}{\Refl{}}}[said to oneself], which does not indicate actual speech but \emph{thinking}.
These are different from the superficially-similar \exfullref{ex:rd:patterns:2:VD-13} in \cref{sec:rd:patterns:2:exx:dependence} above (\C{\ellipsis{} \gl{ddywedodd}{\dweud{}.\odd{}} \gl{ei}{\eif{}} \gl{mam}{mother} \gl{wrthi}{\wrthi{}} \gl{ei}{\eif{}} \gl{hun}{\Refl{}} \gl{fwy}{\Adv{}\textbackslash{}more} \gl{na}{than} \gl{neb}{\neb{anyone}}}[\ellipsis{} her mother said, to herself more than anyone]), which does represent speech (not thoughts which are not shared) and has a different verb in a different tense%
\footnote{%
	Structurally speaking, \C{meddai} has no tense in the environments it is used here and normally (see \cref{sec:rd:internal:nucl:generic:meddai+ebe:verbhood}).
},
a different configuration and different ‘addressee’ (\C{\gl{wrthi}{\wrthi{}} \gl{ei}{\eif{}} \gl{hun}{\Refl{}} \gl{fwy}{\Adv{}\textbackslash{}more} \gl{na}{than} \gl{neb}{\neb{anyone}}}[to herself more than anyone]).

In three of the examples (exx.~\getfullref{ex:rd:thoughts:meddai-refl:dialogue-1}–\getfullref{ex:rd:thoughts:meddai-refl:dialogue-3}) occur right after a line of dialogue, and represent a response the character chose not to utter.
Take note of the allocutive second-person forms \C{\gl{gweitiwch}{wait.\Imp{}.\Spl{}} \gl{chi}{\chi{}}}[wait!] and \C{\gl{gwelwch}{see.\Prs{}.\Spl{}} \gl{chi}{\chi{}}}[you (will) see] in \exfullref{ex:rd:thoughts:meddai-refl:dialogue-1}, which are used without actual addressing of the imagined allocutor (Begw’s mother) by the ‘speaker’ (Begw).

\preex{}\ex<ex:rd:thoughts:meddai-refl:dialogue-1>
	\gloss
	{%
		\inlinecomment{Mam Begw:} \Qquote{‘\ellipsis{},} \Qquote{a} \Qquote{fedar} \Qquote{neb} \Qquote{fyta} \Qquote{llawar} \Qquote{o} \Qquote{frechdan} \Qquote{efo} \Qquote{crempog.’}
		¶
		\nogloss{\hlbegin{}} @ ‘Gweitiwch chi nes gwelwch chi Winni yn byta,’ meddai Begw wrth hi ei hun.\hlend{}
	}
	{%
		{} {} and \Neg{}\textbackslash{}\medru{}.\Prs{}.\Tsg{} \neb{anyone} eat.\Inf{} much of bread\_and\_butter with pancake
		{}
		wait.\Imp{}.\Spl{} \chi{} until see.\Prs{}.\Spl{} \chi{} \Pn{} \ynD{} eat.\Inf{} \meddai{} \Pn{} with \hi{} \eif{} \Refl{}
	}
	{%
		\inlinecomment{Begw’s mother:} \Qquote{‘\ellipsis{}, and nobody can eat much bread-and-butter with pancakes.’}
		{}
		\highlight{‘Wait until you see Winni eat,’ Begw said to herself.}
	}
	{\exsrctyyg{5}{57}}
\xe\postex{}

\preex{}\ex<ex:rd:thoughts:meddai-refl:dialogue-2>
	\gloss
	{%
		\Qquote{‘Cymerwch} \Qquote{ragor.’} A chododd {Elin Gruffydd} dair arall ar y fforc. \nogloss{\hlbegin{}} @ Dyna’r nawfed, meddai Begw wrthi ei hun.\hlend{}
	}
	{%
		take.\Imp{}.\Spl{} more and raise.\odd{} \Pn{} three other on \Def{} fork \dyna{}-\Def{} ninth \meddai{} \Pn{} \wrthi{} \eif{} \Refl{}
	}
	{%
		\Qquote{‘Have some more.’} And Elin Gruffydd lifted another three on the fork. \highlight{That’s the ninth, Begw said to herself.}
	}
	{\exsrctyyg{5}{58}}
\xe\postex{}

\preex{}\ex<ex:rd:thoughts:meddai-refl:dialogue-3>
	\gloss
	{%
		\Qquote{‘'D} \Qquote{wn} \Qquote{i} \Qquote{ddim} \Qquote{i} \Qquote{beth} \Qquote{mae} \Qquote{eisiau} \Qquote{hen} \Qquote{gwarfodydd} \Qquote{llenyddol.’}
		¶
		\nogloss{\hlbegin{}} @ ‘O! Bilw annwyl, yn meddwl yr un fath â fi,’ meddai Begw wrthi ei hun.\hlend{}
	}
	{%
		\Neg{} know.\Prs{}.\Fsg{} \Fsg{} \dim{\Neg{}} to what \mae{} need old meeting.\Pl{} literary
		{}
		\Interj{} \Pn{} dear \ynD{} think.\Inf{} \Def{} one thing with \Fsg{} \meddai{} \Pn{} \wrthi{} \eif{} \Refl{}
	}
	{%
		\Qquote{‘I don’t know why there’s a need for old literary meetings.’}
		¶
		\highlight{‘Oh! Dear Bilw, thinking the same as me,’ Begw said to herself.}
	}
	{\exsrctyyg{3}{34}}
\xe\postex{}

In \exfullref{ex:rd:thoughts:meddai-refl:meddyliau} the construction is used within the course of a narrative paragraph, right after the author refers to the quality of one character’s thoughts in response to recent events in the narrative.

\preex{}\ex<ex:rd:thoughts:meddai-refl:meddyliau>
	\gloss
	{%
		\ellipsis{}
		Yr oedd rhywbeth heblaw \nogloss{\subhlbegin{}} @ meddyliau\subhlend{} balch na difalch yn ei chalon, \nogloss{\subhlbegin{}} @ meddyliau\subhlend{} digalon oedd y rheini wrth weld y tri yn troi am y ffordd. \nogloss{\hlbegin{}} @ ‘Pam?’ meddai wrthi ei hun. ‘Pam?’\hlend{}
		\ellipsis{}
	}
	{%
		{}
		\yr{} \oedd{} something beside thought.\Pl{} glad nor \Abe{}.glad \ynA{} \eif{} heart thought.\Pl{} disheartened \copoedd{} \Def{} \rheini{} with see.\Inf{} \Def{} three \ynD{} turn about \Def{} road why \meddai{} \wrthi{} \eif{} \Refl{} why
	}
	{%
		There was something besides pleased or displeased thoughts in her heart, they were depressed thoughts as she saw the three turn towards the road. \highlight{“Why?” she said to herself, “why?”}
		\ellipsis{}
	}
	{\exsrctyyg{5}{64}}
\xe\postex{}

The reflexive construction in question is used in other works by Kate Roberts and others.
The only attestations of \C{\gl{ebe}{\ebe{}}} instead of \C{meddai} in the first slot I could find in works of Kate Roberts are from her first collection of short stories, \GB{} \parencite{roberts.k:1932:gors-bryniau}.
As discussed in \cref{sec:rd:internal:nucl:generic:meddai+ebe:two} above, the use of \C{ebe} and \C{meddai} in this early publication is strikingly different from that of her later works, in which \C{ebe} is restricted to actual quotative QI1 constructions.
