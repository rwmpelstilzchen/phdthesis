\subsection{Reciting verses and singing}%
\label{sec:rd:verses}

Several verses are embedded in the running text of the corpus, four in \TG{} and one in \HD{}.
These are distinguished from regular speech not only in their internal form (rhyming, short lines, typographic indentation, etc.) but also in other ways, as discussed in this subsection.
They are always introduced with a preposed reference and paragraph break, but this cannot be regarded as QI2, for three reasons:
\begin{compactitem}
	\item QI2 is structurally defined in opposition to QI1 and QI3%
		\footnote{%
			See \cref{sec:intro:text:types:struct:int:value-commutation} for theoretical background.
		}.
		Since the formal properties which are similar to QI2 are \emph{obligatory} (meaning intraposed or postposed forms, or integration into a narrative paragraph do not co-occur with verses) there is no structural opposition, and therefore no structural \foreign{valeur}\index{valeur@\foreign{valeur}}.

	\item Verses are not turns in the turn-taking of dialogue.
		There \emph{are} texts which present conversation in verse form%
		\footnote{%
			See, for example, \textcite{helsinger.e:2017:conversing-verse} regarding English poetry.
			Many works of epic poetry have dialogue portions in them.
		},
		but our corpus is not one of them, and the embedded verses are alien to the general flow of the text, dialogue included.

	\item The way in the verses (except \exfullref{ex:rd:verses:VD-24}) are introduced and function within the text is distinct from QI2.
\end{compactitem}

The verse which is most similar to the quotes in dialogue is that of \exfullref{ex:rd:verses:VD-24}%
\footnote{%
	As described in \cref{sec:rd:intro:annotation}, a \grapheme{/} marks a line break, and \grapheme{>} marks indentation.
},
which occurs as an extension of the quote (cited for \mbox{co(n)text}), describing the social inequality in the community in a verse form, introduced by \C{\gl{Dechreuodd}{begin.\odd{}} \gl{lafar-ganu}{say-sing.\Inf{}}}[She began to chant].

\preex{}\ex<ex:rd:verses:VD-24>
	\gloss
	{%
		\Qquote{‘’R} \Qquote{ydw} \Qquote{i} \Qquote{yn} \Qquote{rhy} \Qquote{flêr.}
		\Qquote{’D} \Qquote{oes} \Qquote{gin} \Qquote{i} \Qquote{ddim} \Qquote{dillad} \Qquote{o} \Qquote{gwbl.}
		\Qquote{A} \Qquote{meddylia} \Qquote{sut} \Qquote{y} \Qquote{basa} \Qquote{dynes} \Qquote{y} \Qquote{pregethwr} \Qquote{yn} \Qquote{edrach} \Qquote{arna} \Qquote{’i.}
		% Mi fasa’n mynd i mewn i’r harmoniam cyn y basa hi’n eistedd wrth f’ ochor, ac yn snwffian dros y capel.’
		\ellipsis{}.’
		¶
		{Dechreuodd} {lafar-ganu}:
		\nogloss{\glafmt{}\examparsymbol{}}
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{‘Gosod} \Qquote{seti} \Qquote{i} \Qquote{bobol} \Qquote{fawr,} \nogloss{\glafmt{}\Qquote{/}}
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{Gadael} \Qquote{tlodion} \Qquote{ar} \Qquote{y} \Qquote{llawr’.}
		% \nogloss{\glafmt{}\examparsymbol{}}
		% ‘Be mae hwnna yn i feddwl?’
		% \nogloss{\glafmt{}\examparsymbol{}}
		% ‘’Down i yn y byd. Paid â holi. Dim ond bod pobl y capel yn trin y tlodion fel tasan nhw yn faw.’
	}
	{%
		\yr{} \wyf{} \Fsg{} \ynC{} too untidy
		\Neg{} \oes{} by \Fsg{} \Neg{} clothes.\Col{} of all
		and think.\Imp{}.\Ssg{} how \yrRel{} \basai{} woman \Def{} preacher \ynD{} look.\Inf{} \arnaf{} \Fsg{}
		{}
		{}
		begin.\odd{} say-sing.\Inf{}
		put.\Inf{} seat.\Pl{} to people big
		let.\Inf{} poor.\Pl{} on \Def{} floor
		% what \mae{} \hwnna{} \ynD{} \eim{} mean.\Inf{}
		% \Neg{}-know.\Prs{}.\Fsg{} \Fsg{} \ynA{} \Def{} world stop.\Imp{}.\Ssg{} with ask.\Inf{} nothing but \bod{} people \Def{} chapel \ynD{} treat.\Inf{} \Def{} poor.\Pl{} like \tasent{} \Tpl{} \ynC{} dung
	}
	{%
		\Qquote{%
			‘I’m too shabby.
			I haven’t any clothes at all.
			And think how the preacher’s woman would look at me.
			\ellipsis{}’
		}
		¶
		{She} {began to chant}:
		¶
		\Qquote{%
			>~‘Setting seats for all the big folks,~/
			>~Leaving poor folks on the floor.’
		}
		% ¶
		% ‘What does that mean?’
		% ¶
		% ‘I don’t know at all. Don’t ask. Just that the people in the chapel treat the poor as if they were dirt.’
	}
	{\exsrctyyg{6}{71}}
\xe\postex{}

Similar verses which are sung or recited by characters in the course of the narrative and are introduced through independent clauses can be seen in \exfullref{ex:rd:verses:VD-25-26-27}, from other texts \parencite{roberts.k:1988:traed,roberts.k:2001:gobaith}.

\preex{}\pex<ex:rd:verses:VD-25-26-27>
	\a
		\gloss
		{%
			{Canent} {dan} {fynd,} neu’n hytrach {adroddent} {i} {dôn}:
			\nogloss{\glafmt{}\examparsymbol{}}
			\Qquote{\shorthand{cân}}
		}
		{%
			sing.\Impf{}.\Tpl{} under go.\Inf{} or-\ynB{} rather recite.\Inf{} to tune
		}
		{%
			{As they went} {they} {sang}, or {chanted to a tune}:
			¶
			\Qquote{\shorthand{verse}}
		}
		{\exsrctc{6}{32}}

	\a
		\gloss
		{%
			Yr oedd y falwen yn yr ardd, un dew a bol gwyn ganddi.
			Dyma {hi}{’n} {adrodd} {uwch} {ei} {phen}:
			\nogloss{\glafmt{}\examparsymbol{}}
			% \nogloss{\glafmt{}\Qquote{>}} \Qquote{‘Malwen,} \Qquote{malwen,} \Qquote{estyn} \Qquote{dy} \Qquote{bedwar} \Qquote{corn} \Qquote{allan~/}
			% \nogloss{\glafmt{}\Qquote{>}} \Qquote{Ne} \Qquote{mi} \Qquote{tafla’i} \Qquote{di} \Qquote{i’r} \Qquote{Môr} \Qquote{Coch} \Qquote{at} \Qquote{y} \Qquote{gwartheg} \Qquote{cochion.’}
			\Qquote{\shorthand{cân}}
		}
		{%
			\yr{} \oedd{} \Def{} slug.\Sgv{}(\F{}) \ynA{} \Def{} garden one fat and belly white by.\Tsg{}.\F{} \dyma{} \hi{}-\ynD{} recite above \eif{} head
			% slug.\Sgv{} slug.\Sgv{} extend.\Imp{}.\Ssg{} \dy{} four horn out
			% or \mi{} throw.\Prs{}.\Fsg{}-\Fsg{} \Ssg{} to-\Def{} sea read to \Def{} cattle red.\Pl{}
		}
		{%
			There was a slug in the garden, a fat one with a white belly.
			Here {she} {recites} {above its head}:
			¶
			% \Qquote{%
				% >~‘Slug, slug, extend your four horns out~/
				% >~Or I through you to the Red Sea to the red cows.’
			% }
			\Qquote{\shorthand{verse}}
		}
		{\exsrcgobaith{Dychwelyd}{10}{89}}

	\a
		\gloss
		{%
			Syllodd arno’n hir; yna {torrodd} {allan} {i} {ganu} {dros} {bob} {man}:
			\nogloss{\glafmt{}\examparsymbol{}}
			\Qquote{\shorthand{cân}}
		}
		{%
			gaze.\odd{} on.\Tsg{}.\M{}-\ynB{} long \ynatmp{} break.\odd{} out to sing.\Inf{} over every place
		}
		{%
			She gazed on it for a long time; then {she} {broke out singing} {all over}:
			¶
			\Qquote{\shorthand{verse}}
		}
		{\exsrcgobaith{Dychwelyd}{10}{91}}
\xe\postex{}

The verse in \exfullref{ex:rd:verses:VD-21} is also an extension of the quote.
This time the association to the quote is lexical, \C{\gl{tirsia}{frown.\Pl{}}}/\C{\gl{tirsia}{frown.\Prs{}.\Tsg{}}}%
\footnote{%
	With the mutated forms \C{dirsia} (soft mutation) and \C{thirsia} (spirant mutation); see \textcite[§§~turs; tursiaf: tursio]{gpc:arlein} for dictionary forms.
}.
Syntactically, the verse is introduced though the modification component of the quotative index (\C{\gl{dan}{under} \gl{lafar-ganu’r}{say-sing.\Inf{}-\Def{}} \gl{rhigwm}{rhyme}}[chanting the thyme]), acting as a bridge between the two.

\preex{}\ex<ex:rd:verses:VD-21>
	\gloss
	{%
		\Qquote{‘\ellipsis{}} \Qquote{Oho,} \Qquote{fel} \Qquote{yna,} \Qquote{Mair,} \Qquote{aiê,} \Qquote{gollwng} \Qquote{dy} \Qquote{dirsia} \Qquote{am} \Qquote{fod} \Qquote{Begw} \Qquote{yn} \Qquote{cael} \Qquote{sylw,’} \nogloss{\hlbegin{}} @ ebe\hlend{} {Twm Huws} \nogloss{\hlbegin{}} @ dan lafar-ganu\hlend{}’r rhigwm:
		\nogloss{\glafmt{}\examparsymbol{}}
		% ‘\shorthand{cwpled}’
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{‘Mwnci} \Qquote{ciat} \Qquote{a} \Qquote{mwnci} \Qquote{ciatas} \nogloss{\glafmt{}\Qquote{/}}
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{Tirsia} \Qquote{mul} \Qquote{a} \Qquote{thirsia} \Qquote{mulas'.}
	}
	{%
		{} \Interj{} like \ynadem{} \Pn{} \Interj{} let\_go \dy{} frown.\Pl{} for \bod{} \Pn{} \ynD{} get.\Inf{} attention \ebe{} \Pn{} under say-sing.\Inf{}-\Def{} rhyme
		monkey {} and monkey {}
		grimace.\Pl{} mule and grimace.\Pl{} donkey.\Pl{}
	}
	{%
		Oho, it’s like that, is it, Mair, giving sulky looks because Begw’s getting attention,’ Twm Huws \highlight{said}, \highlight{chanting} the rhyme:
		¶
		% ‘\shorthand{couplet}’
		\Qquote{%
			>~‘Monkey see and monkey do;~/
			>~When one mule sulks, the others do too.’
		}
	}
	{\exsrctyyg{2}{18}}
\xe\postex{}

The remaining three verses (exx.~\getfullref{ex:rd:verses:VD-22}–\getfullref{ex:rd:verses:y mul a gododd i ben}) are not sung or recited by any character in the course of the narrative.

The verse in \exfullref{ex:rd:verses:VD-22} acts as the complement of the comparative construction \C{\gl{mor}{so} \gl{wirion}{silly} \gl{â}{with}}[as silly as] in a descriptive, commentative portion of the text.

\preex{}\ex<ex:rd:verses:VD-22>
	\gloss
	{%
		Nid oedd fawr o bleser mewn edrych ymlaen at y Nadolig pan oedd yn rhaid i chwi fynd ar ben llwyfan ac adrodd hen beth mor wirion â
		\nogloss{\glafmt{}\examparsymbol{}}
		\Qquote{‘\shorthand{cân}’}
		\nogloss{\glafmt{}\examparsymbol{}}
	}
	{%
		\Neg{} \oedd{} big of pleasure in look.\Inf{} forward to \Def{} Christmas when \oedd{} \ynC{} need to \Spl{} go.\Inf{} on head stage and recite.\Inf{} old thing so silly with
	}
	{%
		There wasn’t much pleasure in looking forward to Christmas when you had to get up on a stage and recite such a silly old thing as:
		¶
		\Qquote{‘\shorthand{verse}’}
		¶
	}
	{\exsrctyyg{3}{27}}
\xe\postex{}

In \exfullref{ex:rd:verses:VD-23} it is inserted within a sentence which describes something a character was about to do but was not doing in actuality (thus, it is a non-event).

\preex{}\ex<ex:rd:verses:VD-23>
	\gloss
	{%
		Bu agos i Begw ddweud:
		\nogloss{\glafmt{}\examparsymbol{}}
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{‘Amen,} \Qquote{dyn} \Qquote{pren,} \nogloss{\glafmt{}/}
		\nogloss{\glafmt{}\Qquote{>}} \Qquote{Hitio} \Qquote{mochyn} \Qquote{yn} \Qquote{ei} \Qquote{ben,’}
		\nogloss{\glafmt{}\examparsymbol{}}
		ond cofiodd fod gan {Dafydd Siôn} gyfleth yn ei boced.
	}
	{%
		\bu{} near to \Pn{} \dweud{}.\Inf{}
		amen man wood
		hit.\Inf{} pig.\Sgv{}(\M{}) \ynA{} \eim{} head
		but remember.\odd{} \bod{} by \Pn{} toffee \ynA{} \eim{} pocket
	}
	{%
		Begw almost said:
		¶
		\Qquote{%
			>~‘Amen, man of wood,~/
			>~Hit a piggy on his head,’
		}
		¶
		but she remembered that Dafydd Siôn had toffee in his pocket.
	}
	{\exsrctyyg{3}{33}}
\xe\postex{}

In \exfullref{ex:rd:verses:y mul a gododd i ben} the \C{\gl{rhigwm}{rhyme}} is referred to in the previous co-text (cited in the example), but there is no syntactic link between it and the verse, which is externally ‘pasted’.

\preex{}\ex<ex:rd:verses:y mul a gododd i ben>
	\gloss
	{%
		\ellipsis{}
		yr un fath â’r rhigwm y buasai yn ei adrodd wrth ei hanner brawd lawer gwaith i’w gadw’n ddiddig, ac yntau’n gweiddi ‘Eto’ o hyd.
		¶
		\nogloss{\glafmt{}\Qquote{>}} ‘Y mul a gododd i ben \nogloss{\glafmt{}\Qquote{/}}
		\nogloss{\glafmt{}\Qquote{>}} Yn uchel tua’r nen, \nogloss{\glafmt{}\Qquote{/}}
		\nogloss{\glafmt{}\Qquote{>}} A’r bobol sy’n gorfoleddu, \nogloss{\glafmt{}\Qquote{/}}
		\nogloss{\glafmt{}\Qquote{>}} ‘Haleliwia byth, Amen.’
	}
	{%
		{}
		\Def{} one kind with-\Def{} rhyme(\M{}) \yrRel{} \buasai{} \ynD{} \eim{} recite.\Inf{} with \eif{} half brother \Adv{}\textbackslash{}many  time to-\eim{} keep-\ynC{} content and \yntau{}-\ynD{} shout.\Inf{} again of length
		{}
		\Def{} mule \aRel{} raise.\odd{} \eim{} head
		\ynB{} high towards-\Def{} sky
		and-\Def{} people \sy{}-\ynD{} triumph.\Inf{}
		hallelujah always amen
	}
	{%
		\ellipsis{}
		just as in the rhyme which she would recite to her half-brother so often to keep him happy, and he would shout ‘again’ all the time.
		¶
		>~‘The donkey lifted his head~/
		>~High up towards the sky,~/
		>~All the people gave a great cry~/
		>~Halleluiah, for ever and aye.’
	}
	{\exsrchd{6}{50}}
\xe\postex{}
