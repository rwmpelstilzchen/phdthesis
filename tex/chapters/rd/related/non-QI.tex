\subsection{Speech verbs outside of quotative indexes}%
\label{sec:rd:related:non-QI}

Speech verbs are commonly used as nuclei of quotative indexes, but are used in other environments as well.
The figurative use of \C{\gl{meddai}{\meddai{}} \gl{wrth}{with}- \gl{ei}{\ei{}} \gl{hun}{\Refl{}}}[said to oneself] has been just discussed, but there are other, nonfigurative uses.

One is to encapsulate parts of the dialogue without explicitly writing it in as direct-speech dialogue paragraphs.
Such is \exfullref{ex:rd:interruptions:resumption:Yna aeth Gwen ymlaen} from \cref{sec:rd:interruptions:resumption} above, in which the author avoids writing the dialogue paragraphs of what Gwen says to Winni.
Other examples are \exfullref{ex:rd:related:non-QI:encapsulate}, where a nominal reference (\C{\gl{helynt}{trouble}}, \C{\gl{sgwrs}{talk}} and \C{\gl{hanes}{history}}) spares repeating events the readers are familiar with or providing information which the author chooses not to include.

\preex{}\pex<ex:rd:related:non-QI:encapsulate>
	\a<Dywedodd hithau ei helynt>
		\gloss
		{%
			\inlinecomment{dyn yn dod at Winni:} \Qquote{‘Be} \Qquote{sy’n} \Qquote{bod} \Qquote{Winni?’}
			¶
			Dywedodd {hithau \inlinecomment{=Winni}} ei helynt.
		}
		{%
			{} what \sy{}-\ynD{} \bod{} \Pn{}
			{}
			\dweud{}.\odd{} \hithau{} \eif{} trouble
		}
		{%
			\inlinecomment{a man coming up to Winni:} \notsic{}‘What’s the matter, Winni?’
			¶
			She \inlinecomment{=Winni} told him her troubles.
		}
		{\exsrchd{6}{51}}

	\a<Soniodd am sgwrs Gwen>
		\gloss
		{%
			Soniodd am sgwrs Gwen wrth ei meistres.
		}
		{%
			mention.\odd{} about talk \Pn{} with \eif{} mistress
		}
		{%
			She mentioned Gwen’s talk to her mistress.
		}
		{\exsrchd{2}{20}}
		\subexamplespacer{}

	\a<Dywedasant bob un ei hanes>
		\gloss
		{%
			\ellipsis{}
			Dywedasant bob un ei hanes wrth ei gilydd.
		}
		{%
			{}
			\dweud{}.\Pret{}.\Tpl{} every one \eif{} history with \eithird{} \Recp{}
		}
		{%
			\ellipsis{}
			They both told each other their life history.
		}
		{\exsrchd{2}{19}}
\xe\postex{}

In addition to \C{\gl{dweud}{\dweud{}.\Inf{}}}, \C{\gl{sôn}{mention.\Inf{}} \gl{am}{about}} and other speech verbs, other verbs can take communicative sense, including the general \C{\gl{rhoi}{give.\Inf{}}}:

\preex{}\ex
	\gloss
	{%
		Rhoes Winni’r hanes a Begw’n gwrando, ei cheg yn agored a’i llygaid yn rhythu.
	}
	{%
		give.\odd{} \Pn{}-\Def{} account and \Pn{}-\ynD{} listen.\Inf{} \eif{} mouth \ynC{} open and-\eif{} eye.\Pl{} \ynD{} stare.\Inf{}
	}
	{%
		Winni told her story, and Begw listened, her mouth open and her eyes wide.
	}
	{\exsrchd{6}{14}}
\xe\postex{}

Nominal reference object is one option to avoid explicit direct dialogue; indirect speech is another (\exfullref{ex:rd:related:non-QI:indirect}).
Interestingly, while indirect speech is commonly dealt in tandem with direct speech in the grammatical literature, it is quite rare in our corpus.
Indirect speech is distinct from direct speech not only in its syntax, but also in its textual function: it is a wholly narrative form, whereas direct speech (in QI1 and QI2) bridges between narrative and dialogue.%
\footnote{%
	Keeping with the bridge metaphor, QI1 stands closer to the dialogue side and QI2 to the narrative side.
}

\preex{}\pex<ex:rd:related:non-QI:indirect>
	\a
		\gloss
		{%
			\ellipsis{}
			\nogloss{\hlbegin{}} @ Dywedodd Robin nad oedd ef am aros i gael te efo {Winni Ffinni Hadog}\hlend{}, a dechreuodd Rhys grio wrth ei glywed yn dweud.
			\ellipsis{}
		}
		{%
			{}
			\dweud{}.\odd{} \Pn{} \Nmlz{}.\Neg{} \oedd{} \ef{} about stay.\Inf{} to get.\Inf{} tea with \Pn{} and begin.\odd{} \Pn{} cry.\Inf{} with \eim{} hear.\Inf{} \ynD{} \dweud{}.\Inf{}
		}
		{%
			\ellipsis{}
			\highlight{Robin said that he didn’t want to stay and have tea with Winni Ffinni Hadog}, and Rhys began crying when he heard him say that.	
			\ellipsis{}
		}
		{\exsrctyyg{5}{57}}

	\a
		\gloss
		{%
			\ellipsis{}
			Ond mi gafodd ei mam afael ar hen gôt ddu dri-chwarter a choler gyrlin cloth arni, a dyma hi'n dechrau ei datod a dweud y gwnâi gôt iawn i Winni at ei godre.
			\ellipsis{}
		}
		{%
			{}
			but \mi{} get.\odd{} \eif{} mother hold.\Inf{} on old coat(\F{}) black three-quarter and collar curling cloth \arni{} and \dyma{} \hi{}-\ynD{} begin.\Inf{} \eif{} undo.\Inf{} and say.\Inf{} \Nmlz{} do.\ai{} coat proper to \Pn{} to \eif{} fringe
		}
		{%
			\ellipsis{}
			But her mother got hold of an old black three-quarter-length coat with a curling cloth collar, and she began to take it apart, saying it would make a fine full-length coat for Winni.
			\ellipsis{}
		}
		{}
\xe\postex{}

There are functions direct speech in dialogue do not cover, such as avoidance from speaking (\exfullref{ex:rd:related:non-QI:avoidance}) and a hypothetical act of speaking (\exfullref{ex:rd:related:non-QI:hypothetical}).

\preex{}\pex<ex:rd:related:non-QI:avoidance>
	\gloss
	{%
		Ni soniodd yr un o'r ddau air am yr helynt ar y ffordd adre, Begw o gywilydd, a Robin am y tro yn deall teimladau ei chwaer.
	}
	{%
		\Neg{} mention.\odd{} \Def{} one of-\Def{} two.\M{} word about \Def{} trouble on \Def{} way \Adv{}\textbackslash{}home \Pn{} from shame and \Pn{} about \Def{} time \ynD{} understand.\Inf{} feeling.\Pl{} \eim{} sister
	}
	{%
		Neither of the two said a word on the way home about the trouble, Begw from shame, and Robin for once understanding his sister’s feelings.
	}
	{\exsrctyyg{4}{47}}
\xe\postex{}

\preex{}\pex<ex:rd:related:non-QI:hypothetical>
	\gloss
	{%
		Penderfynodd fynd i'r lôn trwy lidiart y drws nesa', ac os deuai Mrs. Huws i gyfarfod â hi, gallai ddweud mai dyfod i alw ar Mair yr oedd.
	}
	{%
		decide.\odd{} go.\Inf{} to-\Def{} lane through gate \Def{} door next and if come.\ai{} Mrs \Pn{} to meet.\Inf{} with \hi{} \gallu{}.\ai{} \dweud{}.\Inf{} \mai{} come.\Inf{} to call.\Inf{} on \Pn{} \yrRel{} \oedd{}
	}
	{%
		She decided to go to the road through the next door’s gate, and if Mrs. Huws came to meet her, she could say that what she was doing was coming to call on Mair.
	}
	{\exsrctyyg{2}{14}}
\xe\postex{}

Speech verbs are in use within the speech of characters in dialogue as well, when they speak about speaking (e.g.\ \exfullref{ex:rd:related:non-QI:dialogue}).
The grammar of the dialogue portions of the text~— as opposed to the \emph{interface} between narrative and dialogue~— lay outside of the limits of this chapter.

\preex{}\pex<ex:rd:related:non-QI:dialogue>
	\a
		\gloss
		{%
			\inlinecomment{Mrs.\ Huws:}
			\Qquote{‘Rŵan,} \Qquote{Winni,} \Qquote{nid} \Qquote{fel} \Qquote{yna} \Qquote{mae} \Qquote{siarad.} \Qquote{Dwedwch} \Qquote{“Mrs.} \Qquote{Jones,”} \Qquote{nid} \Qquote{“y} \Qquote{Mrs.} \Qquote{Jones} \Qquote{yna} \Qquote{sy’n} \Qquote{dŵad} \Qquote{yma} \Qquote{i} \Qquote{llnau.”} \Qquote{\ellipsis{}’}
		}
		{%
			{}
			now \Pn{} \Neg{} like \yna{} \mae{} talk say.\Imp{}.\Spl{} Mrs \Pn{} \Neg{} \Def{} Mrs \Pn{} \yna{} \sy{}-\ynD{} come.\Inf{} \ymaloc{} to clean.\Inf{}
		}
		{%
			\inlinecomment{Mrs Huws:}
			\Qquote{%
				‘Now, Winni, that’s no way to talk.
				Say~— “Mrs.\ Jones” not “that Mrs. Jones who comes here to clean.” \ellipsis{}’
			}
		}
		{\exsrchd{6}{46}}

	\a
		\gloss
		{%
			\inlinecomment{Begw:}
			\Qquote{‘\ellipsis{}}
			\Qquote{Mi} \Qquote{glywais} \Qquote{i} \Qquote{Mr.} \Qquote{Huws} \Qquote{yn} \Qquote{deud} \Qquote{—} \Qquote{“trwy} \Qquote{Iesu Grist.} \Qquote{Amen.’}
		}
		{%
			{}
			{}
			\mi{} hear.\ais{} \Fsg{} Mr \Pn{} \ynD{} \dweud{}.\Inf{} {} through \Pn{} amen
		}
		{%
			\inlinecomment{Begw:}
			\Qquote{%
				‘\ellipsis{}
				I heard Mr. Huws saying—“through Jesus Christ. Amen.”’
			}
		}
		{\exsrctyyg{2}{16}}
\xe\postex{}
