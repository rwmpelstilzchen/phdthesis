\section{Conclusion}%
\label{sec:rd:conclusion}

\rev{2}{}%
This chapter delineates the seam between two primary modes of narrative (see \cref{sec:intro:text:sub-textual:narrmodes}): the dialogue portions and the narration-proper portions of narratives.
It portrays the micro- and macro-syntactic signs which have to do with the integration of \emph{mimetic} speech into \emph{diegetic} narration.
These signs are described with regard to their structural properties~— their signifiers, their signifieds and the systematic relationships that define them.
Complex, intricate and interesting linguistic patterns emerge from the text through the lens of structural text linguistics.
A key notion in this chapter is that of \emph{quotative index}, the linguistic expression which signals, embeds and introduces a \emph{quote} (direct reported speech) into the broader discourse.

Three distinct types of quotative indexes are attested in the corpus, showing distinct syntagmatic, paradigmatic, systematic and textual-functional features (\cref{sec:rd:patterns}).
The first, QI1 (\cref{sec:rd:patterns:1}), is the basic, conventionalised type of quotative index in the corpus, which unmarkedly tags turns in dialogue.
It may be followed by a special narrative addendum (\cref{sec:rd:patterns:1:addenda}) and may split the quote in two parts (\cref{sec:rd:patterns:1:position}).
Formally QI1 begins with a paragraph break and a quote, which may be followed by an overt quotative index (\QIaplus{}) or stand on its own, occupying its own paragraph (\QIaminus{}, with a zero quotative index).
The second type, QI2 (\cref{sec:rd:patterns:2}), is strictly preposed, located at the coda of a previous narrative paragraph and has a paragraph break between it and the quote.
Functionally, it marks the act or manner of speech as an event with narrative significance.
While QI2 is much rarer than QI1, the third type, QI3 (\cref{sec:rd:patterns:3}), is even less prevalent; while QI1 operates essentially in the domain of dialogue and QI2 in both the domain of dialogue and narrative, QI3’s function lies strictly within narrative, with no dialogue component.
Formally, QI3 is embedded within the flow of a narrative paragraph.%
\footnote{%
	Some aspects of phonetics and prosody in an audiobook produced for \TG{} (one work which is used as a part of the corpus) are cursorily explored in \cref{sec:rd:patterns:3:sain}.
}

Next the internal structure of the quotative indexes is analysed (\cref{sec:rd:internal}).
Four basic components are recognised: the nucleus, the speaker, the addressee and the modification.%
\footnote{%
	Each intersects with the type of quotative index, making $3 \times 4$ paradigms.
}
The different types of nuclei attested in the corpus (\cref{sec:rd:internal:nucl}) are diverse in terms of their form and syntax, and include:
generic speech verbs%
\footnote{%
	\C{meddai} and \C{ebe} for QI1 (\cref{sec:rd:internal:nucl:generic:meddai+ebe}), which arguably are not verbs synchronically, or at least not fully verbal.
	QI2 has \C{meddai} and \C{dweud} as generic forms (\cref{sec:rd:internal:nucl:dweud}).
},
specific speech verbs (which bear specific lexical meaning, such as \C{\gl{gofyn}{ask.\Inf{}}} or \C{\gl{gweiddi}{shout.\Inf{}}}),
the compound prepositional phrase \C{\gl{oddi}{from} \gl{wrth}{with} \Sp{}},
and nominal predication patterns.
Speakers (\cref{sec:rd:internal:sp}) are indicated pronominally or nominally; the syntactic role of the speaker component depends on the nucleus.
Addressees (\cref{sec:rd:internal:ad}) are rarely indexed explicitly in the quotative index, but when needed they are within the quote (as a vocative phrase, for example).
The modification component (\cref{sec:rd:internal:mod}) intersects with QI-type in complex ways that involves syntactic and textual factors.

The quotative index of QI1 can be either overt (\QIaplus{}) or zeroed (\QIaminus{}), leaving only the typographical devices of quotation marks and paragraph breaks as markers (\cref{sec:rd:zero}).
The distribution between the two forms can be described by the following rule of thumb (simplified here): unless the identification of the speaker is not unambiguous at that point in the text or the author wishes to describe something specific regarding the speech%
\footnote{%
	Through the modification component or~— marginally~— through a lexical choice of specific speech verb.
},
\QIaminus{} is used; otherwise, \QIaplus{} is used.
Several conversations are examined as case studies (\cref{sec:rd:zero:corpus}): very long conversations between two characters are carried out using \QIaminus{} almost exclusively, while conversations between three characters exhibit complex use of \QIaplus{} and \QIaminus{}%
\footnote{%
	As complementary evidence, two such conversations are annotated and commented upon in an appendix (\cref{app:rd:zero}).
}.
The special cases of rapid narrative-dialogue alternation (\cref{sec:rd:zero:alternation}) and stories which open with speech (\cref{sec:rd:zero:opening}) are also considered, in order to provide a fuller picture.
The corpus is not a record of actual real-life occurrences and conversations, but an artificial, literary construct that mimics such records.
The author presents several situations in which characters violate social conventions and interrupt one another (\cref{sec:rd:interruptions}); these interruptions~— and resumptions~— have linguistic consequences.

Finally, \cref{sec:rd:related} is dedicated to three issues which are related to the topic in question but fit nowhere in the other sections.
One is reciting verses and singing (\cref{sec:rd:verses}), which have linguistic characteristics that are different from those which normal speech has.
Another is reported thoughts: \cref{sec:rd:thoughts} covers similarities and dissimilarities between the linguistic representation and reporting of speech and thought%
\footnote{%
	Both fall under the umbrella term \emph{reported discourse} and are often grouped together in linguistic treatments.
};
a number of strategies and syntactic constructions are described.
The third is the use of speech verbs outside of quotative indexes (\cref{sec:rd:related:non-QI}); while speech verbs are commonly used as nuclei of quotative indexes, they are used in other environments as well, for several purposes.
