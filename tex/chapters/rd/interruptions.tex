\section{Interruption and resumption in conversation}%
\label{sec:rd:interruptions}

{}\index{interruption in conversation|(}

\subsection{Interruption}%
\label{sec:rd:interruptions:interruption}

Our corpus is literary, and as such every piece that make it is orchestrated by the author in a deliberate, orderly manner.
Nevertheless, the author can stage a disruption of order through literary and linguistic means.
With respect to the topic in question, quotes can be interrupted before they are completed and literary conversations~— like actual conversations which they imitate~— do not necessarily follow a simple pattern.

In conversation analysis\index{conversation analysis} (CA), the \emph{turn construction units}\index{turn-taking}\index{turn construction unit} (TCU) are fundamental organising units of conversation, in which speakers realise in turns their interpersonal right to speak, and points in which transitions between speakers can occur without defying social conventions are termed \emph{transition-relevance places}\index{transition-relevance place} (TRP).%
\footnote{%
	These terms were coined by \textcite{sacks.h+:1974:turn-taking}.
}
Interruption may occur when a transition takes place outside of a TRP%
\footnote{%
	Note that backchanneling\index{backchannel} is not interruption, as it does not interrupt the speaker’s turn \parencite{peters.p+:2015:turn-backchannels}.
	At any rate, our corpus has no backchanneling, as is customary in written texts.
}.
The forces which structure the interactional turn-taking ‘ritual’ of conversation are not only linguistic but are extralinguistic as well.
One such force is \emph{face} \parencite{goffman.e:1967:face-work}, which people strive to maintain.
Interruption of another interlocutor may constitute a \emph{face-threatening act} (FTA; \cite{brown.p+:1987:politeness}).
See \textcite{schegloff.e.a:2001:conduct-interaction,bennett.a:1978:interruptions} on the topic of interruption in (English-language) conversation.%
\footnote{%
	A cursory examination of a recorded conversation from a Welsh-language corpus (\C{Corpws Siarad Bangor}, \cite{deuchar.m+:2014:bilingual-corpora}) has been conducted in \textcite{ronen.j:2019:interruption}, which focusses on \emph{where} the interlocutor is interrupted (specifically, whether the interruption keeps high-juncture syntactic structures intact or not) and differentiating \emph{collaboration} \parencite{sacks.h:1995:lectures-1.III.7,sacks.h:1995:lectures-2.I.5,fais.l:1994:conversation-collaboration}~— also referred to as \emph{joint production} and \emph{co-production} in the literature~— and genuine interruption.
}

Back to our written-language literary corpus, the author utilises a set of literary-linguistic techniques and conventions to depict cases of interruption and resumption.
Being conversation-organising structures, they operate differently than regular, unmarked turn-taking in dialogue; this is expressed in particular with regard to the use of paragraph breaks and the distinction between preposed or intraposed~/ postposed quotative indexes, which normally clearly distinguish between QI1 and QI2.
% Interruptions and resumptions seem to use QI1 or QI2 \emph{forms} without \emph{functional} distinction.

{}\index{phrasal verbs|(}
Phrasal verbs are used as the nuclei of most of the constructions in question%
\footnote{%
	See \textcite{klonowska-listewnik.m:2018:phrasal-verbs}, in particular
	§~3.3.2.3 for \C{mynd rhag-},
	§~3.3.2.5 for \C{(rhoi) i mewn},
	§~4.5.2 for \C{mynd ymlaen},
	§~4.5.5 for \C{torri ar draws}, and
	§~4.5.7 for \C{torri i mewn}.
}
For signalling interruption, the most common structure is \C{\gl{torri}{break.\Inf{}} \gl{ar}{on} \gl{draws}{cross}}\index[welsh]{torri ar draws@\igl{torri ar draws}{interrupt}}, where the final component (\C{\gl{traws}{cross}} in an isolated, non-mutated form) is in a possessive construction with a reference to the character who is said to be interrupted; e.g.\ \C{\gl{torri}{break.\Inf{}} \gl{ar}{on} \gl{ei}{\eif{}} \gl{thraws}{cross}}[to interrupt her] or \C{\gl{torri}{break.\Inf{}} \gl{ar}{on} \gl{draws}{cross} \Pn{}}[to interrupt \Pn{}].
Similarly \C{\gl{torri}{break.\Inf{}} \gl{i}{to} \gl{mewn}{in}}\index[welsh]{torri i mewn@\igl{torri i mewn}{break in}} (without a complement; cf.\ English \E{to break in}) and \C{\gl{rhoi}{give.\Inf{}} \gl{pig}{beak} \gl{i}{to} \gl{mewn}{in}}\index[welsh]{rhoi pig i mewn@\igl{rhoi pig i mewn}{interrupt (\textasciitilde{}‘stick one’s nose into’)}} (with a possessive construction on \C{\gl{pig}{beak}}; cf.\ English \E{stick one’s nose into}) are used, but less often.
{}\index{phrasal verbs|)}

Plain \C{\gl{meddai}{\meddai{}}}\index[welsh]{meddai@\igl{meddai}{\meddai{}}} is also used, with multiple dots in the previous quote signalling it was not completed.
\begin{fragment}
	\caption{Overview of interrupted storytelling in \worktitle{Marwolaeth Stori}}%
	\label{frg:rd:interruptions:TG3}
	\footnotesize%
	\egaliterianbiling{%
		\Qquote{%
			‘\inlinecomment{naratif hir gan Dafydd Siôn}.
			Wedi eistedd am ryw ugain munud i chi, ’r oedd hi tua naw erbyn hyn, mi gychwynnais adre\hlbegin{}.....’
		}
	}
	{%
		\Qquote{%
			‘\inlinecomment{a long narrative by Dafydd Siôn}.
			After I sat for some twenty minutes, you see, it was about nine by this time, I started off home\hlbegin{}…’
		}
	}
	\egaliterianbiling{%
		\Qquote{‘R ydach chi wedi anghofio dweud sut oedd Gwen,’} \Qnucl{meddai} \Qsp{Begw}.\hlend{}
	}
	{%
		\Qquote{‘You’ve forgotten to say how Gwen was,’} \Qsp{Begw} \Qnucl{said}.\hlend{}
	}
	\egaliterianbiling{%
		\Qquote{‘Hisht,’} \Qnucl{oddi wrth} \Qsp{ei thad}.
	}
	{%
		\Qquote{‘Hush,’} \Qnucl{from} \Qsp{her father}.
	}
	\egaliterianbiling{%
		Chwerthin oddi wrth Bilw, a Modryb Sara’n gwenu. \Qnucl{Aeth} \Qsp{Dafydd Siôn} \Qnucl{ymlaen}:
	}
	{%
		Laughter from Bilw, with Aunt Sara smiling. \Qsp{Dafydd Siôn} \Qnucl{went on}:
	}
	\egaliterianbiling{%
		\Qquote{%
			‘O, do wir, mi ddar’u imi anghofio.
			\inlinecomment{naratif hir gan Dafydd Siôn}
			Wel, mi gerddais ac mi gerddais, ’r oeddwn i’n meddwl ’mod i wedi cerdded am oriau heb gyrraedd glan yn unman\hlbegin{}.....’
		}
	}
	{%
		\Qquote{%
			‘Oh, yes indeed, I did forget.
			\inlinecomment{a long narrative by DS}.
			Well, I walked and I walked, I thought I’d been walking for hours without reaching a bankside anywhere\hlbegin{}…’
		}
	}
	\egaliterianbiling{%
		\Qquote{‘Dyma fi’n clywed sŵn meddal ffrwd,’} \Qnucl{meddai} \Qsp{Begw}.\hlend{}
	}
	{%
		\Qquote{‘Then I hear the soft sound of a brook,’} \Qsp{Begw} \Qnucl{said}.\hlend{}
	}
	\egaliterianbiling{%
		\Qquote{%
			‘Paid ti â mynd o ’mlaen i rŵan; ie, sŵn meddal ffrwd, a dyma fi’n gweld bod siawns imi wybod p’run ’te ’nghefn i, ynte’ f’wyneb i oedd tuag adref.
			\inlinecomment{DS yn mynd ymlaen wrth y naratif}
			— rŵan, Begw, y fi sydd i ddweud hyn, nid ychdi —
			\inlinecomment{parth olaf y naratif}
		}
	}
	{%
		\Qquote{%
			‘Don’t get ahead of me now; yes, the soft sound of a brook, and then I see there’s a chance for me to know whether I had my back, or my face, towards home.
			\inlinecomment{DS continues the narrative}
			—now Begw, I’m the one telling this, not you—
			\inlinecomment{the final part of the narrative}
		}
	}
	\egaliterianbiling{%
		\symbolglyph{⋮}
	}
	{%
		\symbolglyph{⋮}
	}
	\egaliterianbiling{%
		\Qquote{‘Hwda, dyma chdi,’} \Qnucl{meddai} \Qsp{o}, \Qquote{‘am beidio â thorri i mewn yn rhy aml.’}
	}
	{%
		\Qquote{‘Here you are, take it,’} \Qsp{he} \Qnucl{said}, \Qquote{‘for not breaking in too often.’}
	}

	\explanationundertable{}\footnotesize%
	—~Source: \exsrctyyg{3}{33}
\end{fragment}
This is demonstrated twice in \cref{frg:rd:interruptions:TG3}

A similar signalling of interruption through graphic means~— albeit but more with more explicit linguistic indication~— can be seen in \exfullref{ex:rd:interruptions:VD-19=60}.
The first quote ends with a dash (\grapheme{—}), which functions similarly to the ellipsis mark (\grapheme{.....}), and the nominal predication construction \C{\gl{oedd}{\copoedd{}} \gl{ei}{\eim{}} \gl{eiriau}{word.\Pl{}} \gl{cyntaf}{\cyntaf{}}}[were his first words], as discussed in \exfullref{ex:rd:internal:nucl:nominal:oedd ei eiriau cyntaf} (\cref{sec:rd:internal:nucl:nominal} above).
Then a clear meta reference is made: Winni’s father does not get to finish his slur since Begw’s father breaks in and stops him.

\preex{}\ex<ex:rd:interruptions:VD-19=60>
	\gloss
	{%
		\inlinecomment{Tad Winni:}
		\Qquote{‘Tyd} \Qquote{o’na} \Qquote{’r} \Qquote{—’} \Qnucl{oedd} \Qsp{ei} \Qnucl{eiriau} \Qnucl{cyntaf}. \nogloss{\hlbegin{}} @ Ond \Qmod{cyn} \Qmod{iddo} \Qmod{orffen} \Qmod{ei} \Qmod{frawddeg} yr oedd \Qsp{tad} \Qsp{Begw} \Qnucl{wedi} \Qnucl{torri} \Qnucl{ar} \Qnucl{ei} \Qnucl{draws}\hlend{}.
		¶
		\Qquote{‘Dim} \Qquote{o} \Qquote{dy} \Qquote{regfeydd} \Qquote{di} \Qquote{yn} \Qquote{y} \Qquote{fan} \Qquote{’ma,} \Qquote{Twm.’}
	}
	{%
		{}
		come.\Imp{}.\Ssg{} of-\ynaloc{} \Voc{} {} \copoedd{} \eim{} word.\Pl{} first but before to.\Tsg{}.\M{} \Len{}\textbackslash{}finish.\Inf{} \eim{} sentence \yr{} \oedd{} father \Pn{} after break on \eim{} over
		{}
		\dim{nothing} of \dy{} swearword.\Pl{} \Ssg{} \ynA{} \Def{} place \ymadem{} \Pn{}
	}
	{%
		\inlinecomment{Winni’s father:}
		\Qquote{‘Come out of there, you—,’} \Qnucl{were} \Qsp{his} \Qnucl{first} \Qnucl{words}. But \Qmod{before he’d finished his sentence} \Qsp{Begw’s father} \Qnucl{had interrupted him}.
		¶
		\Qquote{‘None} \Qquote{of} \Qquote{your} \Qquote{swearwords} \Qquote{here,} \Qquote{Twm.’}
	}
	{\exsrctyyg{6}{75}}
\postex{}\xe

Another case of unfinished slur can be seen in \exfullref{sec:rd:interruptions:y cena drwg}.
Here Winni talks to her father, about to insult him with a harsher slur%
\footnote{%
	\C{uffar(n) o \NP{}}[one hell of a \NP{}] perhaps \parencite[§~uffern]{gpc:arlein}.
}
and than comes down from this one to the milder \C{\gl{y}{\Voc{}} \gl{cena}{rascal} \gl{drwg}{bad}}[\textasciitilde{}you rascal].
A major difference is that here there is no external interruption by another interlocutor: it is the speaker who changes her mind midsentence.
From a literary perspective, this ability to overcome her unhappy upbringing is indicative of the change that the character of Winni has undergone from when the readers encounter her for the first time (in the fourth story of \TG{}) to this last story.

\preex{}\ex<sec:rd:interruptions:y cena drwg>
	\gloss
	{%
		\inlinecomment{Tad Winni wrth Winni:} \Qquote{‘Pwy} \Qquote{sydd} \Qquote{wedi} \Qquote{dy} \Qquote{fagu} \Qquote{di} \Qquote{tybed?’}
		¶
		\inlinecomment{Winni wrth ei thad:} \Qquote{‘Nid} \Qquote{ychi} \Qquote{yr} \Qquote{u…} \Qquote{y} \Qquote{cena} \Qquote{drwg.’}
	}
	{%
		{} who \sydd{} after \dy{} rear.\Inf{} \Ssg{} \tybed{}
		{}
		{} \Neg{} \yPro{}.\chi{} \Voc{} … \Voc{} rascal bad
	}
	{%
		\inlinecomment{Winni’s father to Winni:} \Qquote{‘Who brought you up I wonder?’}
		¶
		\inlinecomment{Winni to her father:} \notsic{}\Qquote{‘Not you, you h…, you old misery.’}
	}
	{\exsrchd{6}{57}}
\xe\postex{}

\Exfullref{ex:rd:interruptions:VD-19=60} is not the only time Winni’s father uses rude language is interrupted by a friendly adult who is protective of Winni.
In \exfullref{ex:rd:interruptions:Dim o’r iaith yna} he manages to finish the expletive and the sentence (but not the turn), and then Mr Hughes stops in a similar manner to \C{‘\gl{Dim}{\dim{nothing}} \gl{o}{of} \gl{dy}{\dy{}} \gl{regfeydd}{swearword.\Pl{}} \gl{di}{\Ssg{}} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{fan}{place} \gl{’ma}{\ymadem{}}, \gl{Twm}{\Pn{}}’}[\,“None of your swearwords here, Twm.”\,] (both clauses have similar syntactic structure).
\revsimple{2}{Note} that here there is no explicit reference to interruption, and a QI1 pattern is used.

\preex{}\ex<ex:rd:interruptions:Dim o’r iaith yna>
	\gloss
	{%
		\inlinecomment{Tad Winni:}
		\Qquote{‘Mi} \Qquote{â} \Qquote{i} \Qquote{â} \Qquote{chdi} \Qquote{rwan,} \Qquote{myn} \Qquote{cythral,} \Qquote{mae} \Qquote{gen} \Qquote{i} \Qquote{hawl,} \Qquote{’rwyt} \Qquote{ti} \Qquote{dan} \Qquote{oed.’}
		¶
		\Qquote{‘Dim} \Qquote{o’r} \Qquote{iaith} \Qquote{yna’,} \nogloss{\hlbegin{}} @ \Qnucl{torrodd} \Qsp{Mr.} \Qsp{Hughes} \Qnucl{ar} \Qnucl{ei} \Qnucl{draws}\hlend{}.
		¶
		\inlinecomment{naratif}
	}
	{%
		{}
		\mi{} go.\Prs{}.\Fsg{} \Fsg{} with \chdi{} now by devil \mae{} by \Fsg{} right \yr{}-\wyt{} \ti{} under age
		{}
		\dim{nothing} of-\Def{} language \ynadem{} break.\odd{} Mr \Pn{} on \eif{} cross
	}
	{%
		\inlinecomment{Winni’s father:}
		\notsic{}\Qquote{‘I’m taking you with me now, you devil, I’ve got the right, you’re under age.’}
		¶
		\Qquote{‘None of that language,’} \highlight{\Qnucl{broke in} \Qsp{Mr.\ Hughes}}.
		¶
		\inlinecomment{narrative paragraph}
	}
	{\exsrchd{2}{21}}
\xe\postex{}

On several occasions Winni’s uncouth language causes uneasiness to the other people present and Begw’s parents Elin and John Gruffydd step in and change the subject (exx.~\getfullref{ex:rd:interruptions:Meddyliodd EG y byddai’n well}–\getfullref{ex:rd:interruptions:meddai mam Begw gan dorri ar ei thraws}).
When the said uneasiness or their decision to change the subject matter at that point are referred to explicitly, the quotative index precedes the quote in a narrative paragraph (QI2; exx.~\getfullref{ex:rd:interruptions:Meddyliodd EG y byddai’n well}–\getfullref{ex:rd:interruptions:Torrodd Elin Gruffydd ar draws Winni}), but when it is not, it incorporated to the unmarked form of dialogue (QI1; \exfullref{ex:rd:interruptions:meddai mam Begw gan dorri ar ei thraws}).
There are not enough examples to make conclusive statements, but judging from these examples it seems that the source of the difference may lie in the content: talking about sexual matters and the body (exx.~\getfullref{ex:rd:interruptions:Meddyliodd EG y byddai’n well}–\getfullref{ex:rd:interruptions:Torrodd Elin Gruffydd ar draws Winni}) causes more uneasiness than stating the house of the hosting Gruffydd family is clean and Winni’s house is \C{\gl{fel}{like} \gl{stabal}{stable}}[like a stable] (\exfullref{ex:rd:interruptions:meddai mam Begw gan dorri ar ei thraws}).

\preex{}\ex<ex:rd:interruptions:Meddyliodd EG y byddai’n well>
	\gloss
	{%
		\Qquote{‘\ellipsis{}.} \Qquote{Ond} \Qquote{biti} \Qquote{na} \Qquote{fasach} \Qquote{chi’n} \Qquote{’i} \Qquote{gweld} \Qquote{nhw} \Qquote{hyd} \Qquote{y} \Qquote{mynydd} \Qquote{yna} \Qquote{yn} \Qquote{y} \Qquote{nos.’}
		¶
		Meddyliodd {Elin Gruffydd} y byddai’n well \nogloss{\hlbegin{}} @ \Qsp{iddi} \Qnucl{dorri} \Qnucl{ar} \Qnucl{ei} \Qnucl{thraws} \Qmod{yn} \Qmod{y} \Qmod{fan} \Qmod{yma}\hlend{}.
		¶
		\Qquote{‘Pryd} \Qquote{y} \Qquote{byddwch} \Qquote{chi} \Qquote{yn} \Qquote{gadael} \Qquote{yr} \Qquote{ysgol,} \Qquote{Winni?’}
	}
	{%
		{} but pity \Neg{}.\Nmlz{} \buasech{} \chi{}-\ynD{} \eu{} see.\Inf{} \nhw{} over \Def{} mountain \ynadem{} \ynA{} \Def{} night
		{}
		think.\odd{} \Pn{} \Nmlz{} \byddai{}-\ynC{} good.\Cmp{} \iddi{} \Len{}\textbackslash{}break on \eif{} cross \ynA{} \Def{} place \ymadem{}
		{}
		time \yrRel{} \byddwchFut{} \chi{} \ynD{} leave.\Inf{} \Def{} school \Pn{}
	}
	{%
		\Qquote{‘\ellipsis{}. But it’s a pity you couldn’t see them up on that mountain at night.’}
		¶
		Elin Gruffydd thought that \highlight{\Qsp{she}’d better \Qnucl{interrupt} \Qmod{at this point}}.
		¶
		\Qquote{‘When} \Qquote{will} \Qquote{you} \Qquote{be} \Qquote{leaving} \Qquote{school,} \Qquote{Winni?’}
	}
	{\exsrctyyg{5}{60}}
\xe\postex{}

\preex{}\ex
	\gloss
	{%
		\Qquote{‘\ellipsis{};}
		\Qquote{a’r} \Qquote{amser} \Qquote{hwnnw} \Qquote{yr} \Qquote{oedd} \Qquote{o’n} \Qquote{edrach} \Qquote{yr} \Qquote{un} \Qquote{fath} \Qquote{â} \Qquote{rhyw} \Qquote{ddelw} \Qquote{gerfiedig} \Qquote{wedi} \Qquote{cysgu.’}
		¶
		Yr oedd yn rhaid iddynt chwerthin rhag eu gwaethaf.
		¶
		Y gair ‘gerfiedig’ a wnaeth i {Elin Gruffydd} newid y sgwrs.
		¶
		\Qquote{‘Fyddwch} \Qquote{chi’n} \Qquote{mynd} \Qquote{i’r} \Qquote{Ysgol} \Qquote{Sul,} \Qquote{Winni?’}
	}
	{%
		{}
		% \wedyn{} \mi{} \byddai{}-\ynD{} go.\Inf{} \ynC{} quiet and \ynD{} close.\Inf{}-\eim{} eye.\Pl{}
		and-\Def{} time \hwnnw{} \yr{} \oedd{} \ef{}-\ynD{} look.\Inf{} \Def{} one kind with \rhyw{} image sculptured after sleep.\Inf{}
		{}
		\yr{} \oedd{} \ynC{} need \iddynt{} laugh.\Inf{} \Prep{} \eu{} bad.\Sup{}
		{}
		\Def{} word sculptured \aRel{} do.\odd{} to \Pn{} change.\Inf{} \Def{} conversation
		{}
		\Q{}\textbackslash{}\byddwchFut{} \chi{}-\ynD{} mynd.\Inf{} to-\Def{} school Sunday \Pn{}
	}
	{%
		\Qquote{‘\ellipsis{}, then he would look just like a carved image asleep.’}
		¶
		They had to laugh in spite of themselves.
		The word ‘carved’ made Elin Gruffydd change the conversation.
		¶
		\Qquote{‘Do you go to Sunday School Winni?’}
	}
	{\exsrchd{6}{62}}
\xe\postex{}

\preex{}\ex<ex:rd:interruptions:Torrodd Elin Gruffydd ar draws Winni>
	\gloss
	{%
		\Qquote{‘’Be} \Qquote{ddyliach} \Qquote{chi,} \Qquote{mistar} \Qquote{sy’n} \Qquote{molchi} \Qquote{mistras} \Qquote{drosti} \Qquote{yn} \Qquote{y} \Qquote{bath} \Qquote{yma;} \Qquote{mi} \Qquote{fasa} \Qquote{gen} \Qquote{i} \Qquote{gywilydd} \Qquote{i} \Qquote{neb} \Qquote{fy} \Qquote{ngweld} \Qquote{i’n} \Qquote{noethlymun} \Qquote{felly…’}
		¶
		Dechreuodd {Elin Gruffydd} chwysu ac edrych ar Robin, a chymerodd rhyw nerfusrwydd ofnus a disgwylgar afael yn Begw. \Qnucl{Torrodd} \Qsp{Elin Gruffydd} \Qnucl{ar} \Qnucl{draws} \Qnucl{Winni}.
		¶
		\Qquote{‘Maen’} \Qquote{nhw’n} \Qquote{dweud} \Qquote{i} \Qquote{mi} \Qquote{mai} \Qquote{dynes} \Qquote{ddelicat} \Qquote{iawn} \Qquote{ydy’ch} \Qquote{mistres,} \Qquote{yn} \Qquote{cael} \Qquote{lot} \Qquote{o} \Qquote{gricymalau,} \Qquote{dyna} \Qquote{pam} \Qquote{reit} \Qquote{siŵr.}
		¶
		\Qnucl{Rhoes} \Qsp{John Gruffydd} \Qsp{ei} \Qnucl{big} \Qnucl{i} \Qnucl{mewn}.
		¶
		\Qquote{‘Oedd} \Qquote{yna} \Qquote{lawer} \Qquote{yn} \Qquote{y} \Qquote{frêc} \Qquote{neithiwr?’}
	}
	{%
		what think.\Impf{}.\Spl{} \chi{} master \sy{}-\ynD{} wash mistress over.\Adv{} \ynA{} \Def{} bath \ymadem{} \mi{} \basai{} by \Fsg{} shame to anyone \fy{} see.\Inf{} \Fsg{}-\ynC{} stark\_naked \felly{}
		{}
		begin.\odd{} \Pn{} sweat.\Inf{} and look.\Inf{} on \Pn{} and take.\odd{} \rhyw{} nervousness fearful and watchful hold \yn{} \Pn{} break.\odd{} \Pn{} on cross \Pn{}
		{}
		\maent{} \nhw{}-\ynD{} \dweud{}.\Inf{} to \Fsg{} \mai{} woman delicate very \copydy{}-\eich{} mistress \ynD{} get.\Inf{} lot of rheumatism \dyna{} why \textit{right} \textit{sure}
		{}
		give.\odd{} \Pn{} \eim{} beak to in
		{}
		\oedd{} \yna{} many \ynA{} \Def{} brake last\_night
	}
	{%
		\Qquote{‘What do you think, the master washes the mistress in the bath there, I’d be ashamed for anyone to see me stark naked like that.’}
		\notsic{}¶
		Elin Gruffydd began to perspire, and look at Robin, and a nervous embarrassment came over Begw.
		\Qsp{Elin Gruffydd} \Qnucl{cut across Winni}.
		¶
		\Qquote{‘They say that your mistress is a very delicate woman, and has a lot of rheumatism, that’s why, I’m sure.’}
		¶
		\Qsp{John Gruffydd} \Qnucl{put} \Qsp{his} \Qnucl{spoke in}.
		¶
		\Qquote{‘Where there many in the brake last night?’}
	}
	{\exsrchd{6}{61}}
\xe\postex{}

\preex{}\ex<ex:rd:interruptions:meddai mam Begw gan dorri ar ei thraws>
	\gloss
	{%
		\inlinecomment{Winni:} \Qquote{‘Dew,} \Qquote{mae} \Qquote{gynnoch} \Qquote{chi} \Qquote{le} \Qquote{glân} \Qquote{yma,’} \Qnucl{meddai}. \Qquote{‘Mae'n} \Qquote{tŷ} \Qquote{ni} \Qquote{fel} \Qquote{stabal.’}
		¶
		\Qquote{‘Well} \Qquote{i} \Qquote{chi} \Qquote{ddŵad} \Qquote{at} \Qquote{y} \Qquote{bwrdd} \Qquote{rŵan,’} \Qnucl{meddai} \Qsp{mam} \Qsp{Begw} \Qmod{gan} \Qmod{dorri} \Qmod{ar} \Qmod{ei} \Qmod{thraws}.
	}
	{%
		{} \Interj{} \mae{} by-\Spl{} \chi{} place clean \yma{} \meddai{} \mae{}-\ein{} house \Fpl{} like stable
		{}
		\Len{}\textbackslash{}good.\Cmp{} to \chi{} \Len{}\textbackslash{}come.\Inf{} to \Def{} table now \meddai{} mother \Pn{} by break.\Inf{} on \eif{} cross
	}
	{%
		\Qquote{‘God, you have a clean place here,’} \Qsp{she} \Qnucl{said}. \Qquote{‘Our house is like a stable.’}
		¶
		\Qquote{‘You’d better come to the table now,’} \Qsp{Begw’s mother} \Qnucl{said}, \Qmod{interrupting her}.
	}
	{\exsrctyyg{5}{57}}
\xe\postex{}

While orderly turn-taking is signalled by sequencing dialogue paragraph, each constituting a turn, interruptions and resumptions call for different signalling of the literary dialogue organisation through linguistic means.
\begin{fragment}
	\caption{Dramatic exchange in \worktitle{Pryder Morwyn}}%
	\label{frg:rd:interruptions:HD1}
	\SingleSpacing{}%
	\footnotesize%
	\egaliterianbiling{%
		\inlinecomment{Mrs.\ Hughes (y feistres):} \Qquote{‘A beth ddywedsoch chi?’}
	}
	{%
		\inlinecomment{Mrs Hughes (the mistress):} \Qquote{‘And what did you say?’}
	}
	\egaliterianbiling{%
		\inlinecomment{Winni:} \Qquote{‘Mi tafodais o yn iawn a dweud wrtho am fynd adre at ’i fam. Mi wnes bob dim ond ei regi.’}
	}
	{%
		\inlinecomment{Winni}: \Qquote{‘I gave him a proper telling off and told him to go home to his mother. I did everything except swear at him.’}
	}
	\egaliterianbiling{%
		Gwenodd y meistr.
		\Qnucl{Aeth} \Qsp{Mrs. Hughes} \Qnucl{ymlaen}.
	}
	{%
		The master smiled.
		\Qsp{Mrs.\ Hughes} \Qnucl{went on}.
	}
	\egaliterianbiling{%
		\Qquote{‘Oeddach chi wedi’i weld o o’r blaen? Mae’n edrach yn beth rhyfedd iawn i mi fod o’n gofyn hynna os \sic{dynna}’r tro cynta i chi ei weld o.’}
	}
	{%
		\Qquote{‘Had you seen him before? It seems strange to me that he should ask that if it was the first time you’d seen him.’}
	}
	\egaliterianbiling{%
		\inlinecomment{Winni:} \Qquote{‘Ia’n wir, Mrs.\ Hughes.’}
	}
	{%
		\inlinecomment{Winni:} \Qquote{‘Yes, indeed Mrs.\ Hughes.’}
	}
	\egaliterianbiling{%
		\Qnucl{Torrodd} \Qsp{y meistr} \Qnucl{i mewn}.
	}
	{%
		\Qsp{The master} \Qnucl{broke in}.
	}
	\egaliterianbiling{%
		\Qquote{%
			‘Mi alla i’n hawdd gredu hynny.
			’Rydw i’n nabod ’i deulu o.
			Rêl ciaridýms.’
		}
	}
	{%
		\Qquote{%
			‘I can easily believe that, I know his family.
			Real scum.’
		}
	}
	\egaliterianbiling{%
		\Qnucl{Aeth} \Qsp{y feistres} \Qnucl{ymlaen}.
	}
	{%
		\Qsp{The mistress} \Qnucl{went on}.
	}
	\egaliterianbiling{%
		\Qquote{‘Ydach chi’n gweld, Winni, mi alla i’n hawdd ych gyrru chi adre oddi yma.’}
	}
	{%
		\Qquote{‘You see Winni. I could easily send you home from here.’}
	}
	\egaliterianbiling{%
		Ar hyn, torrodd Winni i grio.
	}
	{%
		At this Winni burst into tears.
	}
	\egaliterianbiling{%
		\Qquote{%
			‘Ond wir, Mrs.\ Hughes, ’doedd gen i ddim o’r help\sic{,}
			A plis, peidiwch â ngyrru fi adre.
			Mi rydd fy nhad gweir i mi, ac ella fy nhroi dros y drws.
			Mi ’rydw i mor hapus yma efo Robert bach.’
		}
	}
	{%
		\Qquote{%
			‘But honestly, Mrs.\ Hughes, I couldn’t help it.
			And please don’t send me home.
			My father would give me a hiding and perhaps throw me out.
			I’m so happy here with little Robert.’
		}
	}
	\egaliterianbiling{%
		\Qnucl{Meddai}\Qsp{’r meistr}
	}
	{%
		\Qsp{The master} \Qnucl{said}.
	}
	\egaliterianbiling{%
		\Qquote{%
			‘Peidiwch â bod yn rhy gas wrthi, Mary.
			% ’Rydach chi’n gwybod bod Winni’n forwyn dda, yn well o lawer na’r rhai sydd wedi bod yma.
			% Colled fawr fyddai ei cholli.’
			\ellipsis{}’
		}
	}
	{%
		\Qquote{%
			‘Don’t be too cross with her Mary.
			% You know Winni’s a good maid, a lot better than the others who’ve been here.
			% It would be a great pity to lose her.’
			\ellipsis{}’
		}
	}
\end{fragment}
\Cref{frg:rd:interruptions:HD1} is an interesting case, which has an almost theatrical feel to it, achieved by the alternating paragraphs (take note of the alternating black and grey of the annotation scheme; \cref{sec:rd:intro:annotation}).

As demonstrated in this chapter, the hundreds of instances of reported direct speech in the narrative portions of the corpus fall into three syntactic patterns~— QI1, QI2 and QI3~— with structural micro-syntactic signifiers and macro-syntactic signifieds.
One example~— \exfullref{ex:rd:interruptions:Dechreuodd Winni weiddi}~— is exceptional, as it does not belong in any of the three.
Two features are unusual:
One is that begins with with a nucleus (\C{\gl{Dechreuodd}{begin.\odd{}} \gl{Winni}{\Pn{}} \gl{weiddi}{shout.\Inf{}}}[Winni began shouting]) that is followed by a quote, both occupying the same paragraph.
The other is that the next paragraph (\C{Ac yr oedd~… gan gŵn}[And there was~… by dogs]) is concatenated by a conjunction (\C{\gl{ac}{and}}) and seems to continue it, as if they make a single compound sentence.
This is neither QI1, nor QI2 nor QI3.
This anomalous structure might have to do with the fact that the turns are incomplete:
\C{\gl{Rhoswch}{wait.\Imp{}.\Spl{}} \gl{imi}{\imi{}} \gl{orffen}{finish.\Inf{}}}[Let me finish] that opens the next quote suggests that Winni’s shouts interrupted Begw’s mother, and the inchoative \C{\gl{Dechreuodd}{begin.\odd{}}}[(She) began] suggest an unfinished turn.
The existence of such an exception does not weaken the theory proposed in this chapter.
In fact, this is an \foreign{exceptio firmans regulam}.
Language \emph{is} systematic in the sense it is governed by systematic structural interrelationships, but this does not means it is rigid and users of the language are not free to make use of rare or extraordinary constructions, \foreign{a fortiori} when language is made use by authors, who craft literature%
\footnote{%
	Cf.\ \C{Crefft y Llenor}[The Craft of Author] \parencite{jones.j:1977:crefft-y-llenor}, a series of lectures printed in a volume that discusses the essence of literature and deals specifically with the works of Kate Roberts and R.\ W.\ Parry.
}.

\preex{}\ex<ex:rd:interruptions:Dechreuodd Winni weiddi>
	\gloss
	{%
		\Qquote{‘Rŵan,’} \Qnucl{meddai} \Qsp{hi} \Qmod{yn} \Qmod{dra} \Qmod{awdurdodol}, \Qquote{‘ewch} \Qquote{chi} \Qquote{adre,} \Qquote{Tomos,} \Qquote{i} \Qquote{oeri} \Qquote{tipyn} \Qquote{ar} \Qquote{eich} \Qquote{tempar.’}
		¶
		\Qnucl{Dechreuodd} \Qsp{Winni} \Qnucl{weiddi}, \Qquote{‘’D} \Qquote{ydw} \Qquote{i} \Qquote{ddim} \Qquote{am} \Qquote{fynd} \Qquote{efo} \Qquote{fo,} \Qquote{’d} \Qquote{ydw} \Qquote{i} \Qquote{ddim} \Qquote{am} \Qquote{fynd} \Qquote{adre.’}
		¶
		Ac yr oedd golwg arni fel anifail wedi ei ddal {ar ôl} ei goethi drwy’r dydd gan gŵn.
		¶
		\Qquote{‘Rhoswch} \Qquote{imi} \Qquote{orffen,} \Qquote{Winni,’} \Qnucl{ebe}\Qsp{’r} \Qsp{fam}. \Qquote{‘Mi} \Qquote{gewch} \Qquote{chi} \Qquote{aros} \Qquote{yma} \Qquote{heno,} \ellipsis{}’
	}
	{%
		now \meddai{} \hi{} \ynB{} very authoritative go.\Imp{}.\Spl{} \chi{} home.\Adv{} \Pn{} to cool.\Inf{} a\_little on \eich{} temper
		{}
		begin.\odd{} \Pn{} shout.\Inf{} \Neg{} \ydw{} \Fsg{} \Neg{} about go.\Inf{} with \ef{} \Neg{} \ydw{} \Fsg{} \Neg{} about go home.\Adv{}
		{}
		and \yr{} \oedd{} look on.\Tsg{}.\F{} like animal(\M{}) after \eim{} catch after \eim{} pursue.\Inf{} through-\Def{} day by dog.\Pl{}
		{}
		wait.\Imp{}.\Spl{} to.\Fsg{} finish.\Inf{} \Pn{} \ebe{}-\Def{} mother \mi{} get.\Prs{}.\Spl{} \chi{} stay.\Inf{} \ymaloc{} tonight
	}
	{%
		\Qquote{‘Now,’} \Qsp{she} \inlinecomment{Begw’s mother} \Qnucl{said}, \Qmod{very authoritatively}, \Qquote{‘go home, Tomos, to cool off your temper a bit.’}
		¶
		\Qsp{Winni} \Qnucl{began shouting}, \Qquote{‘I don’t want to go with him, I don’t want to go home.’}
		¶
		And there was a look on her like an animal caught after being harried all day by dogs.
		¶
		\Qquote{‘Let me finish, Winni,’} \Qsp{Begw’s mother} \Qnucl{said}. \Qquote{‘You may stay here tonight, \ellipsis{}’}
	}
	{\exsrctyyg{6}{76}}
\xe\postex{}



\subsection{Resumption}%
\label{sec:rd:interruptions:resumption}

For resumption after a pause, preposed \C{\gl{aeth}{go.\odd{}} \gl{ymlaen}{\ymlaen{}}\index[welsh]{mynd ymlaen@\igl{mynd ymlaen}{go on}}%
\footnote{%
	The infinitival dictionary form is \C{mynd (ymlaen)}; \C{mynd}[go] is an irregular, suppletive verb.
}
}[went on] is the most prevalent form; see \cref{frg:rd:interruptions:TG3} (\C{\gl{Aeth}{go.\odd{}} \gl{Dafydd Siôn}{\Pn{}} \gl{ymlaen}{\ymlaen{}}}) and \cref{frg:rd:interruptions:HD1} (\C{\gl{Aeth}{go.\odd{}} \gl{Mrs.}{Mrs}\ \gl{Hughes}{\Pn{}}~/ \gl{y}{\Def{}} \gl{feistres}{mistress} \gl{ymlaen}{\ymlaen{}}}) above.
See also \cref{app:rd:zero:grug,app:rd:zero:ymwelydd}, where it occurs both in preposed QI2 form (\exfullref{ex:rd:interruptions:resumption:mynd phrasal:QI2-TG5}) and intraposed QI1 form (\exfullref{ex:rd:interruptions:resumption:mynd phrasal:QI1}), without a clear functional distinction between the two (in all cases the resumption is after a short narrative or descriptive break that stands between quotes of the same speaker).
\C{aeth ymlaen} and similar construction marks the textual cohesion between the quotes before and after the break; see \textcite[p.~265~ff.]{halliday.m+:1976:cohesion}.

\preex{}\ex<ex:rd:interruptions:resumption:mynd phrasal:QI2-TG5>
		\gloss
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Chwarddodd Winni, am y tro cyntaf er pan gyraeddasai.
			¶
			\Qnucl{Aeth} \Qnucl{ymlaen} \Qmod{wedyn:}
			¶
			\Qquote{‘\Quote{}’}
		}
		{%
			{} {}
			{}
			laugh.\odd{} \Pn{} about \Def{} time \cyntaf{} since when arrive.\Plup{}.\Tsg{}
			{}
			go.\odd{} \ymlaen{} \wedyn{}
		}
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Winni laughed, for the first time since she’d arrived.
			¶
			\notsic{}\Qnucl{She} \Qnucl{went on} \Qmod{after that:}
			¶
			\Qquote{‘\Quote{}’}
		}
		{\exsrctyyg{5}{60}}
\xe\postex{}

\preex{}\ex<ex:rd:interruptions:resumption:mynd phrasal:QI1>
		\gloss
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Rhythai Begw arni gydag edmygedd, a'r fam gyda thosturi.
			¶
			\Qquote{‘\Quote{},’} \Qnucl{aeth} \Qsp{Winni} \Qnucl{ymlaen}, \Qquote{‘\Quote{}’}
		}
		{%
			{} {}
			{}
			stare.\ai{} \Pn{} \arni{} with admiration a-\Def{} mother with compassion
			{}
			{} go.\odd{} \Pn{} \ymlaen{}
		}
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Begw was staring at her with admiration, and her mother with compassion.
			¶
			\Qquote{‘\Quote{},’} Winni went on, \Qquote{‘\Quote{}’}
		}
		{\exsrctyyg{5}{61}}
\xe\postex{}

An interesting case can be seen in \exfullref{ex:rd:interruptions:resumption:mynd phrasal:QI2-TG4} (from \cref{app:rd:zero:grug} as well), where in addition to the preposed \C{\gl{Aeth}{go.\odd{}} \gl{Winni}{\Pn{}} \gl{ymlaen}{\ymlaen{}}}[Winni went on] we have an intraposed \C{\gl{meddai}{\meddai{}}, \gl{dan}{under} \gl{grensian}{grind.\Inf{}} \gl{ei}{\eif{}} \gl{dannedd}{tooth.\Pl{}}}.
A reasonable contributing factor for the intraposed quotative index is so it can be a structural anchor for the modification component in this position within the quote (see \exfullref{ex:rd:internal:mod:QI1:cvb:dan.dan grensian ei dannedd} in \cref{sec:rd:internal:mod:QI1:cvb:dan} above).
Whether it is preferable to analyse \C{\gl{Aeth}{go.\odd{}} \gl{Winni}{\Pn{}} \gl{ymlaen}{\ymlaen{}}}[Winni went on] as a quotative index or not depends on what is deemed less important:
failing to account the similarities of this resumptive formula to other cases where it acts as a quotative index, or
failing to account to a case there there are two quotative indexes for one dialogue paragraph (and thus this examples joins \exfullref{ex:rd:interruptions:Dechreuodd Winni weiddi} in not falling to any of the three patterns, adding up to two examples in total).
Both alternatives are not optimal, but both do not diminish the validity or the explanatory power of the theory.

\preex{}\ex<ex:rd:interruptions:resumption:mynd phrasal:QI2-TG4>
		\gloss
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Edrychai Mair i lawr ar ei ffrog heb ddweud dim, a Begw a holai. Cafodd ei brifo gan yr ateb olaf.
			¶
			Aeth Winni ymlaen.
			¶
			\Qquote{‘Tendiwch} \Qquote{chi,’} \Qnucl{meddai}, \Qmod{dan} \Qmod{grensian} \Qmod{ei} \Qmod{dannedd}, \Qquote{‘mi} \Qquote{fydda’} \Qquote{i’n} \Qquote{mynd} \Qquote{fel} \Qquote{yr} \Qquote{awal} \Qquote{ryw} \Qquote{ddiwrnod,} \Qquote{\ellipsis{}’}
		}
		{%
			{} {}
			{}
			look.\Impf{}.\Tsg{} \Pn{} to floor on \eif{} frock without \dweud{}.\Inf{} \dim{anything} and \Pn{} \aRel{} ask.\ai{} get.\odd{} \eif{} hurt.\Inf{} by \Def{} answer last
			{}
			go.\odd{} \Pn{} \ymlaen{}
			{}
			tend.\Imp{}.\Spl{} \chi{} \meddai{} under grind.\Inf{} \eif{} tooth.\Pl{} \mi{} \byddafFut{} \Fsg{}-\ynD{} go.\Inf{} like \Def{} light\_wind \Adv{}\textbackslash{}\rhyw{} day
		}
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Mair was looking down at her frock without saying anything, and it was Begw asked the questions. She was hurt by the last answer.
			¶
			Winni went on.
			¶
			\Qquote{‘Mind you,’} \Qsp{she} \Qnucl{said}, \Qmod{grinding her teeth}, \Qquote{‘I’ll be going like the breeze some day, \ellipsis{}’}
		}
		{\exsrctyyg{4}{44}}
\xe\postex{}

Another phrasal construction, \C{\gl{mynd}{go.\Inf{}} \gl{rhag-}{\Prep{}}} is used in a very similar manner (\exfullref{ex:rd:interruptions:resumption:mynd phrasal:mynd rhag-}).
\Textcite[§~3.3.2.3]{klonowska-listewnik.m:2018:phrasal-verbs} describes \C{mynd rhag-} as a variant of of \C{\gl{mynd}{go.\Inf{}} \gl{ymlaen}{\ymlaen{}}}.

\preex{}\ex<ex:rd:interruptions:resumption:mynd phrasal:mynd rhag->
		\gloss
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Dechreuodd Begw grynu, gan ofn yr âi'r rhegi yn waeth.
			¶
			\Qnucl{Aeth} \Qsp{Winni} \Qnucl{rhagddi}.
			¶
			\Qquote{‘\Quote{}’}
		}
		{%
			{} {}
			{}
			begin.\odd{} \Pn{} tremble.\Inf{} by fear \Nmlz{} go.\Impf{}.\Tsg{}-\Def{} swear.\Inf{} \ynC{} bad.\Cmp{}
			{}
			go.\odd{} \Pn{} \Prep{}.\Tsg{}.\F{}
		}
		{%
			\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
			¶
			Begw began to tremble, for fear the swearing would get worse.
			¶
			\Qsp{Winni} \Qnucl{went ahead}.
			¶
			\Qquote{‘\Quote{}’}
		}
		{\exsrctyyg{5}{58}}
\xe\postex{}

Another resumptive phrasal verbal construction is attested in the corpus, \C{\gl{dechrau}{begin.\Inf{}} \gl{arni}{\arni{}} \gl{wedyn}{\wedyn{}}}%
\footnote{%
	The feminine morphology of \C{arni}[\arni{}] is frozen and non-referential; cf.\ \E{it} in the English\index[langs]{English!Modern} \E{set about it} and see also \cref{fn:mae hi’n bwrw glaw} on p.~\pageref{fn:mae hi’n bwrw glaw} and \cite[§~dechreuaf: dechrau]{gpc:arlein}.
}.
In \exfullref{ex:rd:interruptions:resumption:mynd phrasal:dechrau arni} the paragraph separating the two quotes by the same speaker is longer, and the resumption is tied up with it temporally; see the final sentence in the paragraph, beginning with \C{\gl{Pan}{when}} and ending with \C{\gl{dyma}{\dyma{}} \gl{Winni}{\Pn{}} \gl{yn}{\ynD{}} \gl{dechrau}{begin.\Inf{}} \gl{arni}{\arni{}} \gl{wedyn}{\wedyn{}}}[Winni started again]%
\footnote{%
	Take note of the parallelism: \C{(d)dechrau ar eu te}[start on their tea] and \C{dechrau arni}.
}.

\preex{}\ex<ex:rd:interruptions:resumption:mynd phrasal:dechrau arni>
	\gloss
	{%
		\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
		¶
		\inlinecomment{paragraff disgrifiadol}.
		Pan oedd Begw yn meddwl pa bryd y caent ddechrau ar eu te, \Qnucl{dyma} \Qsp{Winni} \Qnucl{yn} \Qnucl{dechrau} \Qnucl{arni} \Qmod{wedyn}.
		¶
		\Qquote{‘\Quote{}’}
	}
	{%
		{} {}
		{}
		{}
		when \oedd{} \Pn{} \ynD{} think.\Inf{} which time \yrRel{} get.\Impf{}.\Tpl{} begin.\Inf{} on \eu{} tea \dyma{} \Pn{} \ynD{} begin.\Inf{} \arni{} \wedyn{}
	}
	{%
		\inlinecomment{Winni:} \Qquote{‘\Quote{}’}
		¶
		\inlinecomment{a descriptive paragraph}.
		As Begw was thinking when they might start on their tea, \Qsp{Winni} \Qnucl{started} \Qmod{again}.
		¶
		\Qquote{‘\Quote{}’}
	}
	{}
\xe\postex{}

In one case (\exfullref{ex:rd:interruptions:resumption:Methai fyned ymlaen}) \C{\gl{mynd}{go.\Inf{}} \gl{ymlaen}{\ymlaen{}}}[go on] (in the conservative spelling \C{myned}) is used as the object of \C{\gl{methu}{fail.\Inf{}}} in parenthesis within a quote in order to indicate a momentary pause.

\preex{}\ex<ex:rd:interruptions:resumption:Methai fyned ymlaen>
	\gloss
	{%
		\Qquote{‘\Quote{}.’} (Methai fyned ymlaen gan chwerthin.) \Qquote{‘\Quote{}’}
	}
	{%
		{} fail.\ai{} go.\Inf{} \ymlaen{} by laugh.\Inf{}
	}
	{%
		\Qquote{‘\Quote{}.’} (He couldn’t go on, with laughing.) \Qquote{‘\Quote{}’}
	}
	{}
\xe\postex{}

In \exfullref{ex:rd:interruptions:resumption:Yna aeth Gwen ymlaen} the \C{\gl{mynd}{go.\Inf{}} \gl{ymlaen}{\ymlaen{}}} construction is used for encapsulating the content of speech in a narrative paragraph, avoiding to reveal the content directly.
The reason for this is extralinguistic: the author chose not to refer to the \C{\gl{newid}{change} \gl{mawr}{big}}[big change] (puberty and menstruation) explicitly due to puritan societal norms.
Although \C{mynd ymlaen} is often resumptive, in this case there is no interruption or pause to resume speech after, and it is used as a literary-linguistic means of avoiding direct reference.

\preex{}\ex<ex:rd:interruptions:resumption:Yna aeth Gwen ymlaen>
	\gloss
	{%
		\inlinecomment{Gwen:} \Qquote{‘Gyda} \Qquote{hyn,} \Qquote{Winni,} \Qquote{mi} \Qquote{fydd} \Qquote{yna} \Qquote{newid} \Qquote{mawr} \Qquote{yn} \Qquote{ych} \Qquote{corff} \Qquote{chi.’}
		¶
		Yna aeth Gwen ymlaen i egluro wrth Winni am y newid hwn yn ei bywyd yn fanwl heb guddio dim, \ellipsis{}
	}
	{%
		{} with \hynn{} \Pn{} \mi{} \byddFut{} \ynaloc{} change big \ynA{} \eich{} body \chi{}
		{}
		\ynatmp{} go.\odd{} \Pn{} \ymlaen{} to explain.\Inf{} with \Pn{} about \Def{} change \hwn{} \ynA{} \eif{} life \ynB{} precise without hide.\Inf{} \dim{anything}
	}
	{%
		\inlinecomment{Gwen:} \notsic{}\Qquote{‘It’s like this, Winni, there’ll soon be a big change in your body.’}
		¶
		Then Gwen went on to explain to Winni about this change in her life in detail, without hiding anything, \ellipsis{}
	}
	{}
\xe\postex{}

{}\index{interruption in conversation|)}
