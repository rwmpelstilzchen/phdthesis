\section{Overt \emph{versus} zero quotative index}%
\label{sec:rd:zero}

When presenting the general characteristics of the three types of quotative indexes in \cref{sec:rd:patterns:overview}, a distinction has been made between instances of QI1 in which there is an overt quotative index segment (\QIaplus{}) and instance of QI1 where it is absent, leaving only the typographical devices of quotation marks and paragraph breaks as markers (\QIaminus{})%
\footnote{%
	Note that these are strictly distinct from free indirect speech, in form (e.g.\ typographic marking and the use of deictic categories such as tense and person) and in function.
	These are direct speech quotes in dialogue, but they have no quotative index connecting them to the text.
}.
\Exfullref{ex:rd:patterns:exx:QI1} in \cref{sec:rd:patterns:overview:examples} exemplifies these types: the first turn (\C{‘\gl{O}{\Interj{}}, \gl{am}{about} \gl{ddel}{pretty}, \ellipsis{} \gl{i}{to} \gl{gael}{get.\Inf{}} \gl{te}{tea}’}) is \QIaplus{} (the quotative index is \C{\gl{meddai}{\meddai{}} \gl{Winni}{\Pn{}} \gl{wrth}{with} \ellipsis{} \gl{a}{\aRel{}} \gl{roddasai}{give.\Plup{}.\Tsg{}} \gl{amdani}{\amdani{}}}) and the second one (\C{‘\gl{Mi}{\mi{}} \gl{fydd}{\byddFut{}} \gl{yn}{\ynC{}} \ellipsis{} \gl{o’ch}{from-\eich{}} \gl{cyflog}{salary} \gl{chi}{\chi{}}’}) is \QIaminus{}.
The question this section is concerned with is the distribution between the two: when is \QIaminus{} used and when \QIaplus{}.
Note that there is no \QIaminus{} equivalent for QI2 and QI3, so the discussion is limited to QI1.

The vast majority of cases follow this rule of thumb:

{%
% \begin{quote}%
	\SingleSpacing{}%
	\footnotesize{}%
	\begin{compactenum}[(a)]
		\item If any of the following applies, \QIaplus{} is used:
			\begin{compactitem}
				\item The identification of the speaker is not unambiguous from the dialogue before the turn in question; e.g.\ if there are more than two participants in the conversation or there is no previous relevant co-text.
				\item The author chooses to describe something specific regarding the speech through the modification component (\cref{sec:rd:internal:mod}) or~— marginally~— through a lexical choice of specific speech verb (\cref{sec:rd:internal:nucl:specific}).%
				\footnote{%
					With the exception of \exfullref{ex:rd:internal:mod:QI1:cvb:i=VD-45} (\cref{sec:rd:internal:mod:QI1:cvb:i}) and the special case of \exfullref{ex:rd:internal:mod:QI1:hyn} (\cref{sec:rd:internal:mod:QI1:hyn}), the modification component cannot stand own its own and has to be syntactically dependent on an overt nucleus.
				}
			\end{compactitem}

		\item If the none of the above two cases applies, \QIaminus{} is used.
			In other words, unless there is a reason to use \QIaplus{}, \QIaminus{} is the default option.
	\end{compactenum}
% \end{quote}
}

This simple rule describes the system that emerges from the corpus quite well.
For example, in the case studies of conversations between three participants (\cref{sec:rd:zero:corpus:three}) only three turns do not follow the rule, out of more than one hundred dialogue paragraphs.
In dialogues between two participants (\cref{sec:rd:zero:corpus:two}) the case studies show only exception (\exfullref{ex:rd:zero:VD-47} in \cref{sec:rd:zero:corpus:tg5-b-eg}) out of almost two hundred dialogue paragraphs, and this exception is easily explainable through refinement of the rule.

Equivalent systems have been discussed in the literature, such as in \textcite[passim; African languages and literature review]{guldemann.t:2008:quotative-indexes}, \textcite[several languages]{longacre.r.e:1994:dialogue-narrative} and \textcite[spoken English]{mathis.t+:1994:zero-quotatives}\index[langs]{English!Modern!conversational}.
Different languages may behave differently in this domain; Biblical Hebrew\index[langs]{Hebrew!Biblical}, for example, does not allow a zero quotative index at all \parencites[§~3.3]{longacre.r.e:1994:dialogue-narrative}{longacre.r.e:2003:joseph-analysis}.
This is a linguistic matter, but it is affected by literary tradition.
Kate Roberts’s works are typical of European \nth{20} century literature with this regard.

From a structural linguistic perspective the term \emph{ellipsis}\index{ellipsis}, which is sometimes used in the literature (e.g.\ \cite{oshima.d.y+:2012:japanese-reported}), may be suboptimal, as it suggests dynamicity (cf.\ \emph{transformation}), as if the quotative index is \emph{elided} by some dynamic process.
In contrast, \emph{zero} (as opposed to \emph{overt}) is a neutral term, putting the two on equal grounds (similarly to the notion of \emph{zero morpheme} in morphology), as opposed to one being a derived or secondary metamorphic result of a change in the other.%
\footnote{%
	Compare with structural linguistic critique of the notion of \emph{neutralisation}\index{neutralisation} \parencite{barri.n:1979:neutralisation}.
}

% \begin{figure}
	% \caption{The structural relationship between \QIaminus{} and \QIaplus{}}%
	% \label{fig:rd:zero:paradigm}
	% \centering{}%
	% \smaller{}%
	% \C{%
		% ‘\Quote{}’ \paradigm{
			% Ø\\
			% \\
			% \paradigm{%
				% \tabgl{{ebe}}{\ebe{}}\\
				% \tabgl{{meddai}}{\meddai{}}\\
				% \tabgl{{gwaeddodd}}{shout.\odd{}}\\
				% \tabgl{{oddi wrth}}{from}\\
				% \tabgl{{oedd}}{\copoedd{}} \tabgl{\Pro{}/…\NP{}}{\Poss{}}\\
				% \symbolglyph{⋮}
			% } (\Sp{}) (\Ad{}) (mod)
		% }
		% \begin{tabular}{l}
			% $\left.\begin{tabular}{l}\\\end{tabular}\right\}$\QIaminus{}\\
			% \\
			% $\left.\begin{tabular}{l}
			% \\
			% \\
			% \\
			% \\
			% \\
			% \\
			% \end{tabular}\right\}$\QIaplus{}
			% %$\left.\hspace{-1.40em}\begin{tabular}{l}\\\\\\\\\\\end{tabular}\right\}$\Iplus
		% \end{tabular}
	% }
% \end{figure}
\begin{marginparfigure}
	{The structural relationship between \QIaminus{} and \QIaplus{}}%
	{The structural relationship between \QIaminus{} and \QIaplus{}}%
	{fig:rd:zero:paradigm}
	% \smaller[5]{}%
    \resizebox{1.1\marginparwidth}{!}{%
		\C{%
			‘\Quote{}’ \paradigm{
				Ø\\
				\\
				\paradigm{%
					ebe\\
					meddai\\
					gwaeddodd\\
					oddi wrth\\
					oedd \NP{}+\Poss{}\\
					\symbolglyph{⋮}
				} (\Sp{}) (\Ad{}) (mod)
			}
			\begin{tabular}{l}
				$\left.\begin{tabular}{l}\\\end{tabular}\right\}$\QIaminus{}\\
				\\
				$\left.\begin{tabular}{l}
						\\
						\\
						\\
						\\
						\\
						\\
				\end{tabular}\right\}$\QIaplus{}
				%$\left.\hspace{-1.40em}\begin{tabular}{l}\\\\\\\\\\\end{tabular}\right\}$\Iplus
			\end{tabular}
		}
	}
\end{marginparfigure}
Schematically, the relationship between \QIaminus{} and \QIaplus{} can be presented as \cref{fig:rd:zero:paradigm}.



\subsection{Case studies from the corpus}%
\label{sec:rd:zero:corpus}

\orientationinitial{}%
In order to make the general description grounded in concrete examples, four representative conversations are examined: two between two characters (\cref{sec:rd:zero:corpus:two}) and two between three (\cref{sec:rd:zero:corpus:three}).


\subsubsection{Conversations between two characters}%
\label{sec:rd:zero:corpus:two}

\paragraph{Begw and her mother in \worktitle{Ymwelydd i De}}%
\label{sec:rd:zero:corpus:tg5-b-eg}

The first one is the conversation between Begw and her Mother in the beginning of \C{\gl{Ymwelydd}{visitor} \gl{i}{to} \gl{De}{tea}}[A Visitor for Tea], the fifth story in \TG{}.
It spans over three and a half pages (in my edition) and consists of about sixty paragraphs, the vast majority of which are QI1 dialogue turns.
Through all of the dialogue only two turns are \QIaplus{}, and the rest are \QIaminus{}.
The first \QIaplus{} instance is \exfullref{ex:rd:zero:VD-46}, which opens the story, anchors the conversation in time and establish the participants: Begw is the first speaker (\C{\gl{meddai}{\meddai{}} \gl{Begw}{\Pn{}}}[Begw said]), initiating the conversation, and her mother is the addressee of the first turn, (\C{\gl{Mam}{mother}}, within the quote, as a coda; see \cref{sec:rd:internal:ad}).

\preex{}\ex<ex:rd:zero:VD-46>
	\gloss
	{%
		\Qquote{‘Ydach} \Qquote{chi’n} \Qquote{licio} \Qquote{Winni,} \Qquote{Mam?’} \Qnucl{meddai} \Qsp{Begw} \Qmod{ymhen} \Qmod{ychydig} \Qmod{ddyddiau} \Qmod{wedi}’r \Qmod{te} \Qmod{parti} \Qmod{rhyfedd} \Qmod{hwnnw} \Qmod{ar} \Qmod{ben} \Qmod{y} \Qmod{mynydd}.
	}
	{%
		\ydych{} \chi{}-\ynD{} like.\Inf{} \Pn{} \fy{}\textbackslash{}mother \meddai{} \Pn{} at\_the\_end\_of few day.\Pl{} after-\Def{} tea party strange \hwnnw{} on head \Def{} mountain
	}
	{%
		\Qquote{‘Do you like Winni, mam?’} \Qsp{Begw} \Qnucl{said}, \Qmod{a few days after that strange tea party on the mountain}.
	}
	{\exsrctyyg{5}{50}}
\xe\postex{}

The second instance of \QIaplus{} is \exfullref{ex:rd:zero:VD-47}, about forty paragraphs into the conversation.
Begw’s mother talks after Begw, her allocutor, and there is no modification component.
This would suggest the use of \QIaminus{}, but the nature of this example is different.
While Begw does ask a question (cited within the example for context), what her mother say is not an answer: \C{\gl{Go drapia}{\Interj{}}}[Drat it] is not directed at Begw at all, but is an interjectional reaction for accidentally pricking herself with a needle while sewing, and the quote after the quotative index refers to the accident and is not an answer to Begw’s question.%
\footnote{%
	On a literary level, pricking herself is a kind of reaction to Begw’s ‘shocking’ question, as her mother holds Mrs Huws in very low esteem.
}
The use of an overt quotative index removes this turn from the unmarked turn-taking cycle of the dialogue.

\preex{}\ex<ex:rd:zero:VD-47>
	\gloss
	{%
		\inlinecomment{Begw:} \Qquote{‘I} \Qquote{be} \Qquote{mae} \Qquote{eisio} \Qquote{inni} \Qquote{dreio} \Qquote{bod} \Qquote{yr} \Qquote{un} \Qquote{fath} \Qquote{â} \Qquote{Mrs.} \Qquote{Huws} \Qquote{ynta?’}
		¶
		\Qquote{‘Go drapia,’} \Qnucl{meddai}\Qsp{’r} \Qsp{fam}, \Qquote{‘dyna} \Qquote{chdi} \Qquote{wedi} \Qquote{gneud} \Qquote{imi} \Qquote{blannu’r} \Qquote{nodwydd} \Qquote{yma} \Qquote{yn} \Qquote{fy} \Qquote{mys.’}
	}
	{%
		{} to what \mae{} want \inni{} \Len{}\textbackslash{}try.\Inf{} \bod{} \Def{} one kind with Mrs \Pn{} \Tag{}
		{}
		\Interj{} \meddai{}-\Def{} mother \dyna{} \chdi{} after do.\Inf{} \imi{} \Len{}\textbackslash{}plant.\Inf{}-\Def{} needle \ymadem{} \ynA{} \fy{} finger
	}
	{%
		\Qquote{‘Why do we want to try and be like Mrs.\ Huws then?’}
		¶
		\Qquote{‘Drat it,’} \Qsp{her mother} \Qnucl{said}, \Qquote{‘there you’ve made me plant this needle in my finger.’}
	}
	{\exsrctyyg{5}{52}}
\xe\postex{}

After \exfullref{ex:rd:zero:VD-47} there is a pause in conversation (\C{\gl{Distawrwydd}{silence} \gl{wedyn}{\wedyn{}}}[Silence then]) and a narrative paragraph, after which the conversation resumes, with \QIaminus{} turns.



\paragraph{Begw and Winni in \worktitle{Dianc i Lundain}}%
\label{sec:rd:zero:corpus:tg6-b-w}

When Begw and Winni walk together in \C{\gl{Dianc}{escape.\Inf{}} \gl{i}{to} \gl{Lundain}{\Pn{}}}[Escape to London], the sixth story in \TG{}, the two talk for over one hundred (mostly dialogue) paragraphs, which make eight pages of text.
The only places where \QIaplus{} is used in this conversation is where there is a modification component (e.g.\ \C{\gl{meddai}{\meddai{}} \gl{Begw}{\Pn{}} \gl{yn}{\ynB{}} \gl{swil}{shy}}[Begw said shyly]) or when the conversation continues after a pause and it is necessary to establish who is the first speaker.



\subsubsection{Conversations between three characters}%
\label{sec:rd:zero:corpus:three}

Research on free forming conversational groups shows such groups tend not to exceed four participants in number \parencite{dunbar.r.i.m+:1995:conversational-groups}.
Several factors contribute to this limit, one of which is the social cognitive ability to effortlessly have an active theory of mind\index{theory of mind} (ToM) regarding the mental states of up to a limited number of people in at once \parencite{dunbar.r.i.m:2004:gossip-evolutionary}.
A higher order kind of theory of mind is found in art of literature, where imaginary characters are created in such a way they represent possible minds whose mental states the readers (and authors) keep track of and can deduce about \textcite[102]{dunbar.r.i.m:1996:grooming-gossip}.
This is one factor that limits the number of active characters in one scene or one conversation.
In our corpus, long and complex conversations are of no more than three participants.

Conversations with more than two participants show an interesting pattern with regard to the use of overt and zero quotative indexes: when more than two actively participate, each turn is marked with a \QIaplus{} identifying the speaker, but if participants become more passive and only converse, \QIaminus{} begins to be used after a while, as the conversation becomes a conversation between two participants \foreign{de facto}.
An explicit reference to this can be seen in \exfullref{ex:rd:zero:corpus:three:meta}, when Mair becomes silent in what was before a three-party conversation%
\footnote{%
	Take note of the cleft structure of \C{Begw a holai}[\notsic{}it was Begw who asked the questions], which focusses Begw (as opposed to Mair or the two of them together).
} (after she rejoins as an active participant, \QIaplus{} is used again).

\preex{}\ex<ex:rd:zero:corpus:three:meta>
	\gloss
	{%
		Edrychai Mair i lawr ar ei ffrog heb ddweud dim, a Begw a holai.
		Cafodd ei brifo gan yr ateb olaf.
	}
	{%
		look.\ai{} \Pn{} to floor on \eif{} frock without \dweud{}.\Inf{} \dim{anything} and \Pn{} \aRel{} ask.\ai{}
		get.\odd{} \eif{} hurt.\Inf{} by \Def{} answer last
	}
	{%
		Mair was looking down at her frock without saying anything, and it was Begw asked the questions.
		She was hurt by the last answer.
	}
	{\exsrctyyg{4}{44}}
\xe\postex{}

As complementary evidence, in an appendix (\cref{app:rd:zero}) the text of two conversations between three characters is typeset in a bilingual manner and is annotated\index{annotation} and commented upon with a running commentary
\begin{table}
	\caption{The conversations annotated and commented upon in \cref{app:rd:zero}}%
	\label{tab:rd:zero:corpus:three:appendix}
	\begin{tabular}{llll}
		\toprule
		Subsection & Participants & Situation & Story\\
		\midrule
		\cref{app:rd:zero:grug} &  Winni, Begw and Mair & on the mountain & %\C{\gl{Te}{tea} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{Grug}{heather}}[Tea in the Heather]\\
		\tabC{Te yn y Grug}\\
		\cref{app:rd:zero:ymwelydd} & Begw, her mother and Winni & having tea &
		%\C{\gl{Ymwelydd}{visitor} \gl{i}{to} \gl{De}{tea}}[A Visitor for Tea] (the fifth story).
		\tabC{Ymwelydd i De}\\
		\bottomrule
	\end{tabular}
\end{table}
(see~\cref{tab:rd:zero:corpus:three:appendix}).
The annotations mark each dialogue paragraph with information regarding the speaker and the use of \QIaminus{} or \QIaplus{} (as described in \cref{tab:app:rd:zero:annotations}).

Normally the presence all participants is established explicitly, but in a few cases a character joins in the conversation without previous explicit introduction into the scene.
Such is \exfullref{ex:rd:zero:corpus:three:new.Robin}, where after Winni explains about mince pies Robin asks if they are tasty.
He did not say anything in the conversation before and in fact his presence is only implied thirteen paragraphs before that, in \C{\gl{Daeth}{come.\odd{}} \gl{y}{\Def{}} \gl{lleill}{other.\Pl{}} \gl{i’r}{to-\Def{}} \gl{tŷ}{house} \gl{o’r}{from-\Def{}} \gl{cyfarfod}{meeting} \gl{a}{and} \gl{rhedeg}{run.\Inf{}} \gl{i}{to} \gl{gynhesu}{warm.\Inf{}} \gl{at}{to} \gl{y}{\Def{}} \gl{tân}{fire}, \ellipsis{}}[The others came to the house from the meeting and ran to warm themselves by the fire, \ellipsis{}].
In \exfullref{ex:rd:zero:corpus:three:new.John Gruffydd}, on the other hand, the integration of a new participant into the conversation is explicitly commented on (\C{\gl{a}{\aRel{}} \gl{ddaethai}{come.\Plup{}.\Tsg{}} \gl{i}{to} \gl{mewn}{in} \gl{o’r}{from-\Def{}} \gl{beudy}{cowshed} \gl{yn}{\ynA{}} \gl{ystod}{course} \gl{y}{\Def{}} \gl{sgwrs}{conversation}}[who’d come in from the cowshed during the conversation]).

\preex{}\pex<ex:rd:zero:corpus:three:new>
	\a<Robin>
		\gloss
		{%
			\Qquote{‘Ydy} \Qquote{o’n} \Qquote{beth} \Qquote{da?’} \Qnucl{gofynnodd} \Qsp{Robin}.
			¶
			\inlinecomment{Winni:} \Qquote{‘Ardderchog.’}
		}
		{%
			\qydy{} \ef{}-\ynC{} thing good ask.\odd{} \Pn{}
			{}
			{} splendid
		}
		{%
			‘Is it nice?’ asked Robin.
			¶
			‘Lovely.’
		}
		{\exsrchd{6}{59}}

	\a<John Gruffydd>
		\gloss
		{%
			\Qquote{‘Ia,} \Qquote{ond} \Qquote{mae'n} \Qquote{anodd} \Qquote{gwybod} \Qquote{be} \Qquote{sy'n} \Qquote{iawn} \Qquote{a} \Qquote{be} \Qquote{sy} \Qquote{ddim,’} \Qnucl{ebe} \Qsp{John Gruffydd}, a ddaethai i mewn o'r beudy yn ystod y sgwrs.
		}
		{%
			yeah but \mae{}-\ynC{} difficult know.\Inf{} what \sy{}-\ynC{} right and what \sy{} \Len{}\textbackslash{}\dim{\Neg{}} \ebe{} \Pn{} \aRel{} come.\Plup{}.\Tsg{} to in from-\Def{} cowshed \ynA{} course \Def{} conversation
		}
		{%
			\Qquote{‘Yes, but it’s hard to know what is right and what isn’t,’} \Qnucl{said} \Qsp{John Gruffydd}, who’d come in from the cowshed during the conversation.
		}
		{\exsrctyyg{7}{83}}
\xe\postex{}



\subsection{Interface with rapid narrative-dialogue alternation}%
\label{sec:rd:zero:alternation}

Occasionally the speech of one character is divided into several dialogue paragraphs, with short narrative paragraphs which depict actions by the speaker themselves punctuating the dialogue.
These \foreign{staccato} breaks have a dramatic narrative effect.
With regard to the topic in question, the dialogue paragraphs which resume the character’s speech have no overt quotative indexes.
Exx.~\getfullref{ex:rd:zero:alternation:Winni} and \getfullref{ex:rd:zero:alternation:mistress} demonstrate this literary-linguistic technique.

\preex{}\ex<ex:rd:zero:alternation:Winni>
	\gloss
	{%
		\inlinecomment{Winni:} \Qquote{‘Mi} \Qquote{brifith} \Qquote{ac} \Qquote{mi} \Qquote{dda’r} \Qquote{ha’.’}
		¶
		Dechreuodd Winni chwerthin.
		¶
		\Qquote{‘Wyddoch} \Qquote{chi,} \Qquote{dyma} \Qquote{beth} \Qquote{rhyfedd.’}
		¶
		Stopiodd.
		¶
		\Qquote{‘Ydach} \Qquote{chi’n} \Qquote{gwybod} \Qquote{beth} \Qquote{ydy} \Qquote{{mince pies}?’}
	}
	{%
		{} \mi{} grow.\Prs{}.\Tsg{} and \mi{} come.\Prs{}.\Tsg{}-\Def{} summer
		{}
		begin.\odd{} \Pn{} laugh
		{}
		know.\Prs{}.\Spl{} \chi{} \dyma{} thing strange
		{}
		stop.\odd{}
		{}
		\ydach{} \chi{}-\ynD{} know.\Inf{} what \copydy{} mince pies
	}
	{%
		\Qquote{‘He’ll grow up, and summer will come.’}
		¶
		Winni began to laugh.
		¶
		\Qquote{‘Do you know, here’s a funny thing.’}
		¶
		She stopped.
		¶
		\Qquote{‘Do you know what mince pies are?’}
	}
	{\exsrchd{6}{56}}
\xe\postex{}

\preex{}\ex<ex:rd:zero:alternation:mistress>
	\gloss
	{%
		\inlinecomment{Winni’s mistress:} \Qquote{‘Fel} \Qquote{hyn} \Qquote{ylwch.’}
		¶
		A chymerodd ei meistres afael efo blaenau ei bysedd yn un o’r ffliwiau a’i dynnu allan.
		¶
		\Qquote{‘Rhaid} \Qquote{i} \Qquote{chi} \Qquote{roi} \Qquote{brws} \Qquote{i} \Qquote{fyny’r} \Qquote{twll} \Qquote{yma,} \Qquote{cyn} \Qquote{belled} \Qquote{ag} \Qquote{y} \Qquote{medrwch} \Qquote{chi,} \Qquote{ac} \Qquote{mi} \Qquote{ddaw’r} \Qquote{huddyg} \Qquote{i} \Qquote{lawr} \Qquote{y} \Qquote{ffordd} \Qquote{yna.’}
		¶
		Ni wnaeth ddim ond pwyntio at y ffordd yna gan ei bod wedi maeddu ei bysedd.
		¶
		\Qquote{‘A} \Qquote{wedyn} \Qquote{mynd} \Qquote{â’r} \Qquote{huddyg} \Qquote{allan} \Qquote{a’i} \Qquote{dywallt} \Qquote{i’r} \Qquote{tun} \Qquote{baw.’}
	}
	{%
		{} like \hynn{} see.\Imp{}.\Spl{}
		{}
		and take.\odd{} \eif{} mistress hold.\Inf{} with front.\Pl{} \eif{} finger.\Pl{} \ynA{} one of-\Def{} flue(\M{}).\Pl{} and-\eim{} pull.\Inf{} out
		{}
		\rhaid{} to \chi{} \Len{}\textbackslash{}give.\Inf{} brush to up-\Def{} hole \ymadem{} \Equ{} far.\Equ{} with \Nmlz{} \medru{}.\Prs{}.\Spl{} \chi{} and \mi{} come.\Prs{}.\Tsg{}-\Def{} soot to floor \Def{} way \ynadem{}
		{}
		\Neg{} make.\odd{} \dim{anything} but point.\Inf{} to \Def{} way \ynadem{} by \eif{} \bod{} after pollute.\Inf{} \eif{} finger.\Pl{}
		{}
		and \wedyn{} go.\Inf{} with-\Def{} soot out and-\eim{} pour.\Inf{} to-\Def{} tin filth
	}
	{%
		\Qquote{‘Like this, look.’}
		¶
		And her mistress took hold of one of the flues with the tips of her fingers and pulled it out.
		¶
		\Qquote{‘You have to put the brush up that hole there, as far as you can, and the soot will come down that way.’}
		¶
		She did not do anything, except point to ‘that way’ as she had dirtied her fingers.
		¶
		\Qquote{‘And then take the soot outside and put it in the dustbin.’}
	}
	{\exsrchd{6}{47}}
\xe\postex{}

That technique is to be differentiated from cases in which a narrative act (or the marked lack of action) by \emph{another} character stands for an answer or reaction.
This is demonstration by \exfullref{ex:rd:zero:alternation:Winni-Sionyn}, where the young Sionyn’s reaction to what Winni says is non-verbal twice:
\begin{compactitem}
	\item Once he does not answer her question.
		The lack of answer is explicitly commented upon by a syntactically independent (‘\emph{absolute}’) nominal phrase: \C{\gl{Dim}{\dim{\Neg{}}} \gl{ateb}{answer}}[No answer].%
		\footnote{%
			This is not dissimilar to the use of typographical ellipsis (‘\Japanese{…}’, colloquially known as \transcribe{\Japanese{てんてんてん}}{tententen} ‘dot dot dot’) in Japanese\index[langs]{Japanese} \foreign{manga} \parencite[§~4.4]{mcurray.d:2021:ellipsis-haiku} and video games \parencite{mandelin.c:2013:japanese-ellipsis}, where it can signify a pregnant pause or a lack of an answer when it stands in its own speech bubble or text window.
		}
	\item And once he begins to cry as a reaction to what she said next: \C{\gl{Dechreuodd}{begin.\odd{}} \gl{yntau}{\yntau{}} \gl{grïo}{cry.\Inf{}}}[He began to cry].
		Take note of the contrastive conjunctive pronoun \C{\gl{yntau}{\yntau{}}} (see \cref{sec:intro:meta:terminology:conj}), which poses Sionyn’s action as reactive (as if saying ‘she said so-and-so, and \emph{he} did so-and-so’).
\end{compactitem}

\preex{}\ex<ex:rd:zero:alternation:Winni-Sionyn>
	\gloss
	{%
		\inlinecomment{Winni:} \Qquote{‘O} \Qquote{ble’r} \Qquote{oedd} \Qquote{hi’n} \Qquote{meddwl} \Qquote{bod} \Qquote{gen} \Qquote{i} \Qquote{arian} \Qquote{i} \Qquote{brynu} \Qquote{presant} \Qquote{i} \Qquote{neb?’}
		¶
		Dim ateb.
		¶
		\Qquote{‘Fedar} \Qquote{neb} \Qquote{brynu} \Qquote{dim} \Qquote{efo} \Qquote{dim.} \Qquote{Dos} \Qquote{i} \Qquote{gysgu.’}
		¶
		Dechreuodd yntau grïo.
		¶
		\Qquote{‘Dos} \Qquote{i} \Qquote{gysgu.} \Qquote{Ella} \Qquote{y} \Qquote{bydd} \Qquote{rhyw} \Qquote{dylwyth} \Qquote{teg} \Qquote{wedi} \Qquote{dŵad} \Qquote{â} \Qquote{rhywbeth} \Qquote{iti} \Qquote{erbyn} \Qquote{y} \Qquote{bore.} \Qquote{’Rydw} \Qquote{i’n} \Qquote{gwybod} \Qquote{i} \Qquote{fod} \Qquote{o} \Qquote{wedi} \Qquote{dŵad.’}
	}
	{%
		{} from where-\yr{} \oedd{} \hi{}-\ynD{} think.\Inf{} \bod{} by \Fsg{} money to buy.\Inf{} present to anyone
		{}
		\dim{\Neg{}} answer
		{}
		\Neg{}\textbackslash{}\medru{}.\Prs{}.\Tsg{} anyone buy.\Inf{} \dim{anything} with \dim{nothing} go.\Imp{}.\Ssg{} to sleep.\Inf{}
		{}
		begin.\odd{} \yntau{} cry.\Inf{}
		{}
		go.\Imp{}.\Ssg{} to sleep.\Inf{} perhaps \Nmlz{} \byddFut{} \rhyw{} kinsfolk fair after come.\Inf{} with something \iti{} by \Def{} morning \yr{}-\ydw{} \Fsg{}-\ynD{} know.\Inf{} \eim{} \bod{} \ef{} after come.\Inf{}
	}
	{%
		\Qquote{‘Where did she think I’d get money from to buy a present for anyone?’}
		¶
		No answer.
		¶
		\Qquote{‘Nobody can buy anything with nothing. Go to sleep.’}
		¶
		He began to cry.
		¶
		\Qquote{‘Go to sleep. Perhaps a fairy will have brought you something by morning. I know that it’s come.’}
	}
	{\exsrchd{6}{55}}
\xe\postex{}



\subsection{Opening stories with speech}%
\label{sec:rd:zero:opening}

In half of the eight stories in \TG{} (and none of the ones in \HD{}) a dialogue paragraph occurs in the absolute beginning of the story (\exfullref{ex:rd:zero:opening}), where there is no previous co-text within the boundaries of the stories.
This is a literary device, a type of \foreign{in medias res} \parencite[see][p.~110~f.]{bonheim.h.w:1982:narrative-modes}, but it has linguistic implications.
In exx.~\getfullref{ex:rd:zero:opening.4} and \getfullref{ex:rd:zero:opening.5}%
\footnote{%
	See \cref{sec:rd:zero:corpus:tg5-b-eg}, which discusses the conversation that begins with \exfullref{ex:rd:zero:opening.5} (\exfullref{ex:rd:zero:VD-46} there).
}
there is an overt quotative index, and in exx.~\getfullref{ex:rd:zero:opening.7} and \getfullref{ex:rd:zero:opening.8} no overt quotative index is used.
The fact that in exx.~\getfullref{ex:rd:zero:opening.4} and \getfullref{ex:rd:zero:opening.5} additional phrases are syntactically dependent on the nucleus might have to do with this difference, although these are not enough examples to deduce conclusively.
Another factor is the \emph{loci} of indicating the participants:
\begin{compactitem}
	\item in \exfullref{ex:rd:zero:opening.4} both are indicated in the quotative index,
	\item in \exfullref{ex:rd:zero:opening.5} one is indicated within the quote and one in the quotative index,
	\item in \exfullref{ex:rd:zero:opening.7} both are indicated within the two opening quotes, and
	\item in \exfullref{ex:rd:zero:opening.8} there is no direct indication, but the addressee is implied by the beginning of the following narrative paragraph (cited below) while the speaker is not indicated at all, and only can be inferred to be his mother%
	\footnote{%
		As in \exfullref{ex:rd:zero:opening.7}, which has an almost identical first clause.
	}
	from extralinguistic world knowledge.
\end{compactitem}

\preex{}\pex<ex:rd:zero:opening>
	\a<4>
		\gloss
		{%
			\Qquote{‘Ga’} \Qquote{i} \Qquote{weld} \Qquote{o?’} \Qnucl{meddai} \Qsp{Begw} \Qad{wrth} \Qad{ei} \Qad{mam} a mynd ar ei phennau-gliniau ar gadair yn y tŷ llaeth.
		}
		{%
			get.\Prs{}.\Fsg{} \Fsg{} see.\Inf{} \ef{} \meddai{} \Pn{} with \eif{} mother and go.\Inf{} on \eif{} knee.\Pl{} on chair \ynA{} \Def{} house milk
		}
		{%
			\Qquote{‘May I see it?’} \Qsp{Begw} \Qnucl{said} \Qad{to her mother}, and got up on the chair in the milkshed on her knees.
		}
		{\exsrctyyg{4}{38}}

	\a<5>
		\gloss
		{%
			\Qquote{‘Ydach} \Qquote{chi’n} \Qquote{licio} \Qquote{Winni,} \Qquote{Mam?’} \Qnucl{meddai} \Qsp{Begw} \Qmod{ymhen} \Qmod{ychydig} \Qmod{ddyddiau} \Qmod{wedi}’r \Qmod{te} \Qmod{parti} \Qmod{rhyfedd} \Qmod{hwnnw} \Qmod{ar} \Qmod{ben} \Qmod{y} \Qmod{mynydd}.
		}
		{%
			\ydych{} \chi{}-\ynD{} like.\Inf{} \Pn{} \fy{}\textbackslash{}mother \meddai{} \Pn{} at\_the\_end\_of few day.\Pl{} after-\Def{} tea party strange \hwnnw{} on head \Def{} mountain
		}
		{%
			\Qquote{‘Do you like Winni, mam?’} \Qsp{Begw} \Qnucl{said}, \Qmod{a few days after that strange tea party on the mountain}.
		}
		{\exsrctyyg{5}{50}}

	\a<7>
		\gloss
		{%
			\Qquote{‘Cadw} \Qquote{dy} \Qquote{draed} \Qquote{yn} \Qquote{llonydd,} \Qquote{Begw.} \Qquote{'Ddaw} \Qquote{Winni} \Qquote{ddim} \Qquote{cynt} \Qquote{wrth} \Qquote{iti} \Qquote{ysgwyd} \Qquote{dy} \Qquote{draed.’}
			¶
			\Qquote{‘Ydach} \Qquote{chi'n} \Qquote{meddwl} \Qquote{\textit{y}} \Qquote{daw} \Qquote{hi,} \Qquote{Mam?’}
		}
		{%
			keep.\Imp{}.\Ssg{} \dy{} foot.\Pl{} \ynC{} calm \Pn{} \Neg{}\textbackslash{}come.\Prs{}.\Tsg{} \Pn{} \dim{\Neg{}} early.\Cmp{} with \iti{} shake.\Inf{}  \dy{} foot.\Pl{}
			{}
			\ydach{} \chi{}-\ynD{} think.\Inf{} \Nmlz{} come.\Prs{}.\Tsg{} \hi{} \mam{}
		}
		{%
			\Qquote{‘Keep your feet still, Begw. Winni won’t come sooner when you swing your feet.’}
			¶
			\Qquote{‘Do you think that she \emph{will} come, mam?’}
		}
		{\exsrctyyg{7}{77}}

	\a<8>
		\gloss
		{%
			\Qquote{‘Dal} \Qquote{dy} \Qquote{draed} \Qquote{yn} \Qquote{llonydd,} \Qquote{a} \Qquote{phaid} \Qquote{â} \Qquote{gwingo.’}
			¶
			Rhoes Rhys un naid arall, a chlep ar ei ddwylo. \ellipsis{}
		}
		{%
			keep.\Imp{}.\Ssg{} \dy{} foot.\Pl{} \ynC{} calm and \Neg{}.\Imp{}.\Ssg{} with wriggle.\Inf{}
			{}
			give.\odd{} \Pn{} one jump other and clap on \eim{} hand.\Pl{}
		}
		{%
			\Qquote{‘Keep your feet still, and don’t wriggle.’}
			¶
			Rhys gave one more jump, and a clap of his hands. \ellipsis{}
		}
		{\exsrctyyg{8}{86}}
\xe\postex{}
