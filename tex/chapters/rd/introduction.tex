\section{Introduction}%
\label{sec:rd:intro}

\subsection{Background}%
\label{sec:rd:intro:background}

\openbilingepigraph%
{%
	\Tengwar{\upshape{}ql2h yljjh6 ] y\textasciitilde 5h}
}%
{%
	Speak, friend, and enter.\\
	(Say ‘friend’ and enter.)
}%
{\worktitle{The Lord of the Rings}, book~II, ch.~4 \parencite{tolkien.j.r.r:1995:lord-rings}}%
Narrative texts are truly \emph{texts} in the etymological sense\index{textile}: the English word \E{text} ultimately comes from Latin \initprotrusion\foreign{\gl{textus}{weave.\Ptcp{}.\Pass{}.\Prf{}.\M{}.\Nom{}}}, literally meaning ‘(that) which is woven’ and ‘style, tissue of a literary work’ by extension \parencite[§~text, n.~1]{oed:online}.
As discussed in \cref{sec:intro:text:sub-textual:narrmodes}, narrative texts consist of several \emph{modes}\index{narrative modes} the storyteller switches between, similarly to a weaver weaving threads.
Although there exist narratives with no dialogue in them~— such as many of the anecdotes discussed in \cref{sec:anecdotes}~— most narratives from a certain length onwards do involve conversations between characters, except perhaps some experimental pieces.

This chapter examines the \emph{interface} between the reported speech portions (primarily, but not exclusively, dialogues) and the narration in which they are embedded.
Keeping with the textile%
\footnote{%
	English \E{textile} has its origin in Latin \foreign{texō} too, through a substantive use of the deverbal adjective \foreign{textilis} ‘woven, intertwined’.
	So is \E{texture}, through Latin \foreign{textūra}.
}
metaphor but put in a slightly different manner, a narrative text can be likened to a pieced quilt made of many pieces of fabric sewn together, making intricate patterns.
\begin{marginparfigure}%
	{The seam between textual components}%
	{The seam between textual components}%
	{fig:rd:intro:background:seam}%
	\includegraphics[width={\marginparwidth}]{chapters/rd/sewing.pdf}
\end{marginparfigure}
This demonstrated visually in \cref{fig:rd:intro:background:seam}.
The main focus of this chapter is not on the pieces of fabric themselves (represented by grey surfaces) but on the \emph{sewing thread} that connects them (represented as a black looping line); more specifically, the thread that sews pieces of the mimetic \emph{dialogue} (in Shisha-Halevy’s terminology; \cref{sec:intro:text:sub-textual:narrmodes}) or \emph{speech} (in Bonheim’s terminology) and pieces of the diegetic \emph{narrative evolution} or \emph{report} (in the respective terminologies).

Aspects of the dialogue and the narrative portions~— separately~— have been linguistically described in Welsh in general and in Kate Roberts’s fiction in particular%
\footnote{%
	For example, \textcite[ch.~6]{emyr.j:1976:enaid-clwyfus} describes aspects of the language she puts in the mouths of characters.
	Shisha-Halevy’s work on Welsh (\cref{sec:intro:object:modern_literary_welsh:research}) is based mainly on her writings as a corpus, and much of it focusses on narrative (see the bibliography section for specific publications).
},
but to the best of my knowledge no scholarly attention beyond passing references in grammars and other publications%
\footnote{%
	These include \textcites%
	[§~242]{rowland.t:1870:exercises}%
	[§§~165–166]{williams.s:1980:welsh-grammar}%
	[§§~297–298]{thorne.d:1993:grammar}%
	[§~6.211]{thomas.p:2006:gramadeg}%
	[§~392]{king.g:2015:welsh-grammar-3}%
	[pp.~110, 118 and 120]{shisha-Halevy.a:2016:presentatives}%
	[§§~2.4 and 2.6]{shisha-halevy.a:2022:converbs-narrative}%
	.
}
has been given to the linguistic means of \emph{reporting} speech in Welsh, let alone in narrative specifically.



\subsubsection{Terminology, framework and object}%
\label{sec:rd:intro:background:terminology}

Much research has been conducted on signalling of \glossaryterm{reported discourse} in the world’s languages; \textcite{guldemann.t+:2002:bibliography-reported} provide a comprehensive bibliography, but it is twenty years old, and a great deal of research has been published since.%
\footnote{%
	For more recent research see \textcite{linguistic-bibliography:2002-}.
	Querying for the subject keywords \texttt{Reported speech} and \texttt{Quotation} yields 647 and 621 results respectively at the moment (2022-9).

	Another~— even more dated than \textcite{guldemann.t+:2002:bibliography-reported}~— bibliography, which focusses on speech in fiction, is \textcite[Bibliography~B]{bonheim.h.w:1982:narrative-modes}.
}
Despite this, not many typological, cross-linguistic studies have been published on the topic.
According to \textcite[§~1.2.2]{guldemann.t:2008:quotative-indexes} this lack has led to a great variation in terminology, with many terms being idiosyncratic and limited to a handful of publications%
\footnote{%
	For example, for the notion he terms \emph{quotative index} (QI) he lists no less than fifteen equivalents found in the literature:
	\emph{quotation~/ quote~/ metapragmatic formula},
	\emph{quotation indicator},
	\emph{quotation~/ quot(ativ)e~/ speech margin},
	\emph{speech-introducing~/ reporting clause},
	\emph{reporting~/ quotative~/ metapragmatic frame},
	\emph{speech-act expression},
	\emph{reporting signal},
	\emph{speech orienter}.
	To these one can add \emph{inquit formula} \parencite[cf.][]{thorgeirsson.h:2013:who-talking}, \emph{speech/dialogue tag} \parencite[cf.][§~4.5]{ribo.i:2019:prose-fiction} and \emph{formula of quotation} \parencite{longacre.r.e:1994:dialogue-narrative} as well.
}.
Two collections of papers stand out in this regard, namely \textcite{gueldemann.t+:2002:reported} and \textcite{buchstaller.i:2012:quotatives}, which cover a variety of languages from both a language-specific perspective and a cross-linguistic, typological perspective.
\rev{2}{An issue of \worktitle{Linguistic Typology} \parencite{lingty:2019:23.1} has several articles about the subject, consisting of one \emph{target paper} \parencite{spronck.s+:2019:reported-syntactic} and nine \emph{commentaries}.}
\Textcite[p.~281~ff.]{buchstaller.i:2012:quotatives} has a glossary for specialist terms relating to this topic.
One of the most in-depth typological studies of the signalling of  reported discourse I am aware of is \textcite{guldemann.t:2008:quotative-indexes}.
Its declared scope is areally limited to the continent of Africa, but thanks to its typological orientation, much of the terminology, framework and insights it presents are readily applicable to other languages \foreign{mutatis mutandis}.
It is for this reason that the basic terminology in this chapter follows it.

\emph{Reported speech} and \emph{reported discourse}\index{reported discourse}%
\footnote{%
	A third term, \emph{reported dialogue} \parencite[cf.][]{longacre.r.e:1994:dialogue-narrative} is not suitable for our needs, since as discussed below (\cref{sec:rd:patterns:3}), not all reported speech in the corpus is in fact dialogue.
}
are related but not identical terms which are commonly used.
At least in the way \textcite[§~1.2.1]{guldemann.t:2008:quotative-indexes} uses these terms, the former is included in the latter, which he defines as:
\begin{quote}\quotesize%
	the representation of a spoken or mental text from which the reporter distances him-/herself by indicating that it is produced by a source of consciousness in a pragmatic and deictic setting that is different from that of the immediate discourse.
\end{quote}
He further comments that across languages the encoding of embedded \emph{spoken} texts shows structural similarities with the encoding of embedded \emph{mental} texts (like thought and perception), which justifies bundling them together under the umbrella term \emph{reported discourse} for general, typological purposes.%
\footnote{%
	Reported thoughts are touched upon, but only in a complementary, secondary way (\cref{sec:rd:thoughts}).
	In the corpus in question (\cref{sec:rd:intro:corpus}) reported speech and reported thoughts show some differences in syntactic behaviour.
	In a manner not dissimilar to the relation between the general Labovian model and our anecdotes (\cref{sec:anecdotes:structure:labov:comparison}), here too studying particular texts by one author allows a finer resolution in comparison to broad generalisations.
}
\begin{marginparfigure}%
	{A schematic representation of the topic of the chapter}%
	{A schematic representation of the topic of the chapter}%
	{fig:rd:intro:background:terminology:venn}%
	\resizebox{\marginparwidth}{!}{%
		% Basic code: https://texample.net/tikz/examples/set-operations-illustrated-with-venn-diagrams/
		\begin{tikzpicture}
			\def\repsp{(0,0) ellipse [x radius=2cm, y radius=1.5cm]}
			\def\narrcon{(0:2.75cm) ellipse [x radius=2cm, y radius=1.5cm]}
			\def\repdisc{[rounded corners] (-2.5cm,-2cm) rectangle (2.5cm,2.5cm)}
			\tikzset{
				filled/.style={fill=sronpalegreen, thick},
				outline/.style={thick},
				font=\normalsize,
			}
			\begin{scope}
				\clip \repsp;
				\fill[filled] \narrcon;
			\end{scope}
			\draw[outline, align=left] \repsp node [xshift=-0.75cm] {reported\\speech};
			\draw[outline, align=right] \narrcon node [xshift=0.75cm] {narrative\\context};
			\draw[outline, align=center] \repdisc node [xshift=-2.5cm, yshift=-3ex] {reported discourse};
		\end{tikzpicture}
	}

	This Venn diagram is schematic; relative sizes are meaningless.
\end{marginparfigure}
As \cref{fig:rd:intro:background:terminology:venn} represents schematically, the main focus here is the intersection between two sets: \emph{reported speech} (a subset of \emph{reported discourse}) within the linguistic context of \emph{narrative}.
Reporting of speech in narrative is characterised by a set of special linguistic features, as is described in details in this chapter.

The particular question of the \mbox{(text-)}linguistic aspects of reported speech (or, more generally, reported discourse) in narrative has been examined in numerous publications, covering different languages as far removed as
Biblical Hebrew\index[langs]{Hebrew!Biblical} (\cites{miller.c.l:2003:speech-hebrew}{longacre.r.e:1994:dialogue-narrative}),
Teribe\index[langs]{Teribe} (a Chibchan language; \cite{koontz.c:1977:dialogue-narrative-teribe}),
English\index[langs]{English} \parencite[ch.~5]{bonheim.h.w:1982:narrative-modes} and
Obolo\index[langs]{Obolo} (an Atlantic-Congo language; \cite{aaron.e:1992:reported}).
As stated above, this question had not been satisfactorily examined in Welsh, so far as I am aware.

\Textcite{guldemann.t:2008:quotative-indexes} uses a set of terms regarding the different parts of signalling reported speech, which is demonstrated by \exfullref{ex:rd:intro:background:terminology:terms} (after ex.~1 in \cite[§~1.1]{guldemann.t:2008:quotative-indexes}):

\preex{}\ex<ex:rd:intro:background:terminology:terms>
	\small%
	\begin{tabular}[t]{lllcl}
		\multicolumn{3}{c}{\color{glosscolour}quotative index (\Qi{})} && \multicolumn{1}{c}{\color{glosscolour}quote (\Quote{})}\\
		\cmidrule{1-3}\cmidrule{5-5}
		\color{glosscolour}speaker (\Sp{}) & \color{glosscolour}nucleus of \Qi{} (\QIN{}) & \color{glosscolour}addressee (\Ad{})\\
		He & said & to me, && ‘Come back tomorrow!’
	\end{tabular}
\xe\postex{}

The basic division is between the \emph{quote} (the direct reported speech itself) and the \emph{quotative index}\index{quotative index} (the linguistic expression which signals, embeds and introduces it into the broader discourse).
The quotative index in the above example can be further divided to \emph{speaker}, \emph{nucleus of quotative index} and \emph{addressee}.
The nucleus of quotative index in our case is the \emph{generic speech verb}%
\footnote{%
	As discussed below in \cref{sec:rd:internal:nucl}, not all of the Welsh examples attested in the corpus have \emph{verbal} nuclei; non-verbal constructions such as \C{\Quote{} oddi wrth \NP{}}[\Quote{} from \NP{}] are also found.
}
(\Gsv{}) ‘said’, which makes a \emph{quotative predicator}.

Furthermore, \textcite{guldemann.t:2008:quotative-indexes} discusses different language-specific \emph{types of quotative indexes}%
\footnote{%
	The plural \emph{indexes} (not \emph{indices}) is used in this context, following the common practice in the scholarly literature \parencite[see][p.~214 n.~6]{haspelmath.m:2013:argument-indexing}.
}
(\emph{QI-types}) and generalises over them.
The micro- and macro-syntactic distinction between three such types in the corpus is central to this chapter.
For brevity, they are hereinafter referred to by shorthand abbreviations: QI1, QI2 and QI3 (read \emph{quotative index type $\mathit{n}$}).



\subsection{Corpus}%
\label{sec:rd:intro:corpus}

{}\index{corpus|(}%
Kate Roberts’ writing is remarkably diverse (\cref{sec:intro:object:corpus}), but she is known best for her short stories.
The corpus for this chapter is two collections of short stories.
These revolve around the lives of two girls growing up in Caernarfonshire, North Wales, the area where the author herself spent her formative years.%
\footnote{%
	\label{fn:Mos=Bilw}%
	In fact, there are some evident parallels between characters, stories and events in the autobiographical works discussed in \cref{sec:anecdotes} (\YLW{} and \Atgofion{}) and their equivalents in the short stories discussed here (Bilw is based on Mos, for example).
	To some extent, Begw is young Kate Roberts.
	I do not know who Winni is based on, if she is indeed based on any particular real-life person.
}

\begin{marginpartable}%
	{The stories in \TG{}}%
	{The stories in \TG{}}%
	{tab:rd:intro:corpus:tg}%
	\tiny%
	\begin{tabular}[t]{rll}
		% \toprule
		1 & Gofid
		  & \glosscolour{Grief}\\
		2 & Y Pistyll
		  & \glosscolour{The Spout}\\
		3 & Marwolaeth Stori
		  & \glosscolour{Death of a Story}\\
		4 & Te yn y Grug
		  & \glosscolour{Tea in the Heather}\\
		5 & Ymwelydd i De
		  & \glosscolour{A Visitor to Tea}\\
		6 & Dianc i Lundain
		  & \glosscolour{Escape to London}\\
		7 & Dieithrio
		  & \glosscolour{Becoming Strangers}\\
		8 & Nadolig y Cerdyn
		  & \glosscolour{The Card Christmas}\\
		% \bottomrule
	\end{tabular}
\end{marginpartable}
One collection is \C{\gl{Te}{tea} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{Grug}{heather}}[Tea in the Heather] \parencite{roberts.k:2004:te}.
It consists of eight short stories (\cref{tab:rd:intro:corpus:tg}) beautifully and perceptively portraying the childhood of Begw Gruffydd, from the age of four in the first story (\C{\gl{Gofid}{grief}}[Grief]) to the age of nine in the last (\C{\gl{Nadolig}{Christmas} \gl{y}{\Def{}} \gl{Cerdyn}{card}}[The Card Christmas]).%
\footnote{%
	See \textcite{parry.g:1959:tyyg} for a review and \textcite[pp.~7–10]{bevan.h:1960:storiau-deffro} for a review and overview of the stories.
}
The stories are interconnected in plot, themes, tone and intertextuality, but each stands on its own to a certain degree.
Linguistically they are quite similar, but they are not uniform with regard to some issues (e.g.\ the difference in use of \C{\gl{meddai}{\meddai{}}} and \C{\gl{ebe}{\ebe{}}}; see \cref{tab:rd:internal:nucl:generic:meddai+ebe:quantitative} in \cref{sec:rd:internal:nucl:generic:meddai+ebe:two}).

\begin{marginpartable}%
	{The stories in \HD{} that are relevant for \cref{sec:rd}}%
	{The stories in \HD{} that are relevant for \cref{sec:rd}}%
	{tab:rd:intro:corpus:hd}%
	\tiny%
	\begin{tabular}[t]{rll}
		% \toprule
		1 & Pryder Morwyn
		  & \glosscolour{A Maid’s Anxiety}\\
		2 & Haul a Drycin
		  & \glosscolour{Sun and Storm}\\
		6 & O! Winni! Winni!
		  & \glosscolour{Oh! Winni! Winni!}\\
		% \bottomrule
	\end{tabular}
\end{marginpartable}
The other collection is \C{\gl{Haul}{sun} \gl{a}{and} \gl{Drycin}{foul\_weather}}[Sun and Storm] \parencite{roberts.k:1981:haul-drycin}, the last book Roberts published.
As she writes in the preface, it consists of six stories printed before in journals (\worktitle{Y Traethodydd}, \worktitle{Pais} and \worktitle{Y Faner}) at different times: they were chosen to be collected in a book so they would not remain scattered across journals.
The first (\C{\gl{Pryder}{anxiety} \gl{Morwyn}{maid}}[Anxiety of a Maid]), second (the titular \C{\gl{Haul}{sun} \gl{a}{and} \gl{Drycin}{bad\_weather}}[Sun and Storm]) and last (\C{\gl{O}{\Interj{}}! \gl{Winni}{\Pn{}}! \gl{Winni}{\Pn{}}!}[Oh! Winni! Winni!]) stories can be seen as a sequel to \TG{} (\cref{tab:rd:intro:corpus:hd}).%
\footnote{%
	The fact that these stories were originally published in journals has some narrative consequences; for example, the readers are reminded in each who some minor characters are.
	This was carried over to the collected volume.
	Unsurprisingly, \HD{} is not as homogeneous as \TG{} in orthographic, literary and linguistic form.
}
Here the focalisation\index{focalisation (narratology)} is shifted from Begw to another character, Winni Ifans (‘Winni Finni Hadog’), who is older than Begw and is introduced in the fourth story of \TG{} (called \worktitle{Te yn y Grug} as well).
Bereft of mother and growing up with her abusive drunken father Twm and her mean and neglecting stepmother Lisi Jên, she begins her way in \TG{} as a wild child.
With the help and under the compassionate care of Begw’s mother Elin she changes and grows, leaving her home to work in service.
The aforementioned later stories follow her life in service and the hardships and joys she faces.
The other stories incidentally bound together in the same volume (№~3–5) are not taken as data for this chapter.

Stories from \TG{} and \HD{} have been mixed and adapted~— rather freely~— as a full-length feature film (\C{\gl{Y}{\Def{}} \gl{Mynydd}{mountain} \gl{Grug}{heather}}[The Heather Mountain], 1998, adapted and directed by Angela Roberts).
A musical (performed at the national \foreignlanguage{welsh}{Eisteddfod} festival in Llanrwst, 2019) was also inspired by the stories, and an audiobook has been produced for \TG{} \parencite{dwyfor.b+:2004:te}.%
\footnote{%
	The film, musical and audiobook are all available on YouTube, albeit the film was uploaded in poor technical quality:
	\begin{compactitem}
		\item Film:\\\url{https://youtube.com/playlist?list=PLHox8if6VDSI1hSd_2Owt3FUDnT1qkdLH}
		\item Musical:\\\url{https://youtube.com/Ak7yOFUwdwI}
		\item Audiobook:\\\url{https://youtube.com/playlist?list=PLHox8if6VDSLRmt6-qVpbR270h3vhLi8p}
	\end{compactitem}
}
The audiobook is relevant to our discussion as it is used as complementary data (\cref{sec:rd:patterns:3:sain}).

\index{methodology}
A methodological note regarding the process of investigation.
I began by examining \TG{} and forming an initial basis for the analysis; only afterwards I had the idea of taking the sequel \HD{} as data as well, which resulted in further development of the analysis.
A fortunate outcome of this unintentional sequence is that \HD{} served in fact as a ‘control group’ of sorts: the analysis of the system whose basic characteristics were discovered using \TG{} as data was tested against a new set of data and was corroborated and refined.

The system that emerges from the corpus is coherent, consistent and subtle.
As stated and demonstrated before, different text-types may exhibit different subsystems.
Thus, the findings cannot be automatically transferred to other texts, which may behave differently to some degree.%
\footnote{%
	For example, \exfullref{ex:anecdotes:structure:examples:dei:development:dialogue} (a dialogue in an anecdote) in \cref{sec:anecdotes:structure:examples:dei:development} shows a syntactic arrangement that is foreign to our corpus of short stories.
	Similarly, \exfullref{ex:anecdotes:comparison:retelling:quotes:tyst} in \cref{sec:anecdotes:comparison:retelling:shared:direct-speech} exhibits a different use of signifiers which superficially resemble QI1 and QI3 discussed here.
}
This is true not only of text-types but of different authors as well; the question of author-specific habits is discussed in \textcite[78]{bonheim.h.w:1982:narrative-modes}.
The topic of applicability is discussed in a more general manner in the conclusion chapter, in \cref{sec:conclusion:further:idiolects,sec:conclusion:further:text-types}.

{}\index{corpus|)}



\subsection{Annotation}%
\label{sec:rd:intro:annotation}

{}\index{annotation|(}%
\begin{table}
	\caption{Annotations used in \cref{sec:rd}}%
	\label{tab:rd:intro:annotation}%
	\begin{tabular}{lll>{/}cl}
		\toprule
		Constituent & Indication & \multicolumn{3}{l}{Example}\\
		\midrule
		Quote & grey colour & \Qquote{‘S’mae byd!’} && \Qquote{‘Hello world!’}\\
		Nucleus of a quotative index & small capitals & \Qnucl{meddai} && \Qnucl{said}\\
		Speaker & italic letters & \multicolumn{3}{l}{\Qsp{Elin Gruffydd}}\\
		Addressee & italic sans-serif & \Qad{wrth Winni} && \Qad{to Winni}\\
		Modification of quotative index & sans-serif & \Qmod{gan chwerthin} && \Qmod{laughing}\\
		\bottomrule
	\end{tabular}
\end{table}
In order to facilitate the use of examples, a system of typographical annotation is employed, as specified in \cref{tab:rd:intro:annotation} and demonstrated in \exfullref{ex:rd:intro:annotation:example}:

\preex{}\ex<ex:rd:intro:annotation:example>
	\gloss
	{%
		¶
		\Qquote{‘Oedd} \Qquote{arnat} \Qquote{ti} \Qquote{ddim} \Qquote{eisio} \Qquote{mynd} \Qquote{i} \Qquote{ddanfon} \Qquote{Winni?’} \Qnucl{meddai} \Qsp{ei} {\notsic{}\Qsp{mam}} \Qad{wrth} \Qad{Begw} \Qmod{wedi} \Qmod{iddynt} \Qmod{fynd}.
		¶
	}
	{%
		{}
		\oedd{} on.\Ssg{} \Ssg{} \dim{\Neg{}} want go.\Inf{} to escort.\Inf{} \Pn{} \meddai{} \eif{} mother to \Pn{} after to.\Tpl{} \Len{}\textbackslash{}go.\Inf{}
		{}
	}
	{%
		¶
		\Qquote{‘Didn’t you want to go see Winni off?’} \Qsp{her mother} \Qnucl{said} \Qad{to Begw} \Qmod{after they’d gone}.
		¶
	}
	{\exsrctyyg{7}{83}}
\xe\postex{}

A pilcrow symbol~(\grapheme{¶}) stands for a paragraph break, while an ellipsis~(\grapheme{\ellipsis{}}) marks that the text continues in the same paragraph.
Verses~— marked by indentation in the printed volumes~— are indicated by a greater-then sign (\grapheme{>}) at the beginning of each line, and line breaks are represented by a slash (\grapheme{/}); see \cref{sec:rd:verses}.

{}\index{annotation|)}


\subsection{Overview of the chapter}%
\label{sec:rd:intro:overview}

Not including a short conclusion which retrospectively mirrors the current overview (\cref{sec:rd:conclusion}), five sections follow this introduction:
\begin{compactitem}
	\item \Cref{sec:rd:patterns} charts the three primary types of quotative indexes in the corpus (QI1, QI2, QI3) and their characteristics, use and function in the text.
	\item \Cref{sec:rd:internal} zooms into the internal structure of the quotative indexes and brings lexical and micro-syntactic issues into consideration.
	\item \Cref{sec:rd:zero} describes the distribution between cases of an overt quotative index in QI1 and a zero quotative index.
	\item \cref{sec:rd:interruptions} deals with the special case of interruption and resumption of the conversation.
	\item The final section, \cref{sec:rd:related}, cursorily examines three topics which are related to the core topic of the chapter: reciting verses, reported thoughts (as part of the umbrella term of \emph{reported discourse}), and the use of speech verbs outside of quotative indexes.
\end{compactitem}
