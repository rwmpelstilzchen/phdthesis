\weakFloatBarrier{}
\section{Introduction}%
\label{sec:drama:intro}

\subsection{Background}%
\label{sec:drama:intro:background}

While memoirs and short stories are texts which are meant to be \emph{read} by the audience%
\footnote{%
	Or listened to in the case of the radio episode upon which \Atgofion{} is based (\cref{sec:anecdotes:corpus}).
	The published version, which \cref{sec:anecdotes} uses as a part of its corpus, is strictly meant to be read, and has no performative or production directions.
},
plays are texts which can be deciphered, interpreted, comprehended, enjoyed and analysed either directly as written texts to be read, or through spectating a performative dramatic actualisation made by a collaborative effort of a director, a cast of actors and a production team.%
\footnote{%
	If one is to apply \posscite{genette.g:1972:discours-recit} terms liberally, the read text and the spectated performance share \foreign{discours} or \foreign{récit} (the narrative text) but not \foreign{narration} (the act of narration); see \cref{fn:intro:text:sub-textual:narrmodes:genette} on p.~\pageref{fn:intro:text:sub-textual:narrmodes:genette}.
}
Situated within the field of linguistics, and aiming at text-linguistic analysis of written Literary Welsh, the current study takes the first route, and examines the corpus~— from the point of view of the \emph{reader}~— as a written literary object, independent of any particular performance.
Even though the portions of the text this chapter deals with contain instructions originally meant for \emph{production}, they can be linguistically analysed as a special kind of text.

\Textcite[§~2.3.1]{helbo.a:1987:theory-performing} distinguishes between two subsets which together make the \emph{theatrical text}:

\begin{compactitem}
	\item The \emph{dramatic text}, which is spoken aloud by the actors.

	\item The \emph{production text} (or \emph{stage directions}), which ‘comprises the scenic annotations destined particularly for actualisation through staging (“\emph{didascalies}”)’%
		\footnote{%
			Cf. \posscite[§~stage directions]{pavis.p:1998:dictionary-theatre} definition:
			‘Stage directions \frde{indications scéniques}{Bühnenanweisungen} include any text \ellipsis{} which is not spoken by the actors and is meant to clarify the understanding or mode of presentation of the play for the reader.
			Examples include names of characters, indications of entrances and exits, descriptions of space, notations on acting.’
		}.
		He distinguishes between four types, according to their target audience:
		\begin{compactitem}
			\item \emph{Meant for the actor}.
				These make the majority of stage directions in our corpus.

			\item \emph{Meant for staging} (set, costumes, lighting, sound effects, music, etc.).
				In our corpus these are absent from the auditory \YC{}, and in the theatrical \YF{} are found mainly in the \C{\gl{\textsc{golygfa}}{setting}} section at the beginning, and sporadically throughout the play.

			\item \emph{Meant for the reader} (of the published play), and have no concretisation on stage.
				These are not found in our plays.

			\item \emph{Meant for the implied spectator}.%
			\footnote{%
				I must admit I do not fully understand what \textcite{helbo.a:1987:theory-performing} means by this: how can such production text can be directed at an (implied) spectator if it does not have an effect on acting or the stage?

				Perhaps examples like \sdcref{c5} (discussed below) fit under this type, as they are worded in a way that refers to the listeners, with the impersonal finite present form \C{-ir}.
				Nevertheless, even though it indirectly and impersonally \emph{refers} to the audience, it is the audio engineer (as a part of the production team) and/or the voice actors who have to interpret that instruction and act accordingly: it is presented ‘through the audience’s ears’, but in actuality it is meant for others to perform.
			}
				These are not found in our plays, as well.
		\end{compactitem}
\end{compactitem}

Thus, this chapter deals with the \mbox{(text-)}linguistic structure of the production text, most of which directs the actors and some of which describes the staging.

\Textcite[208]{ingarden.r:1973:literary-art} formulates a distinction between \initprotrusion\foreign{\gl{Haupttext}{head.text}} ‘primary text’ and \initprotrusion\foreign{\gl{Nebentext}{next\_to-text}} ‘ancillary text’.
The two constitute two parallel signifying systems, where the former constitutes the dialogue of the characters (\posscite[§~2.3.1]{helbo.a:1987:theory-performing} \emph{dramatic text}) and the latter constitutes the stage directions which frame that dialogue (\posscite{helbo.a:1987:theory-performing} \emph{production text}).
Ingarden offers a hierarchical structure, but this view is not shared by all scholars; see \textcite[p.~72~ff.]{aston.e+:2013:theatre-sign} for discussion.
\Textcite{thomasseau.j.m:1984:para-texte}, for example, rejects Ingarden’s terms and the hierarchy they suggest, and proposes the terms \initprotrusion{}\foreign{\gl{texte}{text(\M{})} \gl{dialogué}{dialogue.\Ppp{}.\M{}}} ‘\mbox{dialogue(d)} text’ and \initprotrusion{}\foreign{\gl{para-texte}{para-text}} ‘para-text’, which are distinguished on typographical basis in the printed play%
\footnote{%
	\foreign{Le para-texte est ce texte imprimé (en italiques ou dans un autre type de caractère le différenciant toujours \emph{visuellement} de l’autre partie de l’œuvre) qui enveloppe le texte dialogué d’une pièce de théâtre.}
	‘The para-text is that printed text (in italics or in another typeface, always differentiating it \emph{visually} from the other part of the work) which envelops the \mbox{dialogue(d)} text of the play’.
}~— an approach the present study \revsimple{1}{shares}.
Similarly, \textcite{veltrusky.j:1977:drama-literature} acknowledges stage directions as integral to the literary structure of the play.

Theatre and drama have received scholarly attention in a wide range of fields, and \emph{theatre studies}\index{theatre studies} (\C{theatrology}, \C{dramatics}) is an inherently interdisciplinary field.
Beyond other studies of the language of plays, the interdisciplinary intersection most akin to the structuralist methodology of the present study (\cref{sec:intro:method:struct}) is the semiotic (or \emph{semiologic}) analysis of theatre and drama; notable publications include \textcite{elam.k:2005:semiotics-drama,aston.e+:2013:theatre-sign,helbo.a:1987:theory-performing}, as well as \textcite{carlson.m:1989:places-performance}%
\footnote{%
	Which moves away from the text \foreign{per se} and treats the related topic of the semiotics of spatial, architectural arrangement.
}.
Two relatively recent doctoral theses have been dedicated to Welsh language drama:
one explores the interrelationship between Welsh language and drama \parencite{morris.c.e:2018:iaith-drama}, and
one focusses on Kate Roberts as a playwright and her dramatic works \parencite{jones.d:2014:kr-drama}.
Both treat language and playwriting as cultural, social, literary and historical phenomena, and do not deal with grammatical matters, which are our focus here.
Similarly to the other topics covered in the present \thesis{}, to the best of my knowledge no scholarly attention has been given hitherto to the \mbox{(text-)}grammatical features of Kate Roberts’s plays%
\footnote{%
	Or Welsh language plays in general, as far as I could find.
},
let alone the more specific topic of the grammar of stage directions.

Similarly to the above chapters, the present discussion is language-specific in focus and aim.
While it does not attempt at studying stage directions in a generic language-agnostic manner, some aspects of the results, presentation and methodology might be helpful for the description of this textual unit~— which is little studied \emph{linguistically}%
\footnote{%
	Linguistic treatments of the special grammar of stage directions include
	\textcite[English]{nikiforidou.k:2021:stage-directions}\index[langs]{English},
	\textcite[Russian]{uryson.e.v:2019:stage-directions}\index[langs]{Russian} and
	\textcite[pp.~199–210, Serbo-Croatian]{murphy.a.b:1974:aspectual-present}\index[langs]{Serbo-Croatian}.
}~—
in other languages.

The stage directions are marked in the studied corpus using the commonplace parentheses and italic letters \parencite[72]{aston.e+:2013:theatre-sign}.
Thus, together with the explicitly designated introductory specifications%
\footnote{%
	This term is adopted from \textcite[282]{smiley.s:2005:playwriting}.
}
(\cref{sec:drama:pre}), the delimitation and definition of the textual unit in question is given by the \mbox{author(s)}.
This is in contrast with the anecdote, whose structural definition had to be derived from analysing the text, identifying recurring text-linguistic patterns.
Another factor which distinguishes the dramatic text from the production text is the form of the language used in each: the first is characterised by a markedly colloquial language~— in structure and orthography%
\footnote{%
	Welsh, in contrast to English, is relatively free when it comes to representing colloquial forms and pronunciations in writing.%
	\revsimple{1}{}%
	% For example, in addition to the literary \C{dweud}[say] and the archaic \C{dywedyd}, the following forms are attested, among others:
	% \C{deud},
	% \C{deyd},
	% \C{dwed},
	% \C{dweyd},
	% \C{dywed},
	% \C{dŵad} and
	% \C{d’wed}.
}~—
while the latter is closer to the contemporary standard literary form.

The different components which make a play are broadly comparable with the ones which make a commonplace non-performative linguistic narrative (discussed in \cref{sec:intro:text:sub-textual:narrmodes} above).%
\footnote{%
	In fact, \posscite[§~2.3]{eshel.o:2015:narrative} \emph{mise-en-scène} narrative mode is directly adopted from theatre studies\index{theatre studies} and film theory\index{film theory} \parencite[n.~27 on p.~29]{eshel.o:2015:narrative}.
	See \textcite[p.~9 and passim]{veinstein.a:1955:mise-en-scene} for considerations regarding terminology and the history of science.
}
\begin{table}%
	\caption{Comparison of the components that make plays and commonplace narrative texts}%
	\label{tab:drama:intro:background:modes}%
	% \footnotesize%
	\begin{tabular}{llll}
		\toprule
		Play & \cite{bonheim.h.w:1975:theory-narrative-modes,bonheim.h.w:1982:narrative-modes} & \cite{shisha-halevy.a:1998:roberts} & \cite{eshel.o:2015:narrative}\\
		\midrule
		introductory specifications & description & (subset of comment mode) & \foreign{mise-en-scène}\\
		dramatic text & speech & dialogue & consciousness mode\\
		(active) stage directions & report & evolution mode & evolution mode\\
		\bottomrule
	\end{tabular}
\end{table}
\Cref{tab:drama:intro:background:modes} presents a general comparison.
The details may vary, but the fundamental common features stem from the fact that both drama and narrative texts are different modalities realising the same core human notion of \emph{narrative}.



\subsection{Corpus}%
\label{sec:drama:intro:corpus}

{}\index{corpus|(}%
Although Roberts is primarily recognised as an author of short stories and novels (\cref{sec:intro:object:corpus}), she has written no less than thirteen plays and translated one \parencite[p.~3~f.]{jones.d:2014:kr-drama}, and was an active figure in the contemporary scene.
For the purpose of this chapter two short plays, one act each%
\footnote{%
	\YC{} has five monologues after the \C{\textsc{llen}}[\textsc{curtain}], but this epilogue is excluded from the present study as it lacks any stage directions but the English \emph{\textsc{cross fade into music}} between the monologues.
},
have been chosen:
\begin{compactitem}
	\item \C{\gl{Y}{\Def{}} \gl{Fam}{mother}}[The Mother] (1920; published as \cite{davies.b.e+:1920:y-fam}), the first original play published by Roberts%
		\footnote{%
			According to \parencite[p.~3~f.]{jones.d:2014:kr-drama} two plays preceded it:
			\C{Y Botel}[The Bottle] in 1910 (a translation of \worktitle{The Bottle} by T.\ P.\ Taylor) and
			\C{Yn Eisiau, Howscipar}[Wanted: Housekeeper] in 1915 (performed by \C{Cymdeithas y Ddraig Goch}[The Red Dragon Society] that year; no copies survived).
		},
		and indeed her first published book.
		It was written together with Betty Eynon Davies, with whom Roberts co-wrote three other plays.
		The fact it was co-written by two dramatists is not taken into consideration here; the text is taken as is.%
		\footnote{%
			If fact, the exact role of each of the two is debated in the scholarly literature \parencite[see][p.~64~ff. and passim]{jones.d:2014:kr-drama}.
		}
		This does not affect the homogeneity of the data in this \thesis{}~— at least not to a significant degree~— as the findings found here generally hold true for other stage plays by Roberts.

	\item \C{\gl{Y}{\Def{}} \gl{Cynddrws}{outer\_door}}[The Outer Door] (1954; edited in \textcite[§~4.ii]{jones.d:2014:kr-drama}~— \textcite{roberts.k:2014:y-cynddrws}~— on the basis of the author’s manuscripts), a radio play broadcast that year on Radio Cymru.
\end{compactitem}

{}\index{corpus|)}



\weakFloatBarrier{}
\subsection{Annotation}%
\label{sec:drama:intro:appendix}

{}\index{annotation|(}%
Similarly to the two previous chapters, this chapter is also accompanied by an appendix (\cref{app:drama}).
It contains the whole text of \YF{} (\cref{app:drama:fam}) and only the stage directions of \YC{}, with minimal necessary co-text (\cref{app:drama:cynddrws}), where the symbol \grapheme{\ellipsis{}} indicates an omission within the same paragraph and \grapheme{\vellipsis{}~×~$n$} an omission of $n$ paragraphs.

To the best of my knowledge, neither of the plays has been translated into English.
For this reason, I translated them myself (the whole of \YF{} and the relevant portions of \YC{}).
The purpose of this translation is not \revsimple{1}{to} be a stand-alone literary translation, but to make the thinnest readable English cover over the Welsh original, sacrificing idiomaticity at times for the sake of reflecting the original as close as possible.
One case in which English idiomaticity is abandoned on purpose in favour of servile rendering is the \C{[\ProNP{} \ynD{} \Inf{}]}\index[welsh]{[ProNP yn INF]@[\ProNP{} \ynD{} \Inf{}]} pattern, which is rendered as \E{[NP \V{}-ing]} (e.g.\ \C{\gl{\ch{Eiry}}{\Pn{}} \gl{yn}{\ynD{}} \gl{crio}{cry.\Inf{}} \gl{mwy}{more}} as ‘\ch{Eiry} crying more’, without ‘is’).

\begin{table}
	\caption{Syntactic forms of stage directions and their colour codes in \cref{app:drama}}%
	\label{tab:drama:intro:appendix:colours}%
	\footnotesizetabruby{}%
	\begin{tabular}{cllllll}
		\toprule
\multicolumn{2}{l}{Colour} & Shorthand & Meaning & \multicolumn{3}{l}{Example}\\
		\midrule\\[\firsttabglosskip]
		\circlegend{sronpalegreen}  & green  & \Prs{} & finite verb in present tense                                 & \sdcref{94} & 
		\tabC{\tabgl{Ysgwyd}{shake.\Prs{}.\Tsg{}} \tabgl{ei}{\eim{}} \tabgl{ben}{head}} & He shakes his head\\[\tabglosskip]
		%\sdcref{88}  & \tabC{\tabgl{Rhed}{run.\Prs{}.\Tsg{}} \tabgl{y}{\Def{}} \tabgl{plant}{child.\Col{}} \tabgl{ati}{\ati{}}} & The children run to her\\[\tabglosskip]
		\circlegend{sronpaleblue}   & blue   & \ynD{} & \tabC{[\ynD{} \Inf{}]} converbal form                        & \sdcref{c25} & \tabC{\tabgl{yn}{\ynD{}} \tabgl{gweiddi}{shout.\Inf{}}} & shouting\\[\tabglosskip]
		\circlegend{sronpalecyan}   & cyan   & \ynB{} & \Stack{\tabC{[\ynB{} \Adj{}]} deadjectival adverb\\and related forms} & \sdcref{130} & \tabC{\tabgl{yn}{\ynB{}} \tabgl{dawel}{quiet}} & quietly\\[\tabglosskip]
		\circlegend{sronpaleyellow} & yellow & \NP{}  & stand-alone noun phrase                                       & \sdcref{121} & \tabC{\tabgl{Seibiant}{pause} \tabgl{byr}{short}} & A short pause\\[\tabglosskip]
		\circlegend{sronpalered}    & red    & \Ad{}  & addressee                                                    & \sdcref{1}   & \tabC{\tabgl{wrth}{with} \tabgl{\textsc{Siencyn}}{\Pn{}}} & to \textsc{Siencyn}\\[\tabglosskip]
		\circlegend{sronpalegrey}   & grey   & other  & other forms                                                  & \sdcref{125} & \tabC{\tabgl{mewn}{in} \tabgl{llais}{voice} \tabgl{ofnus}{fearful}} & in a fearful voice\\
		\bottomrule
	\end{tabular}
\end{table}
Each basic stage directions segment (sentence or phrase)%
\footnote{%
	Dependent clauses (such as adjunctive converbs or the \C{[a \Inf{}]} construction) are segmented together with their nucleus.
	On the other hand, sentences which consist of two segments which make a distinct head syntactic type each are split.
}
is numbered%
\footnote{%
	Similarly to the anecdotes in \cref{app:anecdotes}, the numbering of the second play, \YC{}, begins from 201, so references to the two plays can be easily distinguished.

	‘\Sd{}’ references throughout this chapter refer to the correspondent stage directions in the appendix.
	Navigating back and forth can be done using hyperlinks (\cref{sec:intro:meta:tech:hyperref}); alternatively, two windows can be opened side by side.
}
and colour-coded%
\footnote{%
	A corresponding shorthand is indicated beneath the number, so there is no need to memorise the colouring scheme.
}
according to a simple scheme described in \cref{tab:drama:intro:appendix:colours}.
The colour of each segment corresponds to its syntactic form.
The minimal nucleus syntagm which determines the syntactic identity of the whole segment according to the said scheme is marked in bold letters;
when a segment has several concatenated nuclei, each is set in bold letters.
Like in previous chapters, the annotation is mirrored in the English translation.

All stage directions have been collected in a database.%
\footnote{%
	It is available in a simple tab-separated values (TSV) format online:\\
	\url{https://gitlab.com/rwmpelstilzchen/phdthesis/-/blob/master/db/drama.csv}

	The \texttt{ID} field in the database refers to internal labels (the same labels used in the \LaTeX{} source files), not the sequential ones found in the PDF output of the \thesis{}.
}
Querying it proved helpful for achieving both quantitative and qualitative findings.

{}\index{annotation|)}



\subsection{Overview of the chapter}%
\label{sec:drama:intro:overview}

This chapter is shorter than \cref{sec:anecdotes,sec:rd}, and its structure is simpler.
Two sections follow this introduction:

\begin{compactitem}
	\item \cref{sec:drama:pre} is a short section which describes the syntactic forms found in the \emph{introductory specifications} set before the dramatic text itself begins:
		\foreign{dramatis personae} (in both plays),
		place and time (\YF{} only)
		and the setting (developed in \YF{}; embryonic to non-existent in \YC{}).

	\item \cref{sec:drama:sd} is the main section of the chapter.
		It examines the syntactic forms found in \emph{stage directions} accompanying the dramatic text and their textual functions.
\end{compactitem}
