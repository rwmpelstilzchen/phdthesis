\section{Introductory specifications}%
\label{sec:drama:pre}

\subsection{\foreign{Dramatis personae}}%
\label{sec:drama:pre:personae}

\index{dramatis personae@\foreign{dramatis personae}|(}%
Following the standard form, both plays begin with a \foreign{dramatis personae} section (\C{(\gl{y}{\Def{}}) \gl{cymeriadau}{character.\Pl{}}}).
The two plays differ in the type of their cast of characters, which is reflected in language.

The characters in \YF{} (\cref{app:drama:fam}) all belong in one household: one servant and several family members.
First the \foreign{‘pater familias’} Ifan is listed as \C{\gl{y}{\Def{}} \gl{Tad}{father}}[the Father], followed by his family in relation to him (with the \C{\gl{ei}{\eim{}}} and their term of relation).
Last Siencyn the servant is listed, as \C{\gl{y}{\Def{}} \gl{Gwas}{Servant}}.
Two related features stand out:
\begin{compactitem}
	\item The definite article \C{\gl{y}{\Def{}}} in front of Ifan and Siencyn’s descriptions, and the possessive article \C{\gl{ei}{\eim{}}} in the descriptions of the others.%
		\footnote{%
			The possessive article in Welsh, similarly to English, marks its noun phrase as definite.
		}

	\item The use of capital letters in all the descriptions (\C{\gl{Tad}{father}}, \C{\gl{Wraig}{\Len{}\textbackslash{}woman}}, \C{\gl{Ferch}{\Len{}\textbackslash{}girl}}, etc.).%
		\footnote{%
			In general, the capitalisation norms of Welsh are more or less the same as in English.
		}
\end{compactitem}

The two features suggest the status of these characters is similar to that of stock characters.
This is in line with the flat nature of the characters in the play: Nano is an archetypical neglecting and abusive \emph{wicked stepmother}%
\footnote{%
	A rounder, more developed take on this trope can be seen later in Roberts’s writing, in the character of Lisi Jên in \TG{} and \HD{}.
},
Eiry and Gwyn are stereotypical little \emph{girl} and \emph{boy}, etc.
This has to do with the folkloristic nature of the play, which reads like a sort of a fairy tale (see \textcite[p.~66~f.]{jones.d:2014:kr-drama} for discussion on the folk origins of the play), whose final part walks the line between the Otherworld, dream and a child’s imagination and interpretation of reality on the one hand, and the down-to-earth adult world on the other hand.%
\footnote{%
	Gwyn the child has an otherworldly interpretation of Eiry’s death (his and Eiry’s dead mother came and took her with her \C{yn bell—bell—i dyfu yn hogan dda}[far~— far away~— to grow up a good girl]), while Ifan the father sees it as a tragic occurrence within the natural world (\C{Syrthio i lawr y \foreign{steps} ’nath hi}[What she did was falling down the stairs]).
	Both scenes and interpretations are played in front of the audience, with no clear judgement by the dramatists which one is ‘true’.

	Blurring the line between different states of conscious is a recurring theme in Roberts’s works.
}

The \foreign{dramatis personae} of \YC{} (\cref{app:drama:cynddrws}) are presented in a more typical form: indefinite nouns (\C{\gl{bardd}{bard}}, \C{\gl{ffermwr}{farmer}}, \C{\gl{crwydryn}{wanderer}}, etc.) modified adnominally; cf.\ the first class (\emph{character: identification}) in table~5.1 (\emph{classification of stage directions}) in \textcite[ch.~5]{aston.e+:2013:theatre-sign}.
\index{dramatis personae@\foreign{dramatis personae}|)}%



\subsection{Place and time}%
\label{sec:drama:pre:spatiotemporal}

These are absent from \YC{}, and are minimal in \YF{}, where:
\begin{compactitem}
	\item The place (\C{\gl{lle}{place}}) is a noun phrase: \C{\gl{Cegin}{kitchen} \gl{fferm}{farm} \gl{Tŷ’n}{house-\ynA{}} \gl{Mynydd}{mountain}}[The kitchen of a farm (called) \foreign{Tŷ’n Mynydd}]%
	\footnote{%
		The name of the farm~— literally \emph{House in Mountain}~— encapsulates its most salient feature, namely its remoteness and loneliness.
		\rev{1}{%
			The use of \C{’n}[\ynA{}] with an indefinite noun phrase (\C{mynydd}) is not unproblematic (in general use \C{mewn}[in] appears with indefinite noun phrases, in complementary distribution\index{complementary distribution}).
			Another possible reading of \C{Tŷ’n} is as a contraction of \C{tyddyn}[homestead] (\textcite[§~tyddyn]{gpc:arlein} gives \C{tyn}), but then the circumflex (\C{to bach}) and the apostrophe are unexplained.
		}
	}.

	\item The time is \C{\gl{Saith}{seven} \gl{o’r}{of-\Def{}} \gl{gloch}{bell}, \gl{Nos}{night} \gl{cyn}{before} \gl{G’lan}{first\_day} \gl{Gaea’}{winter}}[Seven o’clock, the night before All Saints’ Day], foreshadowing an otherworldly atmosphere.
\end{compactitem}



\subsection{Setting}%
\label{sec:drama:pre:setting}

The setting (\C{\gl{golygfa}{set, scene}}) section of \YF{} contains forms which occur in the main stage directions (discussed in \cref{sec:drama:sd} below).
Nevertheless, these forms bear different functions here and there.

The setting is a special unit, linguistically set apart from the rest of the play in both form and function.
With regard to information structure\index{information structure}, it is \emph{almost} a \foreign{tabula rasa}: not all of the information given in the \foreign{dramatis personae}, \foreign{scene} and \foreign{time} sections is acknowledged as given.%
\footnote{%
	For example, the place is reiterated in \sdcref{g6}, the time in \sdcref{g12}, and Nano and Siencyn are described again in \cref{appex:drama:g13,appex:drama:g14}.
	Ifan is treated as known in \sdcref{g15}, though.
}
\Textcites[§~1.4]{erteschik-shir.n:1997:focus-structure}[§~2.1.4]{erteschik-shir.n:2007:is} terms similar structures which do not depend on preceding information as their topic but present all-new information~— fittingly enough for our discussion%
\footnote{%
	 \Textcite[§~1.4]{erteschik-shir.n:1997:focus-structure}: ‘Such topics I call Stage topics using stage in the theatrical sense, the place where events appear before an audience.’
}~—
as having \emph{stage topics}\index{stage topic}.
Several nouns are definite, though, as they are assumed as shared knowledge about theatre:
\C{\gl{y}{\Def{}} \gl{goleu}{light}} (\sdcref{g8})
and \C{\gl{y}{\Def{}} \gl{goleuadau}{light.\Pl{}}} (\sdref{g6});
\C{\gl{y}{\Def{}} \gl{gynulleidfa}{audience}} (\sdref{g7}) and \C{\gl{y}{\Def{}} \gl{bobl}{people}} (\sdref{g8});
\C{\gl{y}{\Def{}} \gl{mur}{wall}} (\cref{appex:drama:g5,appex:drama:g6,appex:drama:g8,appex:drama:g10});
and the directional \C{\gl{y}{\Def{}} \gl{dde}{right}} (\sdref{g6}) and \C{\gl{y}{\Def{}} \gl{chwith}{left}} (\cref{appex:drama:g8,appex:drama:g9}), which refer to the blocking of the stage.
In addition, the definite \C{\gl{y}{\Def{}} \gl{tân}{fire}}[the hearth] (\sdref{g7}) and \C{\gl{y}{\Def{}} \gl{dresel}{dresser}}[the dresser%
\footnote{%
	The Welsh dresser is a type of dresser consisting of a cabinet of cupboards and drawers surmounted by rows of shelves, on which plates, dishes, and kitchen utensils are arranged \parencite[§~Welsh, \textit{adj.} and \textit{n.}~\symbolglyph{👉} Welsh dresser, \textit{n.}]{oed:online}.
	It used to have an important cultural role \parencite[ch.~5]{barnwell.r+:2014:cartrefi-cymru}.
}]
are also assumed as shared knowledge, not about theatre but of Welsh kitchens.

The setting section begins with \sdcref{g1},
a noun phrase which restates the place (\C{\gl{lle}{place}}).
Generally speaking, bare syntactically self-standing noun phrases are uncommon in Welsh, but they are found here in the setting section, as well as in the main stage directions (where their function is different; see \cref{sec:drama:sd:syntax:np}).

Next (\sdcref{g2}) there occurs a deontic binomial \C{[\gl{rhaid}{\rhaid{}} \gl{i}{to} \ProNP{} \Len{}\textbackslash{}\Inf{}]}[\ProNP{} has to \Inf{}] pattern%
\footnote{%
	Literal approximation: \emph{Necessary for \ProNP{} to \Inf{}}.
	The first constituent, \C{rhaid}[need], makes the rheme\index{rheme} and the clause-content pattern \C{i \ProNP{} \Len{}\textbackslash{}\Inf{}} makes the theme in this binomial pattern \parencite[§~3.4.1]{shisha-halevy.a:1998:roberts}.
} which refers back to the kitchen (now in a definite form, after it has been presented).
For comparison, in commonplace third-person prose narrative writing such modal constructions are usually limited to the \emph{comment mode}\index{narrative modes!comment mode} or to the content of dialogues%
\footnote{%
	See \cref{sec:intro:text:sub-textual:narrmodes} for the various narrative modes.
}.
Here the deontic modality is directed at the set designer.
This is the only place in both plays where an explicit deontic language is used in the production text; even though we deal with stage directions~— that is \emph{instructions} which tell the theatre personnel how to realise the play in actual performance~— all other instances have plain indicative forms, which are instructive only in implication.

The rest of the first paragraph has one of three forms describing the props on set, all fulfilling the same basic function:
\begin{compactitem}
	\item Self-standing noun phrases:
		a table, dishes and milk (\sdcref{g3});
		a sewing machine (\sdref{g4});
		a bed (\sdref{g6});
		the hearth (\sdref{g7});
		two doors (\sdref{g8}).

	\item An \C{(\gl{y}{\yr{}}) \gl{mae}{\mae{}} \NP{}}[there is \NP{}] existential pattern (\sdcref{g9}).

	\item Stative posture verbal constructions, either finite (\cref{appex:drama:g10,appex:drama:g11}) or converbal (\sdcref{g5}).
\end{compactitem}

Stative posture verbs are being used in various languages not only for literal posture predicates but also for denoting location and existence (the second step in \posscite{kuteva.t.a:1992:sit-stand-lie} \textsc{posture}~> \textsc{locative/existential}~> \textsc{aspectual} path of grammaticalisation)%
\footnote{%
	See also \textcite{newman.j:2002:sitting-standing-lying}, a volume dedicated to typological and diachronic aspects of posture verbs.

	While Welsh did develop a periphrastic aspectual system which is rooted in a spatiotemporal metaphor (and spatiotemporal paradigmatic commutation), this system is not based on posture metaphor but on existence metaphor (for example, \C{Mae hi’n canu}[she is singing] literally means, approximately, ‘she is (exists) in singing’); cf., for example, \C{Ond mae gobaith}[But there is hope] in Roberts’s short story \worktitle{Cyfeillgarwch}.
	The forms used in this system ultimately originated from the common Indo-European \E{‘be’} roots
	\foreign{*h\textsubscript{1}es-} (whence come vowel-initial forms such as \C{wyf}[\wyf{}]; cf.\ English\index[langs]{English} \emph{am}) and
	\foreign{*bʰu̯eh\textsubscript{2}-} (whence come the \C{b-} forms such as \C{bod}[\bod{}]; cf.\ English\index[langs]{English} \E{be}).
	See \textcites{zimmer.s:1999:be-old-welsh}[§§~*h\textsubscript{1}es-, *bʰu̯eh\textsubscript{2}-]{rix.h+:2001:indogermanischen-verben} for further discussion.
}.
Even though Welsh has no posture-based grammatical system of location and existence, \C{\gl{yn}{\ynD{}} \gl{hongian}{hang}}, \C{\gl{saif}{stand.\Prs{}.\Tsg{}}} and \C{\gl{gorwedd}{lie.\Prs{}.\Tsg{}}} function here in a similar manner, but still bear full lexical (not grammaticalised) force.
The difference between the converbal form and the finite form cannot be asserted here in this particular environment due to the paucity of examples in the studied corpus.

The second paragraph of the setting proceeds from describing the static set to describing the opening scene.
It states the time in a common idiomatic manner (\C{\gl{Mae’n}{\mae{}-\ynC{}} \gl{saith}{seven} \gl{o’r}{of-\Def{}} \gl{gloch}{bell} \ellipsis{}}[It is seven o’clock \ellipsis{}]; \sdcref{g12}).
Then each of the characters present on stage is introduced: Nano with a posture verb (\sdcref{g13}), and Siencyn and Ifan without such syntactic intermediation but as syntactically independent noun phrases (\cref{appex:drama:g14,appex:drama:g15}).
The setting concludes with \sdcref{g16}, a sentence whose predicate is a finite verb; it is not an event but a state.

Being a radio play, \YC{} lacks a real setting.
The closest thing to an acoustic ‘setting’ is the first word right after the \foreign{dramatis personae}: \C{\gl{Distawrwydd}{silence}} (\sdcref{cg1}), which is immediately broken by two sighs.
Although there is no explicit \C{\gl{golygfa}{set, scene}} heading in \YC{}, this first paragraph is set apart typographically, distinguished from the ways both the dramatic text%
\footnote{%
	Set with upright letters and no parentheses, tabulated to the right of the name of the character who is performing the dramatic text.
}
and the production text%
\footnote{%
	Set in italic letters within parentheses.
}
are set: it is printed in upright letters, with no tabulation and no parentheses.
