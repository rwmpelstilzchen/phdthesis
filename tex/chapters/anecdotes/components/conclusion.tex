\weakFloatBarrier{}%
\subsection{Conclusion}%
\label{sec:anecdotes:components:conclusion}
{}\index{anecdote components!conclusion|(}%

The \Cconclusion{}, as its name suggests, is the last section.
Its textual function is to refer back to the anecdote, commenting about it as a whole from outside (as defined in \cref{sec:anecdotes:structure:overview:sections}).
It is the rarest section, with only \anecnum{nine} instances%
\footnote{%
	For this reason all examples are presented here with glosses, as exx.~\getfullref{ex:anecdotes:components:conclusion:2}–\getfullref{ex:anecdotes:components:conclusion:169} (\cref{sec:anecdotes:components:conclusion:exx}).
	% The \Cconclusions{} in exx.~\getfullref{ex:anecdotes:components:conclusion:78} and \getfullref{ex:anecdotes:components:conclusion:98} are longer than presented here; omittable parts have been omitted in order to make the presentation focussed (see the respective \cref{app:anecdotes:78,app:anecdotes:98} for the full text).
},
but it is structurally distinct and thus deserves to be distinguished.

Similarly to the \Cabstract{}, the \Cconclusion{} has~— so to speak~— one foot in the anecdote and one \revsimple{1}{in the} broader text in which it is embedded, serving as an intermediatory element (see also \cref{sec:anecdotes:embedding}).
Textually, its function is to reinforce the theme of the text in which the anecdote is embedded or to expand upon the anecdote.



\subsubsection{Meta-reference}%
\label{sec:anecdotes:components:conclusion:meta}

Another resemblance to the \Cabstract{} is that most \Cconclusions{} have a meta-referential element in them%
\footnote{%
	Typeset in small capital letters.
},
but a minority (in fact one) does not.
That one example is \exfullref{ex:anecdotes:components:conclusion:172}, which presents the ‘moral’ of the anecdote as an exclamative statement (opened with \C{\gl{Mor}{\mor{}}}[So~\ellipsis{}~!, How~\ellipsis{}~!]) with no cataphoric meta-reference (an equivalent with such a reference could look like \exfullref{sec:anecdotes:components:conclusion:127+meta}).

\preex{}\ex<sec:anecdotes:components:conclusion:127+meta>
	\gloss
	{%
		Dengys straeon fel \Ameta{yna} pa mor dynn yw’r llinynnau \ellipsis{}
	}
	{%
		show.\Prs{}.\Tsg{} story.\Pl{} like \ynadem{} \Q{} \mor{} tight \copyw{}-\Def{} string.\Pl{} 
	}
	{%
		Stories like \Ameta{that} show how strong the ties are \ellipsis{}
	}
	{\srcolour{(a paraphrase of \exfullref{ex:anecdotes:components:conclusion:172})}}
\xe\postex{}

The meta-reference can be lexical or~— differently from the \Cabstract{}~— a demonstrative element that stands on its own.
Welsh has a complex demonstrative system \parencites[§§~4.183–4.187]{thomas.p:2006:gramadeg}[§§~236–237]{thorne.d:1993:grammar}.
It has several series of \revsimple{1}{demonstratives}, which can be formally classified according to their first phonemes, as \C{h-} (demonstratives proper), \C{y-} (homonymic with locative markers), \C{dy-} (homonymic with presentatives), \C{rh-} (plural; originally from \C{\gl{rhai}{\rhai{}}}~+ other elements), and a compound series that combines the first two; these form an intricate system, the full details of which lay beyond the scope of this subsection.
To our purpose here it is relevant to note that the \C{\gl{yna}{\ynadem{}}} and the compound \C{\gl{honyna}{\honyna{}}} are strikingly common in the \Cconclusion{}.
The phrase \C{\gl{fel}{like} \gl{yna}{\ynadem{}}}[like that]%
\footnote{%
	This is a rather frequent collocation, which has a contracted form in colloquial speech (represented as \C{felna} or \C{fel’na} in writing).
}
is found in four \Cconclusions{} (exx.~\getfullref{ex:anecdotes:components:conclusion:2}, \getfullref{ex:anecdotes:components:conclusion:72}, \getfullref{ex:anecdotes:components:conclusion:78} and \getfullref{ex:anecdotes:components:conclusion:98}).
As a part of \C{[\Def{} \NP{} \Dem{}]}[this/that \NP{}], \C{\gl{yna}{\ynadem{}}} is found in \exfullref{ex:anecdotes:components:conclusion:98} (\C{\gl{y}{\Def{}} \gl{stori}{story(\F{})} \gl{yna}{\ynadem{}}}[that story]); the equivalent with \C{h-} is found in \exfullref{ex:anecdotes:components:conclusion:78} (\C{\gl{y}{\Def{}} \gl{stori}{story(\F{})} \gl{hon}{\hon{}}}[that story]).
The compound form \C{\gl{honyna}{\honyna{}}} is found in \exfullref{ex:anecdotes:components:conclusion:63}.

The attested \Cconclusions{} share the lexical meta-references \C{\gl{stori}{story}} (\getfullref{ex:anecdotes:components:conclusion:6}, \getfullref{ex:anecdotes:components:conclusion:78} and \getfullref{ex:anecdotes:components:conclusion:98}) and \C{\gl{enghraifft}{example}} (\exfullref{ex:anecdotes:components:conclusion:63}) with the \Cabstract{} (\cref{sec:anecdotes:components:abstract:meta-reference:lexeme}).
In addition, \C{\gl{hanes}{history}}[history, story]%
\footnote{%
	The exact difference in use between \C{stori} and \C{hanes} is interesting.
	While they may have some difference in denotation or connotation, it is not impossible that the \C{stori} which that \C{hanes} reminds the author of in \exfullref{ex:anecdotes:components:conclusion:78} affects the choice of lexeme, as if to avoid repetition of \C{stori} twice in proximity.
	One way or the other, \C{hanes} here does not mean \emph{history} in the common way one uses the word in English.
}
is found in \exfullref{ex:anecdotes:components:conclusion:78}.

Similarly to the specific lexemes used in the \Cabstract{} (\C{\gl{trychineb}{disaster} and \C{\gl{trasiedi}{tragedy}}); see \cref{sec:anecdotes:components:abstract:meta-reference:lexeme}}, here too we see such lexemes:
\C{\gl{cyd-ymddibyniaeth}{co-\Refl{}.dependence}} in \exfullref{ex:anecdotes:components:conclusion:2}, which refers to the kind of interdependence described in the anecdote, and
\C{\gl{ymweliadau}{visit.\Pl{}}} in \exfullref{ex:anecdotes:components:conclusion:169}, which refers back to the visit described in that anecdote (\aneccref{169}) and the one immediately before it (\aneccref{168}), as representatives for other visits as well.



\subsubsection{Tense}%
\label{sec:anecdotes:components:conclusion:tense}

A number of tense forms are attested in the \Cconclusions{}, which mark the temporal and aspectual relation of the statement.
\begin{compactitem}
	\item The preterite is attested referring to general (\exfullref{ex:anecdotes:components:conclusion:2}) and personal (\exfullref{ex:anecdotes:components:conclusion:169}) history, as well as to events told in the anecdote (\exfullref{ex:anecdotes:components:conclusion:78}%
		\footnote{%
			\C{Gwers ar gyfer y dyfodol a \highlight{roes} Modryb Neli}[It was a lesson for the future that Neli \highlight{gave}].
		})~— all in a way that treats the past as complete, without interior composition.

	\item The habitual imperfect in \exfullref{ex:anecdotes:components:conclusion:72} (\C{\gl{byddem}{\byddem{}}}) describes a general statement about how things used to be, while the (non-habitual) imperfect in \exfullref{ex:anecdotes:components:conclusion:1} (\C{\gl{oedd}{\oedd{}}}) seems like it might be more limited.%
		\footnote{%
			A more minute and more strongly established generalisation on the system has to take more examples into consideration, of course.
		}
		In \exfullref{ex:anecdotes:components:conclusion:6} \C{\gl{oedd}{\oedd{}}} would not be readily substitutable with \C{\gl{byddai}{\gl{byddai}{}}}, as the need to know XY is not a habitual matter.
	
	\item Present forms can be
		atemporal (as
		\exfullref{ex:anecdotes:components:conclusion:63},
		\C{\gl{Dengys}{show.\Prs{}.\Tsg{}}} in \exfullref{ex:anecdotes:components:conclusion:78},
		\C{\gl{nid}{\Neg{}} \gl{yw}{\yw{}}} in \exfullref{ex:anecdotes:components:conclusion:98}, and
		the general conclusion drawn in \exfullref{ex:anecdotes:components:conclusion:172}) or
		% mae’n sicr
		% ex:anecdotes:components:conclusion:78
		% tadogir
		% ex:anecdotes:components:conclusion:98
		concrete and actual, bound to the author’s present (as
		\C{\gl{Mae’r}{\mae{}-\Def{}} \gl{hanes}{history} \gl{yn}{\ynD{}} \gl{fy}{\fy{}} \gl{atgoffa}{remind.\Inf{}}} in \exfullref{ex:anecdotes:components:conclusion:78} or
		\C{\gl{Gwn}{know.\Prs{}.\Fsg{}}},
		\C{\gl{yr}{\yr{}} \gl{wyf}{\wyf{}} \gl{mor}{\mor{}} \gl{sicr}{sure} \ellipsis{}}, and
		\C{\gl{Nid}{\Neg{}} \gl{wyf}{\wyf{}} \gl{yn}{\ynC{}} \gl{siŵr}{sure}}
		in \exfullref{ex:anecdotes:components:conclusion:98}).
\end{compactitem}



\subsubsection{Examples from the corpus}%
\label{sec:anecdotes:components:conclusion:exx}

\preex{}\ex<ex:anecdotes:components:conclusion:2>
	\gloss
	{%
		Allan o \Ameta{gyd-ymddibyniaeth} fel \Ameta{yna} y tyfodd rhyw fath o ffyddlondeb a theyrngarwch a chyfeillgarwch.
	}
	{%
		out of co-\Refl{}.dependence like \ynadem{} \yrRel{} grow.\odd{} \rhyw{} kind of trust and loyalty and friendship
	}
	{%
		\notsic{}From \Ameta{inter-dependence} like \Ameta{that} grew a kind of trust, loyalty and friendship.
	}
	{\exsrcapp{anecdote}{2}{ylw}{3}{31}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:6>
	\gloss
	{%
		Wrth reswm, rhaid oedd adnabod XY yn drwyadl, fel y gwnâi ei gyd-weithwyr, i allu gwerthfawrogi\Ameta{’r} \Ameta{stori}.
	}
	{%
		with reason \rhaid{} \copoedd{} be\_acquainted.\Inf{} \Pn{} \ynB{} thorough like \yrRel{} do.\ai{} \eim{} co-worker.\Pl{} to \gallu{}.\Inf{} appreciate.\Inf{}-\Def{} story
	}
	{%
		\notsic{}Of course, you’d need to know XY thoroughly, as his fellow workers did, to appreciate \Ameta{the story}.
	}
	{\exsrcapp{anecdote}{6}{ylw}{3}{36}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:63>
	\gloss
	{%
		Mae \Ameta{honyna} cystal \Ameta{enghraifft} â’r un o’i ffordd o drugarhau.
	}
	{%
		\mae{} \honyna{} good.\Equ{} example(\F{}) with-\Def{} one of-\eif{} way of be\_merciful.\Inf{}
	}
	{%
		\Ameta{That} is as good an \Ameta{example} as any of her sympathetic ways.
	}
	{\exsrcapp{anecdote}{63}{ylw}{9}{106}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:72>
	\gloss
	{%
		¶
		Fel \Ameta{yna} y byddem, pawb y pryd hynny yn gallu chwerthin.
	}
	{%
		{}
		like \ynadem{} \yrRel{} \byddem{} everyone \Def{} time \hynnyn{} \ynD{} \gallu{}.\Inf{} laugh.\Inf{}
	}
	{%
		¶
		\notsic{}It was like \Ameta{that} that we were, we could all laugh then.
	}
	{\exsrcapp{anecdote}{72}{ylw}{9}{112}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:78>
	\gloss
	{%
		Mae\Ameta{’r} \Ameta{hanes} yn fy atgoffa am stori a glywsom ganwaith gan fy nhad am ryw ddyn a arferai roi cweir i’w fab am wneud drwg, ac adrodd y fformiwla hon uwch ei ben bob tro,
		‘’R wyt ti’n cael cweir nid am y drwg wnest ti, ond rhag iti wneud drwg eto.’
		Gwers ar gyfer y dyfodol a roes Modryb Neli, mae’n sicr!
		¶
		Dengys \Ameta{y} \Ameta{stori} \Ameta{hon} amdani gymaint o’r Piwritan a oedd ynddi, er gwaethaf ei hystyried yn dipyn o bagan.
	}
	{%
		\mae{}-\Def{} history \ynD{} \fy{} remind.\Inf{} about story \aRel{} hear.\Pret{}.\Fpl{} \Adv{}\textbackslash{}hundred.time by \fy{} father about \rhyw{} man \aRel{} be\_used\_to.\ai{} give.\Inf{} thrashing to-\eim{} son about do.\Inf{} bad and tell.\Inf{} \Def{} formula(\F{}) \hon{} high.\Cmp{} \eim{} head \Adv{}\textbackslash{}every time
		\yr{} \wyt{} \ti{}-\ynD{} get.\Inf{} thrashing \Neg{} about \Def{} bad \aRel{}\textbackslash{}do.\Pret{}.\Ssg{} \ti{} but lest \iti{} \Len{}\textbackslash{}do.\Inf{} bad again
		lesson on direction \Def{} future \aRel{} give.\odd{} aunt \Pn{} \mae{}-\ynC{} sure
		{}
		show.\Prs{}.\Tsg{} \Def{} story(\F{}) \hon{} \amdani{} big.\Equ{} of-\Def{} puritan \aRel{} \oedd{} \ynddi{} despite bad.\Sup{} \eif{} consider.\Inf{} \ynC{} bit of pagan
	}
	{%
		\notsic{}\Ameta{The story} reminds me of a story we have heard a hundred times from my father about a man who used to give his son a beating for wrongdoing, reciting this formula over his head, ‘You’re having a beating not for the wrong you have done but to stop you doing wrong again.’
		It was a lesson for the future that Neli gave, certainly!
		¶
		\Ameta{This story} about her shows how much of the Puritan was in her, despite being thought a bit of a pagan.
	}
	{\exsrcapp{anecdote}{78}{ylw}{10}{119}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:172>
	\gloss
	{%
		Mor dynn yw’r llinynnau sy’n dal llawer teulu wrth ei gilydd.
	}
	{%
		\mor{} tight \copyw{}-\Def{} string.\Pl{} \sy{}-\ynD{} keep.\Inf{} many family with \eithird{} \Recp{}
	}
	{%
		How strong the ties are that bind a family together.
	}
	{\exsrcapp{anecdote}{172}{ylw}{10}{123}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:1>
	\gloss
	{%
		Fel \Ameta{yna}, er y gweithio caled yn y ’Rala, yr oedd amser hefyd i chwarae drama ar ganol gwaith.
	}
	{%
		like \ynadem{} despite \Def{} work.\Inf{} hard \ynA{} \Def{} \Pn{} \yr{} \oedd{} time also to play.\Inf{} drama on middle work
	}
	{%
		\notsic{}Like \Ameta{that}, in spite of the hard work at the ’Rala, there was always time to play-act in the midst of work.
	}
	{\exsrcapp{anecdote}{1}{ylw}{10}{127}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:98>
	\gloss
	{%
		Gwn y tadogir \Ameta{y} \Ameta{stori} \Ameta{yna} ar rai eraill erbyn hyn, ond yr wyf mor sicr â’m bod yn ysgrifennu rŵan, fod \Ameta{y} \Ameta{stori} wedi digwydd fel \Ameta{yna}.
		Wrth gwrs, nid yw’n amhosibl iddi fod wedi digwydd yn rhywle arall hefyd.
		Nid wyf yn siŵr ai {John James} a ddywedodd pan weithiai yn y pwll glo, wrth glywed crynfeydd yn y ddaear, ‘Clyw, mae hi’n bwrw glaw y tu allan.’
	}
	{%
		know.\Prs{}.\Fsg{} \Nmlz{} attribute.\Prs{}.\Imprs{} \Def{} story \ynadem{} on \rhai{} other.\Pl{} by \hynn{} but \yr{} \wyf{} \mor{} sure with-\fy{} \bod{} \ynD{} write.\Inf{} now \Len{}\textbackslash{}\bod{} \Def{} story after happen.\Inf{} like \ynadem{}
		with course \Neg{} \yw{}-\ynC{} impossible \iddi{} \Len{}\textbackslash{}\bod{} after happen.\Inf{} \ynA{} \rhyw{}.place other also
		\neg{} \wyf{} \ynC{} sure whether \Pn{} \aRel{} say.\odd{} when work.\ai{} \ynA{} \Def{} pit coal with hear.\Inf{} tremor.\Pl{} \ynA{} \Def{} earth hear.\Imp{}.\Ssg{} \mae{} \hi{}-\Def{} throw.\Inf{} rain \Def{} side out
	}
	{%
		I know \Ameta{this story} has been attributed to others by now, but I’m as sure as I’m writing now that \Ameta{the story} did happen like \Ameta{that}.
		Of course it’s not impossible that it happened somewhere else too.
		But I am not certain whether it was John James who said, when working in the pit and hearing earth tremors, ‘Listen, it’s raining outside.’
	}
	{\exsrcapp{anecdote}{98}{ylw}{12}{144}}
\xe\postex{}

\preex{}\ex<ex:anecdotes:components:conclusion:169>
	\gloss
	{%
		Trwy\Ameta{’r} \Ameta{ymweliadau} \Ameta{hyn} cawsom weld dyfnder dioddef rhai o bobl y Rhondda.
	}
	{%
		through-\Def{} visit.\Pl{} \hynpl{} get.\Prt{}.\Fpl{} see.\Inf{} depth suffering \rhai{} of people \Def{} \Pn{}
	}
	{%
		Through \Ameta{these visits} we got to see the depth of suffering of some of people of Rhondda.
	}
	{\exsrcapp{anecdote}{169}{atgofion}{}{34}}
\xe\postex{}

{}\index{anecdote components!conclusion|)}%
