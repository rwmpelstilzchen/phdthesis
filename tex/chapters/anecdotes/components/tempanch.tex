\weakFloatBarrier{}%
\subsection{Temporal anchor}%
\label{sec:anecdotes:components:tempanch}
{}\index{anecdote components!temporal anchor|(}%

The last component of the anecdote described in this section is the \Ctempanch{}, which sets the temporal deictic centre of the anecdote at some point in the past; see \cref{sec:anecdotes:structure:overview:anchors} for a glossed example (\exfullref{ex:anecdotes:overview:tempanch}).
As such, the \Ctempanch{} plays a role in the bound nature of the anecdotes, which describe specific, concrete and singular sequences of events (as opposed to habitual or generic ones); see \cref{sec:anecdotes:intro:background} and \cite{herring.s:1986:marking-unmarking} (as cited in \textcite[§~4.2.2]{fleischman.s:1990:tense}).
This component is quite common, occurring in more than two thirds of the anecdotes%
\footnote{%
	Sometimes more than once, as discussed below.
},
making it second only to the obligatory \Cdevelopment{}.



\subsubsection{Classification into two groups}%
\label{sec:anecdotes:components:tempanch:two groups}

The temporal anchors can be divided into two groups, although the boundary between them is not unambiguous.
One group can be characterised by its prototypical%
\footnote{%
	The choice of definition by prototypicality aims at bypassing strict definitions. For theoretical background, see \posscite{rosch.e:1978:prinicples-categorization} seminal work, as well as \textcite{givon.t:1986:plato-wittgenstein}.
}
members: nominal phrases denoting time, with the following tendencies:
\begin{compactitem}
	\item Commonly without a preposition (the vast majority of cases%
		% was: 92
		); more marginally with one (such as \C{\gl{ar}{on} \gl{ddiwrnod}{day}%
			\footnote{%
				The lenition in \C{ddiwrnod} in this case is a wholly different issue: it is obligatorily triggered by the preposition \C{ar}[on].
			}
		\gl{poeth}{hot}}[on a hot day]%
		% was: 24
		).

		The cases without a preposition can be subdivided into three types:
		\begin{inparaenum}[(a)]
			\item with lenition that marks adverbiality%
				\footnote{%
					These cases are marked by an initial ‘(\Adv{})’ in their glosses in the tables of \cref{sec:anecdotes:components:tempanch:tables}.
				}
				(e.g.\ \C{\gl{ryw}{\Adv{}\textbackslash{}\rhyw{}} \gl{ddiwrnod}{day}}[some day]%
				% was: 14
				)
			\item with a lack of lenition where it could have been marked%
				\footnote{%
					As noted by \textcite[§~1.4.4]{borsley.r+:2007:syntax} (regarding ex.~58 there) and \textcite[§~83]{thorne.d:1993:grammar} adverbiality is not always consistently marked by lenition.
					This is corroborated in our small sample with \C{\mbox{ryw(-)}~/ \mbox{rhyw(-)}}.
				}
				(e.g.\ \C{\gl{rhyw}{\rhyw{}} \gl{ddirwnod}{day}}[some day]%
				% was: 9
				).
			\item cases in which lenition is irrelevant (%
				% was: 69
				most cases; the primary reason is phonological: not all consonants can be lenited).
		\end{inparaenum}

	\item Commonly with an element that marks indefiniteness (\C{\gl{un}{one}}, with %
		% was: 66
		about half the total occurrences, or \C{\gl{rhyw}{\rhyw{}}}%
		% was: 18
		); more marginally without such an element (zero article, e.g.\ \C{\gl{ar}{on} \gl{noson}{evening} \gl{waith}{work}}[on a weekday evening]%
		% was: 16
		) or with a definite status (e.g.\ \C{\gl{y}{\Def{}} \gl{tro}{time} \gl{hwn}{\hwn{}}}[this time]%
		% was: 15
		).
		Welsh has no indefinite \emph{article} like the English\index[langs]{English} \E{\mbox{a(n)}} (see \cref{fn:no indf} on p.~\pageref{fn:no indf}); \C{un}[one] is a numeral, and the determiner \C{\gl{rhyw}{\rhyw{}}} does not function as an article.
		This feature, pointing to some certain%
		\footnote{%
			The \Ctempanchs{} do not point to \emph{any} time in the past, generically, but to a \emph{concrete} and \emph{specific} point of time.
		}
		time in the past, has to do with the bound nature of the anecdotes mentioned above.

	\item Commonly with a broad meaning (e.g.\ \C{\gl{unwaith}{once}}); more marginally carrying more specific information (e.g.\ \C{\gl{rhyw}{\rhyw{}} \gl{Basg}{Easter}}[some Easter]).
\end{compactitem}

A typological note.
Marking adverbiality by lenition is broadly comparable with the use of oblique cases in many languages for marking adverbiality (mostly with a temporal sense, including duration).%
\footnote{%
	Examples include:
	% the Russian\index[langs]{Russian} instrumental (e.g.\ \transcribe{\Russian{днём}}{\gl{dnjóm}{{day.\Ins{}.\Sg{}}}} ‘by day’),
	% the Russian\index[langs]{Russian} instrumental (e.g.\ \transcribe{\Russian{днём}}{dnjóm} ‘by day’),
	Polish\index[langs]{Polish} \parencite[pp.~344 and 363]{swan.o:2002:grammar-polish},
	% the fossilised Semitic accusative in Biblical Hebrew\index[langs]{Hebrew!Biblical} (e.g.\ \transcribe{\BiblicalHebrew{יוֹמָם וָלַיְלָה}}{\gl{yōmåm}{day.\Acc{}.\Sg{}} \gl{wå-laylå}{and-night.\Sg{}}} ‘day and night (in the sense of \E{at all times})’; see \cite{meek.th.j:1940:hebrew-accusative})
	% the fossilised Semitic accusative in Biblical Hebrew\index[langs]{Hebrew!Biblical} (e.g.\ the first constituent in \transcribe{\BiblicalHebrew{יוֹמָם וָלַיְלָה}}{yōmåm wå-laylå} ‘day and night (in the sense of \E{at all times})’; see \cite{meek.th.j:1940:hebrew-accusative})
	Biblical Hebrew\index[langs]{Hebrew!Biblical} \parencite{meek.th.j:1940:hebrew-accusative},
	% and its more productive Arabic\index[langs]{Arabic} counterpart (e.g.\ \foreign{\gl{ḍarabtu-hu}{hit.\Act{}.\Pst{}.\Ind{}.\Fsg{}-\Tsg{}.\M{}} \gl{l-yawma}{\Def{}-day.\Acc{}.\Sg{}}} ‘I hit him today’ in \cite[111]{versteegh.k+:2014:arabic}),
	% and its more productive Arabic\index[langs]{Arabic} counterpart (e.g.\ \foreign{ḍarabtu-hu l-yawma} ‘I hit him today’ in \cite[111]{versteegh.k+:2014:arabic}),
	Arabic\index[langs]{Arabic} \parencite[111]{versteegh.k+:2014:arabic},
	Inari Saami\index[langs]{Saami!Inari} \parencite{nelson.d:2007:events-case},
	German\index[langs]{German} \parencite[§~3.2.2.2]{fagan.s.m.b:2009:german-linguistic} and
	Latin\index[langs]{Latin} \parencite[§~10.32]{pinkster.h:2015:latin-syntax-simple}.
	See \textcite[§~8.1.1]{haspelmath.m:1997:spacetime} for a typological account of the phenomenon.
}
The Welsh lenition is specifically comparable with the accusative%
\footnote{%
	As a cross-linguistic comparative concept \parencite[see][]{haspelmath.m:2010:comparative}; the exact meaning of \emph{‘accusative’} is of course dependent on the grammatical terminology used for specific languages.
}:
both are used for marking direct objects%
% \footnote{%
	% Lenition marks the direct objects of finite verbs \parencites[§~70]{thorne.d:1993:grammar}, e.g.\ \C{Gwelais gath}[I saw a cat], where the radical, unmutated form of \C{gath} is \C{cath}[cat].
% }
,
as well as other similar syntactic functions (like those discussed in \textcite{hewitt.s:2019:accusative}, comparing Welsh and Arabic\index[langs]{Arabic}).
This does not mean Welsh has a morphophonological case system, naturally, but it does mean lenition and the accusative case have some syntactic features in common.

The other group of \Ctempanchs{} is complementary to the first one: its members are marked by a number of constructions (see \cref{sec:anecdotes:components:tempanch:syntax:second} below), lack the said indicators of indefiniteness and convey concrete information that is specific to the story (e.g.\ \C{\gl{pan}{when} \gl{oedd}{\oedd{}} \gl{i}{to} \gl{lawr}{down} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{pwll}{hole}}[\notsic{}when he was down the mine] in \aneccref{97}).%
\footnote{%
	Instances of these two groups are differentiated in \cref{app:anecdotes}: the first is marked with a burgundy underline and the other with a black one.
}
The reason to divide the \Ctempanchs{} into these two groups is due to structural differences between the two: in addition to the ones mentioned above, members of the first (basal, simpler, more generic) group tend to occur on their own or to be accompanied (modified and specified) by members of the second (extended, more convoluted, more specific and grounded to the story) group, usually following it%
\footnote{%
	\anecCref{122} has a reversed order.
	Interestingly, Clarke’s English translation swapped it, making it follow the common order.
	In order to make the English order of constituents correspond better with the original, it is reversed back in the appendix.
}.
There are instances where members of the second group occur independently of members of the first group, but these are less common.

These are not the only expressions of time in the anecdotes, but these are the ones which play a consistent and systematic role in the internal structural make-up of the anecdotes.
Other, more loosely defined expressions include setting the time of the anecdote calendrically (such as \C{\gl{Gwyliau}{holiday.\Pl{}} \gl{Pasg}{Easter} \gl{1917}{} \gl{oedd}{\oedd{}} \gl{hi}{\hi{}}}[It was the Easter holidays, 1917], \aneccref{129}) or the nebulous \C{\gl{ar}{on} \gl{y}{\Def{}} \gl{pryd}{time}}[at that time], which do not serve as anchors that set a reference point for the concrete set of events of the anecdote.



\weakFloatBarrier{}%
\subsubsection{Attestation of temporal anchors}%
\label{sec:anecdotes:components:tempanch:tables}

\input{chapters/anecdotes/components/tempanch-texnical}
% yn aXadv pan yn+pan cyn adeg arol ar
\begin{table}
	\caption{Temporal anchors of the first group and temporal anchors of the second group which modify them (forms with multiple occurrences)}%
	\label{tab:anecdotes:components:tempanch:multiple}%
	\setSingleSpace{0.8}%
	% \SingleSpacing
	% \footnotesize%
	\OnehalfSpacing%
	\begin{tabularx}{\myfullwidth}{llcXr}
		\toprule
		Form &
		Section &
		Location &
		\Anecs{} &
		\multicolumn{1}{c}{\#}\\
		\midrule
		\tempanchrow{un tro}[one time]{131}{abs}{fin}
		\tempanchrow{}{151}{exp}{fin}
		\tempanchrow{}{156 157-pan 160-pan 165 9 38 42-cyn 78-pan 89}{dev}{init}
		\tempanchrow{}{19}{dev}{cnj init}
		\tempanchrow{}{163 11-pan 107 122-pan 54 70 74-pan 81}{dev}{mid}
		\tempanchrow{unwaith}[once]{31}{abs}{mid}
		\tempanchrow{}{155}{abs}{fin}
		\tempanchrow{}{85 170}{exp}{mid}
		\tempanchrow{}{41}{dev}{init}% Was `init-cleft`, but since it’s only a single example, it doesn’t make sense to make a whole symbol only for it
		\tempanchrow{}{173}{dev}{cnj init}
		\tempanchrow{}{171 161 5 7 27-ar 108 111 112 37 128 63 80 1 90}{dev}{mid}
		\tempanchrow{un diwrnod}[one day]{147 114 117 93 97-pan}{dev}{init}
		\tempanchrow{}{159 62-arol}{dev}{mid}
		\tempanchrow{ryw ddiwrnod}[(\Adv{}) some day]{46}{abs}{fin}
		\tempanchrow{}{113 57 72 100}{dev}{mid}
		\tempanchrow{dro arall}[(\Adv{}) another time]{50 87 88 101}{dev}{init}
		\tempanchrow{un bore}[one morning]{148-pan}{dev}{init}
		\tempanchrow{}{142 3-pan}{dev}{cnj init}
		\tempanchrow{yn y bore}[in the morning]{153 136-aXadv 60}{dev}{init}
		\tempanchrow{rhywdro}[sometime]{49-adeg 58-yn}{dev}{init}
		\tempanchrow{}{98-yn}{dev}{cnj init}
		\tempanchrow{ar noson \NP{}}[on a \NP{} evening]{6 48-pan 81-yn}{dev}{mid}
		\tempanchrow{un noson}[one night]{4}{dev}{init}
		\tempanchrow{}{134 8}{dev}{mid}
		\tempanchrow{ar un o’r achlysuron hyn}[on one of these occasions]{60}{dev}{init}
		\tempanchrow{}{61}{dev}{mid}
		\tempanchrow{ar brynhawn \tempanchwildcard{dydd} \Adj{} \ellipsis{}}[on a \Adj{} \tempanchwildcard{day} afternoon \ellipsis{}]{171-yn 153}{dev}{mid}
		\tempanchrow{ryw brynhawn \tempanchwildcard{dydd}}[(\Adv{}) some \tempanchwildcard{day} afternoon]{12}{exp}{mid}
		\tempanchrow{}{96}{dev}{mid}
		\tempanchrow{rhyw brynhawn \tempanchwildcard{dydd}}[some \tempanchwildcard{day} afternoon]{162}{abs}{fin}
		\tempanchrow{}{103}{dev}{init}
		\tempanchrow{un nos \tempanchwildcard{dydd}}[one \tempanchwildcard{day} night]{167 20}{dev}{mid}
		\tempanchrow{un nos \tempanchwildcard{dydd} \Adj{}}[one \Adj{} \tempanchwildcard{day} night]{25 47-yn}{abs}{mid}
		\tempanchrow{un bore \Adj{}}[one \Adj{} morning]{135 152-yn+pan}{dev}{init}
		\tempanchrow{y tro hwn}[this time]{132 86}{dev}{init}%\\[0.25ex]
		% \\
		\midrule
	\end{tabularx}
\end{table}
\begin{table}
	\caption{Temporal anchors of the first group and temporal anchors of the second group which modify them (cont.\ of \cref{tab:anecdotes:components:tempanch:multiple}; forms with a single occurrence each)}%
	\label{tab:anecdotes:components:tempanch:single}%
	\smaller%
	\begin{tabular}{lllcl}
		\toprule
		\multicolumn{2}{l}{Form} &
		Section &
		Location &
		\Anec{}\\
		\midrule
		\tempanchrow[1]{ar ddiwedd un tymor}[at the end of one season]{113}{dev}{mid}
		\tempanchrow[1]{ar ddiwrnod \Adj{}}[on a \Adj{} day]{54-yn}{dev}{mid}
		\tempanchrow[1]{ar un adeg}[on one occasion]{55-pan}{dev}{mid}
		\tempanchrow[1]{ar un o’r dyddiau \Adj{} hynny}[on one of these \Adj{} days]{84}{dev}{init}
		\tempanchrow[1]{ar y \tempanchwildcard{dydd}}[on \tempanchwildcard{day}]{44}{dev}{mid}
		\tempanchrow[1]{ar yr adeg \ellipsis{}}[on the occasion \ellipsis{}]{92-pan}{dev}{init}%???
		% \tempanchrow[1]{ben bore}[(\Adv) first thing in the morning]{51}{dev}{mid}
		\tempanchrow[1]{diwrnod \Def{} \NP{}}[the day of the \NP{}]{51-pan}{abs}{mid}
		\tempanchrow[1]{ddiwrnod \Def{} \NP{}}[(\Adv{}) the day of the \NP{}]{63}{dev}{cnj init}
		\tempanchrow[1]{ryw nos \tempanchwildcard{dydd}}[(\Adv{}) \tempanchwildcard{day} night]{71}{dev}{mid}
		\tempanchrow[1]{rywdro}[(\Adv{}) time]{65-pan}{dev}{mid}
		\tempanchrow[1]{rhyw Basg}[some Easter]{69-pan}{dev}{cnj init}
		\tempanchrow[1]{rhyw ddiwrnod}[some day]{146-aXadv}{dev}{init}
		\tempanchrow[1]{rhyw fore \tempanchwildcard{dydd}}[some \tempanchwildcard{day} morning]{154}{dev}{init}
		\tempanchrow[1]{rhyw nos \tempanchwildcard{dydd}}[some \tempanchwildcard{day} night]{39-pan}{dev}{init}
		\tempanchrow[1]{un bore \tempanchwildcard{dydd}}[one \tempanchwildcard{day} morning]{76}{dev}{mid}
		\tempanchrow[1]{un dechreunos}[one nightfall]{44}{dev}{mid}
		\tempanchrow[1]{un noswaith}[one evening]{139}{dev}{cnj init}
		\tempanchrow[1]{un o’r troeon hyn}[one of these times]{53-pan}{dev}{init}
		\tempanchrow[1]{un o’r troeon hynny}[one of those times]{79}{dev}{cnj init}
		\tempanchrow[1]{un waith}[one time]{18-pan}{dev}{mid}
		\tempanchrow[1]{un wythnos}[one week]{150}{dev}{init}
		% \tempanchrow[1]{y bore hwn}[this morning]{1}{dev}{cnj init}
		\tempanchrow[1]{y diwrnod dan sylw}[the day in question]{129}{dev}{init}
		\tempanchrow[1]{y nos \tempanchwildcard{dydd} tan sylw}[the \tempanchwildcard{day} night in question]{45}{dev}{init}
		\tempanchrow[1]{y noson gyntaf}[the first evening]{170}[second]{dev}{init}
		\tempanchrow[1]{y tro hwnnw}[that time]{174}{dev}{init}
		\tempanchrow[1]{ymhen blynyddoedd lawer iawn}[after many years]{32}{dev}{init}
		\tempanchrow[1]{ymhen tipyn o ddyddiau}[in a few days]{73}{dev}{init}
		\tempanchrow[1]{yn yr amser yma}[in this time]{77}{dev}{mid}
		\tempanchrow[1]{yr wythnos \yrRel{} \Clause{}}[the week that \Clause{}]{66}{dev}{init}
		\bottomrule
	\end{tabular}
\end{table}
\begin{table}
	\caption{Temporal anchors of the second group which occur independently of temporal anchors of the first group}%
	\label{tab:anecdotes:components:tempanch:indep-Atempanchcom}%
	\begin{tabular}{llcl}
		\toprule
		Form &
		Section &
		Location &
		\Anec{}\\
		\midrule
		\tempanchrow[c]{cyn iddynt briodi}[before they were married]{119}{mid}{fin}
		\tempanchrow[c]{ychydig amser cyn cychwyn}[a little time before setting off]{164}{dev}{cnj init}
		\tempanchrow[c]{ychydig cyn ei marw pan orweddai ar wely cystudd}[a little before her death when she was lying on her sickbed]{68}{abs}{fin}
		\tempanchrow[c]{ychydig cyn inni symud oddi yno i Gae’r Gors}[shortly before we moved from there to Cae’r Gors]{118}{dev}{mid}
		\tempanchrow[c]{pan gychwynnwn i’r coleg}[when I started College]{121}{dev}{fin}
		\tempanchrow[c]{pan oedd ei mam newydd fod yn sâl}[when her mother had been ill]{56}{dev}{mid}
		\tempanchrow[c]{pan oeddwn gartref am dro o Donypandy}[when I was home for a visit from Tonypandy]{127}{dev}{mid}
		\tempanchrow[c]{Wedi i’r dynion fyned i’r chwarel yn y bore}[After the men had left for the quarry in the morning]{28}{dev}{init}
		\tempanchrow[c]{wrth ymadael â Chae’r Gors}[when leaving Cae’r Gors]{52}{dev}{mid}
		\bottomrule
	\end{tabular}
\end{table}
The examples from the corpus are presented in a tabular manner in \cref{tab:anecdotes:components:tempanch:multiple,tab:anecdotes:components:tempanch:single,tab:anecdotes:components:tempanch:indep-Atempanchcom}%
\footnote{%
	The first two tables (\cref{tab:anecdotes:components:tempanch:multiple,tab:anecdotes:components:tempanch:single}) are indeed one which had to be split into two due to technical reasons: I could not fit the data in one page without making the font unreadably small.
	The first table covers \Ctempanchs{} which occur multiple times in the texts, while the second one covers ones which occur once each.
	% The latter happen all to be contained in \Cdevelopments{}, so the section column is omitted from the second table in order to make its presentation cleaner.
	The third table (\cref{tab:anecdotes:components:tempanch:indep-Atempanchcom}) co
}.
The first two tables are ordered firstly by prevalence (which expectedly shows a long-tail distribution; cf.\ \cref{sec:configurations_of_the_components}) and then alphabetically; the word \C{\gl{dydd}{day}} with an underline marks a class consisting of the names of days of the week.
Each form is subdivided according to the section in which the \Ctempanch{} occurs (one of the first three sections; see \cref{sec:anecdotes:structure:overview}) and its location within the sentence: initial (\circlegend{sronpalegreen}~green bar to the left)%
\footnote{%
	A thin white segment before the green bar marks that the \Ctempanch{} is preceded by \C{a(c)}[and] or \C{beth bynnag}[however, anyway].
},
medial (\circlegend{sronpaleblue}~blue bar in the centre) and final (\circlegend{sronpaleyellow}~yellow bar to the right).
Words in parentheses after the references in the first two tables mark the kind of \Ctempanch{} from the second group that modifies the one from the first group.
The last column (\#) counts the number of examples where multiple examples share a row.




\subsubsection{Micro-syntactic constituent order}%
\label{sec:anecdotes:components:tempanch:order}

The location column hints at some tendencies, but in most cases there are not enough examples for substantiating possible hypotheses; this issue is to be described as a part of the broader question of adverbiality and constituent order.
The data does suggest, though, there is a reason to single out the initial position as distinct from the medial or final%
\footnote{%
	I am not sure the distinction between the medial and final positions is structurally justified.
	It is not impossible an \emph{initial:non-initial} classification would be more appropriate, but there are not enough examples here to decide either way.
}.
Of the more common forms, which allow some generalisation, it seems \C{\gl{un}{one} \gl{tro}{time}}[one time] is more or less equally present both initially and medially, yet \C{\gl{unwaith}{once}} shows an inclination towards the medial (or \emph{non-initial}) position.
The two examples where it does occur in initial position describe a single special instance of a recurring situation:
\begin{compactitem}
	\item In \aneccref{41} the recurring situation is described right before the anecdote (\C{\gl{Ymweliad}{visit} \gl{cyfeillion}{friend.\Pl{}} \gl{a’i}{\aRel{}-\eim{}} \gl{cadwai}{keep.\ai{}} \gl{\sic{}}{} \gl{ar}{on} \gl{ei}{\eim{}} \gl{draed}{foot.\Pl{}} \gl{yn}{\ynB{}} \gl{hwy}{long.\Cmp{}} \gl{na}{than} \gl{hynny}{\hynnyn{}}}[\notsic{}It was visits from friends that kept him on his feet later than that]).
		As discussed in bullet list at the beginning of \cref{sec:anecdotes:components:intanch}, the initial position of \C{\gl{unwaith}{once}} in this case is due to the constituent order of the cleft sentence: it was \emph{only} once that so-and-so happened.

	\item In \aneccref{173} the recurring situation is described in the exposition.
		In this case we do not have a fully-fledged cleft sentence (\C{\gl{unwaith}{once} \gl{yr}{\yrRel{}} \gl{aeth}{go.\odd{}}}[it was once that he went]) but an adverbial in the first position without a relative marker (\C{\gl{unwaith}{once} \gl{aeth}{go.\odd{}}}[once he went])%
		\footnote{%
			This is termed \emph{left dislocation} in certain grammatical traditions.
		}.
\end{compactitem}



\subsubsection{Syntactic form and function}%
\label{sec:anecdotes:components:tempanch:syntax}

\paragraph{First group}%
\label{sec:anecdotes:components:tempanch:syntax:first}

Almost all the \Ctempanchs{} of the first group function quite straightforwardly as adverbial phrases, modifying the sentence or clause in which they occur%
\footnote{%
	As discussed in \cref{sec:anecdotes:components:intanch:cofiaf}, sometimes a \Ctempanch{} (such as \C{unwaith}[once] or the more complex \C{pan oeddwn gartref am dro o Donypandy}[when I was home for a visit from Tonypandy]) occurs between \C{cofiaf}[I remember] and its complement, yet semantically it belongs with the complement, not \C{cofiaf}[I remember].
}.
In one borderline case, \aneccref{51}, has \C{\gl{diwrnod}{day} \gl{yr}{\Def{}} \gl{arwerthiant}{auction}}[the day of the auction] as the object of \C{\gl{cofiaf}{\cofiaf{}}}.
Semantically, it does set the temporal deictic centre of the anecdote, but syntactically it is different from all other \Ctempanchs{}; specifically, compare it with \aneccref{63}, where \C{\gl{ddiwrnod}{\Adv{}\textbackslash{}day} \gl{y}{\Def{}} \gl{claddu}{bury.\Inf{}}} (sharing the same basic form) serves as an adverbial phrase marked by lenition%
\footnote{%
	Resulting in the same form, \C{ddiwrnod}.
	In \aneccref{51} the lenition serves as a marker of the direct object of the finite verb; in \aneccref{63} as a marker of adverbiality.
}.



\paragraph{Second group}%
\label{sec:anecdotes:components:tempanch:syntax:second}

\Ctempanchs{} of the second group are also adverbial.
As stated above, they can modify \Ctempanchs{} of the first group or stand on their own.
They can be divided into four types with respect to their syntactic form and the element that marks them, as follows.

The most common type has the conjunction \C{\gl{pan}{when}}, followed by a clause (\exfullref{ex:anecdotes:components:tempanch:syntax:second:pan}).

\preex{}\ex<ex:anecdotes:components:tempanch:syntax:second:pan>
	\gloss
	{%
		Clywais ddynes ifanc yn dweud, \nogloss{\hlbegin{}} @ pan oedd ei mam newydd fod yn sâl\hlend{}, y \ellipsis{}
	}
	{%
		hear.\ais{} woman young \ynD{} \dweud{}.\Inf{} when \oedd{} \eif{} mother new \bod{} \ynC{} sick \Nmlz{}
	}
	{%
		\notsic{}I heard one young woman say, \highlight{when her mother had recently been ill}, that \ellipsis{}
	}
	{\exsrcapp{anecdote}{56}{ylw}{9}{103}}
\xe\postex{}

The second type has prepositions, followed either by noun phrases (such as in \exfullref{ex:anecdotes:components:tempanch:syntax:second:prep.yn}) or infinitival constructions (such as the simple infinitive in \exfullref{ex:anecdotes:components:tempanch:syntax:second:prep.wrth}).
The relevant prepositions attested in the corpus are \C{\gl{ar}{on}}, \C{\gl{ar}{on} \gl{ôl}{back}}[after], \C{\gl{cyn}{before}}%
\footnote{%
	Can be preceded by \C{ychydig}[a little] or \C{ychydig amser}[a little time].
},
\C{\gl{wedi}{after}}, \C{\gl{wrth}{with}} and \C{\gl{yn}{\ynA{}}}.

\preex{}\pex<ex:anecdotes:components:tempanch:syntax:second:prep>
	\a<yn>
		\gloss
		{%
			\ellipsis{} a \nogloss{\subhlbegin{}} @ rhywdro\subhlend{} \nogloss{\hlbegin{}} @ yng ngwanwyn 1918\hlend{} penderfynodd eglwysi Lerpwl roi un Sul i weddïo am ddiwedd y rhyfel.
		}
		{%
			{} and \rhyw{}.time \ynA{} spring {} decide.\odd{} church.\Pl{} \Pn{} give.\Inf{} one Sunday to pray.\Inf{} about end \Def{} war
		}
		{%
			\ellipsis{} and \subhighlight{some time} \highlight{in the spring of 1918} the churches of Liverpool decided to devote one Sunday to pray for the end of the war.
		}
		{\exsrcapp{anecdote}{98}{ylw}{12}{144}}
		\subexamplespacer{}

	\a<wrth>
		\gloss
		{%
			Cofiaf mor brudd yr oedd fy nhad \nogloss{\hlbegin{}} @ wrth ymadael â Chae’r Gors\hlend{}, \ellipsis{}
		}
		{%
			\cofiaf{} \mor{} sad \yrRel{} \copoedd{} \fy{} father with leave.\Inf{} with field-\Def{} swamp
		}
		{%
			\notsic{}I remember how sad my father was \highlight{when leaving Cae’r Gors},
		}
		{\exsrcapp{anecdote}{52}{ylw}{8}{96}}
\xe\postex{}

The two remaining types are attested once each.
One is the circumstantial \C{\gl{a}{and} \ProNP{} \Adv{}} construction: \exfullref{ex:anecdotes:components:tempanch:syntax:second:aXadv}.
This construction shows curious similarities with the structurally analogous Biblical Hebrew\index[langs]{Hebrew!Biblical} construction demonstrated in \exfullref{ex:anecdotes:components:tempanch:syntax:second:aXadv:beibl.BH}%
\footnote{%
	\label{fn:BH glosses BiDi}%
	The glosses are arranged from left to right, but in each one the Hebrew letters of the first tier are arranged from right to left, in accord with the direction of the Hebrew script.
}
(which has been translated literally yet idiomatically in \posscite{morgan.w:1588:beibl} translation of the Bible; \exfullref{ex:anecdotes:components:tempanch:syntax:second:aXadv:beibl.1588}).%
\footnote{%
	See feature~14 in \textcite{hewitt.s:2009:question} for this construction in the broader typological context of comparing Celtic and Afro-Asiatic languages.
}

\preex{}\ex<ex:anecdotes:components:tempanch:syntax:second:aXadv>
	\gloss
	{%
		{} @ \nogloss{\subhlbegin{}} @ Rhyw ddiwrnod\subhlend{} \nogloss{\hlbegin{}} @ a ninnau’n cael gwers Ladin\hlend{}, cerddodd gŵr bychan gwargam i mewn, \ellipsis{}
	}
	{%
		{} \rhyw{} day and \ninnau{}-\ynD{} get.\Inf{} lesson Latin walk.\odd{} man small humpbacked to in
	}
	{%
		\subhighlight{One day} \highlight{when we had a Latin class}, came a small humpbacked man in, \ellipsis{}
	}
	{\exsrcapp{anecdote}{146}{atgofion}{}{19}}
\xe\postex{}

\preex{}\pex[lingstyle=transliterated]<ex:anecdotes:components:tempanch:syntax:second:aXadv:beibl>
	\a<BH>
		\transliteratedgloss
		{%
			\BiblicalHebrew{וַיֵּרָ֤א}
			\BiblicalHebrew{אֵלָיו֙}
			\BiblicalHebrew{יְהוָ֔ה}
			\BiblicalHebrew{בְּאֵלֹנֵ֖י}
			\BiblicalHebrew{מַמְרֵ֑א}
			\nogloss{\hlbegin{}} @ \BiblicalHebrew{וְה֛וּא}
			\BiblicalHebrew{יֹשֵׁ֥ב}\hlend{}
			\BiblicalHebrew{פֶּֽתַח־הָאֹ֖הֶל}
			\BiblicalHebrew{כְּחֹ֥ם}
			\BiblicalHebrew{הַיּֽוֹם׃}
		}
		{%
			way-yērå ʾēlå-w \textsc{yhwh} bə-ʾēlōnē mamrē wə-hū yōšēḇ pɛṯaḥ-hå-ʾōhɛl kə-ḥōm hay-yōm
		}
		{%
			and-see\textbackslash{}\Narr{}.\Medp{}.\Tsg{}.\M{} to.\Tsg{}.\M{} \Pn{} in-oak.\Pl{}.\Cnst{} \Pn{} and-\Tsg{}.\M{} sit\textbackslash{}\Ptcp{}.\Act{}.\Tsg{}.\M{} opening\textbackslash{}\Cnst{}-\Def{}-tent as-heat\textbackslash{}\Cnst{} \Def{}-day
		}
		{%
			And the \textsc{lord} appeared unto him in the plains of Mamre and he sat \inlinecomment{literally ‘\highlight{and he sitting}’, meaning ‘\highlight{while he was sitting}’. J.~R.} in the tent door in the heat of the day; (KJV)
		}
		{Genesis 18:1}

	\a<1588>
		\gloss
		{%
			A’r Arglwydd a ymddangoſodd iddo ef yng-waſtadedd Mamre: \nogloss{\hlbegin{}} @ ac efe yn eiſtedd\hlend{} [wꝛth] ddꝛŵs y babell, yng-wꝛês y dydd.
		}
		{%
			and-\Def{} lord \aRel{} \Refl{}.show.\odd{} \iddo{} \ef{} \ynA{}-plain.\Pl{} \Pn{} and \ef{} \ynD{} sit.\Inf{} with door \Def{} tent \ynA{}-heat \Def{} day
		}
		{%
			And the Lord appeared to him in the plains of Mamre \highlight{while he was sitting} by the door of the tent, in the heat of the day.
		}
		{Geneſis 18:1}
\xe\postex{}

The second type with only one example is that of \exfullref{ex:anecdotes:components:tempanch:syntax:second:adeg}, which uses \C{adeg}[time, period] followed by a conjugated infinitive for signalling a temporal clause.
% 55/92
The use of a lexically-transparent element meaning ‘time’ or ‘hour’ for such temporal constructions is found in other languages, such as 
\transcribe{\ChineseCN{(的)時候}~/ \ChineseTW{(的)时候}}{(\gl{de}{\Rel{}}) \gl{shíhòu}{time}} in Mandarin Chinese\index[langs]{Chinese!Mandarin} \parencite[§~8.4.2]{sun.ch:2006:chinese},
\transcribe{\Japanese{時}}{\gl{toki}{time, when}} in Japanese\index[langs]{Japanese} \parencite[§~9.4]{hasegawa.y:2014:japanese} or
\transcribe{\Hebrew{(בְּ)שָׁעָה (שֶׁ־)}}{\gl{(be-)ša’a}{in-hour} (\gl{še-}{\Rel{}})} and \transcribe{\Hebrew{בִּזְמַן (שֶׁ־)}}{\gl{bi-zman}{in-time} (\gl{še-}{\Rel{}})} in Hebrew\index[langs]{Hebrew}.
This construction is not very common in Welsh.

\preex{}\pex<ex:anecdotes:components:tempanch:syntax:second:adeg>
	\gloss
	{%
		{} @ \nogloss{\subhlbegin{}} @ Rhywdro\subhlend{}, \nogloss{\hlbegin{}} @ adeg i’r plant ddweud eu hadnodau\hlend{}, clywodd besychiad arwyddocaol ei thad, arwydd a adwaenai’n rhy dda.
	}
	{%
		{} \rhyw{}.time time to-\Def{} child.\Col{} \Len{}\textbackslash{}\dweud{}.\Inf{} \eu{} verse.\Pl{} hear.\odd{} cough meaningful \eif{} father sign \aRel{} be\_acquainted.\ai{}-\ynB{} too good
	}
	{%
		\subhighlight{Once}, \highlight{when the children went to say their verses}, she heard her father’s meaningful cough, a signal she knew too well.
	}
	{\exsrcapp{anecdote}{49}{ylw}{8}{95}}
\xe\postex{}

In exx.~\getfullref{ex:anecdotes:components:tempanch:syntax:second:prep.yn}, \getfullref{ex:anecdotes:components:tempanch:syntax:second:aXadv} and \getfullref{ex:anecdotes:components:tempanch:syntax:second:adeg} the \Ctempanchs{} of the second group are dependent on \Ctempanchs{} of the first group; the latter are marked by \grapheme{\subhighlight{~}} (in grey, as opposed to the red \grapheme{\highlight{~}}).



\subsubsection{Macro-syntactic consideration}%
\label{sec:anecdotes:components:tempanch:macro}

Regarding the definition of the \emph{report mode} \parencite[335]{bonheim.h.w:1975:theory-narrative-modes} and the beginnings of short stories \parencite[104]{bonheim.h.w:1982:narrative-modes} Bonheim points out the prevalence of \emph{time markers} (or \emph{time indicators} in the 1982 book) that signal the outset of the report, indicating a temporal reference from which the plot can flow.%
\footnote{%
	This is found in many languages and literary or storytelling tradition around the world.
	Cf.\ Jewish Zakho Neo-Aramaic\index[langs]{Aramaic!Neo-Aramaic!Jewish dialect of Zakho} \foreign{xá yōma} ‘one day’, which has received scholarly attention, as referred to in \cref{fn:xa yoma} on p.~\pageref{fn:xa yoma}.
}
Our data corroborates that, as evident from the table of the attested configurations of the anecdotes’ components (\cref{tab:anecdotes:configurations} in \cref{sec:configurations_of_the_components}) as well as the section columns of \cref{tab:anecdotes:components:tempanch:multiple,tab:anecdotes:components:tempanch:single,tab:anecdotes:components:tempanch:indep-Atempanchcom}: the \Ctempanch{} often opens the \Cdevelopment{}, not only anchoring it in time (as the name suggests) but also functioning as a textual boundary marker, indicating its beginning%
\footnote{%
	Whether as the first component (without an \Cabstract{} or an \Cexposition{}), delimiting the anecdote from the broader text in which it is embedded, or after an introductory component, delimiting the actual \Cdevelopment{} from the introduction.
}.
This tendency does not imply, though, that \Ctempanchs{} are limited to the \Cdevelopment{}, as can be observed from the tables.

(In)definiteness of the \Ctempanch{} plays a role in how the \Ctempanch{} takes part in the macro-syntactic cohesion of the anecdote.
The common use of the indefinite elements \C{\gl{un}{one}} and \C{\gl{rhyw}{\rhyw{}}} has been touched upon above in \cref{sec:anecdotes:components:tempanch:two groups}; these elements convey the anecdote happened in \emph{some} particular point in the past.
Definite \Ctempanchs{}, on the other hand, occur in fewer anecdotes, and their textual function is different.
Definiteness is a complex matter, consisting of several phenomena which are bundled together differently in different languages.
Two main aspects are anaphoricity (or, more generally beyond deixis, familiarity) and uniqueness%
\footnote{%
	Not all languages combine them in one form.
	See \textcite{schwarz.f:2019:weak-strong} for theoretical and typological considerations and \textcite[§~3.5]{shisha-halevy.a:2007:topics} for a description of the system of one language (Bohairic Coptic\index[langs]{Egyptian!Coptic!Bohairic}), including text-linguistic matters.
}.
\begin{table}
	\caption{Temporal anchors referring back to previous points in the text}%
	\label{tab:anecdotes:components:tempanch:macro:backref}
	\newcommand{\row}[4]{%
		\Stack{
			\C{#2}\\
			\glosscolour{#3}
		} &
		\anecref{#1} &
		#4\\
	}%
	\footnotesize{}%
	\begin{tabularx}{\myfullwidth}{llX}
		\toprule
		Temporal anchor & \Anec{} & Reference\\
		\midrule
		%
		\row{63}
		{\gl{ddiwrnod}{\Adv{}\textbackslash{}day} \gl{y}{\Def{}} \gl{claddu}{bury.\Inf{}}}
		{the day of the burial}
		{%
			The \Cexposition{} introduced the death of a boy into the text (\C{\ellipsis{} \gl{i}{to} \gl{berthynas}{relation} \gl{imi}{\imi{}} \gl{golli}{\Len{}\textbackslash{}lose.\Inf{}} \gl{bachgen}{boy}}), which is makes it culturally expected for a burial to take place (a definite \C{\gl{y}{\Def{}} \gl{claddu}{bury.\Inf{}}}).
		}
		%
		\row{129}
		{\gl{y}{\Def{}} \gl{diwrnod}{day} \gl{dan}{under} \gl{sylw}{attention}}
		{the day in question}
		{%
			In the \Cabstract{} the abstract the author says she remembers the last time (\C{\gl{y}{\Def{}} \gl{tro}{time} \gl{olaf}{last} \gl{y}{\yrRel{}} \gl{gwelais}{see.\ais{}} \gl{hi}{\hi{}}}) she saw Neli, the time the anecdote revolves around.
			After a long \Cepilogue{}, providing many details, the author returns to the point, and recentre the temporal reference to \emph{the day in question}.
		}
		%
		\row{45}
		{\gl{y}{the} \gl{nos}{night} \gl{Sadwrn}{Saturday} \gl{tan}{under} \gl{sylw}{attention}}
		{\pbox{99cm}{the Saturday night in\\[0.5ex]question}}
		{%
			Here there is no clear reference beyond \C{\gl{hyn}{\hynn{}}}[that, it] in the \Cexposition{}, which refers to the events told in the anecdote.
			Nothing in the text before states explicitly that it was night or Saturday (that is, beyond cultural expectation about the day one runs such errands or the time one returns home).
		}
		%
		\row{86}
		{\gl{y}{\Def{}} \gl{tro}{time} \gl{hwn}{\hwn{}}}
		{this time}
		{%
			The \Cabstract{} presented a few disasters (\C{\gl{ychydig}{few} \gl{drychinebau}{disaster.\Pl{}}}) that happened, and the \Cexposition{} describes the usual way the cat jumped (with which \emph{this time} is contrasted).
		}
		%
		\row{174}
		{\gl{y}{\Def{}} \gl{tro}{time} \gl{hwnnw}{\hwnnw{}}}
		{that time}
		{%
			This anecdote comes right after the previous anecdote (\aneccref{161}) and shares the same time reference with it, telling another anecdote about the same drama performance.
			Take note of \C{\gl{hefyd}{also}} after the opening \Ctempanch{}.
		}
		%
		\row{132}
		{}
		{}
		{%
			This is the same anecdote retold in \Atgofion{}; it follows the same pattern (now with \C{\gl{trychineb}{disaster} \gl{arall}{other}}[another disaster] in the \Cabstract{}, because this time it is not the first in the series; see \cref{sec:anecdotes:embedding:clusters}).
		}
		%
		\row{60}
		{\gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{bore}{morning}}
		{in the morning}
		{%
			This is the morning after the night they had been on their feet (\C{\gl{Buasai}{\buasai{}} \ellipsis{} \gl{ar}{on} \gl{eu}{\eu{}} \gl{traed}{foot.\Pl{}} \gl{trwy’r}{through-\Def{}} \gl{nos}{night}} in the \Cexposition{}).
		}
		%
		\row{153}
		{}
		{}
		{%
			\C{\gl{ar}{on} \gl{bnawn}{afternoon} \gl{Sadwrn}{Saturday}}[on a Saturday afternoon] has been established in the \Cexposition{}.
			The definite \Ctempanch{} in question uses it as a relative future reference and sets the beginning of the anecdote to the morning before that afternoon.
		}
		%
		\bottomrule
	\end{tabularx}
\end{table}
Phoricity is covered by \cref{tab:anecdotes:components:tempanch:macro:backref}, which describe the ways definite \Ctempanch{} refer back to previous points in the text.
The uniqueness without phoricity can be seen in \aneccref{51}, where the definite \C{\gl{diwrnod}{day} \gl{yr}{\Def{}} \gl{arwerthiant}{auction}}[the day of the auction], modified by \C{\gl{pan}{when} \gl{ymadawem}{leave.\Impf{}.\Fpl{}} \gl{â}{with} \gl{Chae’r Gors}{\Pn{}}}[when we were leaving Cae’r Gors], refers to a specific event that had not been mentioned above, or in \anecref{170}, where \C{\gl{y}{\Def{}} \gl{noson}{evening} \gl{gyntaf}{\cyntaf{}}}[the first evening] implies a unique (first) evening out of a number of evenings.
Not all cases with a definite article are indeed definite in the syntactic sense; \C{\gl{ar}{on} \gl{y}{\Def{}} \gl{Sadwrn}{Satuday}}[on Saturday] in \aneccref{44} has a definite article, but this is just an idiomatic to refer to Saturday, without a definite force%
\footnote{%
	Both Saturday (\C{Sadwrn}) and Sunday (\C{Sul}), but not the other days, share this property; see \C{(ar) ə sadurn} and \C{(ar) ə syːl} on \textcite[pp.\ 471 and 512, respectively]{fynes-clinton.o.h:1913:vocabulary-bangor}.
}.
Similarly, \C{\gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{bore}{morning}}[in the morning] does not imply a specific morning, but is an idiomatic way to refer to the morning as a time of the day%
\footnote{%
	This time the use of a definite article is idiomatic in the English equivalent \E{in the morning} as well (while \E{on the Saturday} on its own is unidiomatic).
}.

A recurring pattern that combines definite and indefinite markers is the partitive \C{un o’r \NP{}.\Pl{} \Dem{}}[one of \Dem{} \NP{}.\Pl{}], which occurs as
\C{\gl{ar}{on} \gl{un}{one} \gl{o’r}{of-\Def{}} \gl{achlysuron}{occasion.\Pl{}} \gl{hyn}{\hynpl{}}}[on one of these occasions] (\cref{appex:anecdote:60,appex:anecdote:61}),
\C{\gl{ar}{on} \gl{un}{one} \gl{o’r}{of-\Def{}} \gl{dyddiau}{day.\Pl{}} \gl{tywyll}{dark} \gl{hynny}{\hynnypl{}}}[on one of those dark days] (\aneccref{84}) and
\C{\gl{un}{one} \gl{o’r}{of-\Def{}} \gl{troeon}{time.\Pl{}} \gl{hyn}{\hynpl{}} \gl{/}{} \gl{hynny}{\hynnypl{}}}[(on) one of these~/ those times] (\cref{appex:anecdote:53,appex:anecdote:79}, respectively).
The \C{\gl{un}{one}} part individuates one occasion out of a number of occasions described above in the text.

{}\index{anecdote components!temporal anchor|)}%
