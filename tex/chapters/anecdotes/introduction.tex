\section{Introduction}%
\label{sec:anecdotes:intro}

\subsection{Background}%
\label{sec:anecdotes:intro:background}

\openbilingepigraph%
{%
	\selectlanguage{welsh}%
	Ond yn lle edrych yn ôl ar y gorffennol, dechreuais edrych i mewn i mi fy hun ac ar fy mhrofiadau.
}%
{%
	But instead of looking back to the past, I began to look inside myself and on my experiences.
}%
{\worktitle{Atgofion}, p.~35}%
%
Narrative is an extremely varied form of communication: be they as dissimilar as a lengthy novel, a bedtime story or a person telling their experiences that day, all consist of a chronologically ordered \parencite[9]{chatman.s:1990:coming-terms} and causally connected \parencite{prince.g:1987:dictionary-narratology} unique sequence of events (\cite{herring.s:1986:marking-unmarking}, as cited in \textcite[§~4.2.2]{fleischman.s:1990:tense}), representing a possible world \parencite{herman.d:2002:story-logic} and communicating human experience through characters of human nature operating in that world \parencite[6]{fludernik.m:2009:introduction-narratology}.
With respect to narrative as a text-type, it is considered a low-level one in typologies proposed by both narratologists\index{narratology} and linguists, including
\textcite[][distinguishing narration, description, exposition, argumentation  and instruction]{werlich.e:1976:text-english}, \textcite[][narration and procedural, hortatory and expository discourse]{longacre.r.e:1976:anatomy-speech},
\textcite[ch.~9 in][narrative, argumentative, descriptive]{de-beaugrande.r+:1981:text-linguistics},
\textcite[][narrative, description, and exposition]{adams.j.k:1996:narrative-explanation} and
\textcite[ch.~1 in][narrative, description and argument]{chatman.s:1990:coming-terms}.%
\footnote{%
	See also \textcites[]{georgakopoulou.a:2005:narrative-type}[§~1.1]{de-fina.a+:2012:analyzing-narrative} for discussions on text-types that focus on narrative.
	For discussion on autobiography within the context of narratology\index{narratology}, see \textcite{aumuller.m:2014:text-types}.
}
Some, like \textcite{virtanen.t:1992:narrative-basic}, go further and regard the narrative as even more fundamental than other text types \parencite[see also][§~1.2]{de-fina.a+:2012:analyzing-narrative}.

In spite of their common narrativity, types of narrative differ greatly in form, length, function, composition and structure.
This chapter explores the text-linguistic features of a particular type of narrative~— the anecdote~— as it is manifested in two autobiographical texts by Kate Roberts: \C{\gl{Y}{\Def{}} \gl{Lôn}{lane} \gl{Wen}{white}}[The White Lane] \parencite{roberts.k:1960:lon-wen} and \C{\gl{Atgofion}{memory.\Pl{}}}[Recollections] \parencite{roberts.k:1972:atgofion}.

These anecdotes are spatiotemporally anchored and linguistically signalled and delimited short accounts of specific (i.e.\ not habitual or generic) past occurrences that are pertinent to the topic under discussion.%
\footnote{%
	Cf.\ \posscite{polanyi.l:1986:organization-disorganization} definition of a story as a specific (vs.\ generic) past-time narrative that has a plot and makes a point \parencite[cited in][§~4.3]{fleischman.s:1990:tense}.
}
They are common in both sources%
\footnote{%
	\YLW{} (148 pages long; more than 58,000 words) has 112 anecdotes.
	\Atgofion{} (30 pages long; more than 10,000 words) has 38 anecdotes.
}, and are embedded into the fabric of the larger text: these small-scale narratives possess a degree of text-linguistic independence yet are always connected to the encompassing text, as discussed in \cref{sec:anecdotes:embedding}.

This embedding has formal implications and functional motivation.
In both texts the anecdotes are used in order to support, illustrate or expand upon topics discussed in the text.
Thus, the anecdote as a textual unit is of a subordinative nature and functions as an eyewitness or reported testimonial \parencite[see][]{muller.f.e+:1995:stories-examples}: its basic textual function is to substantiate statements, claims and comments by means of concrete narrative instances.%
\footnote{%
	This use of anecdotal narratives as evidence conforms with \posscite{bruner.j:1986:two-modes} distinction between two \emph{modes of thought}: the logico-scientific one (which dismisses anecdotes) and the narrative one.
}



\subsection{Corpus}%
\label{sec:anecdotes:corpus}

{}\index{corpus|(}%
As stated above, the corpus for this chapter consists of two autobiographical texts by Kate Roberts.

\begin{marginpartable}%
	{The chapters of \YLW{}}%
	{The chapters of \YLW{}}%
	{tab:anecdotes:corpus:ylw}%
	\begin{tabular}[t]{rll}
		 1 & \C{Darluniau} & \rdelim]{2}{*}[\crotatebox{-90}{pict.}]\\
		   & \glosscolour{Pictures}\\
		 2 & \C{Fy Ardal} & \rdelim]{10}{*}[\crotatebox{-90}{community}]\\
		   & \glosscolour{My Neighbourhood}\\
		 3 & \C{Diwylliant a Chymdeithas}\\
		   & \glosscolour{Culture and Community}\\
		 4 & \C{Diwylliant a’r Capel}\\
		   & \glosscolour{Culture and the Chapel}\\
		 5 & \C{Mathau Eraill o Ddiwylliant}\\
		   & \glosscolour{Other Kinds of Culture}\\
		 6 & \C{Chwaraeon Plant}\\
		   & \glosscolour{Children’s Games}\\
		 7 & \C{Fy Nheulu} & \rdelim]{10}{*}[\crotatebox{-90}{family and social circles}]\\
		   & \glosscolour{My Family}\\
		 8 & \C{Fy Nhad}\\
		   & \glosscolour{My Father}\\
		 9 & \C{Fy Mam}\\
		   & \glosscolour{My Mother}\\
		10 & \C{Perthnasau Eraill}\\
		   & \glosscolour{Other Relations}\\
		11 & \C{Hen Gymeriad}\\
		   & \glosscolour{An Old Character}\\
		12 & \C{Amgylchiadau’r Cyfnod} & \rdelim]{3}{*}[\crotatebox{-90}{comm.}]\\
		   & \Stack{\glosscolour{The Circumstances}\\\glosscolour{of the Time}}\\
		13 & \C{Y Darlun Diwethaf} & \rdelim]{2}{*}[\crotatebox{-90}{pict.}]\\
		   & \glosscolour{The Final Picture}\\
	\end{tabular}
\end{marginpartable}
One~— \YLW{} ‘The White Lane’ \parencite{roberts.k:1960:lon-wen}, subtitled \C{\gl{Darn}{piece} \gl{o}{of} \gl{hunangofiant}{self-memoir}}[a piece of autobiography]~— is centred around the rural Welsh community in Arfon, north Wales, where Roberts (b.~1891) grew up (\cite{llywelyn-williams.a:1969:arfon-roberts}; see \cref{fig:intro:object:corpus:rhosgadfan} for a map).
While not an anthropological or sociological study \foreign{per se}, it is not an autobiography in the usual sense of the word either: instead of chronologically portraying the author’s life from infancy up to the time of writing~— when she was almost seventy years old~— it presents the author’s observations and memories regarding her childhood community (chs.~2–6 and 12) and members of family and social circles (chs.~7–11) in a thematically structured manner, as well as painting ‘memory pictures’ in chronological order (chs.~1 and 13); see \cref{tab:anecdotes:corpus:ylw}.
The first (\C{\gl{Darluniau}{picture.\Pl{}}}[Pictures]) and last (\C{\gl{Y}{\Def{}} \gl{Darlun}{picture} \gl{Diwethaf}{last}}[The Final Picture], ch.~13) chapters are of a wholly different kind in comparison to the others.
\C{\gl{Darluniau}{picture.\Pl{}}} consists of twenty-two ‘pictures’ from the author’s reminiscences of her childhood and adolescence and \C{\gl{Y}{\Def{}} \gl{Darlun}{picture} \gl{Diwethaf}{last}} describes her in the age when she writes the book.
Both contain no anecdotes \foreign{sensu stricto} as defined here and both exhibit striking linguistic differences with respect to the rest of the book.
The sixth chapter is also unique~— thematically, structurally and linguistically~— as it is a description of the children’s games of the author’s childhood.

The other text~— \Atgofion{} ‘Recollections’ \parencite{roberts.k:1972:atgofion}~— was published twelve years later.
It makes one chapter in \textcite{roberts-williams.j+:1972:atgofion-1}, a volume based on \C{\gl{Y}{\Def{}} \gl{Llwybrau}{path.\Pl{}} \gl{Gynt}{early.\Cmp{}}} ‘The Former Paths’, a radio series of personal memoirs by well-known Welsh figures \parencite[3]{gramich.k:2011:kate-roberts}.%
\footnote{%
	\label{fn:anecdotes:corpus:radio-vs-written}%
	A short two-minute fragment of Kate Roberts’s episode (1971) is available on the BBC website at \url{https://bbc.co.uk/sounds/play/p06s6zss}.
	Judging from it, it seems that our written version follows the radio episode quite closely, but the exact wording does differ ever so slightly at times.
}
It begins with a vivid and detailed image of the author’s childhood home, \C{\gl{Cae’r}{field-\Def{}} \gl{Gors}{marsh}} in Rhosgadfan%
\footnote{%
	See \textcite{tomos.d:2009:llyfr-lloffion} for more information and \cref{fig:intro:object:corpus:rhosgadfan} for a map.
},
and its surroundings, seamlessly moving to depicting her life’s journey%
\footnote{%
	The last decades of her life until the time of writing are sketched in a less detailed manner and make only a few paragraphs, as they were less eventful.
}
\parencite[see][]{gramich.k:2011:intimate-circle}.

These two texts share many linguistic and non-linguistic features.
Even though the latter was originally read as a radio episode, both are literary texts, written in Literary Welsh with distinct influence of the local colloquial language, which is characteristic of the author’s writing in general \parencite[ch.~6]{emyr.j:1976:enaid-clwyfus}.
See \cref{sec:anecdotes:comparison} for a comparison of the two texts.
%The texts differ nonetheless in some aspects that are relevant to our discussion.
% One of these is difference in their subject matter and the way the texts are organised, which leads to difference in the way anecdotes are utilised as an illustrative device: while in \Atgofion{}, which is mostly chronologically structured, anecdotes occur mainly in order to expand upon the author’s experiences and people she met along her life by the way of concrete narrative recollections, in \YLW{}, which is thematically structured, they are employed for the function of substantiating statements, claims and comments about society as well.

{}\index{corpus|)}



\subsection{Annotation}%
\label{sec:anecdotes:annotation}

{}\index{annotation|(}%
\Cref{app:anecdotes} covers anecdotes from the corpus.
For convenience, the numbering of anecdotes from \Atgofion{} begins from 201, so references to the two texts can be easily distinguished.
The examples in the appendix are translated into English and are segmented and annotated according to the structure described in \cref{sec:anecdotes:structure} below, following a typographical convention as specified in \cref{app:anecdotes:annotation}.

{}\index{annotation|)}



\subsection{Overview of the chapter}%
\label{sec:anecdotes:overview}

Five sections follow this introduction.
\Cref{sec:anecdotes:structure} outlines the linguisti\-cally-signalled structure of the anecdotes and deals with general issues.
Each of the seven components presented in \cref{sec:anecdotes:structure} is further discussed in \cref{sec:anecdotes:components}.
As the anecdotes are dependent on the text in which they are embedded, \cref{sec:anecdotes:embedding} portrays the interaction between the two.
In spite of sharing core features, the anecdotes are by no means uniform; \cref{sec:anecdotes:edge} touches upon the boundaries of their structural diversity.
\Cref{sec:anecdotes:comparison} concludes the chapter with a comparative examination of the anecdotes in the two source texts.

Thus, by examining the text-linguistic patterns that recur in the anecdotes, the next three sections deal with qualities that define them as distinct bound sub-textual narrative units%
\footnote{%
	Their internal macro-syntactic structure in \cref{sec:anecdotes:structure,sec:anecdotes:components} and
	their external relationship with the surrounding text in \cref{sec:anecdotes:embedding}.
}
and the two last sections explore diversity and unity%
\footnote{%
	Between the extremities of simplicity and complexity in \cref{sec:anecdotes:edge} and
	between versions of the same occurrences to in the two texts in \cref{sec:anecdotes:comparison}.
}.
