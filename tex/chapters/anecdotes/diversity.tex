\weakFloatBarrier{}%
\section{Edge cases}%
\label{sec:anecdotes:edge}

As demonstrated in \cref{sec:configurations_of_the_components}, the anecdotes in the corpus are by no means uniform.
Although they exhibit a common structure~— which is text-linguistically signalled and whose description makes the lion’s share of this chapter~— there is a great degree of diversity and variety when it comes to what parts of that structure \revsimple{2}{occur} in any given anecdote or the extent to which they are developed.
It is the author’s literary choice how long or short, developed or succinct, an anecdote would be.%
\footnote{%
	This choice seems to be affected by a number of factors, including
	the amount of actual content the \foreign{histoire} (in \posscite{genette.g:1972:discours-recit} terms; see \cref{fn:intro:text:sub-textual:narrmodes:genette} on p.~\pageref{fn:intro:text:sub-textual:narrmodes:genette}) has (basic events, twists and turns, etc.),
	the flow of the text in the particular point in question (how much of a digression an anecdote would make), and
	the vividness in which the author remembers the story (see \cref{sec:anecdotes:edge:max}).
}

This short section cursorily explores the edges of what makes an anecdote: \cref{sec:anecdotes:edge:min,sec:anecdotes:edge:max} look at the minimal and maximal extremities, and \cref{sec:anecdotes:edge:nonanecdote} goes ‘beyond the edge’ and touches on other textual units which show some resemblance to anecdotes.

Anecdotes~— like all linguistic (\emph{text-linguistic} included) elements~— are not given entities.
As discussed in the introduction chapter (\cref{sec:intro:text:text-types,sec:intro:method:procedure:iterative}), text-types in general are defined and delineated \foreign{a posteriori}, on the basis of structural regularities found in the data (see the approach described in \cref{sec:intro:method:neutral}), and it is not necessary for them to conform to preconceived notions.
While the vast majority of cases can be unambiguously described as anecdotes, ‘fringe’ or peripheral cases might prove helpful for defining what counts as an anecdote.



\subsection{Minimal cases}%
\label{sec:anecdotes:edge:min}

\subsubsection{Examples for laconic anecdotes}%
\label{sec:anecdotes:edge:min:laconic}

Very short and laconic cases are one such peripheral extremity.
Three examples (exx.~\getfullref{ex:anecdotes:edge:min:140}–\getfullref{ex:anecdotes:edge:min:23}) are taken as representative for such cases.

\anecCref{140} (15 words; \exfullref{ex:anecdotes:edge:min:140}) consists only of a \Cdevelopment{}, and is about as minimal and compact as it goes.
Syntactically, it is a cleft sentence, which might imply an already established familiarity with the story (and consequently an ability to leave out almost any excess detail) and focus on the identification of Mos as the person the story is told about, as this anecdote is located within a section of the text which portrays him.%
\footnote{%
	\label{fn:Mos=Bilw2}%
	This assumed familiarity might have to do with the close-knit community Roberts was part of, where stories about acquaintances were told and retold, reinforcing social cohesion.
	The target audience of \Atgofion{}, though, is much wider than that community, and removed from the time period in which the stories occurred by some decades (even though it might have seemed yesterday in the author’s eyes…).
	Anyway, Mos was ‘commemorated’ as a character in \TG{}, a well-known collection of short stories, which makes a part of the corpus of \cref{sec:rd} (see \cref{fn:Mos=Bilw} on p.~\pageref{fn:Mos=Bilw}).
	It is not impossible that setting the minimal story as a non-focal \foreign{glose}\index{cleft sentence!glose@\foreign{glose}} of a cleft sentence has to do with that, assuming the audience is familiar with the fictionalised version.
}

\preex{}\ex<ex:anecdotes:edge:min:140>
	\gloss
	{%
		Y fo a adawodd i’r pwdin Nadolig ferwi’n sych nes oedd y pwdin yn golsyn.
	}
	{%
		\Foc{} \fo{} \aRel{} leave.\odd{} to-\Def{} pudding Christmas \Len{}\textbackslash{}boil.\Inf{}-\ynC{} dry until \oedd{} \Def{} pudding \ynC{} cinder.\Sgv{}
	}
	{%
		It was him who let the Christmas pudding cook dry until it became a cinder.
	}
	{\exsrcapp{anecdote}{140}{atgofion}{}{16}}
\xe\postex{}

Just before \anecCref{163} (19 words; \exfullref{ex:anecdotes:edge:min:163}) the author reminisce about the good tea Mr.\ Morgan, the physics teacher, used to make after school dinners.
The anecdote tells of a single incident (take note of the \Ctempanch{} \C{\gl{un}{one} \gl{tro}{time}}[one time]) in which she made a \foreign{faux pas}.
The second sentence (\C{\gl{Braint}{privilege} \gl{Mr.}{Mr} \gl{Morgan}{\Pn{}} \gl{oedd}{\copoedd{}} \gl{hynny}{\hynnyn{}}}) is not a part of the plot (i.e.\ the \emph{evolution mode}), but a comment; this demonstrates that even in such a minimal narrative a ‘division of labour’ between narrative modes can occur.

\preex{}\ex<ex:anecdotes:edge:min:163>
	\gloss
	{%
		Fe rois i fy nhroed ynddi un tro wrth gynnig gwneud y te yma. Braint Mr. Morgan oedd hynny.
	}
	{%
		\fe{} give.\ais{} \Fsg{} \fy{} foot \ynddi{} one time with offer.\Inf{} do.\Inf{} \Def{} tea \ymadem{} privilege Mr \Pn{} \copoedd{} \hynnyn{}
	}
	{%
		I did put my foot in it one time by offering to make tea there. It was Mr.\ Morgan’s position.
	}
	{\exsrcapp{anecdote}{163}{atgofion}{}{29}}
\xe\postex{}

\anecCref{23} (32 words) occurs as the last in a cluster%
\footnote{%
	The use of \C{\gl{hefyd}{also}} is related to the fact it is a non-initial member of a cluster.
}
of four anecdotes (\anecs{}~\anecref{20}-\anecref{21}-\anecref{22}-\anecref{23}; see \cref{sec:anecdotes:embedding:clusters}) about a preacher named David Williams.
Similarly to \cref{ex:anecdotes:edge:min:140} above, here too the main sentence is a cleft sentence.
The reason for that might be as above, although here to the best of my knowledge there is no fictionalised version.
With respect to the story being presented linguistically as familiar, take note of \C{\gl{y}{\Def{}} \gl{fam}{mother}}[the mother], which is definite even though she was not mentioned anywhere before in the text.

\preex{}\ex<ex:anecdotes:edge:min:23>
	\gloss
	{%
		Ef hefyd a ddywedodd wrth y fam a âi allan o’r capel pan grïodd ei babi, am ddyfod ag ef i’r sêt fawr, y byddai’n siŵr o gysgu yn y fan honno.
	}
	{%
		\ef{} also \aRel{} \dweud{}.\odd{} with \Def{} mother \aRel{} go.\ai{} out from-\Def{} chapel when cry.\odd{} \eif{} baby about come.\Inf{} with \ef{} to-\Def{} seat big \Nmlz{} \byddaisbjv{}-\ynB{} sure of sleep.\Inf{} \ynA{} \Def{} place(\F{}) \honno{}
	}
	{%
		\notsic{}It was he who also told the \revsimple{1}{mother} leaving the chapel when her baby cried, to bring him to the big pew in the front as he’d be sure to fall asleep there.
	}
	{\exsrcapp{anecdote}{23}{ylw}{4}{52}}
\xe\postex{}



\subsubsection{The question of narrativity}%
\label{sec:anecdotes:edge:min:narrativity}

On the whole, anecdotes fall under the broad umbrella term \emph{narrative}, but some of the simpler examples do not feel very ‘storylike’ in the sense of how stories are prototypically built and developed (or alternatively how they are stereotypically expected to be).
This tension can be resolved using \posscite[91]{herman.d:2002:story-logic} distinction between \emph{narrativehood} (which is binary: either a text is a narrative or not) and \emph{narrativity} (which is a scalar: a text can be closer or more distant from a prototypical narrative).
So, the anecdotes are not homogeneous with respect to \emph{narrativity}: some are characterised by a high degree of it, while some (like the shorter ones) show a lower degree.
% From \textcite[165]{baldick.ch:2001:dictioanry-literary}: The category of narratives includes both the shortest accounts of events (e.g. the cat sat on the mat, or a brief news item) and the longest historical or biographical works, diaries, travelogues, etc., as well as novels, ballads, epics, short stories, and other fictional forms.

One criterion of narrativity found in some traditional narratological\index{narratology} approaches is that the text presents goal-directed actions \parencite[§~1.1.2–1.1.3]{de-fina.a+:2012:analyzing-narrative}: the basic structure is that a complication or disruption is introduced, and the characters react to deal with it in a goal-directed manner.
Regarding this criterion as a cornerstone of narrativity is by no means universally accepted.%
\footnote{%
	Beyond theory, \textcite{stein.n+:1984:concept-story} (as cited in \cite[§~1.1.2]{de-fina.a+:2012:analyzing-narrative}) tested experimentally what people~— both children and adults~— recognise as stories, and could not demonstrate it was necessary for a text to include an account of goal-directed behaviour in order to be considered a \emph{story}.
}
Many of the anecdotes described in this chapter~— not only the simplest ones~— do not have goal-directed actions (driven by some complication or otherwise) as an organising mechanism.
This does not diminish their narrativity: while complications and goals motivate the plot of many kinds of stories, such as fairy tales, they are not \foreign{sine quibus non} for a text to be a narrative.
Moreover, the notion of a \emph{story} is at least partially dependent on culture; the Welsh \C{\gl{stori}{story}} does not necessarily align exactly with that of the English\index[langs]{English} \E{story}.
See \cref{fn:stori-gpc} on p.~\pageref{fn:stori-gpc} for a dictionary definition of \C{stori}; see also \cref{fn:stori-anecdote and HIDE AND TAIL} on p.~\pageref{fn:stori-anecdote and HIDE AND TAIL}, which expands on the relation between \C{stori} and the anecdotes.



\weakFloatBarrier%
\subsection{Maximal cases}%
\label{sec:anecdotes:edge:max}

{}\index{quantitative analysis|(}%

\begin{table}
	\caption{Overview of the five longest anecdotes}%
	\label{tab:anecdotes:edge:max}
	\newcommand{\row}[7]{%
		\anecref{#1} &
		#2 &
		\colratblock{#2}{827}{darkgray}{12em} &
		\cfgcell{#3} &
		\cfgcell{#4} &
		\cfgcell{#5} &
		\cfgcell{#6} &
		\cfgcell{#7}\\
	}
	\begin{tabular}{lrlccccc}
		\toprule
		\Anec{} &
		\multicolumn{2}{l}{Length (words)} &
		\narrcomp{abs} &
		\narrcomp{exp} &
		\narrcomp{dev} &
		\narrcomp{epi} &
		\narrcomp{con}\\
		\midrule
		\row {45}{827}{1i }{1  }{1t }{   }{   }
		\row{129}{707}{1i }{1  }{1t }{1  }{   }
		\row{102}{597}{1i }{1  }{1  }{1  }{   }
		\row {66}{431}{1i }{1  }{1t }{   }{   }
		\row {15}{400}{   }{   }{1it}{1  }{   }
		\bottomrule
	\end{tabular}
\end{table}
On the other end, some of the anecdotes in the corpus are rather lengthy; see \cref{tab:anecdotes:edge:max} for an overview of the five longest anecdotes%
\footnote{%
	\label{fn:anecdotes:edge:max:length-words}%
	Length is calculated in ‘words’, which in this context are defined as any sequence of characters delimited by a white space.
	For example, \C{o’r}[of the] is counted as one word.

	A blue square (\Pint{}) represents an \Cintanch{} and a red rhombus (\Ptemp{}) a \Ctempanch{}, similarly to \cref{tab:anecdotes:configurations}.
}.
A quick quantitative comparison shows the longest anecdote (\aneccref{45}) is about a half the length of Kate Roberts’s shorter short stories in terms of simple word \revsimple{2}{count}.
Unsurprisingly, the longer anecdotes tend to be more developed and intricate as stories, including features as commentative excursus and episodes.
Another facet of complexity is reflected in their use of the different components; \cref{tab:anecdotes:edge:max} shows that while \aneccref{15} has only two sections, the other are more complex.
As discussed in \cref{sec:anecdotes:components:intanch} above, two of the longest anecdotes (\cref{appex:anecdote:66,appex:anecdote:129}) have \Ctempanchs{} which explicitly refer to how vivid the author remembers them: \C{\gl{Mae}{\mae{}} \gl{\textsc{meta-reference}}{} \gl{yn}{\ynC{}} \gl{fyw}{alive} \gl{iawn}{very} \gl{yn}{\ynA{}}\gl{/}{}\gl{ar}{on} \gl{fy}{\fy{}} \gl{nghof}{memory} \gl{(i)}{\Fsg{}}}[\textsc{meta-reference} is very alive in my memory/mind].
Another notable feature is the use of paragraph breaks to mark different episodes in \cref{appex:anecdote:45,appex:anecdote:66}.
This typographic device is used only sporadically, unsystematically, but the length and narrative complexity of these anecdotes supposedly made them lend themselves better to such a use, thanks to both narratological reasons (they can have more distinct episodes, each of which can be more developed) and typographical reasons (to avoid unwieldy paragraphs).



\subsubsection{Embedded narratives}%
\label{sec:anecdotes:edge:max:embed}

Beyond quantitative length (which correlates to some extent with complexity), another type of complexity can be seen in the embedding of second-degree; that is, subordinate narratives which are incorporated inside an anecdote.

Such fragments are attested in the \Cexposition{}, the \Cdevelopment{} and the \Cconclusion{} (and to a limited degree in the \Cepilogue{}), but not in the \Cabstract{}, which is more restricted and goes no further in narrative complexity than the ‘titles’ discussed in \cref{sec:anecdotes:components:abstract:no-meta:no-intanch}.



\paragraph{Exposition}%
\label{sec:anecdotes:edge:max:embed:exp}

The \Cexposition{} of \aneccref{117} has a lengthy narrative which is given as an explanation for why Owen Jones was in prison.
In fact, it is longer and more complex than the rest of the anecdote, and has an internal ‘abstract’, ‘exposition’ and ‘development’ of its own.%
\footnote{%
	The internal structure is annotated in \cref{app:anecdotes}, and square brackets are employed for marking the boundaries of the embedded narrative.
	The same applies to \aneccref{113}.
}
The abstract presents the source of information (explicitly, within parentheses) and encapsulates this portion of the text as a reason (\C{\gl{A}{and} \gl{dyma}{\dyma{}} \gl{pam}{why} (\ellipsis{}):}[And this is why (\ellipsis{}):]).
The internal exposition gives necessary background information%
\footnote{%
	After all, about 113 years have passed between the time of the occurrence and the time \YLW{} was published.
}, 
and it ends when the \Cdevelopment{} begins with the temporal anchor \C{\gl{Un}{one} \gl{diwrnod}{day}}[One day].
Only after the whole digression the \Cdevelopment{} of the main anecdote begins, which is quite short.

See \aneccref{66} for another case of a narrative embedded in the \Cexposition{}, this time a simpler one.
It is presented with \C{\gl{Fel}{like} \gl{y}{\Nmlz{}} \gl{crybwyllais}{mention.\ais{}} \gl{mewn}{in} \gl{lle}{place} \gl{arall}{other}}[As I mentioned elsewhere].
Here too a temporal anchor (\C{\gl{Yr}{\Def{}} \gl{wythnos}{week} \gl{y}{\yrRel{}} \gl{cyrhaeddodd}{arrive.\odd{}} \gl{y}{\Def{}} \gl{newydd}{new(s)}}[The week the news came]) marks the beginning of the \Cdevelopment{}.



\paragraph{Development}%
\label{sec:anecdotes:edge:max:embed:dev}

\Cdevelopments{} can have multiple layers, and can diverge and reunify.
A true framed narrative, though, is found in \aneccref{103}.
The first paragraph of the anecdote ends with the disappearance of a \C{warpaig} (a small bag with a drawstring) full of marbles, and the story told in the second one resolves the mystery.
It, too, has an abstract-exposition-development structure:
the abstract presents the source of information and binds the story the minister’s wife tells to the first part of the anecdote (with \C{Modd bynnag}[However]),
the exposition informs the reader about the age of the children, and
the \Cdevelopment{} presents the internal story itself.
The \Cdevelopment{} begins with \C{\gl{A}{and} \gl{stori}{story} \gl{Mrs.}{Mrs} \gl{CW}{\Pn{}} \gl{oedd}{\copoedd{}}}[And Mrs CW’s story was]%
\footnote{%
	This looks like a ‘second abstract’.
	There are not enough embedded narrative of this kind in order to make any generalisations about their structure.
},
followed by a \C{\gl{bod}{\bod{}}} construction (\C{\gl{ei}{\eif{}} \gl{bod}{\bod{}} \gl{yn}{\ynD{}} \gl{edrych}{look.\Inf{}} \gl{allan}{out}}[that she looked out]) and another infinitive (\C{(\gl{a}{and}) \gl{gweled}{see.\Inf{}}}), and then it continues without grammatical markers of subordinations (the matrix forms \C{\gl{Ni}{\Neg{}} \gl{allai}{\gallu{}.\ai{}}}[She couldn’t], \C{\gl{fe hitiwyd}{hit.\wyd{}}}[was struck], \C{\gl{aeth}{go.\odd{}}}[went, (began)], etc.).

Another kind of internal storytelling can be seen in \aneccref{142}, where Ned Ryd tells a jocular dream%
\footnote{%
	Narratives that report dreams (genuine, or as in our case, made up) are a fascinating topic which cannot be adequately explored here.
	The way people construct and tell dream reports varies across languages and cultures, and is closely linked to text-linguistic (mainly narrative grammar) and other grammatical matters; see, for example,
	\textcite[§~5.4.7]{cohen.e:2012:syntax-zakho} for Jewish Zakho Neo-Aramaic\index[langs]{Aramaic!Neo-Aramaic!Jewish dialect of Zakho},
	\textcite{kracke.w.h:2009:dream-grammar} for Kagwahiv\index[langs]{Tupí-Guaraní!Kagwahiv} (Tupí-Guaraní; see \textcite{tedlock.b:1999:dreams-amerindian} for a broad indigenous American context),
	\textcite{perelmutter.r:2008:dream} for Russian\index[langs]{Russian}, and
	\textcite[§~1.1.3 (e)]{shisha-halevy.a:2007:topics} for Bohairic Coptic\index[langs]{Egyptian!Coptic!Bohairic}.
}.
The dream is told \emph{within} a dialogue portion, not as a ‘low-level’ portion of the text (like in \aneccref{113}).
It has a sentence that looks like an abstract (\C{\gl{Mi}{\mi{}} \gl{ges}{get.\ais{}} \gl{hen}{old} \gl{freuddwyd}{dream} \gl{cas}{hateful} \gl{iawn}{very}}[I had a terrible dream]), followed by \C{\gl{Mi}{\mi{}} \gl{freuddwydis}{dream.\ais{}}}[I dreamt], which is in turn complemented~— similarly to the above case~— by a \C{\gl{bod}{\bod{}}} construction (\C{\gl{fy}{\fy{}} \gl{mod}{\bod{}} \gl{i}{\Fsg{}} \gl{wedi}{after} \gl{marw}{die.\Inf{}}, \gl{ac}{and} \gl{wedi}{after} \gl{mynd}{go.\Inf{}} \gl{i}{to} \gl{uffern}{inferno}}[that I had died and had gone to hell]) and then a matrix form \C{\gl{faswn}{\Neg{}\textbackslash{}\buaswn{}} \gl{i}{\Fsg{}} \gl{ddim}{\Neg{}}}[I wouldn’t be].
The dream report has an instance of indirect speech in it, making it a fourth layer of embedding (indirect speech within a dream report within a dialogue within an anecdote within the broader text).



\weakFloatBarrier%
\paragraph{Epilogue}%
\label{sec:anecdotes:edge:max:embed:epi}

\CEpilogues{} \rev{2}{on their own are generally} not very storylike.
They do present information that follows the events told in the anecdote, but without a proper \rev{2}{self-contained, fully-fledged} narrative structure.
\begin{fragment}
	\caption{The epilogue of \aneccref{102}}%
	\label{frg:anecdotes:edge:max:embed:epi:most-storylike}%
	\SingleSpacing{}%
	\footnotesize%
	\egaliterianbiling{%
		Ond ni bu’r hen gyfaill yn lladd gwair i lawer wedyn. Dechrau ei salwch oedd ei ymddygiad rhyfedd y prynhawn hwnnw. Yr oedd ei ymennydd yn dechrau darfod. Un o Sir y Fflint ydoedd, ac wedi treulio llawer o flynyddoedd yn America.
		Pan af i fynwent Rhosgadfan ac edrych ar ei garreg fedd, byddaf yn dychryn wrth ddarllen nad oedd ond 42 mlwydd oed pan fu farw. Edrychai yn llawer nes i drigain.
	}
	{%
		But our old friend did not cut hay for many people after that. The start of his illness was his strange behaviour that day. His brain was beginning to go. He was from Flint, and had spent many years in America.
		When I go to the Rhosgadfan cemetery and look at his gravestone it frightens me to read that he was only 42 years old when he died. He looked closer to sixty.
	}

	\begin{explanationundertable}%
		\footnotesize%
		—~Source: \exsrcapp{anecdote}{102}{ylw}{12}{146}
	\end{explanationundertable}
\end{fragment}
The closest thing to a story in an \Cepilogue{} is that of \aneccref{102} (\cref{frg:anecdotes:edge:max:embed:epi:most-storylike}).



\paragraph{Conclusion}%
\label{sec:anecdotes:edge:max:embed:con}

\anecCref{98} (glossed as \exfullref{ex:anecdotes:components:conclusion:98}) has an embryonic narrative in the \Cconclusion{}, which is brought up in the context of a short discussion about stories attributed to John James.

\anecCref{78} (glossed as \exfullref{ex:anecdotes:components:conclusion:78}) has a fragment that is referred to as a \C{\gl{stori}{story}} (about the man who used to hit his son).
It does not describe a sequence of events that occurred once, but one which repeated habitually (\C{\gl{arferai}{do\_habitually.\ai{}} \gl{roi}{give.\Inf{}} \gl{cweir}{thrashing}}[used to beat], unfortunately for the boy) and repeatedly (\C{\gl{bob}{\Adv{}\textbackslash{}every} \gl{tro}{time}}[every time]).



\weakFloatBarrier%
\subsection{Other textual units that bear some resemblance to anecdotes}%
\label{sec:anecdotes:edge:nonanecdote}

As defined in \cref{sec:anecdotes:intro:background}, our chapter deals with accounts of specific (singular, occurring once; not habitual or generic) past occurrences.
There are, though, narratives which describe recurring sets of events (see \textcite{dahl.o:1995:episodic-generic} for a general treatment of the related notion of \emph{episodic} vs.\ \emph{generic} sentences).%
\footnote{%
	As mentioned in \cref{fn:anecdotes:structure:labov:osiecka} on p.~\pageref{fn:anecdotes:structure:labov:osiecka}, the anecdotes in Osiecka’s \worktitle{Galeria Potworów} show striking structural commonalities with Roberts’s in our corpus.
	\textcite[§~3]{sawicki.l:2013:portrait} deals with ‘non-single quasi-events’; these show similarities with the recurring narratives discussed here (the as the use of the imperfective/imperfect forms), but with an important difference: the former are presented as fictional, while the latter are presented as real event that actually happened.
}

\begin{fragment}
	\caption{A recurrent narrative with \C{cofiaf} ‘I remember’}%
	\label{frg:anecdotes:edge:nonanecdote:recurnarr-cofiaf}%
	\SingleSpacing{}%
	\footnotesize%
	\egaliterianbiling{%
		Cofiaf y byddai J.\ R.\ Williams yn dyfod â’r cylchgronau i’r capel i bawb, pob cylchgrawn, y rhai enwadol a’r rhai cenedlaethol, gydag enw’r tŷ arnynt.
		Cofiaf fel y byddem yn rhuthro o’n seti ar y Sul cyntaf yn y mis, a stwffio at y ffenestr yn y lobi lle byddai’r cylchgronau, er mwyn cael rhedeg adref efo hwy, a chael eu darllen yn gyntaf.
	}
	{%
		\notsic{}I remember that J.\ R.\ Williams used to bring the magazines to chapel for everyone, all magazines, the denominational and the national ones, with the name of the house written on them.
		I remember how we would rush from our seats on the first Sunday of the month and press against the window in the lobby where the magazines would be, so that we could race home with them, to be first to read them.
	}

	\begin{explanationundertable}%
		\footnotesize%
		—~Source: \exsrcylw{5}{56}
	\end{explanationundertable}
\end{fragment}
\Cref{frg:anecdotes:edge:nonanecdote:recurnarr-cofiaf}, for example, describes such a case: a set of events that repeated itself every first Sunday of the month (\C{\gl{ar}{on} \gl{y}{\Def{}} \gl{Sul}{Sunday} \gl{cyntaf}{\cyntaf{}} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{mis}{month}}, not a specific, particular temporal reference like the \Ctempanchs{}).
Take note of the use of \C{\gl{cofiaf}{\cofiaf{}}}, which is repeated twice; such repetition is foreign to our anecdotes%
\footnote{%
	\anecCref{106} has a repeated \C{cofiaf}, but it is a special case, as discussed in \cref{sec:anecdotes:components:intanch} above.
}.
On the surface it might resemble \C{cofiaf} the \Cintanch{}, functions differently from a structural, textual perspective, as it does not participate in the same larger textual pattern that is the anecdote.
As mentioned in \cref{sec:anecdotes:components:intanch} (on p.~\pageref{pnt:anecdotes:components:intanch:cofiaf:in other parts of the text}), while it occurs commonly as a \Cintanch{}, \C{cofiaf} as a verb is not restricted to that function.
Beside the lexical temporal expression (\C{\gl{ar}{\gl{ar}{on}} \gl{y}{\Def{}} \gl{Sul}{Sunday} \gl{cyntaf}{\cyntaf{}} \gl{yn}{\ynA{}} \gl{y}{\Def{}} \gl{mis}{month}}), the habitual imperfect forms (\C{\gl{byddai}{\byddai{}}}, \gl{byddem}{\byddem{}}) are signs for the non-singular nature of this portion of the text.

\begin{fragment}
	\caption{A recurrent narrative referred to as a \C{stori}[story]}%
	\label{frg:anecdotes:edge:nonanecdote:recurnarr-stori}
	\SingleSpacing{}%
	\footnotesize%
	\egaliterianbiling{%
		Wrth fynd heibio yn y fan yma, mae arnaf flys dweud stori am y bachgen yma.
		Yr oedd ef ac un arall o’r un oed yn aelodau o ddosbarth llenyddiaeth llewyrchus a fu gan R.\ Williams-Parry yn Rhosgadfan.
		Pan ddywedai’r bardd ar ddiwedd ei darlith a chyn dechrau’r drafodaeth, ‘Y sawl sydd am ysmygu, ysmyged’, byddai’r bachgen hwn a’i gyfaill yn tynnu Woodbine o’u pocedi, ac yn tanio fel gweddill y dosbarth.
	}
	{%
		In passing, I’d like to tell a story about this boy.
		He and another of the same age were members of a flourishing literature class that R.\ Williams-Parry ran in Rhosgadfan.
		\notsic{}When the poet would say, at the end of the talk and before discussion, ‘Those who want to smoke, smoke,’ this boy and his friend would take Woodbines from their pockets and light up like the rest of the class.
	}
	—~Source: \exsrcylw{9}{106}
\end{fragment}
Another example for a recurring narrative is \cref{frg:anecdotes:edge:nonanecdote:recurnarr-stori}.
It occurs right after \aneccref{63}, and presents another thing the author is reminded of about the boy whose funeral is the setting for that anecdote.
While \cref{frg:anecdotes:edge:nonanecdote:recurnarr-cofiaf} has \C{cofiaf} as an element it shares anecdotes, here the first sentence resembles an \Cabstract{}, the second an \Cexposition{} and the third a \Cdevelopment{}.
Another pertinent feature is the reference to this narrative as a \C{\gl{stori}{story}}, a lexeme which is used for the anecdotes as well (see \cref{sec:anecdotes:components:abstract:meta-reference:lexeme,sec:anecdotes:components:conclusion:meta}).
In fact, the only thing that marks this as something that happened multiple times is the use of (habitual) imperfect forms (\C{\gl{dywedai}{\dweud{}.\ai{}}} and \C{\gl{byddai}{\byddai{}}}).
Had the verbal forms been different, this fragment would present its narrative as singular and would happily fall under the definition of \emph{anecdote} used in this chapter.

So far, \C{cofiaf}, \C{\gl{stori}{story}} and a form that resembles the sections of the anecdote have been encountered.
\Exfullref{ex:anecdotes:edge:nonanecdote:unwaith} demonstrates the use of \C{\gl{unwaith}{once}} outside an anecdote.
The context, in a chapter dedicated to the depiction of the author’s mother, is the medical support her mother offered to others: once a professional nurse came to the neighbourhood, her mother stopped visiting others for that purpose, but there was an exception one time.
This exception does not make \rev{2}{an actual} anecdote, nor does \C{unwaith} make a \Ctempanch{} here.

\preex{}\ex<ex:anecdotes:edge:nonanecdote:unwaith>
	\gloss
	{%
		Wedi i nyrs ddyfod i’r ardal, daeth pen ar fynd allan fel hyn, \nogloss{\hlbegin{}} @ er i mam gael ei galw unwaith wedi i’r nyrs fynd ar wyliau\hlend{}.
	}
	{%
		after to nurse \Len{}\textbackslash{}come.\Inf{} to-\Def{} district come.\odd{} end on go.\Inf{} out like \hynn{} though to \fy{}\textbackslash{}mother \Len{}\textbackslash{}get.\Inf{} \eif{} call.\Inf{} once after to-\Def{} nurse \Len{}\textbackslash{}go.\Inf{} on holiday.\Pl{}
	}
	{%
		When a nurse came to the neighbourhood, Mam going out like that came to an end, \highlight{though she was called out once when the nurse was away on holiday}.
	}
	{\exsrcylw{9}{105}}
\xe\postex{}

These three examples were given here in order to show that while entities similar to the components of the anecdote \emph{on a surface level} can be found outside an anecdote, it is the anecdotes’ particular structural regularities and textual function that define them as such.
Moreover, considering textual units which externally resemble anecdotes, at least to some degree, can assist in understanding the anecdotes themselves better.
