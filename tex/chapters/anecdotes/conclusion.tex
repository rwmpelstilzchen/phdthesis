\weakFloatBarrier{}%
\section{Conclusion}%
\label{sec:anecdotes:conclusion}

\rev{2}{}%
This chapter explores anecdotes as a special kind of sub-textual units which are embedded within a broader text and exhibit a sophisticated recurrent internal structure, with more than 150 instances in the corpus.

A zoomed-out description of the anecdotes’ structure is presented (\cref{sec:anecdotes:structure}).
It consists of seven components (\cref{sec:anecdotes:structure:overview}): five textual sections (\Cabstract{}, \Cexposition{}, \Cdevelopment{}, \Cepilogue{} and \Cconclusion{}) and two anchors (\Cintanch{} and \Ctempanch{}), which are smaller-scale phrases contained within the first three sections.
The sections are organised in a concentric form (\cref{sec:anecdotes:structure:organisation}):
the \Cdevelopment{} is obligatory and constitutes the semantic and linguistic core (or nucleus) of the anecdote;
the \Cexposition{} and \Cepilogue{} make an intermediate layer, providing anterior~/ atemporal (\Cexposition{}) or posterior (\Cepilogue{}) supplementary information;
the outer layer (\Cabstract{} and \Cconclusion{}) refers to the anecdote externally.
Numerous configurations are found in the corpus (\cref{sec:configurations_of_the_components}), showing some quantitative characteristics.%
\footnote{%
	Observations:
	\begin{inparaenum}[(a)]
		\item the distribution of the configurations has a long tail (a few have many instances while many have only a few instances);
		\item configurations with the \Cdevelopment{} alone (without any satellite sections) account for a significantly high number of instances;
		\item the sections make a general bell-shaped distribution (the sections intermediate layer are more common than their outer layer counterparts); and
		\item the preparatory sections are more common than their conclusory counterparts.
	\end{inparaenum}
}
A comparison with the widely-applied Labovian model (\cref{sec:anecdotes:structure:labov}) is made, highlighting similarities and dissimilarities and discussing the reasons for both.

Then a more in-depth examination of each of the components follows the zoomed-out description (\cref{sec:anecdotes:components}).
The structural features of the \Cabstract{} (\cref{sec:anecdotes:components:abstract}) are described, distinguishing between instances that contain a meta-reference and ones that do not and portraying intersecting micro- and macro-syntactic aspects%
\footnote{%
	For example, the lexical choice of meta-reference and its relation to its modification or the special characteristics of \C{arall}[other].
}.
Even though the \Cexposition{} (\cref{sec:anecdotes:components:exposition}) is not as rigidly structured as the \Cabstract{}, it still exhibits characteristic linguistic features that have to do with temporal expressions%
\footnote{%
	Subdivisible into three types: durative, frequentative and time references.
},
plurality and
the syntactic form of its sentences.
The \Cdevelopment{} is the least rigid component, which seems to freely make full use of the narrative ‘toolbox’ or ‘palette’ the Welsh language provides, as demonstrated in \cref{sec:anecdotes:components:development}.
The \Cepilogue{} (\cref{sec:anecdotes:components:epilogue}) shows a number of linguistics features related to anaphoric meta-references (with posterior, ‘after’, semantics or without it), temporal expressions (indicating duration or otherwise), negation and the preterite form of \C{\gl{bod}{\bod{}}}%
\footnote{%
	Which occurs in the \Cepilogue{} much more commonly than in other portions of the text.
}.
The \Cconclusion{} (\cref{sec:anecdotes:components:conclusion}) refers back to the anecdote as a whole and~— similarly to the abstract~— has one foot in the anecdote and another in the broader text.
The \Cintanch{} (\cref{sec:anecdotes:components:intanch}) usually occurs at the beginning of the first section of the anecdote and links it to the text; \C{\gl{cofiaf}{\cofiaf{}}} is notably common, but other forms also occur.
The \Ctempanchs{} (\cref{sec:anecdotes:components:tempanch}) set the temporal \foreign{origo} of the anecdote to some point in the past; they can be divided into two groups, with certain syntactic and textual characteristics.

Anecdotes function within the broader text.
Some features of the interface between the two have been discussed in \cref{sec:anecdotes:embedding}, including aspects of cohesion, clusters of anecdotes, paragraph division and information status (\cref{sec:anecdotes:embedding:cohesion}).
The discursive notions of credibility and reportability are also touched upon (\cref{sec:anecdotes:embedding:cred-report}).

Edge cases can assist in understanding complex systems (\cref{sec:anecdotes:edge}).
Minimal, laconic anecdotes raise questions about the nature of narrativity and narrativehood (\cref{sec:anecdotes:edge:min}).
On the other extremity, there occur in the corpus some long, developed and intricate anecdotes, some of which even include embedded narrative of varying complexity (\cref{sec:anecdotes:edge:max}).
Beyond these, units that show some resemblance to anecdotes yet do not fall under the structural definition presented in the chapter are cursorily discussed (\cref{sec:anecdotes:edge:nonanecdote}).%
\footnote{%
	In this context it is relevant to note no Welsh lexeme corresponds exactly to the text-linguistic construction termed here \emph{an anecdote}; see \cref{fn:stori-anecdote and HIDE AND TAIL} on p.~\pageref{fn:stori-anecdote and HIDE AND TAIL} for a comparable case of disparity between classification and lexicon.
}

This chapter ends with a comparison of the two works that makes its corpus (\cref{sec:anecdotes:comparison}): both generally (\cref{sec:anecdotes:comparison:broad:general}) and more specifically with regard to anecdotes (\cref{sec:anecdotes:comparison:broad:anec}).
The fact some the occurrences told as anecdotes in one work (\YLW{}) are retold in the other (\Atgofion{}) presents an opportunity for a comparative examination, which is pursued in \cref{sec:anecdotes:comparison:retelling}.
