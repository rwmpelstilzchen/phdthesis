\noindent
This \thesis{} presents a study of the linguistic means of text construction and organisation in Literary Modern Welsh (Celtic, Indo-European).

Its approach is situated within structural and text linguistics.
The former describes language as a \emph{system} of signs (conflations of formal signifiers and functional signifieds) whose structural value is derived from their paradigmatic commutation within syntagmatic environments.
The latter is concerned with the linguistic treatment of \emph{texts}, beyond the traditional scope of the sentence.
In particular, the focus of this \thesis{} is on the distinct grammatical systems in different text-types and the diverse ways they are structured linguistically, both internally and in relation to other textual components in the broader text.

The data is based on the writings of the \nth{20} century Welsh-language author Kate Roberts (1891–1985).
Roberts was a prolific writer in a great variety of forms and genres: short stories (for which she is most famous), novels, novellas, recollections, letters, plays, essays and journalistic writing.
This variety presents an opportunity to describe different text-types while the variable of idiosyncratic linguistic features is held constant.
Of the plentiful options available, three topics have been chosen, each investigating one text-linguistic component on the basis of two works (which not only strengthens the validity of the findings, but allows a more refined analysis as well):

\begin{plainlist}
	\item \Cref{sec:anecdotes} explores \emph{anecdotes} in autobiographical texts.
		These anecdotes are distinct and bound embedded sub-textual units, which show a recurrent macro-syntactic structure consisting of five ordered sections and two movable elements (‘anchors’).
		Each of these constituents exhibits particular linguistic properties and structure.
		Being succinct narratives~— minimal at times~— anecdotes offer a glimpse into the essence of narrativity and the grammar of narrative.
	
		The corpus is two memoirs: \textcite{roberts.k:1960:lon-wen,roberts.k:1972:atgofion}.

	\item \Cref{sec:rd} is dedicated to \emph{reporting of speech in narrative}, focussing on the linguistic ‘sewing thread’ of the seam between two primary components (or \emph{modes}) of most narratives: dialogue, and the narration in which it is embedded.
		Three basic syntactic patterns of quotative indexes (\emph{quotation formulae}) are distinguished, with macro-syntactic signifieds.
		Their internal structure is characterised, and the distribution between overt and zero quotative indexes is portrayed.

		The corpus is two collections of short stories: \textcite{roberts.k:2004:te,roberts.k:1981:haul-drycin}.

	% \item \Cref{sec:diary} portrays grammatical features of another narrative text-type: \emph{fictional diary entries}, in which a story is told through a diary from the perspective of one character.

	\item \Cref{sec:drama} deals with \emph{stage directions} in plays.
		They exhibit unique linguistic characteristics and show interesting interrelations with the dramatic text (i.e.\ the text which is spoken aloud by the actors) with which they are intertwined.
		Three different main textual environments in which stage directions occur in relation to the dramatic text are distinguished.
		The interaction between textual environments and the syntactic forms and functions of stage directions differs in the two plays under discussion, which are dissimilar in performativity and modality (one is a stage play and the other a radio play).

		The corpus is two plays: \textcite{davies.b.e+:1920:y-fam,roberts.k:2014:y-cynddrws}.
\end{plainlist}

The choice of topics is not incidental, as all three have broader theoretical implications and three themes thread through them all, namely the \emph{linguistic expression} of the following:

\begin{compactitem}
	\item \emph{The structural regularities of textual functions.}

		The chapters treat constructions which show regular behaviour on a textual level, often in an organisational way.
		
	\item \emph{The interrelation and interconnectivity of textual units or components.}

		Anecdotes are inherently embedded within a broader text, reporting of speech by its very nature mediates between two textual components, and stage directions are intertwined with the dramatic text.

	\item \emph{The multifaceted nature of narrative.}

		All three topics deal with aspects of narrative: anecdotes are basic ‘miniature’ narratives, signals of reported speech in narrative have narrative on one end and the reported speech on the other, and stage directions make the narrative spine of the events which unfold on stage.
\end{compactitem}

The diverse writings of Roberts offer an opportunity to uncover linguistic systems that lie at the foundation of text construction and organisation in a minority language which is underresearched with regard to text linguistics.
None of the above topics has been hitherto described in Welsh from the perspectives offered in this \thesis{}, and numerous new findings are reported.
While these findings contribute to our understanding of the particular topics and language in question, they may also shed some light on core text-linguistic questions.
