# Grammar and textuality: A structural linguistic study of text-types in Modern Welsh

This is the [LuaLaTeX](http://www.luatex.org/) source code for [my](https://ac.digitalwords.net/) PhD thesis, which is now under evaluation.
A precompiled PDF file is available [here](https://ac.digitalwords.net/digital/phdthesis.pdf).
Note that the PDF file is heavily hyperlinked, so printing it out would not only cut down trees but result in a poorer reading experience as well.



## How to compile?

* Prerequisite: install [TeX Live](https://www.tug.org/texlive/), [Biber](https://biblatex-biber.sourceforge.net/) and the fonts described in the colophon.
* Copy [`bibliography.bib`](https://gitlab.com/rwmpelstilzchen/bibliography.bib/-/blob/master/bibliography.bib) to a local path and change the path of `\addbibresource` in `tex/preamble/bibliography.tex` accordingly.
* Run `make final`. The `-shell-escape` option is required for compiling the indices using `texindy`.
* Make yourself some tea, because compilation can take a long time 🍵
